## co-access


### 10.2307/j.ctv105b9gq.15
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctv105b9gq.15)
- [Production](https://doi.org/10.2307/j.ctv105b9gq.15)
- [JSON](https://api.crossref.org/works/10.2307/j.ctv105b9gq.15)
- [XML](https://api.crossref.org/works/10.2307/j.ctv105b9gq.15.xml)
<hr>

### 10.2307/j.ctvddznv3.16
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctvddznv3.16)
- ** [Production](https://doi.org/10.2307/j.ctvddznv3.16)
- [JSON](https://api.crossref.org/works/10.2307/j.ctvddznv3.16)
- [XML](https://api.crossref.org/works/10.2307/j.ctvddznv3.16.xml)
<hr>

### 10.1215/9780822394334-012
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822394334-012)
- [Production](https://doi.org/10.1215/9780822394334-012)
- [JSON](https://api.crossref.org/works/10.1215/9780822394334-012)
- [XML](https://api.crossref.org/works/10.1215/9780822394334-012.xml)
<hr>

### 10.14361/9783839428221-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839428221-006)
- [Production](https://doi.org/10.14361/9783839428221-006)
- [JSON](https://api.crossref.org/works/10.14361/9783839428221-006)
- [XML](https://api.crossref.org/works/10.14361/9783839428221-006.xml)
<hr>

### 10.14361/9783839437315-029
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839437315-029)
- [Production](https://doi.org/10.14361/9783839437315-029)
- [JSON](https://api.crossref.org/works/10.14361/9783839437315-029)
- [XML](https://api.crossref.org/works/10.14361/9783839437315-029.xml)
<hr>

### 10.1515/9781400883615-014
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781400883615-014)
- [Production](https://doi.org/10.1515/9781400883615-014)
- [JSON](https://api.crossref.org/works/10.1515/9781400883615-014)
- [XML](https://api.crossref.org/works/10.1515/9781400883615-014.xml)
<hr>

### 10.1515/9780271060606-034
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9780271060606-034)
- [Production](https://doi.org/10.1515/9780271060606-034)
- [JSON](https://api.crossref.org/works/10.1515/9780271060606-034)
- [XML](https://api.crossref.org/works/10.1515/9780271060606-034.xml)
<hr>

### 10.14361/transcript.9783839420157
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839420157)
- [Production](https://doi.org/10.14361/transcript.9783839420157)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839420157)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839420157.xml)
<hr>

### 10.1215/9780822380122-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822380122-004)
- [Production](https://doi.org/10.1215/9780822380122-004)
- [JSON](https://api.crossref.org/works/10.1215/9780822380122-004)
- [XML](https://api.crossref.org/works/10.1215/9780822380122-004.xml)
<hr>

### 10.14361/9783839445150-013
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839445150-013)
- [Production](https://doi.org/10.14361/9783839445150-013)
- [JSON](https://api.crossref.org/works/10.14361/9783839445150-013)
- [XML](https://api.crossref.org/works/10.14361/9783839445150-013.xml)
<hr>

### 10.14361/9783839403242-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839403242-004)
- [Production](https://doi.org/10.14361/9783839403242-004)
- [JSON](https://api.crossref.org/works/10.14361/9783839403242-004)
- [XML](https://api.crossref.org/works/10.14361/9783839403242-004.xml)
<hr>

### 10.14361/transcript.9783839425855.261
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839425855.261)
- [Production](https://doi.org/10.14361/transcript.9783839425855.261)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839425855.261)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839425855.261.xml)
<hr>

### 10.14361/9783839447000-014
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839447000-014)
- [Production](https://doi.org/10.14361/9783839447000-014)
- [JSON](https://api.crossref.org/works/10.14361/9783839447000-014)
- [XML](https://api.crossref.org/works/10.14361/9783839447000-014.xml)
<hr>

### 10.14361/9783839406861-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839406861-004)
- [Production](https://doi.org/10.14361/9783839406861-004)
- [JSON](https://api.crossref.org/works/10.14361/9783839406861-004)
- [XML](https://api.crossref.org/works/10.14361/9783839406861-004.xml)
<hr>

### 10.14361/9783839444481-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839444481-004)
- [Production](https://doi.org/10.14361/9783839444481-004)
- [JSON](https://api.crossref.org/works/10.14361/9783839444481-004)
- [XML](https://api.crossref.org/works/10.14361/9783839444481-004.xml)
<hr>

### 10.1515/9781400848256-010
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781400848256-010)
- [Production](https://doi.org/10.1515/9781400848256-010)
- [JSON](https://api.crossref.org/works/10.1515/9781400848256-010)
- [XML](https://api.crossref.org/works/10.1515/9781400848256-010.xml)
<hr>

### 10.14361/transcript.9783839416099.57
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839416099.57)
- [Production](https://doi.org/10.14361/transcript.9783839416099.57)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839416099.57)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839416099.57.xml)
<hr>

### 10.14361/transcript.9783839427026.59
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839427026.59)
- [Production](https://doi.org/10.14361/transcript.9783839427026.59)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839427026.59)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839427026.59.xml)
<hr>

### 10.14361/9783839406748-012
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839406748-012)
- [Production](https://doi.org/10.14361/9783839406748-012)
- [JSON](https://api.crossref.org/works/10.14361/9783839406748-012)
- [XML](https://api.crossref.org/works/10.14361/9783839406748-012.xml)
<hr>

### 10.1201/9780367456238-16
- [Chooser POC](https://chooser.fly.dev/?doi=10.1201/9780367456238-16)
- [Production](https://doi.org/10.1201/9780367456238-16)
- [JSON](https://api.crossref.org/works/10.1201/9780367456238-16)
- [XML](https://api.crossref.org/works/10.1201/9780367456238-16.xml)
<hr>

### 10.14361/9783839444283-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839444283-003)
- [Production](https://doi.org/10.14361/9783839444283-003)
- [JSON](https://api.crossref.org/works/10.14361/9783839444283-003)
- [XML](https://api.crossref.org/works/10.14361/9783839444283-003.xml)
<hr>

### 10.14361/9783839420881-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839420881-009)
- [Production](https://doi.org/10.14361/9783839420881-009)
- [JSON](https://api.crossref.org/works/10.14361/9783839420881-009)
- [XML](https://api.crossref.org/works/10.14361/9783839420881-009.xml)
<hr>

### 10.3366/edinburgh/9780748670307.003.0005
- [Chooser POC](https://chooser.fly.dev/?doi=10.3366/edinburgh/9780748670307.003.0005)
- [Production](https://doi.org/10.3366/edinburgh/9780748670307.003.0005)
- [JSON](https://api.crossref.org/works/10.3366/edinburgh/9780748670307.003.0005)
- [XML](https://api.crossref.org/works/10.3366/edinburgh/9780748670307.003.0005.xml)
<hr>

### 10.1215/9780822382652-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822382652-002)
- [Production](https://doi.org/10.1215/9780822382652-002)
- [JSON](https://api.crossref.org/works/10.1215/9780822382652-002)
- [XML](https://api.crossref.org/works/10.1215/9780822382652-002.xml)
<hr>

### 10.1215/9780822385226-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822385226-004)
- [Production](https://doi.org/10.1215/9780822385226-004)
- [JSON](https://api.crossref.org/works/10.1215/9780822385226-004)
- [XML](https://api.crossref.org/works/10.1215/9780822385226-004.xml)
<hr>

### 10.1515/9781400874910-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781400874910-008)
- [Production](https://doi.org/10.1515/9781400874910-008)
- [JSON](https://api.crossref.org/works/10.1515/9781400874910-008)
- [XML](https://api.crossref.org/works/10.1515/9781400874910-008.xml)
<hr>

### 10.1215/9780822393306-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822393306-007)
- [Production](https://doi.org/10.1215/9780822393306-007)
- [JSON](https://api.crossref.org/works/10.1215/9780822393306-007)
- [XML](https://api.crossref.org/works/10.1215/9780822393306-007.xml)
<hr>

### 10.14361/9783839435304-018
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839435304-018)
- [Production](https://doi.org/10.14361/9783839435304-018)
- [JSON](https://api.crossref.org/works/10.14361/9783839435304-018)
- [XML](https://api.crossref.org/works/10.14361/9783839435304-018.xml)
<hr>

### 10.11126/stanford/9780804784542.003.0008
- [Chooser POC](https://chooser.fly.dev/?doi=10.11126/stanford/9780804784542.003.0008)
- [Production](https://doi.org/10.11126/stanford/9780804784542.003.0008)
- [JSON](https://api.crossref.org/works/10.11126/stanford/9780804784542.003.0008)
- [XML](https://api.crossref.org/works/10.11126/stanford/9780804784542.003.0008.xml)
<hr>

### 10.14361/transcript.9783839425671.91
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839425671.91)
- [Production](https://doi.org/10.14361/transcript.9783839425671.91)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839425671.91)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839425671.91.xml)
<hr>

### 10.1017/9781787446564.008
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9781787446564.008)
- [Production](https://doi.org/10.1017/9781787446564.008)
- [JSON](https://api.crossref.org/works/10.1017/9781787446564.008)
- [XML](https://api.crossref.org/works/10.1017/9781787446564.008.xml)
<hr>

### 10.14361/transcript.9783839414361.163
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839414361.163)
- [Production](https://doi.org/10.14361/transcript.9783839414361.163)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839414361.163)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839414361.163.xml)
<hr>

### 10.14361/transcript.9783839422977.75
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839422977.75)
- [Production](https://doi.org/10.14361/transcript.9783839422977.75)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839422977.75)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839422977.75.xml)
<hr>

### 10.14361/9783839446966-011
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839446966-011)
- [Production](https://doi.org/10.14361/9783839446966-011)
- [JSON](https://api.crossref.org/works/10.14361/9783839446966-011)
- [XML](https://api.crossref.org/works/10.14361/9783839446966-011.xml)
<hr>

### 10.14361/9783839408964-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839408964-009)
- [Production](https://doi.org/10.14361/9783839408964-009)
- [JSON](https://api.crossref.org/works/10.14361/9783839408964-009)
- [XML](https://api.crossref.org/works/10.14361/9783839408964-009.xml)
<hr>

### 10.14361/9783839450338
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839450338)
- [Production](https://doi.org/10.14361/9783839450338)
- [JSON](https://api.crossref.org/works/10.14361/9783839450338)
- [XML](https://api.crossref.org/works/10.14361/9783839450338.xml)
<hr>

### 10.1215/9780822385882-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822385882-005)
- [Production](https://doi.org/10.1215/9780822385882-005)
- [JSON](https://api.crossref.org/works/10.1215/9780822385882-005)
- [XML](https://api.crossref.org/works/10.1215/9780822385882-005.xml)
<hr>

### 10.14361/9783839445426-012
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839445426-012)
- [Production](https://doi.org/10.14361/9783839445426-012)
- [JSON](https://api.crossref.org/works/10.14361/9783839445426-012)
- [XML](https://api.crossref.org/works/10.14361/9783839445426-012.xml)
<hr>

### 10.1215/9780822374176-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822374176-005)
- [Production](https://doi.org/10.1215/9780822374176-005)
- [JSON](https://api.crossref.org/works/10.1215/9780822374176-005)
- [XML](https://api.crossref.org/works/10.1215/9780822374176-005.xml)
<hr>

### 10.1515/9789048528707-014
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9789048528707-014)
- [Production](https://doi.org/10.1515/9789048528707-014)
- [JSON](https://api.crossref.org/works/10.1515/9789048528707-014)
- [XML](https://api.crossref.org/works/10.1515/9789048528707-014.xml)
<hr>

### 10.1515/9781400827879-013
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781400827879-013)
- [Production](https://doi.org/10.1515/9781400827879-013)
- [JSON](https://api.crossref.org/works/10.1515/9781400827879-013)
- [XML](https://api.crossref.org/works/10.1515/9781400827879-013.xml)
<hr>

### 10.1215/9780822375920-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822375920-003)
- [Production](https://doi.org/10.1215/9780822375920-003)
- [JSON](https://api.crossref.org/works/10.1215/9780822375920-003)
- [XML](https://api.crossref.org/works/10.1215/9780822375920-003.xml)
<hr>

### 10.1215/9780822372660-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822372660-009)
- [Production](https://doi.org/10.1215/9780822372660-009)
- [JSON](https://api.crossref.org/works/10.1215/9780822372660-009)
- [XML](https://api.crossref.org/works/10.1215/9780822372660-009.xml)
<hr>

### 10.14361/9783839456712-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839456712-009)
- [Production](https://doi.org/10.14361/9783839456712-009)
- [JSON](https://api.crossref.org/works/10.14361/9783839456712-009)
- [XML](https://api.crossref.org/works/10.14361/9783839456712-009.xml)
<hr>

### 10.1515/9781400839483-013
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781400839483-013)
- [Production](https://doi.org/10.1515/9781400839483-013)
- [JSON](https://api.crossref.org/works/10.1515/9781400839483-013)
- [XML](https://api.crossref.org/works/10.1515/9781400839483-013.xml)
<hr>

### 10.1201/9781003068341-26
- [Chooser POC](https://chooser.fly.dev/?doi=10.1201/9781003068341-26)
- [Production](https://doi.org/10.1201/9781003068341-26)
- [JSON](https://api.crossref.org/works/10.1201/9781003068341-26)
- [XML](https://api.crossref.org/works/10.1201/9781003068341-26.xml)
<hr>

### 10.14361/9783839430460-014
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839430460-014)
- [Production](https://doi.org/10.14361/9783839430460-014)
- [JSON](https://api.crossref.org/works/10.14361/9783839430460-014)
- [XML](https://api.crossref.org/works/10.14361/9783839430460-014.xml)
<hr>

### 10.1215/9780822372905-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822372905-005)
- [Production](https://doi.org/10.1215/9780822372905-005)
- [JSON](https://api.crossref.org/works/10.1215/9780822372905-005)
- [XML](https://api.crossref.org/works/10.1215/9780822372905-005.xml)
<hr>

### 10.2307/j.ctv1txdfrw.20
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctv1txdfrw.20)
- [Production](https://doi.org/10.2307/j.ctv1txdfrw.20)
- [JSON](https://api.crossref.org/works/10.2307/j.ctv1txdfrw.20)
- [XML](https://api.crossref.org/works/10.2307/j.ctv1txdfrw.20.xml)
<hr>

### 10.23943/9781400889211-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.23943/9781400889211-004)
- [Production](https://doi.org/10.23943/9781400889211-004)
- [JSON](https://api.crossref.org/works/10.23943/9781400889211-004)
- [XML](https://api.crossref.org/works/10.23943/9781400889211-004.xml)
<hr>

### 10.3828/liverpool/9781904113171.003.0047
- [Chooser POC](https://chooser.fly.dev/?doi=10.3828/liverpool/9781904113171.003.0047)
- [Production](https://doi.org/10.3828/liverpool/9781904113171.003.0047)
- [JSON](https://api.crossref.org/works/10.3828/liverpool/9781904113171.003.0047)
- [XML](https://api.crossref.org/works/10.3828/liverpool/9781904113171.003.0047.xml)
<hr>

### 10.1515/9781575068657-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781575068657-008)
- [Production](https://doi.org/10.1515/9781575068657-008)
- [JSON](https://api.crossref.org/works/10.1515/9781575068657-008)
- [XML](https://api.crossref.org/works/10.1515/9781575068657-008.xml)
<hr>

### 10.1215/9780822371793-102
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822371793-102)
- [Production](https://doi.org/10.1215/9780822371793-102)
- [JSON](https://api.crossref.org/works/10.1215/9780822371793-102)
- [XML](https://api.crossref.org/works/10.1215/9780822371793-102.xml)
<hr>

### 10.46692/9781447352068.006
- [Chooser POC](https://chooser.fly.dev/?doi=10.46692/9781447352068.006)
- [Production](https://doi.org/10.46692/9781447352068.006)
- [JSON](https://api.crossref.org/works/10.46692/9781447352068.006)
- [XML](https://api.crossref.org/works/10.46692/9781447352068.006.xml)
<hr>

### 10.14361/9783839454947-012
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839454947-012)
- [Production](https://doi.org/10.14361/9783839454947-012)
- [JSON](https://api.crossref.org/works/10.14361/9783839454947-012)
- [XML](https://api.crossref.org/works/10.14361/9783839454947-012.xml)
<hr>

### 10.14361/transcript.9783839415566.19
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839415566.19)
- [Production](https://doi.org/10.14361/transcript.9783839415566.19)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839415566.19)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839415566.19.xml)
<hr>

### 10.1215/9780822394969-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822394969-003)
- [Production](https://doi.org/10.1215/9780822394969-003)
- [JSON](https://api.crossref.org/works/10.1215/9780822394969-003)
- [XML](https://api.crossref.org/works/10.1215/9780822394969-003.xml)
<hr>

### 10.14361/9783839440209-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839440209-003)
- [Production](https://doi.org/10.14361/9783839440209-003)
- [JSON](https://api.crossref.org/works/10.14361/9783839440209-003)
- [XML](https://api.crossref.org/works/10.14361/9783839440209-003.xml)
<hr>

### 10.1515/9781400883189-022
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781400883189-022)
- [Production](https://doi.org/10.1515/9781400883189-022)
- [JSON](https://api.crossref.org/works/10.1515/9781400883189-022)
- [XML](https://api.crossref.org/works/10.1515/9781400883189-022.xml)
<hr>

### 10.14361/9783839407707-011
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839407707-011)
- [Production](https://doi.org/10.14361/9783839407707-011)
- [JSON](https://api.crossref.org/works/10.14361/9783839407707-011)
- [XML](https://api.crossref.org/works/10.14361/9783839407707-011.xml)
<hr>

### 10.14361/9783839445655
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839445655)
- [Production](https://doi.org/10.14361/9783839445655)
- [JSON](https://api.crossref.org/works/10.14361/9783839445655)
- [XML](https://api.crossref.org/works/10.14361/9783839445655.xml)
<hr>

### 10.14361/9783839435953
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839435953)
- [Production](https://doi.org/10.14361/9783839435953)
- [JSON](https://api.crossref.org/works/10.14361/9783839435953)
- [XML](https://api.crossref.org/works/10.14361/9783839435953.xml)
<hr>

### 10.14361/9783839437926-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839437926-002)
- [Production](https://doi.org/10.14361/9783839437926-002)
- [JSON](https://api.crossref.org/works/10.14361/9783839437926-002)
- [XML](https://api.crossref.org/works/10.14361/9783839437926-002.xml)
<hr>

### 10.1215/9780822386452-010
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822386452-010)
- [Production](https://doi.org/10.1215/9780822386452-010)
- [JSON](https://api.crossref.org/works/10.1215/9780822386452-010)
- [XML](https://api.crossref.org/works/10.1215/9780822386452-010.xml)
<hr>

### 10.2307/j.ctvx5w9fh.9
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctvx5w9fh.9)
- [Production](https://doi.org/10.2307/j.ctvx5w9fh.9)
- [JSON](https://api.crossref.org/works/10.2307/j.ctvx5w9fh.9)
- [XML](https://api.crossref.org/works/10.2307/j.ctvx5w9fh.9.xml)
<hr>

### 10.1215/9780822388579-015
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822388579-015)
- [Production](https://doi.org/10.1215/9780822388579-015)
- [JSON](https://api.crossref.org/works/10.1215/9780822388579-015)
- [XML](https://api.crossref.org/works/10.1215/9780822388579-015.xml)
<hr>

### 10.3828/liverpool/9781874774051.003.0011
- [Chooser POC](https://chooser.fly.dev/?doi=10.3828/liverpool/9781874774051.003.0011)
- [Production](https://doi.org/10.3828/liverpool/9781874774051.003.0011)
- [JSON](https://api.crossref.org/works/10.3828/liverpool/9781874774051.003.0011)
- [XML](https://api.crossref.org/works/10.3828/liverpool/9781874774051.003.0011.xml)
<hr>

### 10.14361/9783839438404
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839438404)
- [Production](https://doi.org/10.14361/9783839438404)
- [JSON](https://api.crossref.org/works/10.14361/9783839438404)
- [XML](https://api.crossref.org/works/10.14361/9783839438404.xml)
<hr>

### 10.14361/9783839448373-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839448373-005)
- [Production](https://doi.org/10.14361/9783839448373-005)
- [JSON](https://api.crossref.org/works/10.14361/9783839448373-005)
- [XML](https://api.crossref.org/works/10.14361/9783839448373-005.xml)
<hr>

### 10.14361/9783839402627
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839402627)
- [Production](https://doi.org/10.14361/9783839402627)
- [JSON](https://api.crossref.org/works/10.14361/9783839402627)
- [XML](https://api.crossref.org/works/10.14361/9783839402627.xml)
<hr>

### 10.23943/princeton/9780691205755.003.0003
- [Chooser POC](https://chooser.fly.dev/?doi=10.23943/princeton/9780691205755.003.0003)
- [Production](https://doi.org/10.23943/princeton/9780691205755.003.0003)
- [JSON](https://api.crossref.org/works/10.23943/princeton/9780691205755.003.0003)
- [XML](https://api.crossref.org/works/10.23943/princeton/9780691205755.003.0003.xml)
<hr>

### 10.2307/j.ctv21d62cm
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctv21d62cm)
- [Production](https://doi.org/10.2307/j.ctv21d62cm)
- [JSON](https://api.crossref.org/works/10.2307/j.ctv21d62cm)
- [XML](https://api.crossref.org/works/10.2307/j.ctv21d62cm.xml)
<hr>

### 10.2307/j.ctvwvr2vr.21
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctvwvr2vr.21)
- [Production](https://doi.org/10.2307/j.ctvwvr2vr.21)
- [JSON](https://api.crossref.org/works/10.2307/j.ctvwvr2vr.21)
- [XML](https://api.crossref.org/works/10.2307/j.ctvwvr2vr.21.xml)
<hr>

### 10.14361/transcript.9783839424179.181
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839424179.181)
- [Production](https://doi.org/10.14361/transcript.9783839424179.181)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839424179.181)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839424179.181.xml)
<hr>

### 10.2307/j.ctt1t88xzs.12
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctt1t88xzs.12)
- [Production](https://doi.org/10.2307/j.ctt1t88xzs.12)
- [JSON](https://api.crossref.org/works/10.2307/j.ctt1t88xzs.12)
- [XML](https://api.crossref.org/works/10.2307/j.ctt1t88xzs.12.xml)
<hr>

### 10.14361/transcript.9783839413869.135
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839413869.135)
- [Production](https://doi.org/10.14361/transcript.9783839413869.135)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839413869.135)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839413869.135.xml)
<hr>

### 10.14361/9783839409060
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839409060)
- [Production](https://doi.org/10.14361/9783839409060)
- [JSON](https://api.crossref.org/works/10.14361/9783839409060)
- [XML](https://api.crossref.org/works/10.14361/9783839409060.xml)
<hr>

### 10.14361/9783839401361-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839401361-008)
- [Production](https://doi.org/10.14361/9783839401361-008)
- [JSON](https://api.crossref.org/works/10.14361/9783839401361-008)
- [XML](https://api.crossref.org/works/10.14361/9783839401361-008.xml)
<hr>

### 10.14361/9783839443323-017
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839443323-017)
- [Production](https://doi.org/10.14361/9783839443323-017)
- [JSON](https://api.crossref.org/works/10.14361/9783839443323-017)
- [XML](https://api.crossref.org/works/10.14361/9783839443323-017.xml)
<hr>

### 10.14361/9783839441657-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839441657-006)
- [Production](https://doi.org/10.14361/9783839441657-006)
- [JSON](https://api.crossref.org/works/10.14361/9783839441657-006)
- [XML](https://api.crossref.org/works/10.14361/9783839441657-006.xml)
<hr>

### 10.1017/9789048553525.005
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9789048553525.005)
- [Production](https://doi.org/10.1017/9789048553525.005)
- [JSON](https://api.crossref.org/works/10.1017/9789048553525.005)
- [XML](https://api.crossref.org/works/10.1017/9789048553525.005.xml)
<hr>

### 10.1215/9780822371649-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822371649-006)
- [Production](https://doi.org/10.1215/9780822371649-006)
- [JSON](https://api.crossref.org/works/10.1215/9780822371649-006)
- [XML](https://api.crossref.org/works/10.1215/9780822371649-006.xml)
<hr>

### 10.1017/9781787445352.015
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9781787445352.015)
- [Production](https://doi.org/10.1017/9781787445352.015)
- [JSON](https://api.crossref.org/works/10.1017/9781787445352.015)
- [XML](https://api.crossref.org/works/10.1017/9781787445352.015.xml)
<hr>

### 10.14361/9783839412480-013
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839412480-013)
- [Production](https://doi.org/10.14361/9783839412480-013)
- [JSON](https://api.crossref.org/works/10.14361/9783839412480-013)
- [XML](https://api.crossref.org/works/10.14361/9783839412480-013.xml)
<hr>

### 10.14361/9783839441749-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839441749-007)
- [Production](https://doi.org/10.14361/9783839441749-007)
- [JSON](https://api.crossref.org/works/10.14361/9783839441749-007)
- [XML](https://api.crossref.org/works/10.14361/9783839441749-007.xml)
<hr>

### 10.1017/9781787448063.003
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9781787448063.003)
- [Production](https://doi.org/10.1017/9781787448063.003)
- [JSON](https://api.crossref.org/works/10.1017/9781787448063.003)
- [XML](https://api.crossref.org/works/10.1017/9781787448063.003.xml)
<hr>

### 10.4324/9780203017869-15
- [Chooser POC](https://chooser.fly.dev/?doi=10.4324/9780203017869-15)
- [Production](https://doi.org/10.4324/9780203017869-15)
- [JSON](https://api.crossref.org/works/10.4324/9780203017869-15)
- [XML](https://api.crossref.org/works/10.4324/9780203017869-15.xml)
<hr>

### 10.14361/9783839450017-019
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839450017-019)
- [Production](https://doi.org/10.14361/9783839450017-019)
- [JSON](https://api.crossref.org/works/10.14361/9783839450017-019)
- [XML](https://api.crossref.org/works/10.14361/9783839450017-019.xml)
<hr>

### 10.1017/9781787444485.009
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9781787444485.009)
- [Production](https://doi.org/10.1017/9781787444485.009)
- [JSON](https://api.crossref.org/works/10.1017/9781787444485.009)
- [XML](https://api.crossref.org/works/10.1017/9781787444485.009.xml)
<hr>

### 10.14361/9783839407332-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839407332-009)
- [Production](https://doi.org/10.14361/9783839407332-009)
- [JSON](https://api.crossref.org/works/10.14361/9783839407332-009)
- [XML](https://api.crossref.org/works/10.14361/9783839407332-009.xml)
<hr>

### 10.14361/9783839451670-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839451670-001)
- [Production](https://doi.org/10.14361/9783839451670-001)
- [JSON](https://api.crossref.org/works/10.14361/9783839451670-001)
- [XML](https://api.crossref.org/works/10.14361/9783839451670-001.xml)
<hr>

### 10.14361/9783839410400-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839410400-007)
- [Production](https://doi.org/10.14361/9783839410400-007)
- [JSON](https://api.crossref.org/works/10.14361/9783839410400-007)
- [XML](https://api.crossref.org/works/10.14361/9783839410400-007.xml)
<hr>

### 10.5325/j.ctv1bxh2tn.20
- [Chooser POC](https://chooser.fly.dev/?doi=10.5325/j.ctv1bxh2tn.20)
- [Production](https://doi.org/10.5325/j.ctv1bxh2tn.20)
- [JSON](https://api.crossref.org/works/10.5325/j.ctv1bxh2tn.20)
- [XML](https://api.crossref.org/works/10.5325/j.ctv1bxh2tn.20.xml)
<hr>

### 10.14361/9783839412176-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839412176-004)
- [Production](https://doi.org/10.14361/9783839412176-004)
- [JSON](https://api.crossref.org/works/10.14361/9783839412176-004)
- [XML](https://api.crossref.org/works/10.14361/9783839412176-004.xml)
<hr>

### 10.2307/j.ctv1sr6gzb.7
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctv1sr6gzb.7)
- [Production](https://doi.org/10.2307/j.ctv1sr6gzb.7)
- [JSON](https://api.crossref.org/works/10.2307/j.ctv1sr6gzb.7)
- [XML](https://api.crossref.org/works/10.2307/j.ctv1sr6gzb.7.xml)
<hr>

### 10.12987/9780300252798-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.12987/9780300252798-008)
- [Production](https://doi.org/10.12987/9780300252798-008)
- [JSON](https://api.crossref.org/works/10.12987/9780300252798-008)
- [XML](https://api.crossref.org/works/10.12987/9780300252798-008.xml)
<hr>

### 10.14361/9783839452707-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839452707-009)
- [Production](https://doi.org/10.14361/9783839452707-009)
- [JSON](https://api.crossref.org/works/10.14361/9783839452707-009)
- [XML](https://api.crossref.org/works/10.14361/9783839452707-009.xml)
<hr>

### 10.1515/9780691190426-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9780691190426-009)
- [Production](https://doi.org/10.1515/9780691190426-009)
- [JSON](https://api.crossref.org/works/10.1515/9780691190426-009)
- [XML](https://api.crossref.org/works/10.1515/9780691190426-009.xml)
<hr>

### 10.14325/mississippi/9781496828644.003.0001
- [Chooser POC](https://chooser.fly.dev/?doi=10.14325/mississippi/9781496828644.003.0001)
- [Production](https://doi.org/10.14325/mississippi/9781496828644.003.0001)
- [JSON](https://api.crossref.org/works/10.14325/mississippi/9781496828644.003.0001)
- [XML](https://api.crossref.org/works/10.14325/mississippi/9781496828644.003.0001.xml)
<hr>

### 10.14361/transcript.9783839425275.intro
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839425275.intro)
- [Production](https://doi.org/10.14361/transcript.9783839425275.intro)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839425275.intro)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839425275.intro.xml)
<hr>

### 10.1515/9781575066929-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781575066929-002)
- [Production](https://doi.org/10.1515/9781575066929-002)
- [JSON](https://api.crossref.org/works/10.1515/9781575066929-002)
- [XML](https://api.crossref.org/works/10.1515/9781575066929-002.xml)
<hr>

### 10.2307/j.ctv1xx9n06.8
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctv1xx9n06.8)
- [Production](https://doi.org/10.2307/j.ctv1xx9n06.8)
- [JSON](https://api.crossref.org/works/10.2307/j.ctv1xx9n06.8)
- [XML](https://api.crossref.org/works/10.2307/j.ctv1xx9n06.8.xml)
<hr>

### 10.14361/transcript.9783839424162.203
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839424162.203)
- [Production](https://doi.org/10.14361/transcript.9783839424162.203)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839424162.203)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839424162.203.xml)
<hr>

### 10.1017/9781787444669.008
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9781787444669.008)
- [Production](https://doi.org/10.1017/9781787444669.008)
- [JSON](https://api.crossref.org/works/10.1017/9781787444669.008)
- [XML](https://api.crossref.org/works/10.1017/9781787444669.008.xml)
<hr>

### 10.1201/9781003068631-9
- [Chooser POC](https://chooser.fly.dev/?doi=10.1201/9781003068631-9)
- [Production](https://doi.org/10.1201/9781003068631-9)
- [JSON](https://api.crossref.org/works/10.1201/9781003068631-9)
- [XML](https://api.crossref.org/works/10.1201/9781003068631-9.xml)
<hr>

### 10.14361/9783839430798-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839430798-003)
- [Production](https://doi.org/10.14361/9783839430798-003)
- [JSON](https://api.crossref.org/works/10.14361/9783839430798-003)
- [XML](https://api.crossref.org/works/10.14361/9783839430798-003.xml)
<hr>

### 10.14361/9783839443712-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839443712-007)
- [Production](https://doi.org/10.14361/9783839443712-007)
- [JSON](https://api.crossref.org/works/10.14361/9783839443712-007)
- [XML](https://api.crossref.org/works/10.14361/9783839443712-007.xml)
<hr>

### 10.14361/9783839409244-027
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839409244-027)
- [Production](https://doi.org/10.14361/9783839409244-027)
- [JSON](https://api.crossref.org/works/10.14361/9783839409244-027)
- [XML](https://api.crossref.org/works/10.14361/9783839409244-027.xml)
<hr>

### 10.2307/j.ctv1mjqv95.31
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctv1mjqv95.31)
- [Production](https://doi.org/10.2307/j.ctv1mjqv95.31)
- [JSON](https://api.crossref.org/works/10.2307/j.ctv1mjqv95.31)
- [XML](https://api.crossref.org/works/10.2307/j.ctv1mjqv95.31.xml)
<hr>

### 10.14361/9783839408308-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839408308-002)
- [Production](https://doi.org/10.14361/9783839408308-002)
- [JSON](https://api.crossref.org/works/10.14361/9783839408308-002)
- [XML](https://api.crossref.org/works/10.14361/9783839408308-002.xml)
<hr>

### 10.14361/9783839407936-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839407936-008)
- [Production](https://doi.org/10.14361/9783839407936-008)
- [JSON](https://api.crossref.org/works/10.14361/9783839407936-008)
- [XML](https://api.crossref.org/works/10.14361/9783839407936-008.xml)
<hr>

### 10.14361/9783839453711-017
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839453711-017)
- [Production](https://doi.org/10.14361/9783839453711-017)
- [JSON](https://api.crossref.org/works/10.14361/9783839453711-017)
- [XML](https://api.crossref.org/works/10.14361/9783839453711-017.xml)
<hr>

### 10.14361/9783839430774-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839430774-004)
- [Production](https://doi.org/10.14361/9783839430774-004)
- [JSON](https://api.crossref.org/works/10.14361/9783839430774-004)
- [XML](https://api.crossref.org/works/10.14361/9783839430774-004.xml)
<hr>

### 10.14361/transcript.9783839424469.7
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839424469.7)
- [Production](https://doi.org/10.14361/transcript.9783839424469.7)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839424469.7)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839424469.7.xml)
<hr>

### 10.14361/9783839430590-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839430590-008)
- [Production](https://doi.org/10.14361/9783839430590-008)
- [JSON](https://api.crossref.org/works/10.14361/9783839430590-008)
- [XML](https://api.crossref.org/works/10.14361/9783839430590-008.xml)
<hr>

### 10.14361/transcript.9783839421260.25
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839421260.25)
- [Production](https://doi.org/10.14361/transcript.9783839421260.25)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839421260.25)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839421260.25.xml)
<hr>

### 10.1017/9789048519590.046
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9789048519590.046)
- [Production](https://doi.org/10.1017/9789048519590.046)
- [JSON](https://api.crossref.org/works/10.1017/9789048519590.046)
- [XML](https://api.crossref.org/works/10.1017/9789048519590.046.xml)
<hr>

### 10.14361/9783839441343-026
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839441343-026)
- [Production](https://doi.org/10.14361/9783839441343-026)
- [JSON](https://api.crossref.org/works/10.14361/9783839441343-026)
- [XML](https://api.crossref.org/works/10.14361/9783839441343-026.xml)
<hr>

### 10.14361/9783839449424-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839449424-007)
- [Production](https://doi.org/10.14361/9783839449424-007)
- [JSON](https://api.crossref.org/works/10.14361/9783839449424-007)
- [XML](https://api.crossref.org/works/10.14361/9783839449424-007.xml)
<hr>

### 10.14361/9783839407608-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839407608-005)
- [Production](https://doi.org/10.14361/9783839407608-005)
- [JSON](https://api.crossref.org/works/10.14361/9783839407608-005)
- [XML](https://api.crossref.org/works/10.14361/9783839407608-005.xml)
<hr>

### 10.14361/9783839403334-016
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839403334-016)
- [Production](https://doi.org/10.14361/9783839403334-016)
- [JSON](https://api.crossref.org/works/10.14361/9783839403334-016)
- [XML](https://api.crossref.org/works/10.14361/9783839403334-016.xml)
<hr>

### 10.14361/transcript.9783839419601.81
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839419601.81)
- [Production](https://doi.org/10.14361/transcript.9783839419601.81)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839419601.81)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839419601.81.xml)
<hr>

### 10.3828/liverpool/9780197100158.003.0002
- [Chooser POC](https://chooser.fly.dev/?doi=10.3828/liverpool/9780197100158.003.0002)
- [Production](https://doi.org/10.3828/liverpool/9780197100158.003.0002)
- [JSON](https://api.crossref.org/works/10.3828/liverpool/9780197100158.003.0002)
- [XML](https://api.crossref.org/works/10.3828/liverpool/9780197100158.003.0002.xml)
<hr>

### 10.1215/9780822389057-011
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822389057-011)
- [Production](https://doi.org/10.1215/9780822389057-011)
- [JSON](https://api.crossref.org/works/10.1215/9780822389057-011)
- [XML](https://api.crossref.org/works/10.1215/9780822389057-011.xml)
<hr>

### 10.14361/9783839400883-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839400883-004)
- [Production](https://doi.org/10.14361/9783839400883-004)
- [JSON](https://api.crossref.org/works/10.14361/9783839400883-004)
- [XML](https://api.crossref.org/works/10.14361/9783839400883-004.xml)
<hr>

### 10.1515/9781618117144-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781618117144-008)
- [Production](https://doi.org/10.1515/9781618117144-008)
- [JSON](https://api.crossref.org/works/10.1515/9781618117144-008)
- [XML](https://api.crossref.org/works/10.1515/9781618117144-008.xml)
<hr>

### 10.1332/policypress/9781447356974.003.0003
- [Chooser POC](https://chooser.fly.dev/?doi=10.1332/policypress/9781447356974.003.0003)
- [Production](https://doi.org/10.1332/policypress/9781447356974.003.0003)
- [JSON](https://api.crossref.org/works/10.1332/policypress/9781447356974.003.0003)
- [XML](https://api.crossref.org/works/10.1332/policypress/9781447356974.003.0003.xml)
<hr>

### 10.14361/9783839436127-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839436127-008)
- [Production](https://doi.org/10.14361/9783839436127-008)
- [JSON](https://api.crossref.org/works/10.14361/9783839436127-008)
- [XML](https://api.crossref.org/works/10.14361/9783839436127-008.xml)
<hr>

### 10.1215/9780822375159-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822375159-004)
- [Production](https://doi.org/10.1215/9780822375159-004)
- [JSON](https://api.crossref.org/works/10.1215/9780822375159-004)
- [XML](https://api.crossref.org/works/10.1215/9780822375159-004.xml)
<hr>

### 10.1515/9781400885299-010
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781400885299-010)
- [Production](https://doi.org/10.1515/9781400885299-010)
- [JSON](https://api.crossref.org/works/10.1515/9781400885299-010)
- [XML](https://api.crossref.org/works/10.1515/9781400885299-010.xml)
<hr>

### 10.14361/9783839428429-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839428429-008)
- [Production](https://doi.org/10.14361/9783839428429-008)
- [JSON](https://api.crossref.org/works/10.14361/9783839428429-008)
- [XML](https://api.crossref.org/works/10.14361/9783839428429-008.xml)
<hr>

### 10.1215/9780822398905-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822398905-003)
- [Production](https://doi.org/10.1215/9780822398905-003)
- [JSON](https://api.crossref.org/works/10.1215/9780822398905-003)
- [XML](https://api.crossref.org/works/10.1215/9780822398905-003.xml)
<hr>

### 10.14361/9783839448434-013
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839448434-013)
- [Production](https://doi.org/10.14361/9783839448434-013)
- [JSON](https://api.crossref.org/works/10.14361/9783839448434-013)
- [XML](https://api.crossref.org/works/10.14361/9783839448434-013.xml)
<hr>

### 10.46692/9781529202175.005
- [Chooser POC](https://chooser.fly.dev/?doi=10.46692/9781529202175.005)
- [Production](https://doi.org/10.46692/9781529202175.005)
- [JSON](https://api.crossref.org/works/10.46692/9781529202175.005)
- [XML](https://api.crossref.org/works/10.46692/9781529202175.005.xml)
<hr>

### 10.14361/transcript.9783839422533.403
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839422533.403)
- [Production](https://doi.org/10.14361/transcript.9783839422533.403)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839422533.403)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839422533.403.xml)
<hr>

### 10.14361/9783839430880
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839430880)
- [Production](https://doi.org/10.14361/9783839430880)
- [JSON](https://api.crossref.org/works/10.14361/9783839430880)
- [XML](https://api.crossref.org/works/10.14361/9783839430880.xml)
<hr>

### 10.14361/transcript.9783839426012.11
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839426012.11)
- [Production](https://doi.org/10.14361/transcript.9783839426012.11)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839426012.11)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839426012.11.xml)
<hr>

### 10.14361/9783839441879-017
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839441879-017)
- [Production](https://doi.org/10.14361/9783839441879-017)
- [JSON](https://api.crossref.org/works/10.14361/9783839441879-017)
- [XML](https://api.crossref.org/works/10.14361/9783839441879-017.xml)
<hr>

### 10.1215/9780822390367-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822390367-006)
- [Production](https://doi.org/10.1215/9780822390367-006)
- [JSON](https://api.crossref.org/works/10.1215/9780822390367-006)
- [XML](https://api.crossref.org/works/10.1215/9780822390367-006.xml)
<hr>

### 10.14361/9783839425596-016
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839425596-016)
- [Production](https://doi.org/10.14361/9783839425596-016)
- [JSON](https://api.crossref.org/works/10.14361/9783839425596-016)
- [XML](https://api.crossref.org/works/10.14361/9783839425596-016.xml)
<hr>

### 10.14361/9783839445761-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839445761-003)
- [Production](https://doi.org/10.14361/9783839445761-003)
- [JSON](https://api.crossref.org/works/10.14361/9783839445761-003)
- [XML](https://api.crossref.org/works/10.14361/9783839445761-003.xml)
<hr>

### 10.2307/j.ctv11smkw6.11
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctv11smkw6.11)
- [Production](https://doi.org/10.2307/j.ctv11smkw6.11)
- [JSON](https://api.crossref.org/works/10.2307/j.ctv11smkw6.11)
- [XML](https://api.crossref.org/works/10.2307/j.ctv11smkw6.11.xml)
<hr>

### 10.14361/transcript.9783839414521.425
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839414521.425)
- [Production](https://doi.org/10.14361/transcript.9783839414521.425)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839414521.425)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839414521.425.xml)
<hr>

### 10.14361/9783839452585-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839452585-007)
- [Production](https://doi.org/10.14361/9783839452585-007)
- [JSON](https://api.crossref.org/works/10.14361/9783839452585-007)
- [XML](https://api.crossref.org/works/10.14361/9783839452585-007.xml)
<hr>

### 10.36019/9781684482801-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.36019/9781684482801-002)
- [Production](https://doi.org/10.36019/9781684482801-002)
- [JSON](https://api.crossref.org/works/10.36019/9781684482801-002)
- [XML](https://api.crossref.org/works/10.36019/9781684482801-002.xml)
<hr>

### 10.14361/9783839452288-020
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839452288-020)
- [Production](https://doi.org/10.14361/9783839452288-020)
- [JSON](https://api.crossref.org/works/10.14361/9783839452288-020)
- [XML](https://api.crossref.org/works/10.14361/9783839452288-020.xml)
<hr>

### 10.1515/9780691198071-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9780691198071-005)
- [Production](https://doi.org/10.1515/9780691198071-005)
- [JSON](https://api.crossref.org/works/10.1515/9780691198071-005)
- [XML](https://api.crossref.org/works/10.1515/9780691198071-005.xml)
<hr>

### 10.11126/stanford/9780804768795.003.0005
- [Chooser POC](https://chooser.fly.dev/?doi=10.11126/stanford/9780804768795.003.0005)
- [Production](https://doi.org/10.11126/stanford/9780804768795.003.0005)
- [JSON](https://api.crossref.org/works/10.11126/stanford/9780804768795.003.0005)
- [XML](https://api.crossref.org/works/10.11126/stanford/9780804768795.003.0005.xml)
<hr>

### 10.1215/9780822384588-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822384588-001)
- [Production](https://doi.org/10.1215/9780822384588-001)
- [JSON](https://api.crossref.org/works/10.1215/9780822384588-001)
- [XML](https://api.crossref.org/works/10.1215/9780822384588-001.xml)
<hr>

### 10.1215/9780822373964-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822373964-006)
- [Production](https://doi.org/10.1215/9780822373964-006)
- [JSON](https://api.crossref.org/works/10.1215/9780822373964-006)
- [XML](https://api.crossref.org/works/10.1215/9780822373964-006.xml)
<hr>

### 10.1201/9781003073222-10
- [Chooser POC](https://chooser.fly.dev/?doi=10.1201/9781003073222-10)
- [Production](https://doi.org/10.1201/9781003073222-10)
- [JSON](https://api.crossref.org/works/10.1201/9781003073222-10)
- [XML](https://api.crossref.org/works/10.1201/9781003073222-10.xml)
<hr>

### 10.14361/9783839411605-013
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839411605-013)
- [Production](https://doi.org/10.14361/9783839411605-013)
- [JSON](https://api.crossref.org/works/10.14361/9783839411605-013)
- [XML](https://api.crossref.org/works/10.14361/9783839411605-013.xml)
<hr>

### 10.1163/9781684175765_007
- [Chooser POC](https://chooser.fly.dev/?doi=10.1163/9781684175765_007)
- [Production](https://doi.org/10.1163/9781684175765_007)
- [JSON](https://api.crossref.org/works/10.1163/9781684175765_007)
- [XML](https://api.crossref.org/works/10.1163/9781684175765_007.xml)
<hr>

### 10.14361/transcript.9783839419083.189
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839419083.189)
- [Production](https://doi.org/10.14361/transcript.9783839419083.189)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839419083.189)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839419083.189.xml)
<hr>

### 10.14361/transcript.9783839419359.17
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839419359.17)
- [Production](https://doi.org/10.14361/transcript.9783839419359.17)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839419359.17)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839419359.17.xml)
<hr>

### 10.14361/9783839409138-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839409138-001)
- [Production](https://doi.org/10.14361/9783839409138-001)
- [JSON](https://api.crossref.org/works/10.14361/9783839409138-001)
- [XML](https://api.crossref.org/works/10.14361/9783839409138-001.xml)
<hr>

### 10.2307/j.ctvk8w01c.4
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctvk8w01c.4)
- [Production](https://doi.org/10.2307/j.ctvk8w01c.4)
- [JSON](https://api.crossref.org/works/10.2307/j.ctvk8w01c.4)
- [XML](https://api.crossref.org/works/10.2307/j.ctvk8w01c.4.xml)
<hr>

### 10.1017/9781787448193.006
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9781787448193.006)
- [Production](https://doi.org/10.1017/9781787448193.006)
- [JSON](https://api.crossref.org/works/10.1017/9781787448193.006)
- [XML](https://api.crossref.org/works/10.1017/9781787448193.006.xml)
<hr>

### 10.1017/9789048529506.005
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9789048529506.005)
- [Production](https://doi.org/10.1017/9789048529506.005)
- [JSON](https://api.crossref.org/works/10.1017/9789048529506.005)
- [XML](https://api.crossref.org/works/10.1017/9789048529506.005.xml)
<hr>

### 10.1215/9780822390701-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822390701-001)
- [Production](https://doi.org/10.1215/9780822390701-001)
- [JSON](https://api.crossref.org/works/10.1215/9780822390701-001)
- [XML](https://api.crossref.org/works/10.1215/9780822390701-001.xml)
<hr>

### 10.14361/9783839423660-020
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839423660-020)
- [Production](https://doi.org/10.14361/9783839423660-020)
- [JSON](https://api.crossref.org/works/10.14361/9783839423660-020)
- [XML](https://api.crossref.org/works/10.14361/9783839423660-020.xml)
<hr>

### 10.1515/9781400855896.198
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781400855896.198)
- [Production](https://doi.org/10.1515/9781400855896.198)
- [JSON](https://api.crossref.org/works/10.1515/9781400855896.198)
- [XML](https://api.crossref.org/works/10.1515/9781400855896.198.xml)
<hr>

### 10.11126/stanford/9780804794084.003.0003
- [Chooser POC](https://chooser.fly.dev/?doi=10.11126/stanford/9780804794084.003.0003)
- [Production](https://doi.org/10.11126/stanford/9780804794084.003.0003)
- [JSON](https://api.crossref.org/works/10.11126/stanford/9780804794084.003.0003)
- [XML](https://api.crossref.org/works/10.11126/stanford/9780804794084.003.0003.xml)
<hr>

### 10.14361/9783839454558-011
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839454558-011)
- [Production](https://doi.org/10.14361/9783839454558-011)
- [JSON](https://api.crossref.org/works/10.14361/9783839454558-011)
- [XML](https://api.crossref.org/works/10.14361/9783839454558-011.xml)
<hr>

### 10.2307/j.ctv1n9dkth
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctv1n9dkth)
- [Production](https://doi.org/10.2307/j.ctv1n9dkth)
- [JSON](https://api.crossref.org/works/10.2307/j.ctv1n9dkth)
- [XML](https://api.crossref.org/works/10.2307/j.ctv1n9dkth.xml)
<hr>

### 10.14361/9783839449752-012
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839449752-012)
- [Production](https://doi.org/10.14361/9783839449752-012)
- [JSON](https://api.crossref.org/works/10.14361/9783839449752-012)
- [XML](https://api.crossref.org/works/10.14361/9783839449752-012.xml)
<hr>

### 10.1515/9780271083025
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9780271083025)
- [Production](https://doi.org/10.1515/9780271083025)
- [JSON](https://api.crossref.org/works/10.1515/9780271083025)
- [XML](https://api.crossref.org/works/10.1515/9780271083025.xml)
<hr>

### 10.14361/9783839405741-010
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839405741-010)
- [Production](https://doi.org/10.14361/9783839405741-010)
- [JSON](https://api.crossref.org/works/10.14361/9783839405741-010)
- [XML](https://api.crossref.org/works/10.14361/9783839405741-010.xml)
<hr>

### 10.5622/illinois/9780252042690.003.0011
- [Chooser POC](https://chooser.fly.dev/?doi=10.5622/illinois/9780252042690.003.0011)
- [Production](https://doi.org/10.5622/illinois/9780252042690.003.0011)
- [JSON](https://api.crossref.org/works/10.5622/illinois/9780252042690.003.0011)
- [XML](https://api.crossref.org/works/10.5622/illinois/9780252042690.003.0011.xml)
<hr>

### 10.1515/9781400873111-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781400873111-009)
- [Production](https://doi.org/10.1515/9781400873111-009)
- [JSON](https://api.crossref.org/works/10.1515/9781400873111-009)
- [XML](https://api.crossref.org/works/10.1515/9781400873111-009.xml)
<hr>

### 10.14361/transcript.9783839414262.125
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839414262.125)
- [Production](https://doi.org/10.14361/transcript.9783839414262.125)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839414262.125)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839414262.125.xml)
<hr>

### 10.3109/9780203414675
- [Chooser POC](https://chooser.fly.dev/?doi=10.3109/9780203414675)
- [Production](https://doi.org/10.3109/9780203414675)
- [JSON](https://api.crossref.org/works/10.3109/9780203414675)
- [XML](https://api.crossref.org/works/10.3109/9780203414675.xml)
<hr>

### 10.14361/9783839451052-011
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839451052-011)
- [Production](https://doi.org/10.14361/9783839451052-011)
- [JSON](https://api.crossref.org/works/10.14361/9783839451052-011)
- [XML](https://api.crossref.org/works/10.14361/9783839451052-011.xml)
<hr>

### 10.14361/9783839456958-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839456958-009)
- [Production](https://doi.org/10.14361/9783839456958-009)
- [JSON](https://api.crossref.org/works/10.14361/9783839456958-009)
- [XML](https://api.crossref.org/works/10.14361/9783839456958-009.xml)
<hr>

### 10.14361/9783839448632-021
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839448632-021)
- [Production](https://doi.org/10.14361/9783839448632-021)
- [JSON](https://api.crossref.org/works/10.14361/9783839448632-021)
- [XML](https://api.crossref.org/works/10.14361/9783839448632-021.xml)
<hr>

### 10.14361/transcript.9783839418178.55
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839418178.55)
- [Production](https://doi.org/10.14361/transcript.9783839418178.55)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839418178.55)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839418178.55.xml)
<hr>

### 10.23943/princeton/9780691190938.003.0004
- [Chooser POC](https://chooser.fly.dev/?doi=10.23943/princeton/9780691190938.003.0004)
- [Production](https://doi.org/10.23943/princeton/9780691190938.003.0004)
- [JSON](https://api.crossref.org/works/10.23943/princeton/9780691190938.003.0004)
- [XML](https://api.crossref.org/works/10.23943/princeton/9780691190938.003.0004.xml)
<hr>

### 10.14361/9783839447451-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839447451-003)
- [Production](https://doi.org/10.14361/9783839447451-003)
- [JSON](https://api.crossref.org/works/10.14361/9783839447451-003)
- [XML](https://api.crossref.org/works/10.14361/9783839447451-003.xml)
<hr>

### 10.14361/9783839409404
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839409404)
- [Production](https://doi.org/10.14361/9783839409404)
- [JSON](https://api.crossref.org/works/10.14361/9783839409404)
- [XML](https://api.crossref.org/works/10.14361/9783839409404.xml)
<hr>

### 10.14361/9783839458044-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839458044-008)
- [Production](https://doi.org/10.14361/9783839458044-008)
- [JSON](https://api.crossref.org/works/10.14361/9783839458044-008)
- [XML](https://api.crossref.org/works/10.14361/9783839458044-008.xml)
<hr>

### 10.1515/9781400833702-022
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781400833702-022)
- [Production](https://doi.org/10.1515/9781400833702-022)
- [JSON](https://api.crossref.org/works/10.1515/9781400833702-022)
- [XML](https://api.crossref.org/works/10.1515/9781400833702-022.xml)
<hr>

### 10.14361/9783839432891-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839432891-008)
- [Production](https://doi.org/10.14361/9783839432891-008)
- [JSON](https://api.crossref.org/works/10.14361/9783839432891-008)
- [XML](https://api.crossref.org/works/10.14361/9783839432891-008.xml)
<hr>

### 10.14325/mississippi/9781496831286.003.0005
- [Chooser POC](https://chooser.fly.dev/?doi=10.14325/mississippi/9781496831286.003.0005)
- [Production](https://doi.org/10.14325/mississippi/9781496831286.003.0005)
- [JSON](https://api.crossref.org/works/10.14325/mississippi/9781496831286.003.0005)
- [XML](https://api.crossref.org/works/10.14325/mississippi/9781496831286.003.0005.xml)
<hr>

### 10.1215/9781478013099-119
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9781478013099-119)
- [Production](https://doi.org/10.1215/9781478013099-119)
- [JSON](https://api.crossref.org/works/10.1215/9781478013099-119)
- [XML](https://api.crossref.org/works/10.1215/9781478013099-119.xml)
<hr>

### 10.11126/stanford/9781503610187.003.0008
- [Chooser POC](https://chooser.fly.dev/?doi=10.11126/stanford/9781503610187.003.0008)
- [Production](https://doi.org/10.11126/stanford/9781503610187.003.0008)
- [JSON](https://api.crossref.org/works/10.11126/stanford/9781503610187.003.0008)
- [XML](https://api.crossref.org/works/10.11126/stanford/9781503610187.003.0008.xml)
<hr>

### 10.14361/transcript.9783839421673.23
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839421673.23)
- [Production](https://doi.org/10.14361/transcript.9783839421673.23)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839421673.23)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839421673.23.xml)
<hr>

### 10.14361/9783839412749-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839412749-007)
- [Production](https://doi.org/10.14361/9783839412749-007)
- [JSON](https://api.crossref.org/works/10.14361/9783839412749-007)
- [XML](https://api.crossref.org/works/10.14361/9783839412749-007.xml)
<hr>

### 10.14361/transcript.9783839417447.54
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839417447.54)
- [Production](https://doi.org/10.14361/transcript.9783839417447.54)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839417447.54)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839417447.54.xml)
<hr>

### 10.14361/transcript.9783839415283.191
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839415283.191)
- [Production](https://doi.org/10.14361/transcript.9783839415283.191)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839415283.191)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839415283.191.xml)
<hr>

### 10.14361/9783839448793-017
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839448793-017)
- [Production](https://doi.org/10.14361/9783839448793-017)
- [JSON](https://api.crossref.org/works/10.14361/9783839448793-017)
- [XML](https://api.crossref.org/works/10.14361/9783839448793-017.xml)
<hr>

### 10.14361/9783839401866-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839401866-006)
- [Production](https://doi.org/10.14361/9783839401866-006)
- [JSON](https://api.crossref.org/works/10.14361/9783839401866-006)
- [XML](https://api.crossref.org/works/10.14361/9783839401866-006.xml)
<hr>

### 10.23943/princeton/9780691202280.003.0005
- [Chooser POC](https://chooser.fly.dev/?doi=10.23943/princeton/9780691202280.003.0005)
- [Production](https://doi.org/10.23943/princeton/9780691202280.003.0005)
- [JSON](https://api.crossref.org/works/10.23943/princeton/9780691202280.003.0005)
- [XML](https://api.crossref.org/works/10.23943/princeton/9780691202280.003.0005.xml)
<hr>

### 10.14361/transcript.9783839423196.275
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839423196.275)
- [Production](https://doi.org/10.14361/transcript.9783839423196.275)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839423196.275)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839423196.275.xml)
<hr>

### 10.14361/9783839410967-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839410967-001)
- [Production](https://doi.org/10.14361/9783839410967-001)
- [JSON](https://api.crossref.org/works/10.14361/9783839410967-001)
- [XML](https://api.crossref.org/works/10.14361/9783839410967-001.xml)
<hr>

### 10.14361/9783839451656-019
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839451656-019)
- [Production](https://doi.org/10.14361/9783839451656-019)
- [JSON](https://api.crossref.org/works/10.14361/9783839451656-019)
- [XML](https://api.crossref.org/works/10.14361/9783839451656-019.xml)
<hr>

### 10.14361/9783839454084-017
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839454084-017)
- [Production](https://doi.org/10.14361/9783839454084-017)
- [JSON](https://api.crossref.org/works/10.14361/9783839454084-017)
- [XML](https://api.crossref.org/works/10.14361/9783839454084-017.xml)
<hr>

### 10.1215/9780822374176-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822374176-002)
- [Production](https://doi.org/10.1215/9780822374176-002)
- [JSON](https://api.crossref.org/works/10.1215/9780822374176-002)
- [XML](https://api.crossref.org/works/10.1215/9780822374176-002.xml)
<hr>

### 10.1515/9781575066493-010
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781575066493-010)
- [Production](https://doi.org/10.1515/9781575066493-010)
- [JSON](https://api.crossref.org/works/10.1515/9781575066493-010)
- [XML](https://api.crossref.org/works/10.1515/9781575066493-010.xml)
<hr>

### 10.1515/9781575065212-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781575065212-006)
- [Production](https://doi.org/10.1515/9781575065212-006)
- [JSON](https://api.crossref.org/works/10.1515/9781575065212-006)
- [XML](https://api.crossref.org/works/10.1515/9781575065212-006.xml)
<hr>

### 10.14361/9783839448366-026
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839448366-026)
- [Production](https://doi.org/10.14361/9783839448366-026)
- [JSON](https://api.crossref.org/works/10.14361/9783839448366-026)
- [XML](https://api.crossref.org/works/10.14361/9783839448366-026.xml)
<hr>

### 10.23943/princeton/9780691175768.003.0015
- [Chooser POC](https://chooser.fly.dev/?doi=10.23943/princeton/9780691175768.003.0015)
- [Production](https://doi.org/10.23943/princeton/9780691175768.003.0015)
- [JSON](https://api.crossref.org/works/10.23943/princeton/9780691175768.003.0015)
- [XML](https://api.crossref.org/works/10.23943/princeton/9780691175768.003.0015.xml)
<hr>

### 10.23943/princeton/9780691202082.003.0020
- [Chooser POC](https://chooser.fly.dev/?doi=10.23943/princeton/9780691202082.003.0020)
- [Production](https://doi.org/10.23943/princeton/9780691202082.003.0020)
- [JSON](https://api.crossref.org/works/10.23943/princeton/9780691202082.003.0020)
- [XML](https://api.crossref.org/works/10.23943/princeton/9780691202082.003.0020.xml)
<hr>

### 10.14361/9783839451014
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839451014)
- [Production](https://doi.org/10.14361/9783839451014)
- [JSON](https://api.crossref.org/works/10.14361/9783839451014)
- [XML](https://api.crossref.org/works/10.14361/9783839451014.xml)
<hr>

### 10.14361/9783839451700-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839451700-004)
- [Production](https://doi.org/10.14361/9783839451700-004)
- [JSON](https://api.crossref.org/works/10.14361/9783839451700-004)
- [XML](https://api.crossref.org/works/10.14361/9783839451700-004.xml)
<hr>

### 10.14361/transcript.9783839423196.373
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839423196.373)
- [Production](https://doi.org/10.14361/transcript.9783839423196.373)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839423196.373)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839423196.373.xml)
<hr>

### 10.1215/9780822390152-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822390152-002)
- [Production](https://doi.org/10.1215/9780822390152-002)
- [JSON](https://api.crossref.org/works/10.1215/9780822390152-002)
- [XML](https://api.crossref.org/works/10.1215/9780822390152-002.xml)
<hr>

### 10.2307/j.ctv1pnc1mr
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctv1pnc1mr)
- [Production](https://doi.org/10.2307/j.ctv1pnc1mr)
- [JSON](https://api.crossref.org/works/10.2307/j.ctv1pnc1mr)
- [XML](https://api.crossref.org/works/10.2307/j.ctv1pnc1mr.xml)
<hr>

### 10.14361/9783839428931-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839428931-005)
- [Production](https://doi.org/10.14361/9783839428931-005)
- [JSON](https://api.crossref.org/works/10.14361/9783839428931-005)
- [XML](https://api.crossref.org/works/10.14361/9783839428931-005.xml)
<hr>

### 10.14361/9783839406083-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839406083-007)
- [Production](https://doi.org/10.14361/9783839406083-007)
- [JSON](https://api.crossref.org/works/10.14361/9783839406083-007)
- [XML](https://api.crossref.org/works/10.14361/9783839406083-007.xml)
<hr>

### 10.14361/transcript.9783839421215.215
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839421215.215)
- [Production](https://doi.org/10.14361/transcript.9783839421215.215)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839421215.215)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839421215.215.xml)
<hr>

### 10.1215/9780822373926-015
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822373926-015)
- [Production](https://doi.org/10.1215/9780822373926-015)
- [JSON](https://api.crossref.org/works/10.1215/9780822373926-015)
- [XML](https://api.crossref.org/works/10.1215/9780822373926-015.xml)
<hr>

### 10.14361/9783839409169
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839409169)
- [Production](https://doi.org/10.14361/9783839409169)
- [JSON](https://api.crossref.org/works/10.14361/9783839409169)
- [XML](https://api.crossref.org/works/10.14361/9783839409169.xml)
<hr>

### 10.1215/9780822387756-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822387756-009)
- [Production](https://doi.org/10.1215/9780822387756-009)
- [JSON](https://api.crossref.org/works/10.1215/9780822387756-009)
- [XML](https://api.crossref.org/works/10.1215/9780822387756-009.xml)
<hr>

### 10.14361/9783839430675-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839430675-003)
- [Production](https://doi.org/10.14361/9783839430675-003)
- [JSON](https://api.crossref.org/works/10.14361/9783839430675-003)
- [XML](https://api.crossref.org/works/10.14361/9783839430675-003.xml)
<hr>

### 10.4324/9781003071860-4
- [Chooser POC](https://chooser.fly.dev/?doi=10.4324/9781003071860-4)
- [Production](https://doi.org/10.4324/9781003071860-4)
- [JSON](https://api.crossref.org/works/10.4324/9781003071860-4)
- [XML](https://api.crossref.org/works/10.4324/9781003071860-4.xml)
<hr>

### 10.14361/9783839450567-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839450567-003)
- [Production](https://doi.org/10.14361/9783839450567-003)
- [JSON](https://api.crossref.org/works/10.14361/9783839450567-003)
- [XML](https://api.crossref.org/works/10.14361/9783839450567-003.xml)
<hr>

### 10.14361/9783839436639-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839436639-004)
- [Production](https://doi.org/10.14361/9783839436639-004)
- [JSON](https://api.crossref.org/works/10.14361/9783839436639-004)
- [XML](https://api.crossref.org/works/10.14361/9783839436639-004.xml)
<hr>

### 10.1215/9780822394044-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822394044-003)
- [Production](https://doi.org/10.1215/9780822394044-003)
- [JSON](https://api.crossref.org/works/10.1215/9780822394044-003)
- [XML](https://api.crossref.org/works/10.1215/9780822394044-003.xml)
<hr>

### 10.14361/9783839447420-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839447420-002)
- [Production](https://doi.org/10.14361/9783839447420-002)
- [JSON](https://api.crossref.org/works/10.14361/9783839447420-002)
- [XML](https://api.crossref.org/works/10.14361/9783839447420-002.xml)
<hr>

### 10.1515/9780691184487-010
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9780691184487-010)
- [Production](https://doi.org/10.1515/9780691184487-010)
- [JSON](https://api.crossref.org/works/10.1515/9780691184487-010)
- [XML](https://api.crossref.org/works/10.1515/9780691184487-010.xml)
<hr>

### 10.1515/9781575066394-030
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781575066394-030)
- [Production](https://doi.org/10.1515/9781575066394-030)
- [JSON](https://api.crossref.org/works/10.1515/9781575066394-030)
- [XML](https://api.crossref.org/works/10.1515/9781575066394-030.xml)
<hr>

### 10.1215/9780822378426-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822378426-007)
- [Production](https://doi.org/10.1215/9780822378426-007)
- [JSON](https://api.crossref.org/works/10.1215/9780822378426-007)
- [XML](https://api.crossref.org/works/10.1215/9780822378426-007.xml)
<hr>

### 10.1515/9780822382522-026
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9780822382522-026)
- [Production](https://doi.org/10.1515/9780822382522-026)
- [JSON](https://api.crossref.org/works/10.1515/9780822382522-026)
- [XML](https://api.crossref.org/works/10.1515/9780822382522-026.xml)
<hr>

### 10.1515/9780691224862
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9780691224862)
- [Production](https://doi.org/10.1515/9780691224862)
- [JSON](https://api.crossref.org/works/10.1515/9780691224862)
- [XML](https://api.crossref.org/works/10.1515/9780691224862.xml)
<hr>

### 10.1525/9780520970656-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.1525/9780520970656-005)
- [Production](https://doi.org/10.1525/9780520970656-005)
- [JSON](https://api.crossref.org/works/10.1525/9780520970656-005)
- [XML](https://api.crossref.org/works/10.1525/9780520970656-005.xml)
<hr>

### 10.7591/cornell/9781501715488.003.0006
- [Chooser POC](https://chooser.fly.dev/?doi=10.7591/cornell/9781501715488.003.0006)
- [Production](https://doi.org/10.7591/cornell/9781501715488.003.0006)
- [JSON](https://api.crossref.org/works/10.7591/cornell/9781501715488.003.0006)
- [XML](https://api.crossref.org/works/10.7591/cornell/9781501715488.003.0006.xml)
<hr>

### 10.14361/transcript.9783839422618.133
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839422618.133)
- [Production](https://doi.org/10.14361/transcript.9783839422618.133)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839422618.133)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839422618.133.xml)
<hr>

### 10.1215/9780822373957-024
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822373957-024)
- [Production](https://doi.org/10.1215/9780822373957-024)
- [JSON](https://api.crossref.org/works/10.1215/9780822373957-024)
- [XML](https://api.crossref.org/works/10.1215/9780822373957-024.xml)
<hr>

### 10.14361/transcript.9783839414101.69
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839414101.69)
- [Production](https://doi.org/10.14361/transcript.9783839414101.69)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839414101.69)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839414101.69.xml)
<hr>

### 10.14361/9783839401163-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839401163-003)
- [Production](https://doi.org/10.14361/9783839401163-003)
- [JSON](https://api.crossref.org/works/10.14361/9783839401163-003)
- [XML](https://api.crossref.org/works/10.14361/9783839401163-003.xml)
<hr>

### 10.1215/9780822386810-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822386810-002)
- [Production](https://doi.org/10.1215/9780822386810-002)
- [JSON](https://api.crossref.org/works/10.1215/9780822386810-002)
- [XML](https://api.crossref.org/works/10.1215/9780822386810-002.xml)
<hr>

### 10.14361/9783839409442-012
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839409442-012)
- [Production](https://doi.org/10.14361/9783839409442-012)
- [JSON](https://api.crossref.org/works/10.14361/9783839409442-012)
- [XML](https://api.crossref.org/works/10.14361/9783839409442-012.xml)
<hr>

### 10.1515/9781501717130-012
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781501717130-012)
- [Production](https://doi.org/10.1515/9781501717130-012)
- [JSON](https://api.crossref.org/works/10.1515/9781501717130-012)
- [XML](https://api.crossref.org/works/10.1515/9781501717130-012.xml)
<hr>

### 10.14361/9783839446515-027
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839446515-027)
- [Production](https://doi.org/10.14361/9783839446515-027)
- [JSON](https://api.crossref.org/works/10.14361/9783839446515-027)
- [XML](https://api.crossref.org/works/10.14361/9783839446515-027.xml)
<hr>

### 10.14361/9783839432488-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839432488-002)
- [Production](https://doi.org/10.14361/9783839432488-002)
- [JSON](https://api.crossref.org/works/10.14361/9783839432488-002)
- [XML](https://api.crossref.org/works/10.14361/9783839432488-002.xml)
<hr>

### 10.1201/9781003067979-6
- [Chooser POC](https://chooser.fly.dev/?doi=10.1201/9781003067979-6)
- [Production](https://doi.org/10.1201/9781003067979-6)
- [JSON](https://api.crossref.org/works/10.1201/9781003067979-6)
- [XML](https://api.crossref.org/works/10.1201/9781003067979-6.xml)
<hr>

### 10.14361/transcript.9783839424360.37
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839424360.37)
- [Production](https://doi.org/10.14361/transcript.9783839424360.37)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839424360.37)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839424360.37.xml)
<hr>

### 10.1515/9780271072258-029
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9780271072258-029)
- [Production](https://doi.org/10.1515/9780271072258-029)
- [JSON](https://api.crossref.org/works/10.1515/9780271072258-029)
- [XML](https://api.crossref.org/works/10.1515/9780271072258-029.xml)
<hr>

### 10.14361/transcript.9783839421727.173
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839421727.173)
- [Production](https://doi.org/10.14361/transcript.9783839421727.173)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839421727.173)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839421727.173.xml)
<hr>

### 10.1215/9780822394778-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822394778-009)
- [Production](https://doi.org/10.1215/9780822394778-009)
- [JSON](https://api.crossref.org/works/10.1215/9780822394778-009)
- [XML](https://api.crossref.org/works/10.1215/9780822394778-009.xml)
<hr>

### 10.1215/9780822396345-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822396345-001)
- [Production](https://doi.org/10.1215/9780822396345-001)
- [JSON](https://api.crossref.org/works/10.1215/9780822396345-001)
- [XML](https://api.crossref.org/works/10.1215/9780822396345-001.xml)
<hr>

### 10.14361/9783839409961-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839409961-008)
- [Production](https://doi.org/10.14361/9783839409961-008)
- [JSON](https://api.crossref.org/works/10.14361/9783839409961-008)
- [XML](https://api.crossref.org/works/10.14361/9783839409961-008.xml)
<hr>

### 10.3998/mpub.7172850
- [Chooser POC](https://chooser.fly.dev/?doi=10.3998/mpub.7172850)
- [Production](https://doi.org/10.3998/mpub.7172850)
- [JSON](https://api.crossref.org/works/10.3998/mpub.7172850)
- [XML](https://api.crossref.org/works/10.3998/mpub.7172850.xml)
<hr>

### 10.14361/9783839457139-010
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839457139-010)
- [Production](https://doi.org/10.14361/9783839457139-010)
- [JSON](https://api.crossref.org/works/10.14361/9783839457139-010)
- [XML](https://api.crossref.org/works/10.14361/9783839457139-010.xml)
<hr>

### 10.1215/9780822397656-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822397656-006)
- [Production](https://doi.org/10.1215/9780822397656-006)
- [JSON](https://api.crossref.org/works/10.1215/9780822397656-006)
- [XML](https://api.crossref.org/works/10.1215/9780822397656-006.xml)
<hr>

### 10.1515/9780823284481-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9780823284481-009)
- [Production](https://doi.org/10.1515/9780823284481-009)
- [JSON](https://api.crossref.org/works/10.1515/9780823284481-009)
- [XML](https://api.crossref.org/works/10.1515/9780823284481-009.xml)
<hr>

### 10.14361/9783839411759
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839411759)
- [Production](https://doi.org/10.14361/9783839411759)
- [JSON](https://api.crossref.org/works/10.14361/9783839411759)
- [XML](https://api.crossref.org/works/10.14361/9783839411759.xml)
<hr>

### 10.14361/9783839429419-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839429419-001)
- [Production](https://doi.org/10.14361/9783839429419-001)
- [JSON](https://api.crossref.org/works/10.14361/9783839429419-001)
- [XML](https://api.crossref.org/works/10.14361/9783839429419-001.xml)
<hr>

### 10.14361/transcript.9783839424407.237
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839424407.237)
- [Production](https://doi.org/10.14361/transcript.9783839424407.237)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839424407.237)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839424407.237.xml)
<hr>

### 10.23943/princeton/9780691197845.003.0005
- [Chooser POC](https://chooser.fly.dev/?doi=10.23943/princeton/9780691197845.003.0005)
- [Production](https://doi.org/10.23943/princeton/9780691197845.003.0005)
- [JSON](https://api.crossref.org/works/10.23943/princeton/9780691197845.003.0005)
- [XML](https://api.crossref.org/works/10.23943/princeton/9780691197845.003.0005.xml)
<hr>

### 10.1515/9780691188621-010
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9780691188621-010)
- [Production](https://doi.org/10.1515/9780691188621-010)
- [JSON](https://api.crossref.org/works/10.1515/9780691188621-010)
- [XML](https://api.crossref.org/works/10.1515/9780691188621-010.xml)
<hr>

### 10.1017/9781787448148.005
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9781787448148.005)
- [Production](https://doi.org/10.1017/9781787448148.005)
- [JSON](https://api.crossref.org/works/10.1017/9781787448148.005)
- [XML](https://api.crossref.org/works/10.1017/9781787448148.005.xml)
<hr>

### 10.4324/9780203017869-7
- [Chooser POC](https://chooser.fly.dev/?doi=10.4324/9780203017869-7)
- [Production](https://doi.org/10.4324/9780203017869-7)
- [JSON](https://api.crossref.org/works/10.4324/9780203017869-7)
- [XML](https://api.crossref.org/works/10.4324/9780203017869-7.xml)
<hr>

### 10.14361/9783839457276-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839457276-005)
- [Production](https://doi.org/10.14361/9783839457276-005)
- [JSON](https://api.crossref.org/works/10.14361/9783839457276-005)
- [XML](https://api.crossref.org/works/10.14361/9783839457276-005.xml)
<hr>

### 10.1215/9780822381174-023
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822381174-023)
- [Production](https://doi.org/10.1215/9780822381174-023)
- [JSON](https://api.crossref.org/works/10.1215/9780822381174-023)
- [XML](https://api.crossref.org/works/10.1215/9780822381174-023.xml)
<hr>

### 10.1515/9781400848959-012
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781400848959-012)
- [Production](https://doi.org/10.1515/9781400848959-012)
- [JSON](https://api.crossref.org/works/10.1515/9781400848959-012)
- [XML](https://api.crossref.org/works/10.1515/9781400848959-012.xml)
<hr>

### 10.14361/transcript.9783839423493.27
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839423493.27)
- [Production](https://doi.org/10.14361/transcript.9783839423493.27)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839423493.27)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839423493.27.xml)
<hr>

### 10.14361/9783839431863-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839431863-005)
- [Production](https://doi.org/10.14361/9783839431863-005)
- [JSON](https://api.crossref.org/works/10.14361/9783839431863-005)
- [XML](https://api.crossref.org/works/10.14361/9783839431863-005.xml)
<hr>

### 10.1515/9781575066042-030
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781575066042-030)
- [Production](https://doi.org/10.1515/9781575066042-030)
- [JSON](https://api.crossref.org/works/10.1515/9781575066042-030)
- [XML](https://api.crossref.org/works/10.1515/9781575066042-030.xml)
<hr>

### 10.7591/9781501708381-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.7591/9781501708381-008)
- [Production](https://doi.org/10.7591/9781501708381-008)
- [JSON](https://api.crossref.org/works/10.7591/9781501708381-008)
- [XML](https://api.crossref.org/works/10.7591/9781501708381-008.xml)
<hr>

### 10.14361/9783839411308-020
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839411308-020)
- [Production](https://doi.org/10.14361/9783839411308-020)
- [JSON](https://api.crossref.org/works/10.14361/9783839411308-020)
- [XML](https://api.crossref.org/works/10.14361/9783839411308-020.xml)
<hr>

### 10.14361/9783839410684
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839410684)
- [Production](https://doi.org/10.14361/9783839410684)
- [JSON](https://api.crossref.org/works/10.14361/9783839410684)
- [XML](https://api.crossref.org/works/10.14361/9783839410684.xml)
<hr>

### 10.14361/9783839439661-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839439661-005)
- [Production](https://doi.org/10.14361/9783839439661-005)
- [JSON](https://api.crossref.org/works/10.14361/9783839439661-005)
- [XML](https://api.crossref.org/works/10.14361/9783839439661-005.xml)
<hr>

### 10.14361/9783839409220-019
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839409220-019)
- [Production](https://doi.org/10.14361/9783839409220-019)
- [JSON](https://api.crossref.org/works/10.14361/9783839409220-019)
- [XML](https://api.crossref.org/works/10.14361/9783839409220-019.xml)
<hr>

### 10.1515/9781575068763-015
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781575068763-015)
- [Production](https://doi.org/10.1515/9781575068763-015)
- [JSON](https://api.crossref.org/works/10.1515/9781575068763-015)
- [XML](https://api.crossref.org/works/10.1515/9781575068763-015.xml)
<hr>

### 10.1215/9781478009009-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9781478009009-003)
- [Production](https://doi.org/10.1215/9781478009009-003)
- [JSON](https://api.crossref.org/works/10.1215/9781478009009-003)
- [XML](https://api.crossref.org/works/10.1215/9781478009009-003.xml)
<hr>

### 10.14361/9783839411001-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839411001-001)
- [Production](https://doi.org/10.14361/9783839411001-001)
- [JSON](https://api.crossref.org/works/10.14361/9783839411001-001)
- [XML](https://api.crossref.org/works/10.14361/9783839411001-001.xml)
<hr>

### 10.14361/9783839446829-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839446829-001)
- [Production](https://doi.org/10.14361/9783839446829-001)
- [JSON](https://api.crossref.org/works/10.14361/9783839446829-001)
- [XML](https://api.crossref.org/works/10.14361/9783839446829-001.xml)
<hr>

### 10.14361/9783839450093-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839450093-009)
- [Production](https://doi.org/10.14361/9783839450093-009)
- [JSON](https://api.crossref.org/works/10.14361/9783839450093-009)
- [XML](https://api.crossref.org/works/10.14361/9783839450093-009.xml)
<hr>

### 10.14361/9783839413524-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839413524-009)
- [Production](https://doi.org/10.14361/9783839413524-009)
- [JSON](https://api.crossref.org/works/10.14361/9783839413524-009)
- [XML](https://api.crossref.org/works/10.14361/9783839413524-009.xml)
<hr>

### 10.1332/policypress/9781447337201.003.0009
- [Chooser POC](https://chooser.fly.dev/?doi=10.1332/policypress/9781447337201.003.0009)
- [Production](https://doi.org/10.1332/policypress/9781447337201.003.0009)
- [JSON](https://api.crossref.org/works/10.1332/policypress/9781447337201.003.0009)
- [XML](https://api.crossref.org/works/10.1332/policypress/9781447337201.003.0009.xml)
<hr>

### 10.1215/9780822387183-013
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822387183-013)
- [Production](https://doi.org/10.1215/9780822387183-013)
- [JSON](https://api.crossref.org/works/10.1215/9780822387183-013)
- [XML](https://api.crossref.org/works/10.1215/9780822387183-013.xml)
<hr>

### 10.14361/9783839406397-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839406397-007)
- [Production](https://doi.org/10.14361/9783839406397-007)
- [JSON](https://api.crossref.org/works/10.14361/9783839406397-007)
- [XML](https://api.crossref.org/works/10.14361/9783839406397-007.xml)
<hr>

### 10.1515/9780822372943-024
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9780822372943-024)
- [Production](https://doi.org/10.1515/9780822372943-024)
- [JSON](https://api.crossref.org/works/10.1515/9780822372943-024)
- [XML](https://api.crossref.org/works/10.1515/9780822372943-024.xml)
<hr>

### 10.1515/9781575065052-021
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781575065052-021)
- [Production](https://doi.org/10.1515/9781575065052-021)
- [JSON](https://api.crossref.org/works/10.1515/9781575065052-021)
- [XML](https://api.crossref.org/works/10.1515/9781575065052-021.xml)
<hr>

### 10.14361/transcript.9783839423974.245
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839423974.245)
- [Production](https://doi.org/10.14361/transcript.9783839423974.245)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839423974.245)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839423974.245.xml)
<hr>

### 10.3366/edinburgh/9781474403573.003.0001
- [Chooser POC](https://chooser.fly.dev/?doi=10.3366/edinburgh/9781474403573.003.0001)
- [Production](https://doi.org/10.3366/edinburgh/9781474403573.003.0001)
- [JSON](https://api.crossref.org/works/10.3366/edinburgh/9781474403573.003.0001)
- [XML](https://api.crossref.org/works/10.3366/edinburgh/9781474403573.003.0001.xml)
<hr>

### 10.14361/9783839407615-017
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839407615-017)
- [Production](https://doi.org/10.14361/9783839407615-017)
- [JSON](https://api.crossref.org/works/10.14361/9783839407615-017)
- [XML](https://api.crossref.org/works/10.14361/9783839407615-017.xml)
<hr>

### 10.14361/9783839436615-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839436615-003)
- [Production](https://doi.org/10.14361/9783839436615-003)
- [JSON](https://api.crossref.org/works/10.14361/9783839436615-003)
- [XML](https://api.crossref.org/works/10.14361/9783839436615-003.xml)
<hr>

### 10.14361/9783839452219-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839452219-009)
- [Production](https://doi.org/10.14361/9783839452219-009)
- [JSON](https://api.crossref.org/works/10.14361/9783839452219-009)
- [XML](https://api.crossref.org/works/10.14361/9783839452219-009.xml)
<hr>

### 10.2307/j.ctvktrxp2.8
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctvktrxp2.8)
- [Production](https://doi.org/10.2307/j.ctvktrxp2.8)
- [JSON](https://api.crossref.org/works/10.2307/j.ctvktrxp2.8)
- [XML](https://api.crossref.org/works/10.2307/j.ctvktrxp2.8.xml)
<hr>

### 10.14361/9783839452738-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839452738-006)
- [Production](https://doi.org/10.14361/9783839452738-006)
- [JSON](https://api.crossref.org/works/10.14361/9783839452738-006)
- [XML](https://api.crossref.org/works/10.14361/9783839452738-006.xml)
<hr>

### 10.3828/liverpool/9781789620207.003.0016
- [Chooser POC](https://chooser.fly.dev/?doi=10.3828/liverpool/9781789620207.003.0016)
- [Production](https://doi.org/10.3828/liverpool/9781789620207.003.0016)
- [JSON](https://api.crossref.org/works/10.3828/liverpool/9781789620207.003.0016)
- [XML](https://api.crossref.org/works/10.3828/liverpool/9781789620207.003.0016.xml)
<hr>

### 10.14361/9783839448632-020
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839448632-020)
- [Production](https://doi.org/10.14361/9783839448632-020)
- [JSON](https://api.crossref.org/works/10.14361/9783839448632-020)
- [XML](https://api.crossref.org/works/10.14361/9783839448632-020.xml)
<hr>

### 10.36019/9781978805521-033
- [Chooser POC](https://chooser.fly.dev/?doi=10.36019/9781978805521-033)
- [Production](https://doi.org/10.36019/9781978805521-033)
- [JSON](https://api.crossref.org/works/10.36019/9781978805521-033)
- [XML](https://api.crossref.org/works/10.36019/9781978805521-033.xml)
<hr>

### 10.1201/9781003078647-54
- [Chooser POC](https://chooser.fly.dev/?doi=10.1201/9781003078647-54)
- [Production](https://doi.org/10.1201/9781003078647-54)
- [JSON](https://api.crossref.org/works/10.1201/9781003078647-54)
- [XML](https://api.crossref.org/works/10.1201/9781003078647-54.xml)
<hr>

### 10.14361/transcript.9783839422878.159
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839422878.159)
- [Production](https://doi.org/10.14361/transcript.9783839422878.159)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839422878.159)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839422878.159.xml)
<hr>

### 10.1215/9780822385912-017
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822385912-017)
- [Production](https://doi.org/10.1215/9780822385912-017)
- [JSON](https://api.crossref.org/works/10.1215/9780822385912-017)
- [XML](https://api.crossref.org/works/10.1215/9780822385912-017.xml)
<hr>

### 10.14361/9783839408001-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839408001-009)
- [Production](https://doi.org/10.14361/9783839408001-009)
- [JSON](https://api.crossref.org/works/10.14361/9783839408001-009)
- [XML](https://api.crossref.org/works/10.14361/9783839408001-009.xml)
<hr>

### 10.1215/9780822384182-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822384182-004)
- [Production](https://doi.org/10.1215/9780822384182-004)
- [JSON](https://api.crossref.org/works/10.1215/9780822384182-004)
- [XML](https://api.crossref.org/works/10.1215/9780822384182-004.xml)
<hr>

### 10.14361/9783839458808-010
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839458808-010)
- [Production](https://doi.org/10.14361/9783839458808-010)
- [JSON](https://api.crossref.org/works/10.14361/9783839458808-010)
- [XML](https://api.crossref.org/works/10.14361/9783839458808-010.xml)
<hr>

### 10.1515/9781400885381-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781400885381-001)
- [Production](https://doi.org/10.1515/9781400885381-001)
- [JSON](https://api.crossref.org/works/10.1515/9781400885381-001)
- [XML](https://api.crossref.org/works/10.1515/9781400885381-001.xml)
<hr>

### 10.1215/9780822374107-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822374107-008)
- [Production](https://doi.org/10.1215/9780822374107-008)
- [JSON](https://api.crossref.org/works/10.1215/9780822374107-008)
- [XML](https://api.crossref.org/works/10.1215/9780822374107-008.xml)
<hr>

### 10.5949/UPO9781846315404.018
- [Chooser POC](https://chooser.fly.dev/?doi=10.5949/UPO9781846315404.018)
- [Production](https://doi.org/10.5949/UPO9781846315404.018)
- [JSON](https://api.crossref.org/works/10.5949/UPO9781846315404.018)
- [XML](https://api.crossref.org/works/10.5949/UPO9781846315404.018.xml)
<hr>

### 10.14361/transcript.9783839419984.7
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839419984.7)
- [Production](https://doi.org/10.14361/transcript.9783839419984.7)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839419984.7)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839419984.7.xml)
<hr>

### 10.14361/9783839450581-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839450581-004)
- [Production](https://doi.org/10.14361/9783839450581-004)
- [JSON](https://api.crossref.org/works/10.14361/9783839450581-004)
- [XML](https://api.crossref.org/works/10.14361/9783839450581-004.xml)
<hr>

### 10.14361/9783839403396-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839403396-003)
- [Production](https://doi.org/10.14361/9783839403396-003)
- [JSON](https://api.crossref.org/works/10.14361/9783839403396-003)
- [XML](https://api.crossref.org/works/10.14361/9783839403396-003.xml)
<hr>

### 10.1515/9781400855438.253
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781400855438.253)
- [Production](https://doi.org/10.1515/9781400855438.253)
- [JSON](https://api.crossref.org/works/10.1515/9781400855438.253)
- [XML](https://api.crossref.org/works/10.1515/9781400855438.253.xml)
<hr>

### 10.1215/9780822388364-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822388364-005)
- [Production](https://doi.org/10.1215/9780822388364-005)
- [JSON](https://api.crossref.org/works/10.1215/9780822388364-005)
- [XML](https://api.crossref.org/works/10.1215/9780822388364-005.xml)
<hr>

### 10.14361/9783839453667-026
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839453667-026)
- [Production](https://doi.org/10.14361/9783839453667-026)
- [JSON](https://api.crossref.org/works/10.14361/9783839453667-026)
- [XML](https://api.crossref.org/works/10.14361/9783839453667-026.xml)
<hr>

### 10.1515/9781575065809-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781575065809-003)
- [Production](https://doi.org/10.1515/9781575065809-003)
- [JSON](https://api.crossref.org/works/10.1515/9781575065809-003)
- [XML](https://api.crossref.org/works/10.1515/9781575065809-003.xml)
<hr>

### 10.14361/9783839449998-010
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839449998-010)
- [Production](https://doi.org/10.14361/9783839449998-010)
- [JSON](https://api.crossref.org/works/10.14361/9783839449998-010)
- [XML](https://api.crossref.org/works/10.14361/9783839449998-010.xml)
<hr>

### 10.14361/9783839402641-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839402641-005)
- [Production](https://doi.org/10.14361/9783839402641-005)
- [JSON](https://api.crossref.org/works/10.14361/9783839402641-005)
- [XML](https://api.crossref.org/works/10.14361/9783839402641-005.xml)
<hr>

### 10.14361/9783839427880-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839427880-009)
- [Production](https://doi.org/10.14361/9783839427880-009)
- [JSON](https://api.crossref.org/works/10.14361/9783839427880-009)
- [XML](https://api.crossref.org/works/10.14361/9783839427880-009.xml)
<hr>

### 10.14361/9783839429624-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839429624-002)
- [Production](https://doi.org/10.14361/9783839429624-002)
- [JSON](https://api.crossref.org/works/10.14361/9783839429624-002)
- [XML](https://api.crossref.org/works/10.14361/9783839429624-002.xml)
<hr>

### 10.3366/edinburgh/9780748641048.003.0008
- [Chooser POC](https://chooser.fly.dev/?doi=10.3366/edinburgh/9780748641048.003.0008)
- [Production](https://doi.org/10.3366/edinburgh/9780748641048.003.0008)
- [JSON](https://api.crossref.org/works/10.3366/edinburgh/9780748641048.003.0008)
- [XML](https://api.crossref.org/works/10.3366/edinburgh/9780748641048.003.0008.xml)
<hr>

### 10.14361/9783839407721-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839407721-006)
- [Production](https://doi.org/10.14361/9783839407721-006)
- [JSON](https://api.crossref.org/works/10.14361/9783839407721-006)
- [XML](https://api.crossref.org/works/10.14361/9783839407721-006.xml)
<hr>

### 10.14361/9783839409367-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839409367-002)
- [Production](https://doi.org/10.14361/9783839409367-002)
- [JSON](https://api.crossref.org/works/10.14361/9783839409367-002)
- [XML](https://api.crossref.org/works/10.14361/9783839409367-002.xml)
<hr>

### 10.3828/liverpool/9781786940438.003.0004
- [Chooser POC](https://chooser.fly.dev/?doi=10.3828/liverpool/9781786940438.003.0004)
- [Production](https://doi.org/10.3828/liverpool/9781786940438.003.0004)
- [JSON](https://api.crossref.org/works/10.3828/liverpool/9781786940438.003.0004)
- [XML](https://api.crossref.org/works/10.3828/liverpool/9781786940438.003.0004.xml)
<hr>

### 10.1215/9780822384335-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822384335-009)
- [Production](https://doi.org/10.1215/9780822384335-009)
- [JSON](https://api.crossref.org/works/10.1215/9780822384335-009)
- [XML](https://api.crossref.org/works/10.1215/9780822384335-009.xml)
<hr>

### 10.14361/9783839441152-022
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839441152-022)
- [Production](https://doi.org/10.14361/9783839441152-022)
- [JSON](https://api.crossref.org/works/10.14361/9783839441152-022)
- [XML](https://api.crossref.org/works/10.14361/9783839441152-022.xml)
<hr>

### 10.14361/transcript.9783839413968.157
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839413968.157)
- [Production](https://doi.org/10.14361/transcript.9783839413968.157)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839413968.157)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839413968.157.xml)
<hr>

### 10.14361/9783839407400-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839407400-002)
- [Production](https://doi.org/10.14361/9783839407400-002)
- [JSON](https://api.crossref.org/works/10.14361/9783839407400-002)
- [XML](https://api.crossref.org/works/10.14361/9783839407400-002.xml)
<hr>

### 10.14361/9783839452660-022
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839452660-022)
- [Production](https://doi.org/10.14361/9783839452660-022)
- [JSON](https://api.crossref.org/works/10.14361/9783839452660-022)
- [XML](https://api.crossref.org/works/10.14361/9783839452660-022.xml)
<hr>

### 10.14361/transcript.9783839421826
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839421826)
- [Production](https://doi.org/10.14361/transcript.9783839421826)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839421826)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839421826.xml)
<hr>

### 10.14361/9783839401200-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839401200-001)
- [Production](https://doi.org/10.14361/9783839401200-001)
- [JSON](https://api.crossref.org/works/10.14361/9783839401200-001)
- [XML](https://api.crossref.org/works/10.14361/9783839401200-001.xml)
<hr>

### 10.3828/liverpool/9781874774594.003.0015
- [Chooser POC](https://chooser.fly.dev/?doi=10.3828/liverpool/9781874774594.003.0015)
- [Production](https://doi.org/10.3828/liverpool/9781874774594.003.0015)
- [JSON](https://api.crossref.org/works/10.3828/liverpool/9781874774594.003.0015)
- [XML](https://api.crossref.org/works/10.3828/liverpool/9781874774594.003.0015.xml)
<hr>

### 10.14361/9783839412268-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839412268-009)
- [Production](https://doi.org/10.14361/9783839412268-009)
- [JSON](https://api.crossref.org/works/10.14361/9783839412268-009)
- [XML](https://api.crossref.org/works/10.14361/9783839412268-009.xml)
<hr>

### 10.1515/9781618118783
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781618118783)
- [Production](https://doi.org/10.1515/9781618118783)
- [JSON](https://api.crossref.org/works/10.1515/9781618118783)
- [XML](https://api.crossref.org/works/10.1515/9781618118783.xml)
<hr>

### 10.1201/9781003068624-1
- [Chooser POC](https://chooser.fly.dev/?doi=10.1201/9781003068624-1)
- [Production](https://doi.org/10.1201/9781003068624-1)
- [JSON](https://api.crossref.org/works/10.1201/9781003068624-1)
- [XML](https://api.crossref.org/works/10.1201/9781003068624-1.xml)
<hr>

### 10.14361/9783839459652-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839459652-006)
- [Production](https://doi.org/10.14361/9783839459652-006)
- [JSON](https://api.crossref.org/works/10.14361/9783839459652-006)
- [XML](https://api.crossref.org/works/10.14361/9783839459652-006.xml)
<hr>

### 10.1017/9789048530694.008
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9789048530694.008)
- [Production](https://doi.org/10.1017/9789048530694.008)
- [JSON](https://api.crossref.org/works/10.1017/9789048530694.008)
- [XML](https://api.crossref.org/works/10.1017/9789048530694.008.xml)
<hr>

### 10.1515/9781575068787-011
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781575068787-011)
- [Production](https://doi.org/10.1515/9781575068787-011)
- [JSON](https://api.crossref.org/works/10.1515/9781575068787-011)
- [XML](https://api.crossref.org/works/10.1515/9781575068787-011.xml)
<hr>

### 10.14361/transcript.9783839427736.15
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839427736.15)
- [Production](https://doi.org/10.14361/transcript.9783839427736.15)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839427736.15)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839427736.15.xml)
<hr>

### 10.14361/transcript.9783839425671.209
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839425671.209)
- [Production](https://doi.org/10.14361/transcript.9783839425671.209)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839425671.209)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839425671.209.xml)
<hr>

### 10.14361/9783839434369-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839434369-001)
- [Production](https://doi.org/10.14361/9783839434369-001)
- [JSON](https://api.crossref.org/works/10.14361/9783839434369-001)
- [XML](https://api.crossref.org/works/10.14361/9783839434369-001.xml)
<hr>

### 10.3366/edinburgh/9781474403795.003.0015
- [Chooser POC](https://chooser.fly.dev/?doi=10.3366/edinburgh/9781474403795.003.0015)
- [Production](https://doi.org/10.3366/edinburgh/9781474403795.003.0015)
- [JSON](https://api.crossref.org/works/10.3366/edinburgh/9781474403795.003.0015)
- [XML](https://api.crossref.org/works/10.3366/edinburgh/9781474403795.003.0015.xml)
<hr>

### 10.14361/9783839405949
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839405949)
- [Production](https://doi.org/10.14361/9783839405949)
- [JSON](https://api.crossref.org/works/10.14361/9783839405949)
- [XML](https://api.crossref.org/works/10.14361/9783839405949.xml)
<hr>

### 10.3366/edinburgh/9781474417716.003.0005
- [Chooser POC](https://chooser.fly.dev/?doi=10.3366/edinburgh/9781474417716.003.0005)
- [Production](https://doi.org/10.3366/edinburgh/9781474417716.003.0005)
- [JSON](https://api.crossref.org/works/10.3366/edinburgh/9781474417716.003.0005)
- [XML](https://api.crossref.org/works/10.3366/edinburgh/9781474417716.003.0005.xml)
<hr>

### 10.14361/9783839427002-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839427002-002)
- [Production](https://doi.org/10.14361/9783839427002-002)
- [JSON](https://api.crossref.org/works/10.14361/9783839427002-002)
- [XML](https://api.crossref.org/works/10.14361/9783839427002-002.xml)
<hr>

### 10.14361/9783839411087-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839411087-004)
- [Production](https://doi.org/10.14361/9783839411087-004)
- [JSON](https://api.crossref.org/works/10.14361/9783839411087-004)
- [XML](https://api.crossref.org/works/10.14361/9783839411087-004.xml)
<hr>

### 10.1332/policypress/9781447307150.003.0008
- [Chooser POC](https://chooser.fly.dev/?doi=10.1332/policypress/9781447307150.003.0008)
- [Production](https://doi.org/10.1332/policypress/9781447307150.003.0008)
- [JSON](https://api.crossref.org/works/10.1332/policypress/9781447307150.003.0008)
- [XML](https://api.crossref.org/works/10.1332/policypress/9781447307150.003.0008.xml)
<hr>

### 10.1215/9780822375425-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822375425-004)
- [Production](https://doi.org/10.1215/9780822375425-004)
- [JSON](https://api.crossref.org/works/10.1215/9780822375425-004)
- [XML](https://api.crossref.org/works/10.1215/9780822375425-004.xml)
<hr>

### 10.14361/9783839454077-017
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839454077-017)
- [Production](https://doi.org/10.14361/9783839454077-017)
- [JSON](https://api.crossref.org/works/10.14361/9783839454077-017)
- [XML](https://api.crossref.org/works/10.14361/9783839454077-017.xml)
<hr>

### 10.14361/9783839448878-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839448878-005)
- [Production](https://doi.org/10.14361/9783839448878-005)
- [JSON](https://api.crossref.org/works/10.14361/9783839448878-005)
- [XML](https://api.crossref.org/works/10.14361/9783839448878-005.xml)
<hr>

### 10.14361/transcript.9783839416907.139
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839416907.139)
- [Production](https://doi.org/10.14361/transcript.9783839416907.139)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839416907.139)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839416907.139.xml)
<hr>

### 10.46692/9781447352228.005
- [Chooser POC](https://chooser.fly.dev/?doi=10.46692/9781447352228.005)
- [Production](https://doi.org/10.46692/9781447352228.005)
- [JSON](https://api.crossref.org/works/10.46692/9781447352228.005)
- [XML](https://api.crossref.org/works/10.46692/9781447352228.005.xml)
<hr>

### 10.1215/9780822394167-014
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822394167-014)
- [Production](https://doi.org/10.1215/9780822394167-014)
- [JSON](https://api.crossref.org/works/10.1215/9780822394167-014)
- [XML](https://api.crossref.org/works/10.1215/9780822394167-014.xml)
<hr>

### 10.14361/9783839440827-011
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839440827-011)
- [Production](https://doi.org/10.14361/9783839440827-011)
- [JSON](https://api.crossref.org/works/10.14361/9783839440827-011)
- [XML](https://api.crossref.org/works/10.14361/9783839440827-011.xml)
<hr>

### 10.14361/transcript.9783839428627.169
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839428627.169)
- [Production](https://doi.org/10.14361/transcript.9783839428627.169)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839428627.169)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839428627.169.xml)
<hr>

### 10.1515/9781575066042-034
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781575066042-034)
- [Production](https://doi.org/10.1515/9781575066042-034)
- [JSON](https://api.crossref.org/works/10.1515/9781575066042-034)
- [XML](https://api.crossref.org/works/10.1515/9781575066042-034.xml)
<hr>

### 10.2307/j.ctvsf1qrj.10
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctvsf1qrj.10)
- [Production](https://doi.org/10.2307/j.ctvsf1qrj.10)
- [JSON](https://api.crossref.org/works/10.2307/j.ctvsf1qrj.10)
- [XML](https://api.crossref.org/works/10.2307/j.ctvsf1qrj.10.xml)
<hr>

### 10.14361/9783839441398-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839441398-008)
- [Production](https://doi.org/10.14361/9783839441398-008)
- [JSON](https://api.crossref.org/works/10.14361/9783839441398-008)
- [XML](https://api.crossref.org/works/10.14361/9783839441398-008.xml)
<hr>

### 10.14361/transcript.9783839418154.99
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839418154.99)
- [Production](https://doi.org/10.14361/transcript.9783839418154.99)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839418154.99)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839418154.99.xml)
<hr>

### 10.1332/policypress/9781861344984.003.0007
- [Chooser POC](https://chooser.fly.dev/?doi=10.1332/policypress/9781861344984.003.0007)
- [Production](https://doi.org/10.1332/policypress/9781861344984.003.0007)
- [JSON](https://api.crossref.org/works/10.1332/policypress/9781861344984.003.0007)
- [XML](https://api.crossref.org/works/10.1332/policypress/9781861344984.003.0007.xml)
<hr>

### 10.1332/policypress/9781861348043.003.0009
- [Chooser POC](https://chooser.fly.dev/?doi=10.1332/policypress/9781861348043.003.0009)
- [Production](https://doi.org/10.1332/policypress/9781861348043.003.0009)
- [JSON](https://api.crossref.org/works/10.1332/policypress/9781861348043.003.0009)
- [XML](https://api.crossref.org/works/10.1332/policypress/9781861348043.003.0009.xml)
<hr>

### 10.1017/9789048514502.009
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9789048514502.009)
- [Production](https://doi.org/10.1017/9789048514502.009)
- [JSON](https://api.crossref.org/works/10.1017/9789048514502.009)
- [XML](https://api.crossref.org/works/10.1017/9789048514502.009.xml)
<hr>

### 10.1215/9780822383888-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822383888-008)
- [Production](https://doi.org/10.1215/9780822383888-008)
- [JSON](https://api.crossref.org/works/10.1215/9780822383888-008)
- [XML](https://api.crossref.org/works/10.1215/9780822383888-008.xml)
<hr>

### 10.1215/9780822395973-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822395973-005)
- [Production](https://doi.org/10.1215/9780822395973-005)
- [JSON](https://api.crossref.org/works/10.1215/9780822395973-005)
- [XML](https://api.crossref.org/works/10.1215/9780822395973-005.xml)
<hr>

### 10.14361/9783839455708-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839455708-007)
- [Production](https://doi.org/10.14361/9783839455708-007)
- [JSON](https://api.crossref.org/works/10.14361/9783839455708-007)
- [XML](https://api.crossref.org/works/10.14361/9783839455708-007.xml)
<hr>

### 10.1215/9780822386599-011
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822386599-011)
- [Production](https://doi.org/10.1215/9780822386599-011)
- [JSON](https://api.crossref.org/works/10.1215/9780822386599-011)
- [XML](https://api.crossref.org/works/10.1215/9780822386599-011.xml)
<hr>

### 10.14361/9783839434758-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839434758-003)
- [Production](https://doi.org/10.14361/9783839434758-003)
- [JSON](https://api.crossref.org/works/10.14361/9783839434758-003)
- [XML](https://api.crossref.org/works/10.14361/9783839434758-003.xml)
<hr>

### 10.3366/edinburgh/9780748620111.003.0009
- [Chooser POC](https://chooser.fly.dev/?doi=10.3366/edinburgh/9780748620111.003.0009)
- [Production](https://doi.org/10.3366/edinburgh/9780748620111.003.0009)
- [JSON](https://api.crossref.org/works/10.3366/edinburgh/9780748620111.003.0009)
- [XML](https://api.crossref.org/works/10.3366/edinburgh/9780748620111.003.0009.xml)
<hr>

### 10.14361/transcript.9783839414538.133
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839414538.133)
- [Production](https://doi.org/10.14361/transcript.9783839414538.133)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839414538.133)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839414538.133.xml)
<hr>

### 10.14361/9783839402306-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839402306-006)
- [Production](https://doi.org/10.14361/9783839402306-006)
- [JSON](https://api.crossref.org/works/10.14361/9783839402306-006)
- [XML](https://api.crossref.org/works/10.14361/9783839402306-006.xml)
<hr>

### 10.14361/transcript.9783839420386.283
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839420386.283)
- [Production](https://doi.org/10.14361/transcript.9783839420386.283)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839420386.283)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839420386.283.xml)
<hr>

### 10.1215/9780822383376-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822383376-006)
- [Production](https://doi.org/10.1215/9780822383376-006)
- [JSON](https://api.crossref.org/works/10.1215/9780822383376-006)
- [XML](https://api.crossref.org/works/10.1215/9780822383376-006.xml)
<hr>

### 10.14361/9783839453162-010
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839453162-010)
- [Production](https://doi.org/10.14361/9783839453162-010)
- [JSON](https://api.crossref.org/works/10.14361/9783839453162-010)
- [XML](https://api.crossref.org/works/10.14361/9783839453162-010.xml)
<hr>

### 10.1215/9780822383369-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822383369-001)
- [Production](https://doi.org/10.1215/9780822383369-001)
- [JSON](https://api.crossref.org/works/10.1215/9780822383369-001)
- [XML](https://api.crossref.org/works/10.1215/9780822383369-001.xml)
<hr>

### 10.1515/9781400884896-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781400884896-002)
- [Production](https://doi.org/10.1515/9781400884896-002)
- [JSON](https://api.crossref.org/works/10.1515/9781400884896-002)
- [XML](https://api.crossref.org/works/10.1515/9781400884896-002.xml)
<hr>

### 10.14361/9783839435199-011
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839435199-011)
- [Production](https://doi.org/10.14361/9783839435199-011)
- [JSON](https://api.crossref.org/works/10.14361/9783839435199-011)
- [XML](https://api.crossref.org/works/10.14361/9783839435199-011.xml)
<hr>

### 10.1215/9780822372967-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822372967-009)
- [Production](https://doi.org/10.1215/9780822372967-009)
- [JSON](https://api.crossref.org/works/10.1215/9780822372967-009)
- [XML](https://api.crossref.org/works/10.1215/9780822372967-009.xml)
<hr>

### 10.14361/transcript.9783839416631.15
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839416631.15)
- [Production](https://doi.org/10.14361/transcript.9783839416631.15)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839416631.15)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839416631.15.xml)
<hr>

### 10.1215/9780822395553-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822395553-009)
- [Production](https://doi.org/10.1215/9780822395553-009)
- [JSON](https://api.crossref.org/works/10.1215/9780822395553-009)
- [XML](https://api.crossref.org/works/10.1215/9780822395553-009.xml)
<hr>

### 10.14361/transcript.9783839417263.236
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839417263.236)
- [Production](https://doi.org/10.14361/transcript.9783839417263.236)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839417263.236)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839417263.236.xml)
<hr>

### 10.5325/j.ctv14gpcfw.7
- [Chooser POC](https://chooser.fly.dev/?doi=10.5325/j.ctv14gpcfw.7)
- [Production](https://doi.org/10.5325/j.ctv14gpcfw.7)
- [JSON](https://api.crossref.org/works/10.5325/j.ctv14gpcfw.7)
- [XML](https://api.crossref.org/works/10.5325/j.ctv14gpcfw.7.xml)
<hr>

### 10.14361/transcript.9783839420713.233
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839420713.233)
- [Production](https://doi.org/10.14361/transcript.9783839420713.233)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839420713.233)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839420713.233.xml)
<hr>

### 10.1515/9781400829408-027
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781400829408-027)
- [Production](https://doi.org/10.1515/9781400829408-027)
- [JSON](https://api.crossref.org/works/10.1515/9781400829408-027)
- [XML](https://api.crossref.org/works/10.1515/9781400829408-027.xml)
<hr>

### 10.1017/9789048552504.008
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9789048552504.008)
- [Production](https://doi.org/10.1017/9789048552504.008)
- [JSON](https://api.crossref.org/works/10.1017/9789048552504.008)
- [XML](https://api.crossref.org/works/10.1017/9789048552504.008.xml)
<hr>

### 10.1017/9781787448995.007
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9781787448995.007)
- [Production](https://doi.org/10.1017/9781787448995.007)
- [JSON](https://api.crossref.org/works/10.1017/9781787448995.007)
- [XML](https://api.crossref.org/works/10.1017/9781787448995.007.xml)
<hr>

### 10.1017/9781942401353.020
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9781942401353.020)
- [Production](https://doi.org/10.1017/9781942401353.020)
- [JSON](https://api.crossref.org/works/10.1017/9781942401353.020)
- [XML](https://api.crossref.org/works/10.1017/9781942401353.020.xml)
<hr>

### 10.14361/9783839432365-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839432365-005)
- [Production](https://doi.org/10.14361/9783839432365-005)
- [JSON](https://api.crossref.org/works/10.14361/9783839432365-005)
- [XML](https://api.crossref.org/works/10.14361/9783839432365-005.xml)
<hr>

### 10.11126/stanford/9780804784115.003.0008
- [Chooser POC](https://chooser.fly.dev/?doi=10.11126/stanford/9780804784115.003.0008)
- [Production](https://doi.org/10.11126/stanford/9780804784115.003.0008)
- [JSON](https://api.crossref.org/works/10.11126/stanford/9780804784115.003.0008)
- [XML](https://api.crossref.org/works/10.11126/stanford/9780804784115.003.0008.xml)
<hr>

### 10.14361/9783839450598-017
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839450598-017)
- [Production](https://doi.org/10.14361/9783839450598-017)
- [JSON](https://api.crossref.org/works/10.14361/9783839450598-017)
- [XML](https://api.crossref.org/works/10.14361/9783839450598-017.xml)
<hr>

### 10.14361/9783839448182-010
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839448182-010)
- [Production](https://doi.org/10.14361/9783839448182-010)
- [JSON](https://api.crossref.org/works/10.14361/9783839448182-010)
- [XML](https://api.crossref.org/works/10.14361/9783839448182-010.xml)
<hr>

### 10.14361/9783839443422-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839443422-005)
- [Production](https://doi.org/10.14361/9783839443422-005)
- [JSON](https://api.crossref.org/works/10.14361/9783839443422-005)
- [XML](https://api.crossref.org/works/10.14361/9783839443422-005.xml)
<hr>

### 10.1515/9781478007456-027
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781478007456-027)
- [Production](https://doi.org/10.1515/9781478007456-027)
- [JSON](https://api.crossref.org/works/10.1515/9781478007456-027)
- [XML](https://api.crossref.org/works/10.1515/9781478007456-027.xml)
<hr>

### 10.1215/9781478021957-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9781478021957-003)
- [Production](https://doi.org/10.1215/9781478021957-003)
- [JSON](https://api.crossref.org/works/10.1215/9781478021957-003)
- [XML](https://api.crossref.org/works/10.1215/9781478021957-003.xml)
<hr>

### 10.14361/9783839407936-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839407936-006)
- [Production](https://doi.org/10.14361/9783839407936-006)
- [JSON](https://api.crossref.org/works/10.14361/9783839407936-006)
- [XML](https://api.crossref.org/works/10.14361/9783839407936-006.xml)
<hr>

### 10.14361/9783839430712-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839430712-001)
- [Production](https://doi.org/10.14361/9783839430712-001)
- [JSON](https://api.crossref.org/works/10.14361/9783839430712-001)
- [XML](https://api.crossref.org/works/10.14361/9783839430712-001.xml)
<hr>

### 10.14361/9783839401439-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839401439-001)
- [Production](https://doi.org/10.14361/9783839401439-001)
- [JSON](https://api.crossref.org/works/10.14361/9783839401439-001)
- [XML](https://api.crossref.org/works/10.14361/9783839401439-001.xml)
<hr>

### 10.14361/transcript.9783839424957.127
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839424957.127)
- [Production](https://doi.org/10.14361/transcript.9783839424957.127)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839424957.127)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839424957.127.xml)
<hr>

### 10.14361/9783839410172-prf
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839410172-prf)
- [Production](https://doi.org/10.14361/9783839410172-prf)
- [JSON](https://api.crossref.org/works/10.14361/9783839410172-prf)
- [XML](https://api.crossref.org/works/10.14361/9783839410172-prf.xml)
<hr>

### 10.11126/stanford/9780804783637.003.0001
- [Chooser POC](https://chooser.fly.dev/?doi=10.11126/stanford/9780804783637.003.0001)
- [Production](https://doi.org/10.11126/stanford/9780804783637.003.0001)
- [JSON](https://api.crossref.org/works/10.11126/stanford/9780804783637.003.0001)
- [XML](https://api.crossref.org/works/10.11126/stanford/9780804783637.003.0001.xml)
<hr>

### 10.14361/9783839430415-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839430415-005)
- [Production](https://doi.org/10.14361/9783839430415-005)
- [JSON](https://api.crossref.org/works/10.14361/9783839430415-005)
- [XML](https://api.crossref.org/works/10.14361/9783839430415-005.xml)
<hr>

### 10.14361/transcript.9783839418192.95
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839418192.95)
- [Production](https://doi.org/10.14361/transcript.9783839418192.95)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839418192.95)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839418192.95.xml)
<hr>

### 10.14361/transcript.9783839423653.307
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839423653.307)
- [Production](https://doi.org/10.14361/transcript.9783839423653.307)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839423653.307)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839423653.307.xml)
<hr>

### 10.14361/9783839457504-067
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839457504-067)
- [Production](https://doi.org/10.14361/9783839457504-067)
- [JSON](https://api.crossref.org/works/10.14361/9783839457504-067)
- [XML](https://api.crossref.org/works/10.14361/9783839457504-067.xml)
<hr>

### 10.2307/j.ctv1zjg8tc
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctv1zjg8tc)
- [Production](https://doi.org/10.2307/j.ctv1zjg8tc)
- [JSON](https://api.crossref.org/works/10.2307/j.ctv1zjg8tc)
- [XML](https://api.crossref.org/works/10.2307/j.ctv1zjg8tc.xml)
<hr>

### 10.14361/9783839438473-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839438473-006)
- [Production](https://doi.org/10.14361/9783839438473-006)
- [JSON](https://api.crossref.org/works/10.14361/9783839438473-006)
- [XML](https://api.crossref.org/works/10.14361/9783839438473-006.xml)
<hr>

### 10.14361/transcript.9783839424643.74
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839424643.74)
- [Production](https://doi.org/10.14361/transcript.9783839424643.74)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839424643.74)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839424643.74.xml)
<hr>

### 10.1215/9780822396208-077
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822396208-077)
- [Production](https://doi.org/10.1215/9780822396208-077)
- [JSON](https://api.crossref.org/works/10.1215/9780822396208-077)
- [XML](https://api.crossref.org/works/10.1215/9780822396208-077.xml)
<hr>

### 10.14361/9783839446461-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839446461-007)
- [Production](https://doi.org/10.14361/9783839446461-007)
- [JSON](https://api.crossref.org/works/10.14361/9783839446461-007)
- [XML](https://api.crossref.org/works/10.14361/9783839446461-007.xml)
<hr>

### 10.14361/9783839405123-029
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839405123-029)
- [Production](https://doi.org/10.14361/9783839405123-029)
- [JSON](https://api.crossref.org/works/10.14361/9783839405123-029)
- [XML](https://api.crossref.org/works/10.14361/9783839405123-029.xml)
<hr>

### 10.14361/9783839459584
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839459584)
- [Production](https://doi.org/10.14361/9783839459584)
- [JSON](https://api.crossref.org/works/10.14361/9783839459584)
- [XML](https://api.crossref.org/works/10.14361/9783839459584.xml)
<hr>

### 10.2307/j.ctt1s17p06.9
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctt1s17p06.9)
- [Production](https://doi.org/10.2307/j.ctt1s17p06.9)
- [JSON](https://api.crossref.org/works/10.2307/j.ctt1s17p06.9)
- [XML](https://api.crossref.org/works/10.2307/j.ctt1s17p06.9.xml)
<hr>

### 10.1201/9781003090120-7
- [Chooser POC](https://chooser.fly.dev/?doi=10.1201/9781003090120-7)
- [Production](https://doi.org/10.1201/9781003090120-7)
- [JSON](https://api.crossref.org/works/10.1201/9781003090120-7)
- [XML](https://api.crossref.org/works/10.1201/9781003090120-7.xml)
<hr>

### 10.14361/transcript.9783839417171.215
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839417171.215)
- [Production](https://doi.org/10.14361/transcript.9783839417171.215)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839417171.215)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839417171.215.xml)
<hr>

### 10.14361/9783839453599-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839453599-009)
- [Production](https://doi.org/10.14361/9783839453599-009)
- [JSON](https://api.crossref.org/works/10.14361/9783839453599-009)
- [XML](https://api.crossref.org/works/10.14361/9783839453599-009.xml)
<hr>

### 10.1017/9781787448995.004
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9781787448995.004)
- [Production](https://doi.org/10.1017/9781787448995.004)
- [JSON](https://api.crossref.org/works/10.1017/9781787448995.004)
- [XML](https://api.crossref.org/works/10.1017/9781787448995.004.xml)
<hr>

### 10.2307/j.ctt9qdz8c
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctt9qdz8c)
- [Production](https://doi.org/10.2307/j.ctt9qdz8c)
- [JSON](https://api.crossref.org/works/10.2307/j.ctt9qdz8c)
- [XML](https://api.crossref.org/works/10.2307/j.ctt9qdz8c.xml)
<hr>

### 10.14361/9783839439456-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839439456-006)
- [Production](https://doi.org/10.14361/9783839439456-006)
- [JSON](https://api.crossref.org/works/10.14361/9783839439456-006)
- [XML](https://api.crossref.org/works/10.14361/9783839439456-006.xml)
<hr>

### 10.14361/9783839441985-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839441985-008)
- [Production](https://doi.org/10.14361/9783839441985-008)
- [JSON](https://api.crossref.org/works/10.14361/9783839441985-008)
- [XML](https://api.crossref.org/works/10.14361/9783839441985-008.xml)
<hr>

### 10.4324/9781003070818-10
- [Chooser POC](https://chooser.fly.dev/?doi=10.4324/9781003070818-10)
- [Production](https://doi.org/10.4324/9781003070818-10)
- [JSON](https://api.crossref.org/works/10.4324/9781003070818-10)
- [XML](https://api.crossref.org/works/10.4324/9781003070818-10.xml)
<hr>

### 10.1515/9780691196695-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9780691196695-006)
- [Production](https://doi.org/10.1515/9780691196695-006)
- [JSON](https://api.crossref.org/works/10.1515/9780691196695-006)
- [XML](https://api.crossref.org/works/10.1515/9780691196695-006.xml)
<hr>

### 10.14361/9783839447895-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839447895-005)
- [Production](https://doi.org/10.14361/9783839447895-005)
- [JSON](https://api.crossref.org/works/10.14361/9783839447895-005)
- [XML](https://api.crossref.org/works/10.14361/9783839447895-005.xml)
<hr>

### 10.14361/9783839401644-024
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839401644-024)
- [Production](https://doi.org/10.14361/9783839401644-024)
- [JSON](https://api.crossref.org/works/10.14361/9783839401644-024)
- [XML](https://api.crossref.org/works/10.14361/9783839401644-024.xml)
<hr>

### 10.14361/9783839406649-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839406649-009)
- [Production](https://doi.org/10.14361/9783839406649-009)
- [JSON](https://api.crossref.org/works/10.14361/9783839406649-009)
- [XML](https://api.crossref.org/works/10.14361/9783839406649-009.xml)
<hr>

### 10.1215/9780822374961-022
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822374961-022)
- [Production](https://doi.org/10.1215/9780822374961-022)
- [JSON](https://api.crossref.org/works/10.1215/9780822374961-022)
- [XML](https://api.crossref.org/works/10.1215/9780822374961-022.xml)
<hr>

### 10.1332/policypress/9781529215144.003.0009
- [Chooser POC](https://chooser.fly.dev/?doi=10.1332/policypress/9781529215144.003.0009)
- [Production](https://doi.org/10.1332/policypress/9781529215144.003.0009)
- [JSON](https://api.crossref.org/works/10.1332/policypress/9781529215144.003.0009)
- [XML](https://api.crossref.org/works/10.1332/policypress/9781529215144.003.0009.xml)
<hr>

### 10.14361/transcript.9783839426838.115
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839426838.115)
- [Production](https://doi.org/10.14361/transcript.9783839426838.115)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839426838.115)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839426838.115.xml)
<hr>

### 10.1332/policypress/9781447332022.003.0014
- [Chooser POC](https://chooser.fly.dev/?doi=10.1332/policypress/9781447332022.003.0014)
- [Production](https://doi.org/10.1332/policypress/9781447332022.003.0014)
- [JSON](https://api.crossref.org/works/10.1332/policypress/9781447332022.003.0014)
- [XML](https://api.crossref.org/works/10.1332/policypress/9781447332022.003.0014.xml)
<hr>

### 10.5622/illinois/9780252041976.003.0003
- [Chooser POC](https://chooser.fly.dev/?doi=10.5622/illinois/9780252041976.003.0003)
- [Production](https://doi.org/10.5622/illinois/9780252041976.003.0003)
- [JSON](https://api.crossref.org/works/10.5622/illinois/9780252041976.003.0003)
- [XML](https://api.crossref.org/works/10.5622/illinois/9780252041976.003.0003.xml)
<hr>

### 10.14361/9783839427644-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839427644-007)
- [Production](https://doi.org/10.14361/9783839427644-007)
- [JSON](https://api.crossref.org/works/10.14361/9783839427644-007)
- [XML](https://api.crossref.org/works/10.14361/9783839427644-007.xml)
<hr>

### 10.1215/9780822395027-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822395027-007)
- [Production](https://doi.org/10.1215/9780822395027-007)
- [JSON](https://api.crossref.org/works/10.1215/9780822395027-007)
- [XML](https://api.crossref.org/works/10.1215/9780822395027-007.xml)
<hr>

### 10.14361/9783839442524-025
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839442524-025)
- [Production](https://doi.org/10.14361/9783839442524-025)
- [JSON](https://api.crossref.org/works/10.14361/9783839442524-025)
- [XML](https://api.crossref.org/works/10.14361/9783839442524-025.xml)
<hr>

### 10.1215/9780822379430-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822379430-007)
- [Production](https://doi.org/10.1215/9780822379430-007)
- [JSON](https://api.crossref.org/works/10.1215/9780822379430-007)
- [XML](https://api.crossref.org/works/10.1215/9780822379430-007.xml)
<hr>

### 10.14361/9783839446263-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839446263-006)
- [Production](https://doi.org/10.14361/9783839446263-006)
- [JSON](https://api.crossref.org/works/10.14361/9783839446263-006)
- [XML](https://api.crossref.org/works/10.14361/9783839446263-006.xml)
<hr>

### 10.14361/9783839457269-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839457269-007)
- [Production](https://doi.org/10.14361/9783839457269-007)
- [JSON](https://api.crossref.org/works/10.14361/9783839457269-007)
- [XML](https://api.crossref.org/works/10.14361/9783839457269-007.xml)
<hr>

### 10.14361/9783839402740-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839402740-005)
- [Production](https://doi.org/10.14361/9783839402740-005)
- [JSON](https://api.crossref.org/works/10.14361/9783839402740-005)
- [XML](https://api.crossref.org/works/10.14361/9783839402740-005.xml)
<hr>

### 10.14361/transcript.9783839418390.129
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839418390.129)
- [Production](https://doi.org/10.14361/transcript.9783839418390.129)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839418390.129)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839418390.129.xml)
<hr>

### 10.1017/9789048541904.004
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9789048541904.004)
- [Production](https://doi.org/10.1017/9789048541904.004)
- [JSON](https://api.crossref.org/works/10.1017/9789048541904.004)
- [XML](https://api.crossref.org/works/10.1017/9789048541904.004.xml)
<hr>

### 10.14361/transcript.9783839426715.99
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839426715.99)
- [Production](https://doi.org/10.14361/transcript.9783839426715.99)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839426715.99)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839426715.99.xml)
<hr>

### 10.14361/transcript.9783839419632.335
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839419632.335)
- [Production](https://doi.org/10.14361/transcript.9783839419632.335)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839419632.335)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839419632.335.xml)
<hr>

### 10.14361/9783839436530
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839436530)
- [Production](https://doi.org/10.14361/9783839436530)
- [JSON](https://api.crossref.org/works/10.14361/9783839436530)
- [XML](https://api.crossref.org/works/10.14361/9783839436530.xml)
<hr>

### 10.14361/9783839409015-012
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839409015-012)
- [Production](https://doi.org/10.14361/9783839409015-012)
- [JSON](https://api.crossref.org/works/10.14361/9783839409015-012)
- [XML](https://api.crossref.org/works/10.14361/9783839409015-012.xml)
<hr>

### 10.14361/9783839449691-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839449691-009)
- [Production](https://doi.org/10.14361/9783839449691-009)
- [JSON](https://api.crossref.org/works/10.14361/9783839449691-009)
- [XML](https://api.crossref.org/works/10.14361/9783839449691-009.xml)
<hr>

### 10.1163/9781684172092_004
- [Chooser POC](https://chooser.fly.dev/?doi=10.1163/9781684172092_004)
- [Production](https://doi.org/10.1163/9781684172092_004)
- [JSON](https://api.crossref.org/works/10.1163/9781684172092_004)
- [XML](https://api.crossref.org/works/10.1163/9781684172092_004.xml)
<hr>

### 10.1017/9781787444393.015
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9781787444393.015)
- [Production](https://doi.org/10.1017/9781787444393.015)
- [JSON](https://api.crossref.org/works/10.1017/9781787444393.015)
- [XML](https://api.crossref.org/works/10.1017/9781787444393.015.xml)
<hr>

### 10.1515/9781646021512-028
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781646021512-028)
- [Production](https://doi.org/10.1515/9781646021512-028)
- [JSON](https://api.crossref.org/works/10.1515/9781646021512-028)
- [XML](https://api.crossref.org/works/10.1515/9781646021512-028.xml)
<hr>

### 10.1201/9781003070276-14
- [Chooser POC](https://chooser.fly.dev/?doi=10.1201/9781003070276-14)
- [Production](https://doi.org/10.1201/9781003070276-14)
- [JSON](https://api.crossref.org/works/10.1201/9781003070276-14)
- [XML](https://api.crossref.org/works/10.1201/9781003070276-14.xml)
<hr>

### 10.1215/9780822396567-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822396567-002)
- [Production](https://doi.org/10.1215/9780822396567-002)
- [JSON](https://api.crossref.org/works/10.1215/9780822396567-002)
- [XML](https://api.crossref.org/works/10.1215/9780822396567-002.xml)
<hr>

### 10.1215/9781478021308-012
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9781478021308-012)
- [Production](https://doi.org/10.1215/9781478021308-012)
- [JSON](https://api.crossref.org/works/10.1215/9781478021308-012)
- [XML](https://api.crossref.org/works/10.1215/9781478021308-012.xml)
<hr>

### 10.5117/9789463722322
- [Chooser POC](https://chooser.fly.dev/?doi=10.5117/9789463722322)
- [Production](https://doi.org/10.5117/9789463722322)
- [JSON](https://api.crossref.org/works/10.5117/9789463722322)
- [XML](https://api.crossref.org/works/10.5117/9789463722322.xml)
<hr>

### 10.1515/9781575068671-024
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781575068671-024)
- [Production](https://doi.org/10.1515/9781575068671-024)
- [JSON](https://api.crossref.org/works/10.1515/9781575068671-024)
- [XML](https://api.crossref.org/works/10.1515/9781575068671-024.xml)
<hr>

### 10.14361/9783839410592-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839410592-009)
- [Production](https://doi.org/10.14361/9783839410592-009)
- [JSON](https://api.crossref.org/works/10.14361/9783839410592-009)
- [XML](https://api.crossref.org/works/10.14361/9783839410592-009.xml)
<hr>

### 10.1332/policypress/9781529215144.003.0007
- [Chooser POC](https://chooser.fly.dev/?doi=10.1332/policypress/9781529215144.003.0007)
- [Production](https://doi.org/10.1332/policypress/9781529215144.003.0007)
- [JSON](https://api.crossref.org/works/10.1332/policypress/9781529215144.003.0007)
- [XML](https://api.crossref.org/works/10.1332/policypress/9781529215144.003.0007.xml)
<hr>

### 10.14361/9783839430842-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839430842-005)
- [Production](https://doi.org/10.14361/9783839430842-005)
- [JSON](https://api.crossref.org/works/10.14361/9783839430842-005)
- [XML](https://api.crossref.org/works/10.14361/9783839430842-005.xml)
<hr>

### 10.14361/9783839454152-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839454152-001)
- [Production](https://doi.org/10.14361/9783839454152-001)
- [JSON](https://api.crossref.org/works/10.14361/9783839454152-001)
- [XML](https://api.crossref.org/works/10.14361/9783839454152-001.xml)
<hr>

### 10.14361/9783839451847-011
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839451847-011)
- [Production](https://doi.org/10.14361/9783839451847-011)
- [JSON](https://api.crossref.org/works/10.14361/9783839451847-011)
- [XML](https://api.crossref.org/works/10.14361/9783839451847-011.xml)
<hr>

### 10.1215/9780822393924-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822393924-004)
- [Production](https://doi.org/10.1215/9780822393924-004)
- [JSON](https://api.crossref.org/works/10.1215/9780822393924-004)
- [XML](https://api.crossref.org/works/10.1215/9780822393924-004.xml)
<hr>

### 10.1215/9781478021308-023
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9781478021308-023)
- [Production](https://doi.org/10.1215/9781478021308-023)
- [JSON](https://api.crossref.org/works/10.1215/9781478021308-023)
- [XML](https://api.crossref.org/works/10.1215/9781478021308-023.xml)
<hr>

### 10.1515/9781400868285-034
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781400868285-034)
- [Production](https://doi.org/10.1515/9781400868285-034)
- [JSON](https://api.crossref.org/works/10.1515/9781400868285-034)
- [XML](https://api.crossref.org/works/10.1515/9781400868285-034.xml)
<hr>

### 10.14361/9783839434994-017
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839434994-017)
- [Production](https://doi.org/10.14361/9783839434994-017)
- [JSON](https://api.crossref.org/works/10.14361/9783839434994-017)
- [XML](https://api.crossref.org/works/10.14361/9783839434994-017.xml)
<hr>

### 10.14361/transcript.9783839402955.43
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839402955.43)
- [Production](https://doi.org/10.14361/transcript.9783839402955.43)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839402955.43)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839402955.43.xml)
<hr>

### 10.14361/9783839406489-015
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839406489-015)
- [Production](https://doi.org/10.14361/9783839406489-015)
- [JSON](https://api.crossref.org/works/10.14361/9783839406489-015)
- [XML](https://api.crossref.org/works/10.14361/9783839406489-015.xml)
<hr>

### 10.3828/liverpool/9781911325406.003.0004
- [Chooser POC](https://chooser.fly.dev/?doi=10.3828/liverpool/9781911325406.003.0004)
- [Production](https://doi.org/10.3828/liverpool/9781911325406.003.0004)
- [JSON](https://api.crossref.org/works/10.3828/liverpool/9781911325406.003.0004)
- [XML](https://api.crossref.org/works/10.3828/liverpool/9781911325406.003.0004.xml)
<hr>

### 10.1515/9783839460917
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9783839460917)
- [Production](https://doi.org/10.1515/9783839460917)
- [JSON](https://api.crossref.org/works/10.1515/9783839460917)
- [XML](https://api.crossref.org/works/10.1515/9783839460917.xml)
<hr>

### 10.14361/9783839432594-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839432594-008)
- [Production](https://doi.org/10.14361/9783839432594-008)
- [JSON](https://api.crossref.org/works/10.14361/9783839432594-008)
- [XML](https://api.crossref.org/works/10.14361/9783839432594-008.xml)
<hr>

### 10.5949/liverpool/9781846311093.003.0009
- [Chooser POC](https://chooser.fly.dev/?doi=10.5949/liverpool/9781846311093.003.0009)
- [Production](https://doi.org/10.5949/liverpool/9781846311093.003.0009)
- [JSON](https://api.crossref.org/works/10.5949/liverpool/9781846311093.003.0009)
- [XML](https://api.crossref.org/works/10.5949/liverpool/9781846311093.003.0009.xml)
<hr>

### 10.14361/9783839408971-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839408971-006)
- [Production](https://doi.org/10.14361/9783839408971-006)
- [JSON](https://api.crossref.org/works/10.14361/9783839408971-006)
- [XML](https://api.crossref.org/works/10.14361/9783839408971-006.xml)
<hr>

### 10.14361/9783839447024-011
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839447024-011)
- [Production](https://doi.org/10.14361/9783839447024-011)
- [JSON](https://api.crossref.org/works/10.14361/9783839447024-011)
- [XML](https://api.crossref.org/works/10.14361/9783839447024-011.xml)
<hr>

### 10.14361/transcript.9783839423707.39
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839423707.39)
- [Production](https://doi.org/10.14361/transcript.9783839423707.39)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839423707.39)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839423707.39.xml)
<hr>

### 10.46692/9781447360391.006
- [Chooser POC](https://chooser.fly.dev/?doi=10.46692/9781447360391.006)
- [Production](https://doi.org/10.46692/9781447360391.006)
- [JSON](https://api.crossref.org/works/10.46692/9781447360391.006)
- [XML](https://api.crossref.org/works/10.46692/9781447360391.006.xml)
<hr>

### 10.23943/princeton/9780691197876.003.0023
- [Chooser POC](https://chooser.fly.dev/?doi=10.23943/princeton/9780691197876.003.0023)
- [Production](https://doi.org/10.23943/princeton/9780691197876.003.0023)
- [JSON](https://api.crossref.org/works/10.23943/princeton/9780691197876.003.0023)
- [XML](https://api.crossref.org/works/10.23943/princeton/9780691197876.003.0023.xml)
<hr>

### 10.4324/9781003047780-9
- [Chooser POC](https://chooser.fly.dev/?doi=10.4324/9781003047780-9)
- [Production](https://doi.org/10.4324/9781003047780-9)
- [JSON](https://api.crossref.org/works/10.4324/9781003047780-9)
- [XML](https://api.crossref.org/works/10.4324/9781003047780-9.xml)
<hr>

### 10.14361/9783839435410-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839435410-004)
- [Production](https://doi.org/10.14361/9783839435410-004)
- [JSON](https://api.crossref.org/works/10.14361/9783839435410-004)
- [XML](https://api.crossref.org/works/10.14361/9783839435410-004.xml)
<hr>

### 10.1215/9780822397496-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822397496-001)
- [Production](https://doi.org/10.1215/9780822397496-001)
- [JSON](https://api.crossref.org/works/10.1215/9780822397496-001)
- [XML](https://api.crossref.org/works/10.1215/9780822397496-001.xml)
<hr>

### 10.2307/j.ctvd1c9d4.7
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctvd1c9d4.7)
- [Production](https://doi.org/10.2307/j.ctvd1c9d4.7)
- [JSON](https://api.crossref.org/works/10.2307/j.ctvd1c9d4.7)
- [XML](https://api.crossref.org/works/10.2307/j.ctvd1c9d4.7.xml)
<hr>

### 10.1515/9781575065403-020
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781575065403-020)
- [Production](https://doi.org/10.1515/9781575065403-020)
- [JSON](https://api.crossref.org/works/10.1515/9781575065403-020)
- [XML](https://api.crossref.org/works/10.1515/9781575065403-020.xml)
<hr>

### 10.14361/9783839454077-011
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839454077-011)
- [Production](https://doi.org/10.14361/9783839454077-011)
- [JSON](https://api.crossref.org/works/10.14361/9783839454077-011)
- [XML](https://api.crossref.org/works/10.14361/9783839454077-011.xml)
<hr>

### 10.14361/9783839411773-010
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839411773-010)
- [Production](https://doi.org/10.14361/9783839411773-010)
- [JSON](https://api.crossref.org/works/10.14361/9783839411773-010)
- [XML](https://api.crossref.org/works/10.14361/9783839411773-010.xml)
<hr>

### 10.1525/9780520975057
- [Chooser POC](https://chooser.fly.dev/?doi=10.1525/9780520975057)
- [Production](https://doi.org/10.1525/9780520975057)
- [JSON](https://api.crossref.org/works/10.1525/9780520975057)
- [XML](https://api.crossref.org/works/10.1525/9780520975057.xml)
<hr>

### 10.14361/9783839412510-013
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839412510-013)
- [Production](https://doi.org/10.14361/9783839412510-013)
- [JSON](https://api.crossref.org/works/10.14361/9783839412510-013)
- [XML](https://api.crossref.org/works/10.14361/9783839412510-013.xml)
<hr>

### 10.14361/9783839413432-016
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839413432-016)
- [Production](https://doi.org/10.14361/9783839413432-016)
- [JSON](https://api.crossref.org/works/10.14361/9783839413432-016)
- [XML](https://api.crossref.org/works/10.14361/9783839413432-016.xml)
<hr>

### 10.14361/9783839408247-010
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839408247-010)
- [Production](https://doi.org/10.14361/9783839408247-010)
- [JSON](https://api.crossref.org/works/10.14361/9783839408247-010)
- [XML](https://api.crossref.org/works/10.14361/9783839408247-010.xml)
<hr>

### 10.14361/9783839451717-017
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839451717-017)
- [Production](https://doi.org/10.14361/9783839451717-017)
- [JSON](https://api.crossref.org/works/10.14361/9783839451717-017)
- [XML](https://api.crossref.org/works/10.14361/9783839451717-017.xml)
<hr>

### 10.14361/9783839441008-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839441008-005)
- [Production](https://doi.org/10.14361/9783839441008-005)
- [JSON](https://api.crossref.org/works/10.14361/9783839441008-005)
- [XML](https://api.crossref.org/works/10.14361/9783839441008-005.xml)
<hr>

### 10.1525/9780520971141-016
- [Chooser POC](https://chooser.fly.dev/?doi=10.1525/9780520971141-016)
- [Production](https://doi.org/10.1525/9780520971141-016)
- [JSON](https://api.crossref.org/works/10.1525/9780520971141-016)
- [XML](https://api.crossref.org/works/10.1525/9780520971141-016.xml)
<hr>

### 10.14361/transcript.9783839425442.45
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839425442.45)
- [Production](https://doi.org/10.14361/transcript.9783839425442.45)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839425442.45)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839425442.45.xml)
<hr>

### 10.14361/transcript.9783839419069.137
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839419069.137)
- [Production](https://doi.org/10.14361/transcript.9783839419069.137)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839419069.137)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839419069.137.xml)
<hr>

### 10.14361/9783839441190-015
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839441190-015)
- [Production](https://doi.org/10.14361/9783839441190-015)
- [JSON](https://api.crossref.org/works/10.14361/9783839441190-015)
- [XML](https://api.crossref.org/works/10.14361/9783839441190-015.xml)
<hr>

### 10.1515/9781400846672-018
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781400846672-018)
- [Production](https://doi.org/10.1515/9781400846672-018)
- [JSON](https://api.crossref.org/works/10.1515/9781400846672-018)
- [XML](https://api.crossref.org/works/10.1515/9781400846672-018.xml)
<hr>

### 10.1215/9780822371793-131
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822371793-131)
- [Production](https://doi.org/10.1215/9780822371793-131)
- [JSON](https://api.crossref.org/works/10.1215/9780822371793-131)
- [XML](https://api.crossref.org/works/10.1215/9780822371793-131.xml)
<hr>

### 10.23943/princeton/9780691159263.003.0007
- [Chooser POC](https://chooser.fly.dev/?doi=10.23943/princeton/9780691159263.003.0007)
- [Production](https://doi.org/10.23943/princeton/9780691159263.003.0007)
- [JSON](https://api.crossref.org/works/10.23943/princeton/9780691159263.003.0007)
- [XML](https://api.crossref.org/works/10.23943/princeton/9780691159263.003.0007.xml)
<hr>

### 10.46692/9781529211887.011
- [Chooser POC](https://chooser.fly.dev/?doi=10.46692/9781529211887.011)
- [Production](https://doi.org/10.46692/9781529211887.011)
- [JSON](https://api.crossref.org/works/10.46692/9781529211887.011)
- [XML](https://api.crossref.org/works/10.46692/9781529211887.011.xml)
<hr>

### 10.14361/transcript.9783839419540.303
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839419540.303)
- [Production](https://doi.org/10.14361/transcript.9783839419540.303)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839419540.303)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839419540.303.xml)
<hr>

### 10.14361/9783839432334-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839432334-006)
- [Production](https://doi.org/10.14361/9783839432334-006)
- [JSON](https://api.crossref.org/works/10.14361/9783839432334-006)
- [XML](https://api.crossref.org/works/10.14361/9783839432334-006.xml)
<hr>

### 10.14361/transcript.9783839413777.317
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839413777.317)
- [Production](https://doi.org/10.14361/transcript.9783839413777.317)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839413777.317)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839413777.317.xml)
<hr>

### 10.14361/9783839409138-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839409138-006)
- [Production](https://doi.org/10.14361/9783839409138-006)
- [JSON](https://api.crossref.org/works/10.14361/9783839409138-006)
- [XML](https://api.crossref.org/works/10.14361/9783839409138-006.xml)
<hr>

### 10.2307/j.ctvdtpgtm
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctvdtpgtm)
- [Production](https://doi.org/10.2307/j.ctvdtpgtm)
- [JSON](https://api.crossref.org/works/10.2307/j.ctvdtpgtm)
- [XML](https://api.crossref.org/works/10.2307/j.ctvdtpgtm.xml)
<hr>

### 10.14361/9783839436622-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839436622-002)
- [Production](https://doi.org/10.14361/9783839436622-002)
- [JSON](https://api.crossref.org/works/10.14361/9783839436622-002)
- [XML](https://api.crossref.org/works/10.14361/9783839436622-002.xml)
<hr>

### 10.2307/j.ctt1s17p06.51
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctt1s17p06.51)
- [Production](https://doi.org/10.2307/j.ctt1s17p06.51)
- [JSON](https://api.crossref.org/works/10.2307/j.ctt1s17p06.51)
- [XML](https://api.crossref.org/works/10.2307/j.ctt1s17p06.51.xml)
<hr>

### 10.11126/stanford/9780804776868.003.0006
- [Chooser POC](https://chooser.fly.dev/?doi=10.11126/stanford/9780804776868.003.0006)
- [Production](https://doi.org/10.11126/stanford/9780804776868.003.0006)
- [JSON](https://api.crossref.org/works/10.11126/stanford/9780804776868.003.0006)
- [XML](https://api.crossref.org/works/10.11126/stanford/9780804776868.003.0006.xml)
<hr>

### 10.1201/9781003071075-1
- [Chooser POC](https://chooser.fly.dev/?doi=10.1201/9781003071075-1)
- [Production](https://doi.org/10.1201/9781003071075-1)
- [JSON](https://api.crossref.org/works/10.1201/9781003071075-1)
- [XML](https://api.crossref.org/works/10.1201/9781003071075-1.xml)
<hr>

### 10.14361/9783839434369-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839434369-004)
- [Production](https://doi.org/10.14361/9783839434369-004)
- [JSON](https://api.crossref.org/works/10.14361/9783839434369-004)
- [XML](https://api.crossref.org/works/10.14361/9783839434369-004.xml)
<hr>

### 10.14361/9783839445761-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839445761-009)
- [Production](https://doi.org/10.14361/9783839445761-009)
- [JSON](https://api.crossref.org/works/10.14361/9783839445761-009)
- [XML](https://api.crossref.org/works/10.14361/9783839445761-009.xml)
<hr>

### 10.14361/9783839406618-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839406618-009)
- [Production](https://doi.org/10.14361/9783839406618-009)
- [JSON](https://api.crossref.org/works/10.14361/9783839406618-009)
- [XML](https://api.crossref.org/works/10.14361/9783839406618-009.xml)
<hr>

### 10.1215/9780822386926-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822386926-008)
- [Production](https://doi.org/10.1215/9780822386926-008)
- [JSON](https://api.crossref.org/works/10.1215/9780822386926-008)
- [XML](https://api.crossref.org/works/10.1215/9780822386926-008.xml)
<hr>

### 10.14361/9783839456149-012
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839456149-012)
- [Production](https://doi.org/10.14361/9783839456149-012)
- [JSON](https://api.crossref.org/works/10.14361/9783839456149-012)
- [XML](https://api.crossref.org/works/10.14361/9783839456149-012.xml)
<hr>

### 10.14361/9783839451298-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839451298-007)
- [Production](https://doi.org/10.14361/9783839451298-007)
- [JSON](https://api.crossref.org/works/10.14361/9783839451298-007)
- [XML](https://api.crossref.org/works/10.14361/9783839451298-007.xml)
<hr>

### 10.14361/9783839439395-010
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839439395-010)
- [Production](https://doi.org/10.14361/9783839439395-010)
- [JSON](https://api.crossref.org/works/10.14361/9783839439395-010)
- [XML](https://api.crossref.org/works/10.14361/9783839439395-010.xml)
<hr>

### 10.14361/9783839451748-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839451748-003)
- [Production](https://doi.org/10.14361/9783839451748-003)
- [JSON](https://api.crossref.org/works/10.14361/9783839451748-003)
- [XML](https://api.crossref.org/works/10.14361/9783839451748-003.xml)
<hr>

### 10.14361/9783839408339-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839408339-004)
- [Production](https://doi.org/10.14361/9783839408339-004)
- [JSON](https://api.crossref.org/works/10.14361/9783839408339-004)
- [XML](https://api.crossref.org/works/10.14361/9783839408339-004.xml)
<hr>

### 10.46692/9781447345169.012
- [Chooser POC](https://chooser.fly.dev/?doi=10.46692/9781447345169.012)
- [Production](https://doi.org/10.46692/9781447345169.012)
- [JSON](https://api.crossref.org/works/10.46692/9781447345169.012)
- [XML](https://api.crossref.org/works/10.46692/9781447345169.012.xml)
<hr>

### 10.14361/9783839413791-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839413791-008)
- [Production](https://doi.org/10.14361/9783839413791-008)
- [JSON](https://api.crossref.org/works/10.14361/9783839413791-008)
- [XML](https://api.crossref.org/works/10.14361/9783839413791-008.xml)
<hr>

### 10.1515/9781400833702-029
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781400833702-029)
- [Production](https://doi.org/10.1515/9781400833702-029)
- [JSON](https://api.crossref.org/works/10.1515/9781400833702-029)
- [XML](https://api.crossref.org/works/10.1515/9781400833702-029.xml)
<hr>

### 10.1515/9781400823499-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781400823499-007)
- [Production](https://doi.org/10.1515/9781400823499-007)
- [JSON](https://api.crossref.org/works/10.1515/9781400823499-007)
- [XML](https://api.crossref.org/works/10.1515/9781400823499-007.xml)
<hr>

### 10.1017/9781787446953.006
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9781787446953.006)
- [Production](https://doi.org/10.1017/9781787446953.006)
- [JSON](https://api.crossref.org/works/10.1017/9781787446953.006)
- [XML](https://api.crossref.org/works/10.1017/9781787446953.006.xml)
<hr>

### 10.1201/9781003071914-5
- [Chooser POC](https://chooser.fly.dev/?doi=10.1201/9781003071914-5)
- [Production](https://doi.org/10.1201/9781003071914-5)
- [JSON](https://api.crossref.org/works/10.1201/9781003071914-5)
- [XML](https://api.crossref.org/works/10.1201/9781003071914-5.xml)
<hr>

### 10.14361/transcript.9783839423523
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839423523)
- [Production](https://doi.org/10.14361/transcript.9783839423523)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839423523)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839423523.xml)
<hr>

### 10.1515/9783839449684
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9783839449684)
- [Production](https://doi.org/10.1515/9783839449684)
- [JSON](https://api.crossref.org/works/10.1515/9783839449684)
- [XML](https://api.crossref.org/works/10.1515/9783839449684.xml)
<hr>

### 10.14361/9783839404539-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839404539-005)
- [Production](https://doi.org/10.14361/9783839404539-005)
- [JSON](https://api.crossref.org/works/10.14361/9783839404539-005)
- [XML](https://api.crossref.org/works/10.14361/9783839404539-005.xml)
<hr>

### 10.14361/9783839402931-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839402931-002)
- [Production](https://doi.org/10.14361/9783839402931-002)
- [JSON](https://api.crossref.org/works/10.14361/9783839402931-002)
- [XML](https://api.crossref.org/works/10.14361/9783839402931-002.xml)
<hr>

### 10.14361/9783839438749-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839438749-003)
- [Production](https://doi.org/10.14361/9783839438749-003)
- [JSON](https://api.crossref.org/works/10.14361/9783839438749-003)
- [XML](https://api.crossref.org/works/10.14361/9783839438749-003.xml)
<hr>

### 10.14361/9783839442609
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839442609)
- [Production](https://doi.org/10.14361/9783839442609)
- [JSON](https://api.crossref.org/works/10.14361/9783839442609)
- [XML](https://api.crossref.org/works/10.14361/9783839442609.xml)
<hr>

### 10.14361/9783839403341-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839403341-007)
- [Production](https://doi.org/10.14361/9783839403341-007)
- [JSON](https://api.crossref.org/works/10.14361/9783839403341-007)
- [XML](https://api.crossref.org/works/10.14361/9783839403341-007.xml)
<hr>

### 10.2307/j.ctvnwbz7q.6
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctvnwbz7q.6)
- [Production](https://doi.org/10.2307/j.ctvnwbz7q.6)
- [JSON](https://api.crossref.org/works/10.2307/j.ctvnwbz7q.6)
- [XML](https://api.crossref.org/works/10.2307/j.ctvnwbz7q.6.xml)
<hr>

### 10.1017/9781800101074.012
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9781800101074.012)
- [Production](https://doi.org/10.1017/9781800101074.012)
- [JSON](https://api.crossref.org/works/10.1017/9781800101074.012)
- [XML](https://api.crossref.org/works/10.1017/9781800101074.012.xml)
<hr>

### 10.1163/9781684171989
- [Chooser POC](https://chooser.fly.dev/?doi=10.1163/9781684171989)
- [Production](https://doi.org/10.1163/9781684171989)
- [JSON](https://api.crossref.org/works/10.1163/9781684171989)
- [XML](https://api.crossref.org/works/10.1163/9781684171989.xml)
<hr>

### 10.1515/9781575064147-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781575064147-009)
- [Production](https://doi.org/10.1515/9781575064147-009)
- [JSON](https://api.crossref.org/works/10.1515/9781575064147-009)
- [XML](https://api.crossref.org/works/10.1515/9781575064147-009.xml)
<hr>

### 10.1017/9781942401902.005
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9781942401902.005)
- [Production](https://doi.org/10.1017/9781942401902.005)
- [JSON](https://api.crossref.org/works/10.1017/9781942401902.005)
- [XML](https://api.crossref.org/works/10.1017/9781942401902.005.xml)
<hr>

### 10.14361/9783839437995-013
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839437995-013)
- [Production](https://doi.org/10.14361/9783839437995-013)
- [JSON](https://api.crossref.org/works/10.14361/9783839437995-013)
- [XML](https://api.crossref.org/works/10.14361/9783839437995-013.xml)
<hr>

### 10.1215/9780822388579-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822388579-007)
- [Production](https://doi.org/10.1215/9780822388579-007)
- [JSON](https://api.crossref.org/works/10.1215/9780822388579-007)
- [XML](https://api.crossref.org/works/10.1215/9780822388579-007.xml)
<hr>

### 10.14361/9783839405024-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839405024-008)
- [Production](https://doi.org/10.14361/9783839405024-008)
- [JSON](https://api.crossref.org/works/10.14361/9783839405024-008)
- [XML](https://api.crossref.org/works/10.14361/9783839405024-008.xml)
<hr>

### 10.14361/9783839430156-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839430156-004)
- [Production](https://doi.org/10.14361/9783839430156-004)
- [JSON](https://api.crossref.org/works/10.14361/9783839430156-004)
- [XML](https://api.crossref.org/works/10.14361/9783839430156-004.xml)
<hr>

### 10.2307/j.ctv1v0907q.9
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctv1v0907q.9)
- [Production](https://doi.org/10.2307/j.ctv1v0907q.9)
- [JSON](https://api.crossref.org/works/10.2307/j.ctv1v0907q.9)
- [XML](https://api.crossref.org/works/10.2307/j.ctv1v0907q.9.xml)
<hr>

### 10.14361/9783839460375-011
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839460375-011)
- [Production](https://doi.org/10.14361/9783839460375-011)
- [JSON](https://api.crossref.org/works/10.14361/9783839460375-011)
- [XML](https://api.crossref.org/works/10.14361/9783839460375-011.xml)
<hr>

### 10.2307/j.ctv69tgs1.4
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctv69tgs1.4)
- [Production](https://doi.org/10.2307/j.ctv69tgs1.4)
- [JSON](https://api.crossref.org/works/10.2307/j.ctv69tgs1.4)
- [XML](https://api.crossref.org/works/10.2307/j.ctv69tgs1.4.xml)
<hr>

### 10.1215/9780822377139-053
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822377139-053)
- [Production](https://doi.org/10.1215/9780822377139-053)
- [JSON](https://api.crossref.org/works/10.1215/9780822377139-053)
- [XML](https://api.crossref.org/works/10.1215/9780822377139-053.xml)
<hr>

### 10.14361/9783839439128-011
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839439128-011)
- [Production](https://doi.org/10.14361/9783839439128-011)
- [JSON](https://api.crossref.org/works/10.14361/9783839439128-011)
- [XML](https://api.crossref.org/works/10.14361/9783839439128-011.xml)
<hr>

### 10.1017/9789048511211.010
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9789048511211.010)
- [Production](https://doi.org/10.1017/9789048511211.010)
- [JSON](https://api.crossref.org/works/10.1017/9789048511211.010)
- [XML](https://api.crossref.org/works/10.1017/9789048511211.010.xml)
<hr>

### 10.14361/transcript.9783839423738.137
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839423738.137)
- [Production](https://doi.org/10.14361/transcript.9783839423738.137)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839423738.137)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839423738.137.xml)
<hr>

### 10.14361/9783839430798-021
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839430798-021)
- [Production](https://doi.org/10.14361/9783839430798-021)
- [JSON](https://api.crossref.org/works/10.14361/9783839430798-021)
- [XML](https://api.crossref.org/works/10.14361/9783839430798-021.xml)
<hr>

### 10.14361/transcript.9783839422663.173
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839422663.173)
- [Production](https://doi.org/10.14361/transcript.9783839422663.173)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839422663.173)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839422663.173.xml)
<hr>

### 10.14361/transcript.9783839425695.395
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839425695.395)
- [Production](https://doi.org/10.14361/transcript.9783839425695.395)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839425695.395)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839425695.395.xml)
<hr>

### 10.11126/stanford/9780804774468.003.0004
- [Chooser POC](https://chooser.fly.dev/?doi=10.11126/stanford/9780804774468.003.0004)
- [Production](https://doi.org/10.11126/stanford/9780804774468.003.0004)
- [JSON](https://api.crossref.org/works/10.11126/stanford/9780804774468.003.0004)
- [XML](https://api.crossref.org/works/10.11126/stanford/9780804774468.003.0004.xml)
<hr>

### 10.14361/9783839450932-025
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839450932-025)
- [Production](https://doi.org/10.14361/9783839450932-025)
- [JSON](https://api.crossref.org/works/10.14361/9783839450932-025)
- [XML](https://api.crossref.org/works/10.14361/9783839450932-025.xml)
<hr>

### 10.1515/9781400836079-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9781400836079-001)
- [Production](https://doi.org/10.1515/9781400836079-001)
- [JSON](https://api.crossref.org/works/10.1515/9781400836079-001)
- [XML](https://api.crossref.org/works/10.1515/9781400836079-001.xml)
<hr>

### 10.3828/liverpool/9781949979237.003.0005
- [Chooser POC](https://chooser.fly.dev/?doi=10.3828/liverpool/9781949979237.003.0005)
- [Production](https://doi.org/10.3828/liverpool/9781949979237.003.0005)
- [JSON](https://api.crossref.org/works/10.3828/liverpool/9781949979237.003.0005)
- [XML](https://api.crossref.org/works/10.3828/liverpool/9781949979237.003.0005.xml)
<hr>

### 10.1215/9781478010302-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9781478010302-006)
- [Production](https://doi.org/10.1215/9781478010302-006)
- [JSON](https://api.crossref.org/works/10.1215/9781478010302-006)
- [XML](https://api.crossref.org/works/10.1215/9781478010302-006.xml)
<hr>

### 10.5117/9789463728607_ch18
- [Chooser POC](https://chooser.fly.dev/?doi=10.5117/9789463728607_ch18)
- [Production](https://doi.org/10.5117/9789463728607_ch18)
- [JSON](https://api.crossref.org/works/10.5117/9789463728607_ch18)
- [XML](https://api.crossref.org/works/10.5117/9789463728607_ch18.xml)
<hr>

### 10.14324/111.9781787355514
- [Chooser POC](https://chooser.fly.dev/?doi=10.14324/111.9781787355514)
- [Production](https://doi.org/10.14324/111.9781787355514)
- [JSON](https://api.crossref.org/works/10.14324/111.9781787355514)
- [XML](https://api.crossref.org/works/10.14324/111.9781787355514.xml)
<hr>

### 10.1017/9789048521142.030
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9789048521142.030)
- [Production](https://doi.org/10.1017/9789048521142.030)
- [JSON](https://api.crossref.org/works/10.1017/9789048521142.030)
- [XML](https://api.crossref.org/works/10.1017/9789048521142.030.xml)
<hr>

### 10.14361/9783839445198-043
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839445198-043)
- [Production](https://doi.org/10.14361/9783839445198-043)
- [JSON](https://api.crossref.org/works/10.14361/9783839445198-043)
- [XML](https://api.crossref.org/works/10.14361/9783839445198-043.xml)
<hr>

### 10.1017/9789048542079.006
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9789048542079.006)
- [Production](https://doi.org/10.1017/9789048542079.006)
- [JSON](https://api.crossref.org/works/10.1017/9789048542079.006)
- [XML](https://api.crossref.org/works/10.1017/9789048542079.006.xml)
<hr>

### 10.1215/9780822386681-012
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822386681-012)
- [Production](https://doi.org/10.1215/9780822386681-012)
- [JSON](https://api.crossref.org/works/10.1215/9780822386681-012)
- [XML](https://api.crossref.org/works/10.1215/9780822386681-012.xml)
<hr>

### 10.14361/transcript.9783839425459.41
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839425459.41)
- [Production](https://doi.org/10.14361/transcript.9783839425459.41)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839425459.41)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839425459.41.xml)
<hr>

### 10.14361/9783839438541-011
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839438541-011)
- [Production](https://doi.org/10.14361/9783839438541-011)
- [JSON](https://api.crossref.org/works/10.14361/9783839438541-011)
- [XML](https://api.crossref.org/works/10.14361/9783839438541-011.xml)
<hr>

### 10.14361/transcript.9783839420843.201
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839420843.201)
- [Production](https://doi.org/10.14361/transcript.9783839420843.201)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839420843.201)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839420843.201.xml)
<hr>

### 10.14361/9783839406137
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839406137)
- [Production](https://doi.org/10.14361/9783839406137)
- [JSON](https://api.crossref.org/works/10.14361/9783839406137)
- [XML](https://api.crossref.org/works/10.14361/9783839406137.xml)
<hr>

### 10.1215/9780822387619-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822387619-007)
- [Production](https://doi.org/10.1215/9780822387619-007)
- [JSON](https://api.crossref.org/works/10.1215/9780822387619-007)
- [XML](https://api.crossref.org/works/10.1215/9780822387619-007.xml)
<hr>

### 10.1017/9781787448759.015
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9781787448759.015)
- [Production](https://doi.org/10.1017/9781787448759.015)
- [JSON](https://api.crossref.org/works/10.1017/9781787448759.015)
- [XML](https://api.crossref.org/works/10.1017/9781787448759.015.xml)
<hr>

### 10.14361/9783839407196-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839407196-001)
- [Production](https://doi.org/10.14361/9783839407196-001)
- [JSON](https://api.crossref.org/works/10.14361/9783839407196-001)
- [XML](https://api.crossref.org/works/10.14361/9783839407196-001.xml)
<hr>

### 10.14361/9783839429594-015
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839429594-015)
- [Production](https://doi.org/10.14361/9783839429594-015)
- [JSON](https://api.crossref.org/works/10.14361/9783839429594-015)
- [XML](https://api.crossref.org/works/10.14361/9783839429594-015.xml)
<hr>

### 10.14361/9783839410240
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839410240)
- [Production](https://doi.org/10.14361/9783839410240)
- [JSON](https://api.crossref.org/works/10.14361/9783839410240)
- [XML](https://api.crossref.org/works/10.14361/9783839410240.xml)
<hr>

### 10.2307/j.ctv1zjg7bn.19
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/j.ctv1zjg7bn.19)
- [Production](https://doi.org/10.2307/j.ctv1zjg7bn.19)
- [JSON](https://api.crossref.org/works/10.2307/j.ctv1zjg7bn.19)
- [XML](https://api.crossref.org/works/10.2307/j.ctv1zjg7bn.19.xml)
<hr>

### 10.1215/9780822380788-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822380788-008)
- [Production](https://doi.org/10.1215/9780822380788-008)
- [JSON](https://api.crossref.org/works/10.1215/9780822380788-008)
- [XML](https://api.crossref.org/works/10.1215/9780822380788-008.xml)
<hr>

### 10.14361/9783839440544
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839440544)
- [Production](https://doi.org/10.14361/9783839440544)
- [JSON](https://api.crossref.org/works/10.14361/9783839440544)
- [XML](https://api.crossref.org/works/10.14361/9783839440544.xml)
<hr>

### 10.14361/9783839435922-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839435922-002)
- [Production](https://doi.org/10.14361/9783839435922-002)
- [JSON](https://api.crossref.org/works/10.14361/9783839435922-002)
- [XML](https://api.crossref.org/works/10.14361/9783839435922-002.xml)
<hr>

### 10.3366/edinburgh/9780748647477.003.0005
- [Chooser POC](https://chooser.fly.dev/?doi=10.3366/edinburgh/9780748647477.003.0005)
- [Production](https://doi.org/10.3366/edinburgh/9780748647477.003.0005)
- [JSON](https://api.crossref.org/works/10.3366/edinburgh/9780748647477.003.0005)
- [XML](https://api.crossref.org/works/10.3366/edinburgh/9780748647477.003.0005.xml)
<hr>

### 10.1215/9780822387527-012
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9780822387527-012)
- [Production](https://doi.org/10.1215/9780822387527-012)
- [JSON](https://api.crossref.org/works/10.1215/9780822387527-012)
- [XML](https://api.crossref.org/works/10.1215/9780822387527-012.xml)
<hr>

### 10.3828/liverpool/9781911325932.003.0001
- [Chooser POC](https://chooser.fly.dev/?doi=10.3828/liverpool/9781911325932.003.0001)
- [Production](https://doi.org/10.3828/liverpool/9781911325932.003.0001)
- [JSON](https://api.crossref.org/works/10.3828/liverpool/9781911325932.003.0001)
- [XML](https://api.crossref.org/works/10.3828/liverpool/9781911325932.003.0001.xml)
<hr>

### 10.14361/transcript.9783839418345.35
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839418345.35)
- [Production](https://doi.org/10.14361/transcript.9783839418345.35)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839418345.35)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839418345.35.xml)
<hr>

### 10.23943/princeton/9780691202754.003.0013
- [Chooser POC](https://chooser.fly.dev/?doi=10.23943/princeton/9780691202754.003.0013)
- [Production](https://doi.org/10.23943/princeton/9780691202754.003.0013)
- [JSON](https://api.crossref.org/works/10.23943/princeton/9780691202754.003.0013)
- [XML](https://api.crossref.org/works/10.23943/princeton/9780691202754.003.0013.xml)
<hr>

### 10.1215/9781478005643-040
- [Chooser POC](https://chooser.fly.dev/?doi=10.1215/9781478005643-040)
- [Production](https://doi.org/10.1215/9781478005643-040)
- [JSON](https://api.crossref.org/works/10.1215/9781478005643-040)
- [XML](https://api.crossref.org/works/10.1215/9781478005643-040.xml)
<hr>

### 10.14361/9783839430460-013
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839430460-013)
- [Production](https://doi.org/10.14361/9783839430460-013)
- [JSON](https://api.crossref.org/works/10.14361/9783839430460-013)
- [XML](https://api.crossref.org/works/10.14361/9783839430460-013.xml)
<hr>

### 10.14361/transcript.9783839425725.153
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/transcript.9783839425725.153)
- [Production](https://doi.org/10.14361/transcript.9783839425725.153)
- [JSON](https://api.crossref.org/works/10.14361/transcript.9783839425725.153)
- [XML](https://api.crossref.org/works/10.14361/transcript.9783839425725.153.xml)
<hr>

### 10.1017/9781787448018.003
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9781787448018.003)
- [Production](https://doi.org/10.1017/9781787448018.003)
- [JSON](https://api.crossref.org/works/10.1017/9781787448018.003)
- [XML](https://api.crossref.org/works/10.1017/9781787448018.003.xml)
<hr>

### 10.1332/policypress/9781447351955.001.0001
- [Chooser POC](https://chooser.fly.dev/?doi=10.1332/policypress/9781447351955.001.0001)
- [Production](https://doi.org/10.1332/policypress/9781447351955.001.0001)
- [JSON](https://api.crossref.org/works/10.1332/policypress/9781447351955.001.0001)
- [XML](https://api.crossref.org/works/10.1332/policypress/9781447351955.001.0001.xml)
<hr>

### 10.1515/9780824877385-117
- [Chooser POC](https://chooser.fly.dev/?doi=10.1515/9780824877385-117)
- [Production](https://doi.org/10.1515/9780824877385-117)
- [JSON](https://api.crossref.org/works/10.1515/9780824877385-117)
- [XML](https://api.crossref.org/works/10.1515/9780824877385-117.xml)
<hr>

### 10.14361/9783839406915-033
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839406915-033)
- [Production](https://doi.org/10.14361/9783839406915-033)
- [JSON](https://api.crossref.org/works/10.14361/9783839406915-033)
- [XML](https://api.crossref.org/works/10.14361/9783839406915-033.xml)
<hr>

### 10.1017/9789048536238.004
- [Chooser POC](https://chooser.fly.dev/?doi=10.1017/9789048536238.004)
- [Production](https://doi.org/10.1017/9789048536238.004)
- [JSON](https://api.crossref.org/works/10.1017/9789048536238.004)
- [XML](https://api.crossref.org/works/10.1017/9789048536238.004.xml)
<hr>

### 10.14361/9783839410820-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839410820-008)
- [Production](https://doi.org/10.14361/9783839410820-008)
- [JSON](https://api.crossref.org/works/10.14361/9783839410820-008)
- [XML](https://api.crossref.org/works/10.14361/9783839410820-008.xml)
<hr>

### 10.1525/luminos.91.a
- [Chooser POC](https://chooser.fly.dev/?doi=10.1525/luminos.91.a)
- [Production](https://doi.org/10.1525/luminos.91.a)
- [JSON](https://api.crossref.org/works/10.1525/luminos.91.a)
- [XML](https://api.crossref.org/works/10.1525/luminos.91.a.xml)
<hr>

### 10.14361/9783839410509-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.14361/9783839410509-004)
- [Production](https://doi.org/10.14361/9783839410509-004)
- [JSON](https://api.crossref.org/works/10.14361/9783839410509-004)
- [XML](https://api.crossref.org/works/10.14361/9783839410509-004.xml)
<hr>

