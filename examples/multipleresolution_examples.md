## mutiple-resolution


### 10.7868/S002347611306009X
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S002347611306009X)
- [Production](https://doi.org/10.7868/S002347611306009X)
- [JSON](https://api.crossref.org/works/10.7868/S002347611306009X)
- [XML](https://api.crossref.org/works/10.7868/S002347611306009X.xml)
<hr>

### 10.1070/QEL17570
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/QEL17570)
- [Production](https://doi.org/10.1070/QEL17570)
- [JSON](https://api.crossref.org/works/10.1070/QEL17570)
- [XML](https://api.crossref.org/works/10.1070/QEL17570.xml)
<hr>

### 10.1049/cp.2018.1305
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp.2018.1305)
- [Production](https://doi.org/10.1049/cp.2018.1305)
- [JSON](https://api.crossref.org/works/10.1049/cp.2018.1305)
- [XML](https://api.crossref.org/works/10.1049/cp.2018.1305.xml)
<hr>

### 10.1070/PU1980v023n08ABEH005032
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/PU1980v023n08ABEH005032)
- [Production](https://doi.org/10.1070/PU1980v023n08ABEH005032)
- [JSON](https://api.crossref.org/works/10.1070/PU1980v023n08ABEH005032)
- [XML](https://api.crossref.org/works/10.1070/PU1980v023n08ABEH005032.xml)
<hr>

### 10.18574/nyu/9781479845309.003.0004
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9781479845309.003.0004)
- [Production](https://doi.org/10.18574/nyu/9781479845309.003.0004)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9781479845309.003.0004)
- [XML](https://api.crossref.org/works/10.18574/nyu/9781479845309.003.0004.xml)
<hr>

### 10.1070/RM1983v038n06ABEH003458
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1983v038n06ABEH003458)
- [Production](https://doi.org/10.1070/RM1983v038n06ABEH003458)
- [JSON](https://api.crossref.org/works/10.1070/RM1983v038n06ABEH003458)
- [XML](https://api.crossref.org/works/10.1070/RM1983v038n06ABEH003458.xml)
<hr>

### 10.5949/UPO9781846315220.006
- [Chooser POC](https://chooser.fly.dev/?doi=10.5949/UPO9781846315220.006)
- [Production](https://doi.org/10.5949/UPO9781846315220.006)
- [JSON](https://api.crossref.org/works/10.5949/UPO9781846315220.006)
- [XML](https://api.crossref.org/works/10.5949/UPO9781846315220.006.xml)
<hr>

### 10.5040/9781350036383.ch-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781350036383.ch-009)
- [Production](https://doi.org/10.5040/9781350036383.ch-009)
- [JSON](https://api.crossref.org/works/10.5040/9781350036383.ch-009)
- [XML](https://api.crossref.org/works/10.5040/9781350036383.ch-009.xml)
<hr>

### 10.15446/dyna.v82n189.42689
- [Chooser POC](https://chooser.fly.dev/?doi=10.15446/dyna.v82n189.42689)
- [Production](https://doi.org/10.15446/dyna.v82n189.42689)
- [JSON](https://api.crossref.org/works/10.15446/dyna.v82n189.42689)
- [XML](https://api.crossref.org/works/10.15446/dyna.v82n189.42689.xml)
<hr>

### 10.1001/archfami.6.5.424
- [Chooser POC](https://chooser.fly.dev/?doi=10.1001/archfami.6.5.424)
- [Production](https://doi.org/10.1001/archfami.6.5.424)
- [JSON](https://api.crossref.org/works/10.1001/archfami.6.5.424)
- [XML](https://api.crossref.org/works/10.1001/archfami.6.5.424.xml)
<hr>

### 10.2307/1863153
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1863153)
- [Production](https://doi.org/10.2307/1863153)
- [JSON](https://api.crossref.org/works/10.2307/1863153)
- [XML](https://api.crossref.org/works/10.2307/1863153.xml)
<hr>

### 10.4135/9781412986199.n13
- [Chooser POC](https://chooser.fly.dev/?doi=10.4135/9781412986199.n13)
- [Production](https://doi.org/10.4135/9781412986199.n13)
- [JSON](https://api.crossref.org/works/10.4135/9781412986199.n13)
- [XML](https://api.crossref.org/works/10.4135/9781412986199.n13.xml)
<hr>

### 10.2113/gsecongeo.25.2.222
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.25.2.222)
- [Production](https://doi.org/10.2113/gsecongeo.25.2.222)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.25.2.222)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.25.2.222.xml)
<hr>

### 10.7868/S0869565214040070
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0869565214040070)
- [Production](https://doi.org/10.7868/S0869565214040070)
- [JSON](https://api.crossref.org/works/10.7868/S0869565214040070)
- [XML](https://api.crossref.org/works/10.7868/S0869565214040070.xml)
<hr>

### 10.1070/IM8985
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/IM8985)
- [Production](https://doi.org/10.1070/IM8985)
- [JSON](https://api.crossref.org/works/10.1070/IM8985)
- [XML](https://api.crossref.org/works/10.1070/IM8985.xml)
<hr>

### 10.2307/2168936
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2168936)
- [Production](https://doi.org/10.2307/2168936)
- [JSON](https://api.crossref.org/works/10.2307/2168936)
- [XML](https://api.crossref.org/works/10.2307/2168936.xml)
<hr>

### 10.1144/SP403.1
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/SP403.1)
- [Production](https://doi.org/10.1144/SP403.1)
- [JSON](https://api.crossref.org/works/10.1144/SP403.1)
- [XML](https://api.crossref.org/works/10.1144/SP403.1.xml)
<hr>

### 10.7868/S0367676513020294
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0367676513020294)
- [Production](https://doi.org/10.7868/S0367676513020294)
- [JSON](https://api.crossref.org/works/10.7868/S0367676513020294)
- [XML](https://api.crossref.org/works/10.7868/S0367676513020294.xml)
<hr>

### 10.18574/nyu/9781479859290.003.0009
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9781479859290.003.0009)
- [Production](https://doi.org/10.18574/nyu/9781479859290.003.0009)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9781479859290.003.0009)
- [XML](https://api.crossref.org/works/10.18574/nyu/9781479859290.003.0009.xml)
<hr>

### 10.7868/S0015323014040160
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0015323014040160)
- [Production](https://doi.org/10.7868/S0015323014040160)
- [JSON](https://api.crossref.org/works/10.7868/S0015323014040160)
- [XML](https://api.crossref.org/works/10.7868/S0015323014040160.xml)
<hr>

### 10.7868/S0031031X1306007X
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0031031X1306007X)
- [Production](https://doi.org/10.7868/S0031031X1306007X)
- [JSON](https://api.crossref.org/works/10.7868/S0031031X1306007X)
- [XML](https://api.crossref.org/works/10.7868/S0031031X1306007X.xml)
<hr>

### 10.1144/GSL.SP.1997.122.01.08
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1997.122.01.08)
- [Production](https://doi.org/10.1144/GSL.SP.1997.122.01.08)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1997.122.01.08)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1997.122.01.08.xml)
<hr>

### 10.2307/1857589
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1857589)
- [Production](https://doi.org/10.2307/1857589)
- [JSON](https://api.crossref.org/works/10.2307/1857589)
- [XML](https://api.crossref.org/works/10.2307/1857589.xml)
<hr>

### 10.2307/2165049
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2165049)
- [Production](https://doi.org/10.2307/2165049)
- [JSON](https://api.crossref.org/works/10.2307/2165049)
- [XML](https://api.crossref.org/works/10.2307/2165049.xml)
<hr>

### 10.1049/icp.2021.0655
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/icp.2021.0655)
- [Production](https://doi.org/10.1049/icp.2021.0655)
- [JSON](https://api.crossref.org/works/10.1049/icp.2021.0655)
- [XML](https://api.crossref.org/works/10.1049/icp.2021.0655.xml)
<hr>

### 10.2752/9781847888990/WWBM0011
- [Chooser POC](https://chooser.fly.dev/?doi=10.2752/9781847888990/WWBM0011)
- [Production](https://doi.org/10.2752/9781847888990/WWBM0011)
- [JSON](https://api.crossref.org/works/10.2752/9781847888990/WWBM0011)
- [XML](https://api.crossref.org/works/10.2752/9781847888990/WWBM0011.xml)
<hr>

### 10.5040/9781501340673.ch-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781501340673.ch-009)
- [Production](https://doi.org/10.5040/9781501340673.ch-009)
- [JSON](https://api.crossref.org/works/10.5040/9781501340673.ch-009)
- [XML](https://api.crossref.org/works/10.5040/9781501340673.ch-009.xml)
<hr>

### 10.5040/9781501316555
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781501316555)
- [Production](https://doi.org/10.5040/9781501316555)
- [JSON](https://api.crossref.org/works/10.5040/9781501316555)
- [XML](https://api.crossref.org/works/10.5040/9781501316555.xml)
<hr>

### 10.5040/9781501323386.ch-017
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781501323386.ch-017)
- [Production](https://doi.org/10.5040/9781501323386.ch-017)
- [JSON](https://api.crossref.org/works/10.5040/9781501323386.ch-017)
- [XML](https://api.crossref.org/works/10.5040/9781501323386.ch-017.xml)
<hr>

### 10.5040/9781501340673.ch-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781501340673.ch-006)
- [Production](https://doi.org/10.5040/9781501340673.ch-006)
- [JSON](https://api.crossref.org/works/10.5040/9781501340673.ch-006)
- [XML](https://api.crossref.org/works/10.5040/9781501340673.ch-006.xml)
<hr>

### 10.2307/1839249
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1839249)
- [Production](https://doi.org/10.2307/1839249)
- [JSON](https://api.crossref.org/works/10.2307/1839249)
- [XML](https://api.crossref.org/works/10.2307/1839249.xml)
<hr>

### 10.15530/AP-URTEC-2021-208350
- [Chooser POC](https://chooser.fly.dev/?doi=10.15530/AP-URTEC-2021-208350)
- [Production](https://doi.org/10.15530/AP-URTEC-2021-208350)
- [JSON](https://api.crossref.org/works/10.15530/AP-URTEC-2021-208350)
- [XML](https://api.crossref.org/works/10.15530/AP-URTEC-2021-208350.xml)
<hr>

### 10.1070/RM9848
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM9848)
- [Production](https://doi.org/10.1070/RM9848)
- [JSON](https://api.crossref.org/works/10.1070/RM9848)
- [XML](https://api.crossref.org/works/10.1070/RM9848.xml)
<hr>

### 10.15530/urtec-2020-3176
- [Chooser POC](https://chooser.fly.dev/?doi=10.15530/urtec-2020-3176)
- [Production](https://doi.org/10.15530/urtec-2020-3176)
- [JSON](https://api.crossref.org/works/10.15530/urtec-2020-3176)
- [XML](https://api.crossref.org/works/10.15530/urtec-2020-3176.xml)
<hr>

### 10.18574/nyu/9781479847624.003.0007
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9781479847624.003.0007)
- [Production](https://doi.org/10.18574/nyu/9781479847624.003.0007)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9781479847624.003.0007)
- [XML](https://api.crossref.org/works/10.18574/nyu/9781479847624.003.0007.xml)
<hr>

### 10.7868/S013234231304009X
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S013234231304009X)
- [Production](https://doi.org/10.7868/S013234231304009X)
- [JSON](https://api.crossref.org/works/10.7868/S013234231304009X)
- [XML](https://api.crossref.org/works/10.7868/S013234231304009X.xml)
<hr>

### 10.18502/ohhp.v4i2.3987
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/ohhp.v4i2.3987)
- [Production](https://doi.org/10.18502/ohhp.v4i2.3987)
- [JSON](https://api.crossref.org/works/10.18502/ohhp.v4i2.3987)
- [XML](https://api.crossref.org/works/10.18502/ohhp.v4i2.3987.xml)
<hr>

### 10.18574/nyu/9780814796108.003.0009
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9780814796108.003.0009)
- [Production](https://doi.org/10.18574/nyu/9780814796108.003.0009)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9780814796108.003.0009)
- [XML](https://api.crossref.org/works/10.18574/nyu/9780814796108.003.0009.xml)
<hr>

### 10.1070/RM1995v050n05ABEH002623
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1995v050n05ABEH002623)
- [Production](https://doi.org/10.1070/RM1995v050n05ABEH002623)
- [JSON](https://api.crossref.org/works/10.1070/RM1995v050n05ABEH002623)
- [XML](https://api.crossref.org/works/10.1070/RM1995v050n05ABEH002623.xml)
<hr>

### 10.3343/lmo.2019.9.4.246
- [Chooser POC](https://chooser.fly.dev/?doi=10.3343/lmo.2019.9.4.246)
- [Production](https://doi.org/10.3343/lmo.2019.9.4.246)
- [JSON](https://api.crossref.org/works/10.3343/lmo.2019.9.4.246)
- [XML](https://api.crossref.org/works/10.3343/lmo.2019.9.4.246.xml)
<hr>

### 10.7868/S0016752513070145
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0016752513070145)
- [Production](https://doi.org/10.7868/S0016752513070145)
- [JSON](https://api.crossref.org/works/10.7868/S0016752513070145)
- [XML](https://api.crossref.org/works/10.7868/S0016752513070145.xml)
<hr>

### 10.2307/2163774
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2163774)
- [Production](https://doi.org/10.2307/2163774)
- [JSON](https://api.crossref.org/works/10.2307/2163774)
- [XML](https://api.crossref.org/works/10.2307/2163774.xml)
<hr>

### 10.1049/cp.2016.1223
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp.2016.1223)
- [Production](https://doi.org/10.1049/cp.2016.1223)
- [JSON](https://api.crossref.org/works/10.1049/cp.2016.1223)
- [XML](https://api.crossref.org/works/10.1049/cp.2016.1223.xml)
<hr>

### 10.5040/9781350071452.ch-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781350071452.ch-005)
- [Production](https://doi.org/10.5040/9781350071452.ch-005)
- [JSON](https://api.crossref.org/works/10.5040/9781350071452.ch-005)
- [XML](https://api.crossref.org/works/10.5040/9781350071452.ch-005.xml)
<hr>

### 10.7868/S1027813314010105
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S1027813314010105)
- [Production](https://doi.org/10.7868/S1027813314010105)
- [JSON](https://api.crossref.org/works/10.7868/S1027813314010105)
- [XML](https://api.crossref.org/works/10.7868/S1027813314010105.xml)
<hr>

### 10.7868/S0002337X13070129
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0002337X13070129)
- [Production](https://doi.org/10.7868/S0002337X13070129)
- [JSON](https://api.crossref.org/works/10.7868/S0002337X13070129)
- [XML](https://api.crossref.org/works/10.7868/S0002337X13070129.xml)
<hr>

### 10.15446/rcdg.v23n2.43387
- [Chooser POC](https://chooser.fly.dev/?doi=10.15446/rcdg.v23n2.43387)
- [Production](https://doi.org/10.15446/rcdg.v23n2.43387)
- [JSON](https://api.crossref.org/works/10.15446/rcdg.v23n2.43387)
- [XML](https://api.crossref.org/works/10.15446/rcdg.v23n2.43387.xml)
<hr>

### 10.15446/rcdg.v27n1.69500
- [Chooser POC](https://chooser.fly.dev/?doi=10.15446/rcdg.v27n1.69500)
- [Production](https://doi.org/10.15446/rcdg.v27n1.69500)
- [JSON](https://api.crossref.org/works/10.15446/rcdg.v27n1.69500)
- [XML](https://api.crossref.org/works/10.15446/rcdg.v27n1.69500.xml)
<hr>

### 10.2307/1836807
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1836807)
- [Production](https://doi.org/10.2307/1836807)
- [JSON](https://api.crossref.org/works/10.2307/1836807)
- [XML](https://api.crossref.org/works/10.2307/1836807.xml)
<hr>

### 10.5040/9781474232104.ch-004
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781474232104.ch-004)
- [Production](https://doi.org/10.5040/9781474232104.ch-004)
- [JSON](https://api.crossref.org/works/10.5040/9781474232104.ch-004)
- [XML](https://api.crossref.org/works/10.5040/9781474232104.ch-004.xml)
<hr>

### 10.7868/S0032816213040010
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0032816213040010)
- [Production](https://doi.org/10.7868/S0032816213040010)
- [JSON](https://api.crossref.org/works/10.7868/S0032816213040010)
- [XML](https://api.crossref.org/works/10.7868/S0032816213040010.xml)
<hr>

### 10.18574/nyu/9780814720059.003.0007
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9780814720059.003.0007)
- [Production](https://doi.org/10.18574/nyu/9780814720059.003.0007)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9780814720059.003.0007)
- [XML](https://api.crossref.org/works/10.18574/nyu/9780814720059.003.0007.xml)
<hr>

### 10.5040/9781838710781.ch-058
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781838710781.ch-058)
- [Production](https://doi.org/10.5040/9781838710781.ch-058)
- [JSON](https://api.crossref.org/works/10.5040/9781838710781.ch-058)
- [XML](https://api.crossref.org/works/10.5040/9781838710781.ch-058.xml)
<hr>

### 10.1070/QE2010v040n12ABEH014509
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/QE2010v040n12ABEH014509)
- [Production](https://doi.org/10.1070/QE2010v040n12ABEH014509)
- [JSON](https://api.crossref.org/works/10.1070/QE2010v040n12ABEH014509)
- [XML](https://api.crossref.org/works/10.1070/QE2010v040n12ABEH014509.xml)
<hr>

### 10.2307/1849072
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1849072)
- [Production](https://doi.org/10.2307/1849072)
- [JSON](https://api.crossref.org/works/10.2307/1849072)
- [XML](https://api.crossref.org/works/10.2307/1849072.xml)
<hr>

### 10.1144/GSL.SP.1986.022.01.18
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1986.022.01.18)
- [Production](https://doi.org/10.1144/GSL.SP.1986.022.01.18)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1986.022.01.18)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1986.022.01.18.xml)
<hr>

### 10.1144/GSL.SP.2004.235.01.15
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.2004.235.01.15)
- [Production](https://doi.org/10.1144/GSL.SP.2004.235.01.15)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.2004.235.01.15)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.2004.235.01.15.xml)
<hr>

### 10.1144/sjg05030301r
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/sjg05030301r)
- [Production](https://doi.org/10.1144/sjg05030301r)
- [JSON](https://api.crossref.org/works/10.1144/sjg05030301r)
- [XML](https://api.crossref.org/works/10.1144/sjg05030301r.xml)
<hr>

### 10.15530/urtec-2015-2172470
- [Chooser POC](https://chooser.fly.dev/?doi=10.15530/urtec-2015-2172470)
- [Production](https://doi.org/10.15530/urtec-2015-2172470)
- [JSON](https://api.crossref.org/works/10.15530/urtec-2015-2172470)
- [XML](https://api.crossref.org/works/10.15530/urtec-2015-2172470.xml)
<hr>

### 10.5935/0103-5053.20160019
- [Chooser POC](https://chooser.fly.dev/?doi=10.5935/0103-5053.20160019)
- [Production](https://doi.org/10.5935/0103-5053.20160019)
- [JSON](https://api.crossref.org/works/10.5935/0103-5053.20160019)
- [XML](https://api.crossref.org/works/10.5935/0103-5053.20160019.xml)
<hr>

### 10.18502/pbr.v6i4.5117
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/pbr.v6i4.5117)
- [Production](https://doi.org/10.18502/pbr.v6i4.5117)
- [JSON](https://api.crossref.org/works/10.18502/pbr.v6i4.5117)
- [XML](https://api.crossref.org/works/10.18502/pbr.v6i4.5117.xml)
<hr>

### 10.7868/S0044457X13050097
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0044457X13050097)
- [Production](https://doi.org/10.7868/S0044457X13050097)
- [JSON](https://api.crossref.org/works/10.7868/S0044457X13050097)
- [XML](https://api.crossref.org/works/10.7868/S0044457X13050097.xml)
<hr>

### 10.15446/innovar.v24n53.43915
- [Chooser POC](https://chooser.fly.dev/?doi=10.15446/innovar.v24n53.43915)
- [Production](https://doi.org/10.15446/innovar.v24n53.43915)
- [JSON](https://api.crossref.org/works/10.15446/innovar.v24n53.43915)
- [XML](https://api.crossref.org/works/10.15446/innovar.v24n53.43915.xml)
<hr>

### 10.5040/9781408166505.ch-010
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781408166505.ch-010)
- [Production](https://doi.org/10.5040/9781408166505.ch-010)
- [JSON](https://api.crossref.org/works/10.5040/9781408166505.ch-010)
- [XML](https://api.crossref.org/works/10.5040/9781408166505.ch-010.xml)
<hr>

### 10.1144/GSL.SP.2002.192.01.03
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.2002.192.01.03)
- [Production](https://doi.org/10.1144/GSL.SP.2002.192.01.03)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.2002.192.01.03)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.2002.192.01.03.xml)
<hr>

### 10.2307/2164432
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2164432)
- [Production](https://doi.org/10.2307/2164432)
- [JSON](https://api.crossref.org/works/10.2307/2164432)
- [XML](https://api.crossref.org/works/10.2307/2164432.xml)
<hr>

### 10.2190/CN.32.3.e
- [Chooser POC](https://chooser.fly.dev/?doi=10.2190/CN.32.3.e)
- [Production](https://doi.org/10.2190/CN.32.3.e)
- [JSON](https://api.crossref.org/works/10.2190/CN.32.3.e)
- [XML](https://api.crossref.org/works/10.2190/CN.32.3.e.xml)
<hr>

### 10.1049/cp.2018.1199
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp.2018.1199)
- [Production](https://doi.org/10.1049/cp.2018.1199)
- [JSON](https://api.crossref.org/works/10.1049/cp.2018.1199)
- [XML](https://api.crossref.org/works/10.1049/cp.2018.1199.xml)
<hr>

### 10.1070/RM1989v044n05ABEH002292
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1989v044n05ABEH002292)
- [Production](https://doi.org/10.1070/RM1989v044n05ABEH002292)
- [JSON](https://api.crossref.org/works/10.1070/RM1989v044n05ABEH002292)
- [XML](https://api.crossref.org/works/10.1070/RM1989v044n05ABEH002292.xml)
<hr>

### 10.4135/9781848607934.n60
- [Chooser POC](https://chooser.fly.dev/?doi=10.4135/9781848607934.n60)
- [Production](https://doi.org/10.4135/9781848607934.n60)
- [JSON](https://api.crossref.org/works/10.4135/9781848607934.n60)
- [XML](https://api.crossref.org/works/10.4135/9781848607934.n60.xml)
<hr>

### 10.7868/S0023476113010050
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0023476113010050)
- [Production](https://doi.org/10.7868/S0023476113010050)
- [JSON](https://api.crossref.org/works/10.7868/S0023476113010050)
- [XML](https://api.crossref.org/works/10.7868/S0023476113010050.xml)
<hr>

### 10.7868/S0869565213190286
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0869565213190286)
- [Production](https://doi.org/10.7868/S0869565213190286)
- [JSON](https://api.crossref.org/works/10.7868/S0869565213190286)
- [XML](https://api.crossref.org/works/10.7868/S0869565213190286.xml)
<hr>

### 10.3341/kjo.2016.0111
- [Chooser POC](https://chooser.fly.dev/?doi=10.3341/kjo.2016.0111)
- [Production](https://doi.org/10.3341/kjo.2016.0111)
- [JSON](https://api.crossref.org/works/10.3341/kjo.2016.0111)
- [XML](https://api.crossref.org/works/10.3341/kjo.2016.0111.xml)
<hr>

### 10.1049/icp.2021.2069
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/icp.2021.2069)
- [Production](https://doi.org/10.1049/icp.2021.2069)
- [JSON](https://api.crossref.org/works/10.1049/icp.2021.2069)
- [XML](https://api.crossref.org/works/10.1049/icp.2021.2069.xml)
<hr>

### 10.1070/QEL16931
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/QEL16931)
- [Production](https://doi.org/10.1070/QEL16931)
- [JSON](https://api.crossref.org/works/10.1070/QEL16931)
- [XML](https://api.crossref.org/works/10.1070/QEL16931.xml)
<hr>

### 10.2307/1848561
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1848561)
- [Production](https://doi.org/10.2307/1848561)
- [JSON](https://api.crossref.org/works/10.2307/1848561)
- [XML](https://api.crossref.org/works/10.2307/1848561.xml)
<hr>

### 10.7868/S0320010813080032
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0320010813080032)
- [Production](https://doi.org/10.7868/S0320010813080032)
- [JSON](https://api.crossref.org/works/10.7868/S0320010813080032)
- [XML](https://api.crossref.org/works/10.7868/S0320010813080032.xml)
<hr>

### 10.2307/1838384
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1838384)
- [Production](https://doi.org/10.2307/1838384)
- [JSON](https://api.crossref.org/works/10.2307/1838384)
- [XML](https://api.crossref.org/works/10.2307/1838384.xml)
<hr>

### 10.2307/1906298
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1906298)
- [Production](https://doi.org/10.2307/1906298)
- [JSON](https://api.crossref.org/works/10.2307/1906298)
- [XML](https://api.crossref.org/works/10.2307/1906298.xml)
<hr>

### 10.2113/gsecongeo.49.4.423
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.49.4.423)
- [Production](https://doi.org/10.2113/gsecongeo.49.4.423)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.49.4.423)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.49.4.423.xml)
<hr>

### 10.18502/ijph.v50i9.7074
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/ijph.v50i9.7074)
- [Production](https://doi.org/10.18502/ijph.v50i9.7074)
- [JSON](https://api.crossref.org/works/10.18502/ijph.v50i9.7074)
- [XML](https://api.crossref.org/works/10.18502/ijph.v50i9.7074.xml)
<hr>

### 10.2307/1904586
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1904586)
- [Production](https://doi.org/10.2307/1904586)
- [JSON](https://api.crossref.org/works/10.2307/1904586)
- [XML](https://api.crossref.org/works/10.2307/1904586.xml)
<hr>

### 10.4041/kjod.2008.38.6.397
- [Chooser POC](https://chooser.fly.dev/?doi=10.4041/kjod.2008.38.6.397)
- [Production](https://doi.org/10.4041/kjod.2008.38.6.397)
- [JSON](https://api.crossref.org/works/10.4041/kjod.2008.38.6.397)
- [XML](https://api.crossref.org/works/10.4041/kjod.2008.38.6.397.xml)
<hr>

### 10.1111/j.1475-4975.2005.00118.x
- [Chooser POC](https://chooser.fly.dev/?doi=10.1111/j.1475-4975.2005.00118.x)
- [Production](https://doi.org/10.1111/j.1475-4975.2005.00118.x)
- [JSON](https://api.crossref.org/works/10.1111/j.1475-4975.2005.00118.x)
- [XML](https://api.crossref.org/works/10.1111/j.1475-4975.2005.00118.x.xml)
<hr>

### 10.7868/S086956521322009X
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S086956521322009X)
- [Production](https://doi.org/10.7868/S086956521322009X)
- [JSON](https://api.crossref.org/works/10.7868/S086956521322009X)
- [XML](https://api.crossref.org/works/10.7868/S086956521322009X.xml)
<hr>

### 10.1070/PU2007v050n10ABEH006776
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/PU2007v050n10ABEH006776)
- [Production](https://doi.org/10.1070/PU2007v050n10ABEH006776)
- [JSON](https://api.crossref.org/works/10.1070/PU2007v050n10ABEH006776)
- [XML](https://api.crossref.org/works/10.1070/PU2007v050n10ABEH006776.xml)
<hr>

### 10.7868/S0032180X13020068
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0032180X13020068)
- [Production](https://doi.org/10.7868/S0032180X13020068)
- [JSON](https://api.crossref.org/works/10.7868/S0032180X13020068)
- [XML](https://api.crossref.org/works/10.7868/S0032180X13020068.xml)
<hr>

### 10.2113/gsecongeo.3.6.465
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.3.6.465)
- [Production](https://doi.org/10.2113/gsecongeo.3.6.465)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.3.6.465)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.3.6.465.xml)
<hr>

### 10.3901/CJME.2011.06.999
- [Chooser POC](https://chooser.fly.dev/?doi=10.3901/CJME.2011.06.999)
- [Production](https://doi.org/10.3901/CJME.2011.06.999)
- [JSON](https://api.crossref.org/works/10.3901/CJME.2011.06.999)
- [XML](https://api.crossref.org/works/10.3901/CJME.2011.06.999.xml)
<hr>

### 10.1070/RM1985v040n03ABEH003599
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1985v040n03ABEH003599)
- [Production](https://doi.org/10.1070/RM1985v040n03ABEH003599)
- [JSON](https://api.crossref.org/works/10.1070/RM1985v040n03ABEH003599)
- [XML](https://api.crossref.org/works/10.1070/RM1985v040n03ABEH003599.xml)
<hr>

### 10.2307/1834323
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1834323)
- [Production](https://doi.org/10.2307/1834323)
- [JSON](https://api.crossref.org/works/10.2307/1834323)
- [XML](https://api.crossref.org/works/10.2307/1834323.xml)
<hr>

### 10.1049/icp.2021.0552
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/icp.2021.0552)
- [Production](https://doi.org/10.1049/icp.2021.0552)
- [JSON](https://api.crossref.org/works/10.1049/icp.2021.0552)
- [XML](https://api.crossref.org/works/10.1049/icp.2021.0552.xml)
<hr>

### 10.2113/gsecongeo.82.8.2199
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.82.8.2199)
- [Production](https://doi.org/10.2113/gsecongeo.82.8.2199)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.82.8.2199)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.82.8.2199.xml)
<hr>

### 10.2307/1850764
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1850764)
- [Production](https://doi.org/10.2307/1850764)
- [JSON](https://api.crossref.org/works/10.2307/1850764)
- [XML](https://api.crossref.org/works/10.2307/1850764.xml)
<hr>

### 10.1007/978-94-6209-116-0
- [Chooser POC](https://chooser.fly.dev/?doi=10.1007/978-94-6209-116-0)
- [Production](https://doi.org/10.1007/978-94-6209-116-0)
- [JSON](https://api.crossref.org/works/10.1007/978-94-6209-116-0)
- [XML](https://api.crossref.org/works/10.1007/978-94-6209-116-0.xml)
<hr>

### 10.1049/icp.2020.0361
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/icp.2020.0361)
- [Production](https://doi.org/10.1049/icp.2020.0361)
- [JSON](https://api.crossref.org/works/10.1049/icp.2020.0361)
- [XML](https://api.crossref.org/works/10.1049/icp.2020.0361.xml)
<hr>

### 10.1124/mi.8.1.5
- [Chooser POC](https://chooser.fly.dev/?doi=10.1124/mi.8.1.5)
- [Production](https://doi.org/10.1124/mi.8.1.5)
- [JSON](https://api.crossref.org/works/10.1124/mi.8.1.5)
- [XML](https://api.crossref.org/works/10.1124/mi.8.1.5.xml)
<hr>

### 10.2307/2165084
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2165084)
- [Production](https://doi.org/10.2307/2165084)
- [JSON](https://api.crossref.org/works/10.2307/2165084)
- [XML](https://api.crossref.org/works/10.2307/2165084.xml)
<hr>

### 10.18502/tbj.v19i4.4521
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/tbj.v19i4.4521)
- [Production](https://doi.org/10.18502/tbj.v19i4.4521)
- [JSON](https://api.crossref.org/works/10.18502/tbj.v19i4.4521)
- [XML](https://api.crossref.org/works/10.18502/tbj.v19i4.4521.xml)
<hr>

### 10.1596/32065
- [Chooser POC](https://chooser.fly.dev/?doi=10.1596/32065)
- [Production](https://doi.org/10.1596/32065)
- [JSON](https://api.crossref.org/works/10.1596/32065)
- [XML](https://api.crossref.org/works/10.1596/32065.xml)
<hr>

### 10.2307/1856362
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1856362)
- [Production](https://doi.org/10.2307/1856362)
- [JSON](https://api.crossref.org/works/10.2307/1856362)
- [XML](https://api.crossref.org/works/10.2307/1856362.xml)
<hr>

### 10.1190/1.9781560801863.ch2
- [Chooser POC](https://chooser.fly.dev/?doi=10.1190/1.9781560801863.ch2)
- [Production](https://doi.org/10.1190/1.9781560801863.ch2)
- [JSON](https://api.crossref.org/works/10.1190/1.9781560801863.ch2)
- [XML](https://api.crossref.org/works/10.1190/1.9781560801863.ch2.xml)
<hr>

### 10.1070/RM1994v049n01ABEH002138
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1994v049n01ABEH002138)
- [Production](https://doi.org/10.1070/RM1994v049n01ABEH002138)
- [JSON](https://api.crossref.org/works/10.1070/RM1994v049n01ABEH002138)
- [XML](https://api.crossref.org/works/10.1070/RM1994v049n01ABEH002138.xml)
<hr>

### 10.5040/9781838710644.0048
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781838710644.0048)
- [Production](https://doi.org/10.5040/9781838710644.0048)
- [JSON](https://api.crossref.org/works/10.5040/9781838710644.0048)
- [XML](https://api.crossref.org/works/10.5040/9781838710644.0048.xml)
<hr>

### 10.3901/CJME.2005.01.071
- [Chooser POC](https://chooser.fly.dev/?doi=10.3901/CJME.2005.01.071)
- [Production](https://doi.org/10.3901/CJME.2005.01.071)
- [JSON](https://api.crossref.org/works/10.3901/CJME.2005.01.071)
- [XML](https://api.crossref.org/works/10.3901/CJME.2005.01.071.xml)
<hr>

### 10.18502/acta.v59i6.6897
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/acta.v59i6.6897)
- [Production](https://doi.org/10.18502/acta.v59i6.6897)
- [JSON](https://api.crossref.org/works/10.18502/acta.v59i6.6897)
- [XML](https://api.crossref.org/works/10.18502/acta.v59i6.6897.xml)
<hr>

### 10.2307/1849514
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1849514)
- [Production](https://doi.org/10.2307/1849514)
- [JSON](https://api.crossref.org/works/10.2307/1849514)
- [XML](https://api.crossref.org/works/10.2307/1849514.xml)
<hr>

### 10.2307/1838996
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1838996)
- [Production](https://doi.org/10.2307/1838996)
- [JSON](https://api.crossref.org/works/10.2307/1838996)
- [XML](https://api.crossref.org/works/10.2307/1838996.xml)
<hr>

### 10.1049/cp.2018.0245
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp.2018.0245)
- [Production](https://doi.org/10.1049/cp.2018.0245)
- [JSON](https://api.crossref.org/works/10.1049/cp.2018.0245)
- [XML](https://api.crossref.org/works/10.1049/cp.2018.0245.xml)
<hr>

### 10.1180/minmag.2013.077.5.22
- [Chooser POC](https://chooser.fly.dev/?doi=10.1180/minmag.2013.077.5.22)
- [Production](https://doi.org/10.1180/minmag.2013.077.5.22)
- [JSON](https://api.crossref.org/works/10.1180/minmag.2013.077.5.22)
- [XML](https://api.crossref.org/works/10.1180/minmag.2013.077.5.22.xml)
<hr>

### 10.1144/GSL.SP.2000.171.01.23
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.2000.171.01.23)
- [Production](https://doi.org/10.1144/GSL.SP.2000.171.01.23)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.2000.171.01.23)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.2000.171.01.23.xml)
<hr>

### 10.1038/bonekey.2016.76
- [Chooser POC](https://chooser.fly.dev/?doi=10.1038/bonekey.2016.76)
- [Production](https://doi.org/10.1038/bonekey.2016.76)
- [JSON](https://api.crossref.org/works/10.1038/bonekey.2016.76)
- [XML](https://api.crossref.org/works/10.1038/bonekey.2016.76.xml)
<hr>

### 10.2307/2652595
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2652595)
- [Production](https://doi.org/10.2307/2652595)
- [JSON](https://api.crossref.org/works/10.2307/2652595)
- [XML](https://api.crossref.org/works/10.2307/2652595.xml)
<hr>

### 10.18574/nyu/9781479846641.003.0015
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9781479846641.003.0015)
- [Production](https://doi.org/10.18574/nyu/9781479846641.003.0015)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9781479846641.003.0015)
- [XML](https://api.crossref.org/works/10.18574/nyu/9781479846641.003.0015.xml)
<hr>

### 10.18352/ts.181
- [Chooser POC](https://chooser.fly.dev/?doi=10.18352/ts.181)
- [Production](https://doi.org/10.18352/ts.181)
- [JSON](https://api.crossref.org/works/10.18352/ts.181)
- [XML](https://api.crossref.org/works/10.18352/ts.181.xml)
<hr>

### 10.18574/nyu/9781479853342.003.0007
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9781479853342.003.0007)
- [Production](https://doi.org/10.18574/nyu/9781479853342.003.0007)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9781479853342.003.0007)
- [XML](https://api.crossref.org/works/10.18574/nyu/9781479853342.003.0007.xml)
<hr>

### 10.15530/AP-URTEC-2019-198319
- [Chooser POC](https://chooser.fly.dev/?doi=10.15530/AP-URTEC-2019-198319)
- [Production](https://doi.org/10.15530/AP-URTEC-2019-198319)
- [JSON](https://api.crossref.org/works/10.15530/AP-URTEC-2019-198319)
- [XML](https://api.crossref.org/works/10.15530/AP-URTEC-2019-198319.xml)
<hr>

### 10.2307/1834414
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1834414)
- [Production](https://doi.org/10.2307/1834414)
- [JSON](https://api.crossref.org/works/10.2307/1834414)
- [XML](https://api.crossref.org/works/10.2307/1834414.xml)
<hr>

### 10.1049/cp.2012.0668
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp.2012.0668)
- [Production](https://doi.org/10.1049/cp.2012.0668)
- [JSON](https://api.crossref.org/works/10.1049/cp.2012.0668)
- [XML](https://api.crossref.org/works/10.1049/cp.2012.0668.xml)
<hr>

### 10.7868/S0024497X13040046
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0024497X13040046)
- [Production](https://doi.org/10.7868/S0024497X13040046)
- [JSON](https://api.crossref.org/works/10.7868/S0024497X13040046)
- [XML](https://api.crossref.org/works/10.7868/S0024497X13040046.xml)
<hr>

### 10.5040/9781838710651.0042
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781838710651.0042)
- [Production](https://doi.org/10.5040/9781838710651.0042)
- [JSON](https://api.crossref.org/works/10.5040/9781838710651.0042)
- [XML](https://api.crossref.org/works/10.5040/9781838710651.0042.xml)
<hr>

### 10.2307/1904984
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1904984)
- [Production](https://doi.org/10.2307/1904984)
- [JSON](https://api.crossref.org/works/10.2307/1904984)
- [XML](https://api.crossref.org/works/10.2307/1904984.xml)
<hr>

### 10.2307/1906278
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1906278)
- [Production](https://doi.org/10.2307/1906278)
- [JSON](https://api.crossref.org/works/10.2307/1906278)
- [XML](https://api.crossref.org/works/10.2307/1906278.xml)
<hr>

### 10.1306/D42696E3-2B26-11D7-8648000102C1865D
- [Chooser POC](https://chooser.fly.dev/?doi=10.1306/D42696E3-2B26-11D7-8648000102C1865D)
- [Production](https://doi.org/10.1306/D42696E3-2B26-11D7-8648000102C1865D)
- [JSON](https://api.crossref.org/works/10.1306/D42696E3-2B26-11D7-8648000102C1865D)
- [XML](https://api.crossref.org/works/10.1306/D42696E3-2B26-11D7-8648000102C1865D.xml)
<hr>

### 10.2307/1853162
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1853162)
- [Production](https://doi.org/10.2307/1853162)
- [JSON](https://api.crossref.org/works/10.2307/1853162)
- [XML](https://api.crossref.org/works/10.2307/1853162.xml)
<hr>

### 10.1144/GSL.SP.2003.209.01.17
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.2003.209.01.17)
- [Production](https://doi.org/10.1144/GSL.SP.2003.209.01.17)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.2003.209.01.17)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.2003.209.01.17.xml)
<hr>

### 10.15446/rcs.v39n1.56346
- [Chooser POC](https://chooser.fly.dev/?doi=10.15446/rcs.v39n1.56346)
- [Production](https://doi.org/10.15446/rcs.v39n1.56346)
- [JSON](https://api.crossref.org/works/10.15446/rcs.v39n1.56346)
- [XML](https://api.crossref.org/works/10.15446/rcs.v39n1.56346.xml)
<hr>

### 10.1144/GSL.SP.1999.155.01.08
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1999.155.01.08)
- [Production](https://doi.org/10.1144/GSL.SP.1999.155.01.08)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1999.155.01.08)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1999.155.01.08.xml)
<hr>

### 10.1038/bonekey.2012.96
- [Chooser POC](https://chooser.fly.dev/?doi=10.1038/bonekey.2012.96)
- [Production](https://doi.org/10.1038/bonekey.2012.96)
- [JSON](https://api.crossref.org/works/10.1038/bonekey.2012.96)
- [XML](https://api.crossref.org/works/10.1038/bonekey.2012.96.xml)
<hr>

### 10.2190/2X4N-UGMW-4M4P-TCH2
- [Chooser POC](https://chooser.fly.dev/?doi=10.2190/2X4N-UGMW-4M4P-TCH2)
- [Production](https://doi.org/10.2190/2X4N-UGMW-4M4P-TCH2)
- [JSON](https://api.crossref.org/works/10.2190/2X4N-UGMW-4M4P-TCH2)
- [XML](https://api.crossref.org/works/10.2190/2X4N-UGMW-4M4P-TCH2.xml)
<hr>

### 10.2307/2652406
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2652406)
- [Production](https://doi.org/10.2307/2652406)
- [JSON](https://api.crossref.org/works/10.2307/2652406)
- [XML](https://api.crossref.org/works/10.2307/2652406.xml)
<hr>

### 10.2307/1861135
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1861135)
- [Production](https://doi.org/10.2307/1861135)
- [JSON](https://api.crossref.org/works/10.2307/1861135)
- [XML](https://api.crossref.org/works/10.2307/1861135.xml)
<hr>

### 10.5040/9781501363726.ch-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781501363726.ch-002)
- [Production](https://doi.org/10.5040/9781501363726.ch-002)
- [JSON](https://api.crossref.org/works/10.5040/9781501363726.ch-002)
- [XML](https://api.crossref.org/works/10.5040/9781501363726.ch-002.xml)
<hr>

### 10.5040/9781474206617.ch-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781474206617.ch-007)
- [Production](https://doi.org/10.5040/9781474206617.ch-007)
- [JSON](https://api.crossref.org/works/10.5040/9781474206617.ch-007)
- [XML](https://api.crossref.org/works/10.5040/9781474206617.ch-007.xml)
<hr>

### 10.1070/RM9728
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM9728)
- [Production](https://doi.org/10.1070/RM9728)
- [JSON](https://api.crossref.org/works/10.1070/RM9728)
- [XML](https://api.crossref.org/works/10.1070/RM9728.xml)
<hr>

### 10.4135/9781412991445.d102
- [Chooser POC](https://chooser.fly.dev/?doi=10.4135/9781412991445.d102)
- [Production](https://doi.org/10.4135/9781412991445.d102)
- [JSON](https://api.crossref.org/works/10.4135/9781412991445.d102)
- [XML](https://api.crossref.org/works/10.4135/9781412991445.d102.xml)
<hr>

### 10.1049/ic.2003.0291
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/ic.2003.0291)
- [Production](https://doi.org/10.1049/ic.2003.0291)
- [JSON](https://api.crossref.org/works/10.1049/ic.2003.0291)
- [XML](https://api.crossref.org/works/10.1049/ic.2003.0291.xml)
<hr>

### 10.15530/urtec-2016-2460784
- [Chooser POC](https://chooser.fly.dev/?doi=10.15530/urtec-2016-2460784)
- [Production](https://doi.org/10.15530/urtec-2016-2460784)
- [JSON](https://api.crossref.org/works/10.15530/urtec-2016-2460784)
- [XML](https://api.crossref.org/works/10.15530/urtec-2016-2460784.xml)
<hr>

### 10.1190/1.9781560802389.ch1
- [Chooser POC](https://chooser.fly.dev/?doi=10.1190/1.9781560802389.ch1)
- [Production](https://doi.org/10.1190/1.9781560802389.ch1)
- [JSON](https://api.crossref.org/works/10.1190/1.9781560802389.ch1)
- [XML](https://api.crossref.org/works/10.1190/1.9781560802389.ch1.xml)
<hr>

### 10.1049/cp:20040398
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp:20040398)
- [Production](https://doi.org/10.1049/cp:20040398)
- [JSON](https://api.crossref.org/works/10.1049/cp:20040398)
- [XML](https://api.crossref.org/works/10.1049/cp:20040398.xml)
<hr>

### 10.1070/PU1977v020n07ABEH005454
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/PU1977v020n07ABEH005454)
- [Production](https://doi.org/10.1070/PU1977v020n07ABEH005454)
- [JSON](https://api.crossref.org/works/10.1070/PU1977v020n07ABEH005454)
- [XML](https://api.crossref.org/works/10.1070/PU1977v020n07ABEH005454.xml)
<hr>

### 10.7868/S004400271311007X
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S004400271311007X)
- [Production](https://doi.org/10.7868/S004400271311007X)
- [JSON](https://api.crossref.org/works/10.7868/S004400271311007X)
- [XML](https://api.crossref.org/works/10.7868/S004400271311007X.xml)
<hr>

### 10.18502/tbj.v18i5.2289
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/tbj.v18i5.2289)
- [Production](https://doi.org/10.18502/tbj.v18i5.2289)
- [JSON](https://api.crossref.org/works/10.18502/tbj.v18i5.2289)
- [XML](https://api.crossref.org/works/10.18502/tbj.v18i5.2289.xml)
<hr>

### 10.5040/9781501338786.ch-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781501338786.ch-002)
- [Production](https://doi.org/10.5040/9781501338786.ch-002)
- [JSON](https://api.crossref.org/works/10.5040/9781501338786.ch-002)
- [XML](https://api.crossref.org/works/10.5040/9781501338786.ch-002.xml)
<hr>

### 10.15530/urtec-2017-2671253
- [Chooser POC](https://chooser.fly.dev/?doi=10.15530/urtec-2017-2671253)
- [Production](https://doi.org/10.15530/urtec-2017-2671253)
- [JSON](https://api.crossref.org/works/10.15530/urtec-2017-2671253)
- [XML](https://api.crossref.org/works/10.15530/urtec-2017-2671253.xml)
<hr>

### 10.1070/PU1970v012n06ABEH004054
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/PU1970v012n06ABEH004054)
- [Production](https://doi.org/10.1070/PU1970v012n06ABEH004054)
- [JSON](https://api.crossref.org/works/10.1070/PU1970v012n06ABEH004054)
- [XML](https://api.crossref.org/works/10.1070/PU1970v012n06ABEH004054.xml)
<hr>

### 10.18574/nyu/9781479871209.003.0007
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9781479871209.003.0007)
- [Production](https://doi.org/10.18574/nyu/9781479871209.003.0007)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9781479871209.003.0007)
- [XML](https://api.crossref.org/works/10.18574/nyu/9781479871209.003.0007.xml)
<hr>

### 10.1070/RM2004v059n06ABEH000799
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM2004v059n06ABEH000799)
- [Production](https://doi.org/10.1070/RM2004v059n06ABEH000799)
- [JSON](https://api.crossref.org/works/10.1070/RM2004v059n06ABEH000799)
- [XML](https://api.crossref.org/works/10.1070/RM2004v059n06ABEH000799.xml)
<hr>

### 10.15530/urtec-2018-2889846
- [Chooser POC](https://chooser.fly.dev/?doi=10.15530/urtec-2018-2889846)
- [Production](https://doi.org/10.15530/urtec-2018-2889846)
- [JSON](https://api.crossref.org/works/10.15530/urtec-2018-2889846)
- [XML](https://api.crossref.org/works/10.15530/urtec-2018-2889846.xml)
<hr>

### 10.2307/1854113
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1854113)
- [Production](https://doi.org/10.2307/1854113)
- [JSON](https://api.crossref.org/works/10.2307/1854113)
- [XML](https://api.crossref.org/works/10.2307/1854113.xml)
<hr>

### 10.18574/nyu/9781479829675.003.0002
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9781479829675.003.0002)
- [Production](https://doi.org/10.18574/nyu/9781479829675.003.0002)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9781479829675.003.0002)
- [XML](https://api.crossref.org/works/10.18574/nyu/9781479829675.003.0002.xml)
<hr>

### 10.1144/SP510-2020-82
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/SP510-2020-82)
- [Production](https://doi.org/10.1144/SP510-2020-82)
- [JSON](https://api.crossref.org/works/10.1144/SP510-2020-82)
- [XML](https://api.crossref.org/works/10.1144/SP510-2020-82.xml)
<hr>

### 10.5040/9781838711979.ch-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781838711979.ch-007)
- [Production](https://doi.org/10.5040/9781838711979.ch-007)
- [JSON](https://api.crossref.org/works/10.5040/9781838711979.ch-007)
- [XML](https://api.crossref.org/works/10.5040/9781838711979.ch-007.xml)
<hr>

### 10.5040/9781501348303.ch-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781501348303.ch-002)
- [Production](https://doi.org/10.5040/9781501348303.ch-002)
- [JSON](https://api.crossref.org/works/10.5040/9781501348303.ch-002)
- [XML](https://api.crossref.org/works/10.5040/9781501348303.ch-002.xml)
<hr>

### 10.2307/1850543
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1850543)
- [Production](https://doi.org/10.2307/1850543)
- [JSON](https://api.crossref.org/works/10.2307/1850543)
- [XML](https://api.crossref.org/works/10.2307/1850543.xml)
<hr>

### 10.18574/nyu/9780814799840.003.0005
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9780814799840.003.0005)
- [Production](https://doi.org/10.18574/nyu/9780814799840.003.0005)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9780814799840.003.0005)
- [XML](https://api.crossref.org/works/10.18574/nyu/9780814799840.003.0005.xml)
<hr>

### 10.18502/aacc.v5i2.750
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/aacc.v5i2.750)
- [Production](https://doi.org/10.18502/aacc.v5i2.750)
- [JSON](https://api.crossref.org/works/10.18502/aacc.v5i2.750)
- [XML](https://api.crossref.org/works/10.18502/aacc.v5i2.750.xml)
<hr>

### 10.1070/RM2002v057n04ABEH000539
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM2002v057n04ABEH000539)
- [Production](https://doi.org/10.1070/RM2002v057n04ABEH000539)
- [JSON](https://api.crossref.org/works/10.1070/RM2002v057n04ABEH000539)
- [XML](https://api.crossref.org/works/10.1070/RM2002v057n04ABEH000539.xml)
<hr>

### 10.1144/SP412.4
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/SP412.4)
- [Production](https://doi.org/10.1144/SP412.4)
- [JSON](https://api.crossref.org/works/10.1144/SP412.4)
- [XML](https://api.crossref.org/works/10.1144/SP412.4.xml)
<hr>

### 10.2307/1835875
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1835875)
- [Production](https://doi.org/10.2307/1835875)
- [JSON](https://api.crossref.org/works/10.2307/1835875)
- [XML](https://api.crossref.org/works/10.2307/1835875.xml)
<hr>

### 10.15530/urtec-2014-1917913
- [Chooser POC](https://chooser.fly.dev/?doi=10.15530/urtec-2014-1917913)
- [Production](https://doi.org/10.15530/urtec-2014-1917913)
- [JSON](https://api.crossref.org/works/10.15530/urtec-2014-1917913)
- [XML](https://api.crossref.org/works/10.15530/urtec-2014-1917913.xml)
<hr>

### 10.1070/RM1991v046n05ABEH002852
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1991v046n05ABEH002852)
- [Production](https://doi.org/10.1070/RM1991v046n05ABEH002852)
- [JSON](https://api.crossref.org/works/10.1070/RM1991v046n05ABEH002852)
- [XML](https://api.crossref.org/works/10.1070/RM1991v046n05ABEH002852.xml)
<hr>

### 10.1070/RM1978v033n02ABEH002418
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1978v033n02ABEH002418)
- [Production](https://doi.org/10.1070/RM1978v033n02ABEH002418)
- [JSON](https://api.crossref.org/works/10.1070/RM1978v033n02ABEH002418)
- [XML](https://api.crossref.org/works/10.1070/RM1978v033n02ABEH002418.xml)
<hr>

### 10.1023/A:1009095531464
- [Chooser POC](https://chooser.fly.dev/?doi=10.1023/A:1009095531464)
- [Production](https://doi.org/10.1023/A:1009095531464)
- [JSON](https://api.crossref.org/works/10.1023/A:1009095531464)
- [XML](https://api.crossref.org/works/10.1023/A:1009095531464.xml)
<hr>

### 10.18502/ijdo.v13i3.7188
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/ijdo.v13i3.7188)
- [Production](https://doi.org/10.18502/ijdo.v13i3.7188)
- [JSON](https://api.crossref.org/works/10.18502/ijdo.v13i3.7188)
- [XML](https://api.crossref.org/works/10.18502/ijdo.v13i3.7188.xml)
<hr>

### 10.2307/1865422
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1865422)
- [Production](https://doi.org/10.2307/1865422)
- [JSON](https://api.crossref.org/works/10.2307/1865422)
- [XML](https://api.crossref.org/works/10.2307/1865422.xml)
<hr>

### 10.15446/revfacmed.v62n3.42867
- [Chooser POC](https://chooser.fly.dev/?doi=10.15446/revfacmed.v62n3.42867)
- [Production](https://doi.org/10.15446/revfacmed.v62n3.42867)
- [JSON](https://api.crossref.org/works/10.15446/revfacmed.v62n3.42867)
- [XML](https://api.crossref.org/works/10.15446/revfacmed.v62n3.42867.xml)
<hr>

### 10.1144/GSL.SP.1987.033.01.08
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1987.033.01.08)
- [Production](https://doi.org/10.1144/GSL.SP.1987.033.01.08)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1987.033.01.08)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1987.033.01.08.xml)
<hr>

### 10.5935/0100-4042.20140143
- [Chooser POC](https://chooser.fly.dev/?doi=10.5935/0100-4042.20140143)
- [Production](https://doi.org/10.5935/0100-4042.20140143)
- [JSON](https://api.crossref.org/works/10.5935/0100-4042.20140143)
- [XML](https://api.crossref.org/works/10.5935/0100-4042.20140143.xml)
<hr>

### 10.1070/QE1999v029n06ABEH001532
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/QE1999v029n06ABEH001532)
- [Production](https://doi.org/10.1070/QE1999v029n06ABEH001532)
- [JSON](https://api.crossref.org/works/10.1070/QE1999v029n06ABEH001532)
- [XML](https://api.crossref.org/works/10.1070/QE1999v029n06ABEH001532.xml)
<hr>

### 10.2307/1859980
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1859980)
- [Production](https://doi.org/10.2307/1859980)
- [JSON](https://api.crossref.org/works/10.2307/1859980)
- [XML](https://api.crossref.org/works/10.2307/1859980.xml)
<hr>

### 10.2307/1866346
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1866346)
- [Production](https://doi.org/10.2307/1866346)
- [JSON](https://api.crossref.org/works/10.2307/1866346)
- [XML](https://api.crossref.org/works/10.2307/1866346.xml)
<hr>

### 10.2307/1841206
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1841206)
- [Production](https://doi.org/10.2307/1841206)
- [JSON](https://api.crossref.org/works/10.2307/1841206)
- [XML](https://api.crossref.org/works/10.2307/1841206.xml)
<hr>

### 10.2190/1GD5-FQD2-6E22-TEGY
- [Chooser POC](https://chooser.fly.dev/?doi=10.2190/1GD5-FQD2-6E22-TEGY)
- [Production](https://doi.org/10.2190/1GD5-FQD2-6E22-TEGY)
- [JSON](https://api.crossref.org/works/10.2190/1GD5-FQD2-6E22-TEGY)
- [XML](https://api.crossref.org/works/10.2190/1GD5-FQD2-6E22-TEGY.xml)
<hr>

### 10.2307/1853168
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1853168)
- [Production](https://doi.org/10.2307/1853168)
- [JSON](https://api.crossref.org/works/10.2307/1853168)
- [XML](https://api.crossref.org/works/10.2307/1853168.xml)
<hr>

### 10.1070/RM1992v047n02ABEH000890
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1992v047n02ABEH000890)
- [Production](https://doi.org/10.1070/RM1992v047n02ABEH000890)
- [JSON](https://api.crossref.org/works/10.1070/RM1992v047n02ABEH000890)
- [XML](https://api.crossref.org/works/10.1070/RM1992v047n02ABEH000890.xml)
<hr>

### 10.2307/2651766
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2651766)
- [Production](https://doi.org/10.2307/2651766)
- [JSON](https://api.crossref.org/works/10.2307/2651766)
- [XML](https://api.crossref.org/works/10.2307/2651766.xml)
<hr>

### 10.5040/9781501338786.ch-007
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781501338786.ch-007)
- [Production](https://doi.org/10.5040/9781501338786.ch-007)
- [JSON](https://api.crossref.org/works/10.5040/9781501338786.ch-007)
- [XML](https://api.crossref.org/works/10.5040/9781501338786.ch-007.xml)
<hr>

### 10.1596/31635
- [Chooser POC](https://chooser.fly.dev/?doi=10.1596/31635)
- [Production](https://doi.org/10.1596/31635)
- [JSON](https://api.crossref.org/works/10.1596/31635)
- [XML](https://api.crossref.org/works/10.1596/31635.xml)
<hr>

### 10.5949/UPO9781846314131.013
- [Chooser POC](https://chooser.fly.dev/?doi=10.5949/UPO9781846314131.013)
- [Production](https://doi.org/10.5949/UPO9781846314131.013)
- [JSON](https://api.crossref.org/works/10.5949/UPO9781846314131.013)
- [XML](https://api.crossref.org/works/10.5949/UPO9781846314131.013.xml)
<hr>

### 10.1093/law/9780199672202.003.0001
- [Chooser POC](https://chooser.fly.dev/?doi=10.1093/law/9780199672202.003.0001)
- [Production](https://doi.org/10.1093/law/9780199672202.003.0001)
- [JSON](https://api.crossref.org/works/10.1093/law/9780199672202.003.0001)
- [XML](https://api.crossref.org/works/10.1093/law/9780199672202.003.0001.xml)
<hr>

### 10.5040/9781350077027.0032
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781350077027.0032)
- [Production](https://doi.org/10.5040/9781350077027.0032)
- [JSON](https://api.crossref.org/works/10.5040/9781350077027.0032)
- [XML](https://api.crossref.org/works/10.5040/9781350077027.0032.xml)
<hr>

### 10.15446/cuad.econ.v34n65.49777
- [Chooser POC](https://chooser.fly.dev/?doi=10.15446/cuad.econ.v34n65.49777)
- [Production](https://doi.org/10.15446/cuad.econ.v34n65.49777)
- [JSON](https://api.crossref.org/works/10.15446/cuad.econ.v34n65.49777)
- [XML](https://api.crossref.org/works/10.15446/cuad.econ.v34n65.49777.xml)
<hr>

### 10.1049/icp.2021.1192
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/icp.2021.1192)
- [Production](https://doi.org/10.1049/icp.2021.1192)
- [JSON](https://api.crossref.org/works/10.1049/icp.2021.1192)
- [XML](https://api.crossref.org/works/10.1049/icp.2021.1192.xml)
<hr>

### 10.18574/nyu/9781479811274.003.0005
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9781479811274.003.0005)
- [Production](https://doi.org/10.18574/nyu/9781479811274.003.0005)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9781479811274.003.0005)
- [XML](https://api.crossref.org/works/10.18574/nyu/9781479811274.003.0005.xml)
<hr>

### 10.1144/GSL.SP.1993.074.01.35
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1993.074.01.35)
- [Production](https://doi.org/10.1144/GSL.SP.1993.074.01.35)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1993.074.01.35)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1993.074.01.35.xml)
<hr>

### 10.1190/1.9781560801795.ch4
- [Chooser POC](https://chooser.fly.dev/?doi=10.1190/1.9781560801795.ch4)
- [Production](https://doi.org/10.1190/1.9781560801795.ch4)
- [JSON](https://api.crossref.org/works/10.1190/1.9781560801795.ch4)
- [XML](https://api.crossref.org/works/10.1190/1.9781560801795.ch4.xml)
<hr>

### 10.4135/9781412973533.n21
- [Chooser POC](https://chooser.fly.dev/?doi=10.4135/9781412973533.n21)
- [Production](https://doi.org/10.4135/9781412973533.n21)
- [JSON](https://api.crossref.org/works/10.4135/9781412973533.n21)
- [XML](https://api.crossref.org/works/10.4135/9781412973533.n21.xml)
<hr>

### 10.2752/9781847880000/FASHSOC0011
- [Chooser POC](https://chooser.fly.dev/?doi=10.2752/9781847880000/FASHSOC0011)
- [Production](https://doi.org/10.2752/9781847880000/FASHSOC0011)
- [JSON](https://api.crossref.org/works/10.2752/9781847880000/FASHSOC0011)
- [XML](https://api.crossref.org/works/10.2752/9781847880000/FASHSOC0011.xml)
<hr>

### 10.2307/1873809
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1873809)
- [Production](https://doi.org/10.2307/1873809)
- [JSON](https://api.crossref.org/works/10.2307/1873809)
- [XML](https://api.crossref.org/works/10.2307/1873809.xml)
<hr>

### 10.1144/GSL.SP.1990.048.01.21
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1990.048.01.21)
- [Production](https://doi.org/10.1144/GSL.SP.1990.048.01.21)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1990.048.01.21)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1990.048.01.21.xml)
<hr>

### 10.1144/sjg14040273
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/sjg14040273)
- [Production](https://doi.org/10.1144/sjg14040273)
- [JSON](https://api.crossref.org/works/10.1144/sjg14040273)
- [XML](https://api.crossref.org/works/10.1144/sjg14040273.xml)
<hr>

### 10.3901/CJME.2002.03.199
- [Chooser POC](https://chooser.fly.dev/?doi=10.3901/CJME.2002.03.199)
- [Production](https://doi.org/10.3901/CJME.2002.03.199)
- [JSON](https://api.crossref.org/works/10.3901/CJME.2002.03.199)
- [XML](https://api.crossref.org/works/10.3901/CJME.2002.03.199.xml)
<hr>

### 10.1070/RM1995v050n05ABEH002613
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1995v050n05ABEH002613)
- [Production](https://doi.org/10.1070/RM1995v050n05ABEH002613)
- [JSON](https://api.crossref.org/works/10.1070/RM1995v050n05ABEH002613)
- [XML](https://api.crossref.org/works/10.1070/RM1995v050n05ABEH002613.xml)
<hr>

### 10.1070/RM1966v021n04ABEH004163
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1966v021n04ABEH004163)
- [Production](https://doi.org/10.1070/RM1966v021n04ABEH004163)
- [JSON](https://api.crossref.org/works/10.1070/RM1966v021n04ABEH004163)
- [XML](https://api.crossref.org/works/10.1070/RM1966v021n04ABEH004163.xml)
<hr>

### 10.5040/9781350017191.ch-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781350017191.ch-008)
- [Production](https://doi.org/10.5040/9781350017191.ch-008)
- [JSON](https://api.crossref.org/works/10.5040/9781350017191.ch-008)
- [XML](https://api.crossref.org/works/10.5040/9781350017191.ch-008.xml)
<hr>

### 10.5040/9781474206617.ch-008
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781474206617.ch-008)
- [Production](https://doi.org/10.5040/9781474206617.ch-008)
- [JSON](https://api.crossref.org/works/10.5040/9781474206617.ch-008)
- [XML](https://api.crossref.org/works/10.5040/9781474206617.ch-008.xml)
<hr>

### 10.18574/nyu/9780814707333.003.0002
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9780814707333.003.0002)
- [Production](https://doi.org/10.18574/nyu/9780814707333.003.0002)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9780814707333.003.0002)
- [XML](https://api.crossref.org/works/10.18574/nyu/9780814707333.003.0002.xml)
<hr>

### 10.7868/S0869565214020224
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0869565214020224)
- [Production](https://doi.org/10.7868/S0869565214020224)
- [JSON](https://api.crossref.org/works/10.7868/S0869565214020224)
- [XML](https://api.crossref.org/works/10.7868/S0869565214020224.xml)
<hr>

### 10.2307/1845280
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1845280)
- [Production](https://doi.org/10.2307/1845280)
- [JSON](https://api.crossref.org/works/10.2307/1845280)
- [XML](https://api.crossref.org/works/10.2307/1845280.xml)
<hr>

### 10.4041/kjod.2010.40.5.325
- [Chooser POC](https://chooser.fly.dev/?doi=10.4041/kjod.2010.40.5.325)
- [Production](https://doi.org/10.4041/kjod.2010.40.5.325)
- [JSON](https://api.crossref.org/works/10.4041/kjod.2010.40.5.325)
- [XML](https://api.crossref.org/works/10.4041/kjod.2010.40.5.325.xml)
<hr>

### 10.1070/SM1968v005n02ABEH001005
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/SM1968v005n02ABEH001005)
- [Production](https://doi.org/10.1070/SM1968v005n02ABEH001005)
- [JSON](https://api.crossref.org/works/10.1070/SM1968v005n02ABEH001005)
- [XML](https://api.crossref.org/works/10.1070/SM1968v005n02ABEH001005.xml)
<hr>

### 10.2307/1846745
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1846745)
- [Production](https://doi.org/10.2307/1846745)
- [JSON](https://api.crossref.org/works/10.2307/1846745)
- [XML](https://api.crossref.org/works/10.2307/1846745.xml)
<hr>

### 10.1070/PU1967v009n04ABEH003004
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/PU1967v009n04ABEH003004)
- [Production](https://doi.org/10.1070/PU1967v009n04ABEH003004)
- [JSON](https://api.crossref.org/works/10.1070/PU1967v009n04ABEH003004)
- [XML](https://api.crossref.org/works/10.1070/PU1967v009n04ABEH003004.xml)
<hr>

### 10.7765/9781526142245.00009
- [Chooser POC](https://chooser.fly.dev/?doi=10.7765/9781526142245.00009)
- [Production](https://doi.org/10.7765/9781526142245.00009)
- [JSON](https://api.crossref.org/works/10.7765/9781526142245.00009)
- [XML](https://api.crossref.org/works/10.7765/9781526142245.00009.xml)
<hr>

### 10.2307/1851912
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1851912)
- [Production](https://doi.org/10.2307/1851912)
- [JSON](https://api.crossref.org/works/10.2307/1851912)
- [XML](https://api.crossref.org/works/10.2307/1851912.xml)
<hr>

### 10.15446/abc.v20n2.42307
- [Chooser POC](https://chooser.fly.dev/?doi=10.15446/abc.v20n2.42307)
- [Production](https://doi.org/10.15446/abc.v20n2.42307)
- [JSON](https://api.crossref.org/works/10.15446/abc.v20n2.42307)
- [XML](https://api.crossref.org/works/10.15446/abc.v20n2.42307.xml)
<hr>

### 10.2110/jsr.2012.7
- [Chooser POC](https://chooser.fly.dev/?doi=10.2110/jsr.2012.7)
- [Production](https://doi.org/10.2110/jsr.2012.7)
- [JSON](https://api.crossref.org/works/10.2110/jsr.2012.7)
- [XML](https://api.crossref.org/works/10.2110/jsr.2012.7.xml)
<hr>

### 10.2307/2169550
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2169550)
- [Production](https://doi.org/10.2307/2169550)
- [JSON](https://api.crossref.org/works/10.2307/2169550)
- [XML](https://api.crossref.org/works/10.2307/2169550.xml)
<hr>

### 10.1134/S2079562913120026
- [Chooser POC](https://chooser.fly.dev/?doi=10.1134/S2079562913120026)
- [Production](https://doi.org/10.1134/S2079562913120026)
- [JSON](https://api.crossref.org/works/10.1134/S2079562913120026)
- [XML](https://api.crossref.org/works/10.1134/S2079562913120026.xml)
<hr>

### 10.7765/9781526145970.00010
- [Chooser POC](https://chooser.fly.dev/?doi=10.7765/9781526145970.00010)
- [Production](https://doi.org/10.7765/9781526145970.00010)
- [JSON](https://api.crossref.org/works/10.7765/9781526145970.00010)
- [XML](https://api.crossref.org/works/10.7765/9781526145970.00010.xml)
<hr>

### 10.15446/rev.colomb.biote.v21n1.80860
- [Chooser POC](https://chooser.fly.dev/?doi=10.15446/rev.colomb.biote.v21n1.80860)
- [Production](https://doi.org/10.15446/rev.colomb.biote.v21n1.80860)
- [JSON](https://api.crossref.org/works/10.15446/rev.colomb.biote.v21n1.80860)
- [XML](https://api.crossref.org/works/10.15446/rev.colomb.biote.v21n1.80860.xml)
<hr>

### 10.2307/1904626
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1904626)
- [Production](https://doi.org/10.2307/1904626)
- [JSON](https://api.crossref.org/works/10.2307/1904626)
- [XML](https://api.crossref.org/works/10.2307/1904626.xml)
<hr>

### 10.2307/2651184
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2651184)
- [Production](https://doi.org/10.2307/2651184)
- [JSON](https://api.crossref.org/works/10.2307/2651184)
- [XML](https://api.crossref.org/works/10.2307/2651184.xml)
<hr>

### 10.1049/cp.2012.0171
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp.2012.0171)
- [Production](https://doi.org/10.1049/cp.2012.0171)
- [JSON](https://api.crossref.org/works/10.1049/cp.2012.0171)
- [XML](https://api.crossref.org/works/10.1049/cp.2012.0171.xml)
<hr>

### 10.7868/S0367059713020121
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0367059713020121)
- [Production](https://doi.org/10.7868/S0367059713020121)
- [JSON](https://api.crossref.org/works/10.7868/S0367059713020121)
- [XML](https://api.crossref.org/works/10.7868/S0367059713020121.xml)
<hr>

### 10.2113/gsecongeo.17.2.132
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.17.2.132)
- [Production](https://doi.org/10.2113/gsecongeo.17.2.132)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.17.2.132)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.17.2.132.xml)
<hr>

### 10.7868/S0044453713070078
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0044453713070078)
- [Production](https://doi.org/10.7868/S0044453713070078)
- [JSON](https://api.crossref.org/works/10.7868/S0044453713070078)
- [XML](https://api.crossref.org/works/10.7868/S0044453713070078.xml)
<hr>

### 10.3343/lmo.2016.6.4.193
- [Chooser POC](https://chooser.fly.dev/?doi=10.3343/lmo.2016.6.4.193)
- [Production](https://doi.org/10.3343/lmo.2016.6.4.193)
- [JSON](https://api.crossref.org/works/10.3343/lmo.2016.6.4.193)
- [XML](https://api.crossref.org/works/10.3343/lmo.2016.6.4.193.xml)
<hr>

### 10.1070/PU1995v038n06ABEH001475
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/PU1995v038n06ABEH001475)
- [Production](https://doi.org/10.1070/PU1995v038n06ABEH001475)
- [JSON](https://api.crossref.org/works/10.1070/PU1995v038n06ABEH001475)
- [XML](https://api.crossref.org/works/10.1070/PU1995v038n06ABEH001475.xml)
<hr>

### 10.2307/1870472
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1870472)
- [Production](https://doi.org/10.2307/1870472)
- [JSON](https://api.crossref.org/works/10.2307/1870472)
- [XML](https://api.crossref.org/works/10.2307/1870472.xml)
<hr>

### 10.1093/law/9780198848639.003.0033
- [Chooser POC](https://chooser.fly.dev/?doi=10.1093/law/9780198848639.003.0033)
- [Production](https://doi.org/10.1093/law/9780198848639.003.0033)
- [JSON](https://api.crossref.org/works/10.1093/law/9780198848639.003.0033)
- [XML](https://api.crossref.org/works/10.1093/law/9780198848639.003.0033.xml)
<hr>

### 10.2113/gsecongeo.5.2.134
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.5.2.134)
- [Production](https://doi.org/10.2113/gsecongeo.5.2.134)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.5.2.134)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.5.2.134.xml)
<hr>

### 10.5040/9781350123908.ch-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781350123908.ch-003)
- [Production](https://doi.org/10.5040/9781350123908.ch-003)
- [JSON](https://api.crossref.org/works/10.5040/9781350123908.ch-003)
- [XML](https://api.crossref.org/works/10.5040/9781350123908.ch-003.xml)
<hr>

### 10.3901/CJME.2005.02.163
- [Chooser POC](https://chooser.fly.dev/?doi=10.3901/CJME.2005.02.163)
- [Production](https://doi.org/10.3901/CJME.2005.02.163)
- [JSON](https://api.crossref.org/works/10.3901/CJME.2005.02.163)
- [XML](https://api.crossref.org/works/10.3901/CJME.2005.02.163.xml)
<hr>

### 10.7868/S0869565213200310
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0869565213200310)
- [Production](https://doi.org/10.7868/S0869565213200310)
- [JSON](https://api.crossref.org/works/10.7868/S0869565213200310)
- [XML](https://api.crossref.org/works/10.7868/S0869565213200310.xml)
<hr>

### 10.2307/1867131
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1867131)
- [Production](https://doi.org/10.2307/1867131)
- [JSON](https://api.crossref.org/works/10.2307/1867131)
- [XML](https://api.crossref.org/works/10.2307/1867131.xml)
<hr>

### 10.2307/1837523
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1837523)
- [Production](https://doi.org/10.2307/1837523)
- [JSON](https://api.crossref.org/works/10.2307/1837523)
- [XML](https://api.crossref.org/works/10.2307/1837523.xml)
<hr>

### 10.7868/S0869587313100034
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0869587313100034)
- [Production](https://doi.org/10.7868/S0869587313100034)
- [JSON](https://api.crossref.org/works/10.7868/S0869587313100034)
- [XML](https://api.crossref.org/works/10.7868/S0869587313100034.xml)
<hr>

### 10.2110/jsr.277
- [Chooser POC](https://chooser.fly.dev/?doi=10.2110/jsr.277)
- [Production](https://doi.org/10.2110/jsr.277)
- [JSON](https://api.crossref.org/works/10.2110/jsr.277)
- [XML](https://api.crossref.org/works/10.2110/jsr.277.xml)
<hr>

### 10.2113/gsecongeo.67.3.281
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.67.3.281)
- [Production](https://doi.org/10.2113/gsecongeo.67.3.281)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.67.3.281)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.67.3.281.xml)
<hr>

### 10.1124/mi.4.1.16
- [Chooser POC](https://chooser.fly.dev/?doi=10.1124/mi.4.1.16)
- [Production](https://doi.org/10.1124/mi.4.1.16)
- [JSON](https://api.crossref.org/works/10.1124/mi.4.1.16)
- [XML](https://api.crossref.org/works/10.1124/mi.4.1.16.xml)
<hr>

### 10.1049/cp.2017.0336
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp.2017.0336)
- [Production](https://doi.org/10.1049/cp.2017.0336)
- [JSON](https://api.crossref.org/works/10.1049/cp.2017.0336)
- [XML](https://api.crossref.org/works/10.1049/cp.2017.0336.xml)
<hr>

### 10.1070/QE1976v006n03ABEH011104
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/QE1976v006n03ABEH011104)
- [Production](https://doi.org/10.1070/QE1976v006n03ABEH011104)
- [JSON](https://api.crossref.org/works/10.1070/QE1976v006n03ABEH011104)
- [XML](https://api.crossref.org/works/10.1070/QE1976v006n03ABEH011104.xml)
<hr>

### 10.2190/1JK6-QWGM-RFK8-V1KP
- [Chooser POC](https://chooser.fly.dev/?doi=10.2190/1JK6-QWGM-RFK8-V1KP)
- [Production](https://doi.org/10.2190/1JK6-QWGM-RFK8-V1KP)
- [JSON](https://api.crossref.org/works/10.2190/1JK6-QWGM-RFK8-V1KP)
- [XML](https://api.crossref.org/works/10.2190/1JK6-QWGM-RFK8-V1KP.xml)
<hr>

### 10.1049/icp.2022.0647
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/icp.2022.0647)
- [Production](https://doi.org/10.1049/icp.2022.0647)
- [JSON](https://api.crossref.org/works/10.1049/icp.2022.0647)
- [XML](https://api.crossref.org/works/10.1049/icp.2022.0647.xml)
<hr>

### 10.1049/icp.2021.0482
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/icp.2021.0482)
- [Production](https://doi.org/10.1049/icp.2021.0482)
- [JSON](https://api.crossref.org/works/10.1049/icp.2021.0482)
- [XML](https://api.crossref.org/works/10.1049/icp.2021.0482.xml)
<hr>

### 10.1070/RM1991v046n01ABEH002721
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1991v046n01ABEH002721)
- [Production](https://doi.org/10.1070/RM1991v046n01ABEH002721)
- [JSON](https://api.crossref.org/works/10.1070/RM1991v046n01ABEH002721)
- [XML](https://api.crossref.org/works/10.1070/RM1991v046n01ABEH002721.xml)
<hr>

### 10.5040/9781350006195.ch-020
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781350006195.ch-020)
- [Production](https://doi.org/10.5040/9781350006195.ch-020)
- [JSON](https://api.crossref.org/works/10.5040/9781350006195.ch-020)
- [XML](https://api.crossref.org/works/10.5040/9781350006195.ch-020.xml)
<hr>

### 10.1070/RM1996v051n02ABEH002889
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1996v051n02ABEH002889)
- [Production](https://doi.org/10.1070/RM1996v051n02ABEH002889)
- [JSON](https://api.crossref.org/works/10.1070/RM1996v051n02ABEH002889)
- [XML](https://api.crossref.org/works/10.1070/RM1996v051n02ABEH002889.xml)
<hr>

### 10.2307/1866640
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1866640)
- [Production](https://doi.org/10.2307/1866640)
- [JSON](https://api.crossref.org/works/10.2307/1866640)
- [XML](https://api.crossref.org/works/10.2307/1866640.xml)
<hr>

### 10.2307/1862660
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1862660)
- [Production](https://doi.org/10.2307/1862660)
- [JSON](https://api.crossref.org/works/10.2307/1862660)
- [XML](https://api.crossref.org/works/10.2307/1862660.xml)
<hr>

### 10.7868/S0321059613060126
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0321059613060126)
- [Production](https://doi.org/10.7868/S0321059613060126)
- [JSON](https://api.crossref.org/works/10.7868/S0321059613060126)
- [XML](https://api.crossref.org/works/10.7868/S0321059613060126.xml)
<hr>

### 10.2113/gsecongeo.29.4.388
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.29.4.388)
- [Production](https://doi.org/10.2113/gsecongeo.29.4.388)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.29.4.388)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.29.4.388.xml)
<hr>

### 10.7868/S036767651309038X
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S036767651309038X)
- [Production](https://doi.org/10.7868/S036767651309038X)
- [JSON](https://api.crossref.org/works/10.7868/S036767651309038X)
- [XML](https://api.crossref.org/works/10.7868/S036767651309038X.xml)
<hr>

### 10.1007/978-94-6209-257-0
- [Chooser POC](https://chooser.fly.dev/?doi=10.1007/978-94-6209-257-0)
- [Production](https://doi.org/10.1007/978-94-6209-257-0)
- [JSON](https://api.crossref.org/works/10.1007/978-94-6209-257-0)
- [XML](https://api.crossref.org/works/10.1007/978-94-6209-257-0.xml)
<hr>

### 10.2307/1859715
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1859715)
- [Production](https://doi.org/10.2307/1859715)
- [JSON](https://api.crossref.org/works/10.2307/1859715)
- [XML](https://api.crossref.org/works/10.2307/1859715.xml)
<hr>

### 10.2307/1860587
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1860587)
- [Production](https://doi.org/10.2307/1860587)
- [JSON](https://api.crossref.org/works/10.2307/1860587)
- [XML](https://api.crossref.org/works/10.2307/1860587.xml)
<hr>

### 10.5040/9781472527912.ch-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781472527912.ch-009)
- [Production](https://doi.org/10.5040/9781472527912.ch-009)
- [JSON](https://api.crossref.org/works/10.5040/9781472527912.ch-009)
- [XML](https://api.crossref.org/works/10.5040/9781472527912.ch-009.xml)
<hr>

### 10.18574/nyu/9781479819850.003.0003
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9781479819850.003.0003)
- [Production](https://doi.org/10.18574/nyu/9781479819850.003.0003)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9781479819850.003.0003)
- [XML](https://api.crossref.org/works/10.18574/nyu/9781479819850.003.0003.xml)
<hr>

### 10.2190/5B7H-6M75-K898-UFHF
- [Chooser POC](https://chooser.fly.dev/?doi=10.2190/5B7H-6M75-K898-UFHF)
- [Production](https://doi.org/10.2190/5B7H-6M75-K898-UFHF)
- [JSON](https://api.crossref.org/works/10.2190/5B7H-6M75-K898-UFHF)
- [XML](https://api.crossref.org/works/10.2190/5B7H-6M75-K898-UFHF.xml)
<hr>

### 10.4135/9781848607934.n47
- [Chooser POC](https://chooser.fly.dev/?doi=10.4135/9781848607934.n47)
- [Production](https://doi.org/10.4135/9781848607934.n47)
- [JSON](https://api.crossref.org/works/10.4135/9781848607934.n47)
- [XML](https://api.crossref.org/works/10.4135/9781848607934.n47.xml)
<hr>

### 10.18502/bccr.v11i3.5718
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/bccr.v11i3.5718)
- [Production](https://doi.org/10.18502/bccr.v11i3.5718)
- [JSON](https://api.crossref.org/works/10.18502/bccr.v11i3.5718)
- [XML](https://api.crossref.org/works/10.18502/bccr.v11i3.5718.xml)
<hr>

### 10.2307/1904766
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1904766)
- [Production](https://doi.org/10.2307/1904766)
- [JSON](https://api.crossref.org/works/10.2307/1904766)
- [XML](https://api.crossref.org/works/10.2307/1904766.xml)
<hr>

### 10.1070/QEL17259
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/QEL17259)
- [Production](https://doi.org/10.1070/QEL17259)
- [JSON](https://api.crossref.org/works/10.1070/QEL17259)
- [XML](https://api.crossref.org/works/10.1070/QEL17259.xml)
<hr>

### 10.1353/emw.2020.0041
- [Chooser POC](https://chooser.fly.dev/?doi=10.1353/emw.2020.0041)
- [Production](https://doi.org/10.1353/emw.2020.0041)
- [JSON](https://api.crossref.org/works/10.1353/emw.2020.0041)
- [XML](https://api.crossref.org/works/10.1353/emw.2020.0041.xml)
<hr>

### 10.1049/et.2017.0439
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/et.2017.0439)
- [Production](https://doi.org/10.1049/et.2017.0439)
- [JSON](https://api.crossref.org/works/10.1049/et.2017.0439)
- [XML](https://api.crossref.org/works/10.1049/et.2017.0439.xml)
<hr>

### 10.1306/D42696C0-2B26-11D7-8648000102C1865D
- [Chooser POC](https://chooser.fly.dev/?doi=10.1306/D42696C0-2B26-11D7-8648000102C1865D)
- [Production](https://doi.org/10.1306/D42696C0-2B26-11D7-8648000102C1865D)
- [JSON](https://api.crossref.org/works/10.1306/D42696C0-2B26-11D7-8648000102C1865D)
- [XML](https://api.crossref.org/works/10.1306/D42696C0-2B26-11D7-8648000102C1865D.xml)
<hr>

### 10.5935/0100-4042.20140240
- [Chooser POC](https://chooser.fly.dev/?doi=10.5935/0100-4042.20140240)
- [Production](https://doi.org/10.5935/0100-4042.20140240)
- [JSON](https://api.crossref.org/works/10.5935/0100-4042.20140240)
- [XML](https://api.crossref.org/works/10.5935/0100-4042.20140240.xml)
<hr>

### 10.2752/9780857854032/BODRESS0012
- [Chooser POC](https://chooser.fly.dev/?doi=10.2752/9780857854032/BODRESS0012)
- [Production](https://doi.org/10.2752/9780857854032/BODRESS0012)
- [JSON](https://api.crossref.org/works/10.2752/9780857854032/BODRESS0012)
- [XML](https://api.crossref.org/works/10.2752/9780857854032/BODRESS0012.xml)
<hr>

### 10.1134/S2079562913070087
- [Chooser POC](https://chooser.fly.dev/?doi=10.1134/S2079562913070087)
- [Production](https://doi.org/10.1134/S2079562913070087)
- [JSON](https://api.crossref.org/works/10.1134/S2079562913070087)
- [XML](https://api.crossref.org/works/10.1134/S2079562913070087.xml)
<hr>

### 10.2752/9781847888815/OCNL0023
- [Chooser POC](https://chooser.fly.dev/?doi=10.2752/9781847888815/OCNL0023)
- [Production](https://doi.org/10.2752/9781847888815/OCNL0023)
- [JSON](https://api.crossref.org/works/10.2752/9781847888815/OCNL0023)
- [XML](https://api.crossref.org/works/10.2752/9781847888815/OCNL0023.xml)
<hr>

### 10.15530/urtec-2017-2673849
- [Chooser POC](https://chooser.fly.dev/?doi=10.15530/urtec-2017-2673849)
- [Production](https://doi.org/10.15530/urtec-2017-2673849)
- [JSON](https://api.crossref.org/works/10.15530/urtec-2017-2673849)
- [XML](https://api.crossref.org/works/10.15530/urtec-2017-2673849.xml)
<hr>

### 10.1353/emw.2017.0051
- [Chooser POC](https://chooser.fly.dev/?doi=10.1353/emw.2017.0051)
- [Production](https://doi.org/10.1353/emw.2017.0051)
- [JSON](https://api.crossref.org/works/10.1353/emw.2017.0051)
- [XML](https://api.crossref.org/works/10.1353/emw.2017.0051.xml)
<hr>

### 10.2307/1868857
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1868857)
- [Production](https://doi.org/10.2307/1868857)
- [JSON](https://api.crossref.org/works/10.2307/1868857)
- [XML](https://api.crossref.org/works/10.2307/1868857.xml)
<hr>

### 10.2307/1838197
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1838197)
- [Production](https://doi.org/10.2307/1838197)
- [JSON](https://api.crossref.org/works/10.2307/1838197)
- [XML](https://api.crossref.org/works/10.2307/1838197.xml)
<hr>

### 10.1190/1.9781560802709.ch2
- [Chooser POC](https://chooser.fly.dev/?doi=10.1190/1.9781560802709.ch2)
- [Production](https://doi.org/10.1190/1.9781560802709.ch2)
- [JSON](https://api.crossref.org/works/10.1190/1.9781560802709.ch2)
- [XML](https://api.crossref.org/works/10.1190/1.9781560802709.ch2.xml)
<hr>

### 10.2307/1870728
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1870728)
- [Production](https://doi.org/10.2307/1870728)
- [JSON](https://api.crossref.org/works/10.2307/1870728)
- [XML](https://api.crossref.org/works/10.2307/1870728.xml)
<hr>

### 10.1785/0120140185
- [Chooser POC](https://chooser.fly.dev/?doi=10.1785/0120140185)
- [Production](https://doi.org/10.1785/0120140185)
- [JSON](https://api.crossref.org/works/10.1785/0120140185)
- [XML](https://api.crossref.org/works/10.1785/0120140185.xml)
<hr>

### 10.2307/1859995
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1859995)
- [Production](https://doi.org/10.2307/1859995)
- [JSON](https://api.crossref.org/works/10.2307/1859995)
- [XML](https://api.crossref.org/works/10.2307/1859995.xml)
<hr>

### 10.2752/9781847888990/WWBM0010
- [Chooser POC](https://chooser.fly.dev/?doi=10.2752/9781847888990/WWBM0010)
- [Production](https://doi.org/10.2752/9781847888990/WWBM0010)
- [JSON](https://api.crossref.org/works/10.2752/9781847888990/WWBM0010)
- [XML](https://api.crossref.org/works/10.2752/9781847888990/WWBM0010.xml)
<hr>

### 10.1144/GSL.SP.1999.168.01.13
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1999.168.01.13)
- [Production](https://doi.org/10.1144/GSL.SP.1999.168.01.13)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1999.168.01.13)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1999.168.01.13.xml)
<hr>

### 10.1049/et.2017.0515
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/et.2017.0515)
- [Production](https://doi.org/10.1049/et.2017.0515)
- [JSON](https://api.crossref.org/works/10.1049/et.2017.0515)
- [XML](https://api.crossref.org/works/10.1049/et.2017.0515.xml)
<hr>

### 10.2113/gsecongeo.84.8.2307
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.84.8.2307)
- [Production](https://doi.org/10.2113/gsecongeo.84.8.2307)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.84.8.2307)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.84.8.2307.xml)
<hr>

### 10.7868/S000233291304005X
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S000233291304005X)
- [Production](https://doi.org/10.7868/S000233291304005X)
- [JSON](https://api.crossref.org/works/10.7868/S000233291304005X)
- [XML](https://api.crossref.org/works/10.7868/S000233291304005X.xml)
<hr>

### 10.2307/1862491
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1862491)
- [Production](https://doi.org/10.2307/1862491)
- [JSON](https://api.crossref.org/works/10.2307/1862491)
- [XML](https://api.crossref.org/works/10.2307/1862491.xml)
<hr>

### 10.2113/gsecongeo.12.8.643
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.12.8.643)
- [Production](https://doi.org/10.2113/gsecongeo.12.8.643)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.12.8.643)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.12.8.643.xml)
<hr>

### 10.2113/gsecongeo.21.1.70
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.21.1.70)
- [Production](https://doi.org/10.2113/gsecongeo.21.1.70)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.21.1.70)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.21.1.70.xml)
<hr>

### 10.1353/sce.2012.0023
- [Chooser POC](https://chooser.fly.dev/?doi=10.1353/sce.2012.0023)
- [Production](https://doi.org/10.1353/sce.2012.0023)
- [JSON](https://api.crossref.org/works/10.1353/sce.2012.0023)
- [XML](https://api.crossref.org/works/10.1353/sce.2012.0023.xml)
<hr>

### 10.1177/1077800405284363
- [Chooser POC](https://chooser.fly.dev/?doi=10.1177/1077800405284363)
- [Production](https://doi.org/10.1177/1077800405284363)
- [JSON](https://api.crossref.org/works/10.1177/1077800405284363)
- [XML](https://api.crossref.org/works/10.1177/1077800405284363.xml)
<hr>

### 10.2307/1856794
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1856794)
- [Production](https://doi.org/10.2307/1856794)
- [JSON](https://api.crossref.org/works/10.2307/1856794)
- [XML](https://api.crossref.org/works/10.2307/1856794.xml)
<hr>

### 10.7868/S0869565214080283
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0869565214080283)
- [Production](https://doi.org/10.7868/S0869565214080283)
- [JSON](https://api.crossref.org/works/10.7868/S0869565214080283)
- [XML](https://api.crossref.org/works/10.7868/S0869565214080283.xml)
<hr>

### 10.1070/RM1985v040n02ABEH003573
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1985v040n02ABEH003573)
- [Production](https://doi.org/10.1070/RM1985v040n02ABEH003573)
- [JSON](https://api.crossref.org/works/10.1070/RM1985v040n02ABEH003573)
- [XML](https://api.crossref.org/works/10.1070/RM1985v040n02ABEH003573.xml)
<hr>

### 10.5040/9781474207065.ch-005
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781474207065.ch-005)
- [Production](https://doi.org/10.5040/9781474207065.ch-005)
- [JSON](https://api.crossref.org/works/10.5040/9781474207065.ch-005)
- [XML](https://api.crossref.org/works/10.5040/9781474207065.ch-005.xml)
<hr>

### 10.1144/GSL.SP.2007.275.01.15
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.2007.275.01.15)
- [Production](https://doi.org/10.1144/GSL.SP.2007.275.01.15)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.2007.275.01.15)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.2007.275.01.15.xml)
<hr>

### 10.1070/RCR5025
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RCR5025)
- [Production](https://doi.org/10.1070/RCR5025)
- [JSON](https://api.crossref.org/works/10.1070/RCR5025)
- [XML](https://api.crossref.org/works/10.1070/RCR5025.xml)
<hr>

### 10.1596/31116
- [Chooser POC](https://chooser.fly.dev/?doi=10.1596/31116)
- [Production](https://doi.org/10.1596/31116)
- [JSON](https://api.crossref.org/works/10.1596/31116)
- [XML](https://api.crossref.org/works/10.1596/31116.xml)
<hr>

### 10.2307/1868356
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1868356)
- [Production](https://doi.org/10.2307/1868356)
- [JSON](https://api.crossref.org/works/10.2307/1868356)
- [XML](https://api.crossref.org/works/10.2307/1868356.xml)
<hr>

### 10.2307/2170862
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2170862)
- [Production](https://doi.org/10.2307/2170862)
- [JSON](https://api.crossref.org/works/10.2307/2170862)
- [XML](https://api.crossref.org/works/10.2307/2170862.xml)
<hr>

### 10.2113/gsecongeo.14.5.424
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.14.5.424)
- [Production](https://doi.org/10.2113/gsecongeo.14.5.424)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.14.5.424)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.14.5.424.xml)
<hr>

### 10.7868/S0869565213360267
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0869565213360267)
- [Production](https://doi.org/10.7868/S0869565213360267)
- [JSON](https://api.crossref.org/works/10.7868/S0869565213360267)
- [XML](https://api.crossref.org/works/10.7868/S0869565213360267.xml)
<hr>

### 10.2307/1833995
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1833995)
- [Production](https://doi.org/10.2307/1833995)
- [JSON](https://api.crossref.org/works/10.2307/1833995)
- [XML](https://api.crossref.org/works/10.2307/1833995.xml)
<hr>

### 10.15530/urtec-2020-2818
- [Chooser POC](https://chooser.fly.dev/?doi=10.15530/urtec-2020-2818)
- [Production](https://doi.org/10.15530/urtec-2020-2818)
- [JSON](https://api.crossref.org/works/10.15530/urtec-2020-2818)
- [XML](https://api.crossref.org/works/10.15530/urtec-2020-2818.xml)
<hr>

### 10.18502/pbr.v6i2.3803
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/pbr.v6i2.3803)
- [Production](https://doi.org/10.18502/pbr.v6i2.3803)
- [JSON](https://api.crossref.org/works/10.18502/pbr.v6i2.3803)
- [XML](https://api.crossref.org/works/10.18502/pbr.v6i2.3803.xml)
<hr>

### 10.18502/tbj.v19i6.5707
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/tbj.v19i6.5707)
- [Production](https://doi.org/10.18502/tbj.v19i6.5707)
- [JSON](https://api.crossref.org/works/10.18502/tbj.v19i6.5707)
- [XML](https://api.crossref.org/works/10.18502/tbj.v19i6.5707.xml)
<hr>

### 10.1070/RM1995v050n04ABEH002587
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1995v050n04ABEH002587)
- [Production](https://doi.org/10.1070/RM1995v050n04ABEH002587)
- [JSON](https://api.crossref.org/works/10.1070/RM1995v050n04ABEH002587)
- [XML](https://api.crossref.org/works/10.1070/RM1995v050n04ABEH002587.xml)
<hr>

### 10.7868/S0044457X13030185
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0044457X13030185)
- [Production](https://doi.org/10.7868/S0044457X13030185)
- [JSON](https://api.crossref.org/works/10.7868/S0044457X13030185)
- [XML](https://api.crossref.org/works/10.7868/S0044457X13030185.xml)
<hr>

### 10.1049/cp.2016.0545
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp.2016.0545)
- [Production](https://doi.org/10.1049/cp.2016.0545)
- [JSON](https://api.crossref.org/works/10.1049/cp.2016.0545)
- [XML](https://api.crossref.org/works/10.1049/cp.2016.0545.xml)
<hr>

### 10.1049/et.2020.0319
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/et.2020.0319)
- [Production](https://doi.org/10.1049/et.2020.0319)
- [JSON](https://api.crossref.org/works/10.1049/et.2020.0319)
- [XML](https://api.crossref.org/works/10.1049/et.2020.0319.xml)
<hr>

### 10.2307/2164401
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2164401)
- [Production](https://doi.org/10.2307/2164401)
- [JSON](https://api.crossref.org/works/10.2307/2164401)
- [XML](https://api.crossref.org/works/10.2307/2164401.xml)
<hr>

### 10.2307/1857271
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1857271)
- [Production](https://doi.org/10.2307/1857271)
- [JSON](https://api.crossref.org/works/10.2307/1857271)
- [XML](https://api.crossref.org/works/10.2307/1857271.xml)
<hr>

### 10.1134/S0040363613100032
- [Chooser POC](https://chooser.fly.dev/?doi=10.1134/S0040363613100032)
- [Production](https://doi.org/10.1134/S0040363613100032)
- [JSON](https://api.crossref.org/works/10.1134/S0040363613100032)
- [XML](https://api.crossref.org/works/10.1134/S0040363613100032.xml)
<hr>

### 10.1144/GSL.SP.1988.039.01.32
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1988.039.01.32)
- [Production](https://doi.org/10.1144/GSL.SP.1988.039.01.32)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1988.039.01.32)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1988.039.01.32.xml)
<hr>

### 10.2307/1867871
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1867871)
- [Production](https://doi.org/10.2307/1867871)
- [JSON](https://api.crossref.org/works/10.2307/1867871)
- [XML](https://api.crossref.org/works/10.2307/1867871.xml)
<hr>

### 10.2307/1846833
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1846833)
- [Production](https://doi.org/10.2307/1846833)
- [JSON](https://api.crossref.org/works/10.2307/1846833)
- [XML](https://api.crossref.org/works/10.2307/1846833.xml)
<hr>

### 10.2307/2166066
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2166066)
- [Production](https://doi.org/10.2307/2166066)
- [JSON](https://api.crossref.org/works/10.2307/2166066)
- [XML](https://api.crossref.org/works/10.2307/2166066.xml)
<hr>

### 10.1070/PU2006v049n05ABEH005863
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/PU2006v049n05ABEH005863)
- [Production](https://doi.org/10.1070/PU2006v049n05ABEH005863)
- [JSON](https://api.crossref.org/works/10.1070/PU2006v049n05ABEH005863)
- [XML](https://api.crossref.org/works/10.1070/PU2006v049n05ABEH005863.xml)
<hr>

### 10.2307/1854760
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1854760)
- [Production](https://doi.org/10.2307/1854760)
- [JSON](https://api.crossref.org/works/10.2307/1854760)
- [XML](https://api.crossref.org/works/10.2307/1854760.xml)
<hr>

### 10.7868/S0869565214090266
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0869565214090266)
- [Production](https://doi.org/10.7868/S0869565214090266)
- [JSON](https://api.crossref.org/works/10.7868/S0869565214090266)
- [XML](https://api.crossref.org/works/10.7868/S0869565214090266.xml)
<hr>

### 10.1177/1522162802005002001
- [Chooser POC](https://chooser.fly.dev/?doi=10.1177/1522162802005002001)
- [Production](https://doi.org/10.1177/1522162802005002001)
- [JSON](https://api.crossref.org/works/10.1177/1522162802005002001)
- [XML](https://api.crossref.org/works/10.1177/1522162802005002001.xml)
<hr>

### 10.2752/9781847888839/RELDRBODY0014
- [Chooser POC](https://chooser.fly.dev/?doi=10.2752/9781847888839/RELDRBODY0014)
- [Production](https://doi.org/10.2752/9781847888839/RELDRBODY0014)
- [JSON](https://api.crossref.org/works/10.2752/9781847888839/RELDRBODY0014)
- [XML](https://api.crossref.org/works/10.2752/9781847888839/RELDRBODY0014.xml)
<hr>

### 10.2307/1873427
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1873427)
- [Production](https://doi.org/10.2307/1873427)
- [JSON](https://api.crossref.org/works/10.2307/1873427)
- [XML](https://api.crossref.org/works/10.2307/1873427.xml)
<hr>

### 10.2307/2166751
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2166751)
- [Production](https://doi.org/10.2307/2166751)
- [JSON](https://api.crossref.org/works/10.2307/2166751)
- [XML](https://api.crossref.org/works/10.2307/2166751.xml)
<hr>

### 10.18502/jpc.v7i1-2.1620
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/jpc.v7i1-2.1620)
- [Production](https://doi.org/10.18502/jpc.v7i1-2.1620)
- [JSON](https://api.crossref.org/works/10.18502/jpc.v7i1-2.1620)
- [XML](https://api.crossref.org/works/10.18502/jpc.v7i1-2.1620.xml)
<hr>

### 10.2307/1849715
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1849715)
- [Production](https://doi.org/10.2307/1849715)
- [JSON](https://api.crossref.org/works/10.2307/1849715)
- [XML](https://api.crossref.org/works/10.2307/1849715.xml)
<hr>

### 10.18502/npt.v5i4.114
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/npt.v5i4.114)
- [Production](https://doi.org/10.18502/npt.v5i4.114)
- [JSON](https://api.crossref.org/works/10.18502/npt.v5i4.114)
- [XML](https://api.crossref.org/works/10.18502/npt.v5i4.114.xml)
<hr>

### 10.2307/2169685
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2169685)
- [Production](https://doi.org/10.2307/2169685)
- [JSON](https://api.crossref.org/works/10.2307/2169685)
- [XML](https://api.crossref.org/works/10.2307/2169685.xml)
<hr>

### 10.1049/cp.2018.1620
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp.2018.1620)
- [Production](https://doi.org/10.1049/cp.2018.1620)
- [JSON](https://api.crossref.org/works/10.1049/cp.2018.1620)
- [XML](https://api.crossref.org/works/10.1049/cp.2018.1620.xml)
<hr>

### 10.7765/9781526147110.00010
- [Chooser POC](https://chooser.fly.dev/?doi=10.7765/9781526147110.00010)
- [Production](https://doi.org/10.7765/9781526147110.00010)
- [JSON](https://api.crossref.org/works/10.7765/9781526147110.00010)
- [XML](https://api.crossref.org/works/10.7765/9781526147110.00010.xml)
<hr>

### 10.18502/tbj.v20i3.7460
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/tbj.v20i3.7460)
- [Production](https://doi.org/10.18502/tbj.v20i3.7460)
- [JSON](https://api.crossref.org/works/10.18502/tbj.v20i3.7460)
- [XML](https://api.crossref.org/works/10.18502/tbj.v20i3.7460.xml)
<hr>

### 10.2307/1851420
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1851420)
- [Production](https://doi.org/10.2307/1851420)
- [JSON](https://api.crossref.org/works/10.2307/1851420)
- [XML](https://api.crossref.org/works/10.2307/1851420.xml)
<hr>

### 10.15446/rcs.v40n1Supl.65906
- [Chooser POC](https://chooser.fly.dev/?doi=10.15446/rcs.v40n1Supl.65906)
- [Production](https://doi.org/10.15446/rcs.v40n1Supl.65906)
- [JSON](https://api.crossref.org/works/10.15446/rcs.v40n1Supl.65906)
- [XML](https://api.crossref.org/works/10.15446/rcs.v40n1Supl.65906.xml)
<hr>

### 10.4135/9781452226545.n21
- [Chooser POC](https://chooser.fly.dev/?doi=10.4135/9781452226545.n21)
- [Production](https://doi.org/10.4135/9781452226545.n21)
- [JSON](https://api.crossref.org/works/10.4135/9781452226545.n21)
- [XML](https://api.crossref.org/works/10.4135/9781452226545.n21.xml)
<hr>

### 10.2307/1850472
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1850472)
- [Production](https://doi.org/10.2307/1850472)
- [JSON](https://api.crossref.org/works/10.2307/1850472)
- [XML](https://api.crossref.org/works/10.2307/1850472.xml)
<hr>

### 10.4135/9781412990172.d97
- [Chooser POC](https://chooser.fly.dev/?doi=10.4135/9781412990172.d97)
- [Production](https://doi.org/10.4135/9781412990172.d97)
- [JSON](https://api.crossref.org/works/10.4135/9781412990172.d97)
- [XML](https://api.crossref.org/works/10.4135/9781412990172.d97.xml)
<hr>

### 10.15446/rcs.v39n1.56351
- [Chooser POC](https://chooser.fly.dev/?doi=10.15446/rcs.v39n1.56351)
- [Production](https://doi.org/10.15446/rcs.v39n1.56351)
- [JSON](https://api.crossref.org/works/10.15446/rcs.v39n1.56351)
- [XML](https://api.crossref.org/works/10.15446/rcs.v39n1.56351.xml)
<hr>

### 10.1134/S2079562912050077
- [Chooser POC](https://chooser.fly.dev/?doi=10.1134/S2079562912050077)
- [Production](https://doi.org/10.1134/S2079562912050077)
- [JSON](https://api.crossref.org/works/10.1134/S2079562912050077)
- [XML](https://api.crossref.org/works/10.1134/S2079562912050077.xml)
<hr>

### 10.1049/cp.2012.0677
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp.2012.0677)
- [Production](https://doi.org/10.1049/cp.2012.0677)
- [JSON](https://api.crossref.org/works/10.1049/cp.2012.0677)
- [XML](https://api.crossref.org/works/10.1049/cp.2012.0677.xml)
<hr>

### 10.18574/nyu/9781479812257.003.0009
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9781479812257.003.0009)
- [Production](https://doi.org/10.18574/nyu/9781479812257.003.0009)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9781479812257.003.0009)
- [XML](https://api.crossref.org/works/10.18574/nyu/9781479812257.003.0009.xml)
<hr>

### 10.2113/gsecongeo.26.4.432
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.26.4.432)
- [Production](https://doi.org/10.2113/gsecongeo.26.4.432)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.26.4.432)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.26.4.432.xml)
<hr>

### 10.1049/ic:20030118
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/ic:20030118)
- [Production](https://doi.org/10.1049/ic:20030118)
- [JSON](https://api.crossref.org/works/10.1049/ic:20030118)
- [XML](https://api.crossref.org/works/10.1049/ic:20030118.xml)
<hr>

### 10.5040/9781474250160.ch-015
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781474250160.ch-015)
- [Production](https://doi.org/10.5040/9781474250160.ch-015)
- [JSON](https://api.crossref.org/works/10.5040/9781474250160.ch-015)
- [XML](https://api.crossref.org/works/10.5040/9781474250160.ch-015.xml)
<hr>

### 10.1049/iet-cdt.2012.0073
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/iet-cdt.2012.0073)
- [Production](https://doi.org/10.1049/iet-cdt.2012.0073)
- [JSON](https://api.crossref.org/works/10.1049/iet-cdt.2012.0073)
- [XML](https://api.crossref.org/works/10.1049/iet-cdt.2012.0073.xml)
<hr>

### 10.18574/nyu/9780814722480.003.0002
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9780814722480.003.0002)
- [Production](https://doi.org/10.18574/nyu/9780814722480.003.0002)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9780814722480.003.0002)
- [XML](https://api.crossref.org/works/10.18574/nyu/9780814722480.003.0002.xml)
<hr>

### 10.1049/cp:20040281
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp:20040281)
- [Production](https://doi.org/10.1049/cp:20040281)
- [JSON](https://api.crossref.org/works/10.1049/cp:20040281)
- [XML](https://api.crossref.org/works/10.1049/cp:20040281.xml)
<hr>

### 10.3901/CJME.2004.01.006
- [Chooser POC](https://chooser.fly.dev/?doi=10.3901/CJME.2004.01.006)
- [Production](https://doi.org/10.3901/CJME.2004.01.006)
- [JSON](https://api.crossref.org/works/10.3901/CJME.2004.01.006)
- [XML](https://api.crossref.org/works/10.3901/CJME.2004.01.006.xml)
<hr>

### 10.2307/1853325
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1853325)
- [Production](https://doi.org/10.2307/1853325)
- [JSON](https://api.crossref.org/works/10.2307/1853325)
- [XML](https://api.crossref.org/works/10.2307/1853325.xml)
<hr>

### 10.1111/j.1475-4975.2011.00215.x
- [Chooser POC](https://chooser.fly.dev/?doi=10.1111/j.1475-4975.2011.00215.x)
- [Production](https://doi.org/10.1111/j.1475-4975.2011.00215.x)
- [JSON](https://api.crossref.org/works/10.1111/j.1475-4975.2011.00215.x)
- [XML](https://api.crossref.org/works/10.1111/j.1475-4975.2011.00215.x.xml)
<hr>

### 10.1093/law/9780198738435.003.0010
- [Chooser POC](https://chooser.fly.dev/?doi=10.1093/law/9780198738435.003.0010)
- [Production](https://doi.org/10.1093/law/9780198738435.003.0010)
- [JSON](https://api.crossref.org/works/10.1093/law/9780198738435.003.0010)
- [XML](https://api.crossref.org/works/10.1093/law/9780198738435.003.0010.xml)
<hr>

### 10.1353/sce.2013.0026
- [Chooser POC](https://chooser.fly.dev/?doi=10.1353/sce.2013.0026)
- [Production](https://doi.org/10.1353/sce.2013.0026)
- [JSON](https://api.crossref.org/works/10.1353/sce.2013.0026)
- [XML](https://api.crossref.org/works/10.1353/sce.2013.0026.xml)
<hr>

### 10.1049/cp:20051441
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp:20051441)
- [Production](https://doi.org/10.1049/cp:20051441)
- [JSON](https://api.crossref.org/works/10.1049/cp:20051441)
- [XML](https://api.crossref.org/works/10.1049/cp:20051441.xml)
<hr>

### 10.1049/cp:19940134
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp:19940134)
- [Production](https://doi.org/10.1049/cp:19940134)
- [JSON](https://api.crossref.org/works/10.1049/cp:19940134)
- [XML](https://api.crossref.org/works/10.1049/cp:19940134.xml)
<hr>

### 10.1049/cp.2019.1169
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp.2019.1169)
- [Production](https://doi.org/10.1049/cp.2019.1169)
- [JSON](https://api.crossref.org/works/10.1049/cp.2019.1169)
- [XML](https://api.crossref.org/works/10.1049/cp.2019.1169.xml)
<hr>

### 10.1049/icp.2021.0440
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/icp.2021.0440)
- [Production](https://doi.org/10.1049/icp.2021.0440)
- [JSON](https://api.crossref.org/works/10.1049/icp.2021.0440)
- [XML](https://api.crossref.org/works/10.1049/icp.2021.0440.xml)
<hr>

### 10.7868/S0869803113040097
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0869803113040097)
- [Production](https://doi.org/10.7868/S0869803113040097)
- [JSON](https://api.crossref.org/works/10.7868/S0869803113040097)
- [XML](https://api.crossref.org/works/10.7868/S0869803113040097.xml)
<hr>

### 10.1049/et.2018.0302
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/et.2018.0302)
- [Production](https://doi.org/10.1049/et.2018.0302)
- [JSON](https://api.crossref.org/works/10.1049/et.2018.0302)
- [XML](https://api.crossref.org/works/10.1049/et.2018.0302.xml)
<hr>

### 10.1070/RCR4797
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RCR4797)
- [Production](https://doi.org/10.1070/RCR4797)
- [JSON](https://api.crossref.org/works/10.1070/RCR4797)
- [XML](https://api.crossref.org/works/10.1070/RCR4797.xml)
<hr>

### 10.1144/SP336.3
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/SP336.3)
- [Production](https://doi.org/10.1144/SP336.3)
- [JSON](https://api.crossref.org/works/10.1144/SP336.3)
- [XML](https://api.crossref.org/works/10.1144/SP336.3.xml)
<hr>

### 10.30820/0171-3434-2017-3-101
- [Chooser POC](https://chooser.fly.dev/?doi=10.30820/0171-3434-2017-3-101)
- [Production](https://doi.org/10.30820/0171-3434-2017-3-101)
- [JSON](https://api.crossref.org/works/10.30820/0171-3434-2017-3-101)
- [XML](https://api.crossref.org/works/10.30820/0171-3434-2017-3-101.xml)
<hr>

### 10.1070/PU1980v023n08ABEH005034
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/PU1980v023n08ABEH005034)
- [Production](https://doi.org/10.1070/PU1980v023n08ABEH005034)
- [JSON](https://api.crossref.org/works/10.1070/PU1980v023n08ABEH005034)
- [XML](https://api.crossref.org/works/10.1070/PU1980v023n08ABEH005034.xml)
<hr>

### 10.1144/GSL.SP.2000.172.01.14
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.2000.172.01.14)
- [Production](https://doi.org/10.1144/GSL.SP.2000.172.01.14)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.2000.172.01.14)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.2000.172.01.14.xml)
<hr>

### 10.5040/9781838710729.ch-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781838710729.ch-001)
- [Production](https://doi.org/10.5040/9781838710729.ch-001)
- [JSON](https://api.crossref.org/works/10.5040/9781838710729.ch-001)
- [XML](https://api.crossref.org/works/10.5040/9781838710729.ch-001.xml)
<hr>

### 10.1111/misp.12040
- [Chooser POC](https://chooser.fly.dev/?doi=10.1111/misp.12040)
- [Production](https://doi.org/10.1111/misp.12040)
- [JSON](https://api.crossref.org/works/10.1111/misp.12040)
- [XML](https://api.crossref.org/works/10.1111/misp.12040.xml)
<hr>

### 10.2307/1853838
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1853838)
- [Production](https://doi.org/10.2307/1853838)
- [JSON](https://api.crossref.org/works/10.2307/1853838)
- [XML](https://api.crossref.org/works/10.2307/1853838.xml)
<hr>

### 10.1144/SP344.17
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/SP344.17)
- [Production](https://doi.org/10.1144/SP344.17)
- [JSON](https://api.crossref.org/works/10.1144/SP344.17)
- [XML](https://api.crossref.org/works/10.1144/SP344.17.xml)
<hr>

### 10.18502/ijaai.v19i(s1.r1).2849
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/ijaai.v19i(s1.r1).2849)
- [Production](https://doi.org/10.18502/ijaai.v19i(s1.r1).2849)
- [JSON](https://api.crossref.org/works/10.18502/ijaai.v19i(s1.r1).2849)
- [XML](https://api.crossref.org/works/10.18502/ijaai.v19i(s1.r1).2849.xml)
<hr>

### 10.18502/aacc.v6i2.2766
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/aacc.v6i2.2766)
- [Production](https://doi.org/10.18502/aacc.v6i2.2766)
- [JSON](https://api.crossref.org/works/10.18502/aacc.v6i2.2766)
- [XML](https://api.crossref.org/works/10.18502/aacc.v6i2.2766.xml)
<hr>

### 10.1144/SP293.9
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/SP293.9)
- [Production](https://doi.org/10.1144/SP293.9)
- [JSON](https://api.crossref.org/works/10.1144/SP293.9)
- [XML](https://api.crossref.org/works/10.1144/SP293.9.xml)
<hr>

### 10.2307/1842480
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1842480)
- [Production](https://doi.org/10.2307/1842480)
- [JSON](https://api.crossref.org/works/10.2307/1842480)
- [XML](https://api.crossref.org/works/10.2307/1842480.xml)
<hr>

### 10.2110/pec.02.73.0207
- [Chooser POC](https://chooser.fly.dev/?doi=10.2110/pec.02.73.0207)
- [Production](https://doi.org/10.2110/pec.02.73.0207)
- [JSON](https://api.crossref.org/works/10.2110/pec.02.73.0207)
- [XML](https://api.crossref.org/works/10.2110/pec.02.73.0207.xml)
<hr>

### 10.1070/QEL18061
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/QEL18061)
- [Production](https://doi.org/10.1070/QEL18061)
- [JSON](https://api.crossref.org/works/10.1070/QEL18061)
- [XML](https://api.crossref.org/works/10.1070/QEL18061.xml)
<hr>

### 10.30820/8218.06
- [Chooser POC](https://chooser.fly.dev/?doi=10.30820/8218.06)
- [Production](https://doi.org/10.30820/8218.06)
- [JSON](https://api.crossref.org/works/10.30820/8218.06)
- [XML](https://api.crossref.org/works/10.30820/8218.06.xml)
<hr>

### 10.18574/nyu/9781479800612.003.0004
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9781479800612.003.0004)
- [Production](https://doi.org/10.18574/nyu/9781479800612.003.0004)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9781479800612.003.0004)
- [XML](https://api.crossref.org/works/10.18574/nyu/9781479800612.003.0004.xml)
<hr>

### 10.18574/nyu/9780814738481.003.0004
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9780814738481.003.0004)
- [Production](https://doi.org/10.18574/nyu/9780814738481.003.0004)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9780814738481.003.0004)
- [XML](https://api.crossref.org/works/10.18574/nyu/9780814738481.003.0004.xml)
<hr>

### 10.5040/9780567686909.ch-006
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9780567686909.ch-006)
- [Production](https://doi.org/10.5040/9780567686909.ch-006)
- [JSON](https://api.crossref.org/works/10.5040/9780567686909.ch-006)
- [XML](https://api.crossref.org/works/10.5040/9780567686909.ch-006.xml)
<hr>

### 10.1070/RM1979v034n03ABEH004006
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1979v034n03ABEH004006)
- [Production](https://doi.org/10.1070/RM1979v034n03ABEH004006)
- [JSON](https://api.crossref.org/works/10.1070/RM1979v034n03ABEH004006)
- [XML](https://api.crossref.org/works/10.1070/RM1979v034n03ABEH004006.xml)
<hr>

### 10.2307/1850726
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1850726)
- [Production](https://doi.org/10.2307/1850726)
- [JSON](https://api.crossref.org/works/10.2307/1850726)
- [XML](https://api.crossref.org/works/10.2307/1850726.xml)
<hr>

### 10.2113/gsecongeo.87.6.1649
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.87.6.1649)
- [Production](https://doi.org/10.2113/gsecongeo.87.6.1649)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.87.6.1649)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.87.6.1649.xml)
<hr>

### 10.1049/cp.2014.0733
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp.2014.0733)
- [Production](https://doi.org/10.1049/cp.2014.0733)
- [JSON](https://api.crossref.org/works/10.1049/cp.2014.0733)
- [XML](https://api.crossref.org/works/10.1049/cp.2014.0733.xml)
<hr>

### 10.2307/1840794
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1840794)
- [Production](https://doi.org/10.2307/1840794)
- [JSON](https://api.crossref.org/works/10.2307/1840794)
- [XML](https://api.crossref.org/works/10.2307/1840794.xml)
<hr>

### 10.1049/icp.2021.0273
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/icp.2021.0273)
- [Production](https://doi.org/10.1049/icp.2021.0273)
- [JSON](https://api.crossref.org/works/10.1049/icp.2021.0273)
- [XML](https://api.crossref.org/works/10.1049/icp.2021.0273.xml)
<hr>

### 10.2307/1833732
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1833732)
- [Production](https://doi.org/10.2307/1833732)
- [JSON](https://api.crossref.org/works/10.2307/1833732)
- [XML](https://api.crossref.org/works/10.2307/1833732.xml)
<hr>

### 10.7868/S0023476113060246
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0023476113060246)
- [Production](https://doi.org/10.7868/S0023476113060246)
- [JSON](https://api.crossref.org/works/10.7868/S0023476113060246)
- [XML](https://api.crossref.org/works/10.7868/S0023476113060246.xml)
<hr>

### 10.2113/gsecongeo.54.4.752
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.54.4.752)
- [Production](https://doi.org/10.2113/gsecongeo.54.4.752)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.54.4.752)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.54.4.752.xml)
<hr>

### 10.15530/urtec-2015-2154947
- [Chooser POC](https://chooser.fly.dev/?doi=10.15530/urtec-2015-2154947)
- [Production](https://doi.org/10.15530/urtec-2015-2154947)
- [JSON](https://api.crossref.org/works/10.15530/urtec-2015-2154947)
- [XML](https://api.crossref.org/works/10.15530/urtec-2015-2154947.xml)
<hr>

### 10.2307/1846107
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1846107)
- [Production](https://doi.org/10.2307/1846107)
- [JSON](https://api.crossref.org/works/10.2307/1846107)
- [XML](https://api.crossref.org/works/10.2307/1846107.xml)
<hr>

### 10.1596/34573
- [Chooser POC](https://chooser.fly.dev/?doi=10.1596/34573)
- [Production](https://doi.org/10.1596/34573)
- [JSON](https://api.crossref.org/works/10.1596/34573)
- [XML](https://api.crossref.org/works/10.1596/34573.xml)
<hr>

### 10.1596/1813-9450-8112
- [Chooser POC](https://chooser.fly.dev/?doi=10.1596/1813-9450-8112)
- [Production](https://doi.org/10.1596/1813-9450-8112)
- [JSON](https://api.crossref.org/works/10.1596/1813-9450-8112)
- [XML](https://api.crossref.org/works/10.1596/1813-9450-8112.xml)
<hr>

### 10.1144/GSL.SP.1995.097.01.03
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1995.097.01.03)
- [Production](https://doi.org/10.1144/GSL.SP.1995.097.01.03)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1995.097.01.03)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1995.097.01.03.xml)
<hr>

### 10.15530/urtec-2018-2902561
- [Chooser POC](https://chooser.fly.dev/?doi=10.15530/urtec-2018-2902561)
- [Production](https://doi.org/10.15530/urtec-2018-2902561)
- [JSON](https://api.crossref.org/works/10.15530/urtec-2018-2902561)
- [XML](https://api.crossref.org/works/10.15530/urtec-2018-2902561.xml)
<hr>

### 10.2307/1855428
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1855428)
- [Production](https://doi.org/10.2307/1855428)
- [JSON](https://api.crossref.org/works/10.2307/1855428)
- [XML](https://api.crossref.org/works/10.2307/1855428.xml)
<hr>

### 10.2190/RGFL-BPMV-347F-TN30
- [Chooser POC](https://chooser.fly.dev/?doi=10.2190/RGFL-BPMV-347F-TN30)
- [Production](https://doi.org/10.2190/RGFL-BPMV-347F-TN30)
- [JSON](https://api.crossref.org/works/10.2190/RGFL-BPMV-347F-TN30)
- [XML](https://api.crossref.org/works/10.2190/RGFL-BPMV-347F-TN30.xml)
<hr>

### 10.2307/1862450
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1862450)
- [Production](https://doi.org/10.2307/1862450)
- [JSON](https://api.crossref.org/works/10.2307/1862450)
- [XML](https://api.crossref.org/works/10.2307/1862450.xml)
<hr>

### 10.1124/mi.2.4.212
- [Chooser POC](https://chooser.fly.dev/?doi=10.1124/mi.2.4.212)
- [Production](https://doi.org/10.1124/mi.2.4.212)
- [JSON](https://api.crossref.org/works/10.1124/mi.2.4.212)
- [XML](https://api.crossref.org/works/10.1124/mi.2.4.212.xml)
<hr>

### 10.7868/S0475145013020067
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0475145013020067)
- [Production](https://doi.org/10.7868/S0475145013020067)
- [JSON](https://api.crossref.org/works/10.7868/S0475145013020067)
- [XML](https://api.crossref.org/works/10.7868/S0475145013020067.xml)
<hr>

### 10.2307/2650906
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2650906)
- [Production](https://doi.org/10.2307/2650906)
- [JSON](https://api.crossref.org/works/10.2307/2650906)
- [XML](https://api.crossref.org/works/10.2307/2650906.xml)
<hr>

### 10.1144/GSL.SP.1995.096.01.03
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1995.096.01.03)
- [Production](https://doi.org/10.1144/GSL.SP.1995.096.01.03)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1995.096.01.03)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1995.096.01.03.xml)
<hr>

### 10.1049/cp.2018.1549
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp.2018.1549)
- [Production](https://doi.org/10.1049/cp.2018.1549)
- [JSON](https://api.crossref.org/works/10.1049/cp.2018.1549)
- [XML](https://api.crossref.org/works/10.1049/cp.2018.1549.xml)
<hr>

### 10.1596/34672
- [Chooser POC](https://chooser.fly.dev/?doi=10.1596/34672)
- [Production](https://doi.org/10.1596/34672)
- [JSON](https://api.crossref.org/works/10.1596/34672)
- [XML](https://api.crossref.org/works/10.1596/34672.xml)
<hr>

### 10.2113/gsecongeo.6.7.701
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.6.7.701)
- [Production](https://doi.org/10.2113/gsecongeo.6.7.701)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.6.7.701)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.6.7.701.xml)
<hr>

### 10.5040/9781350137936.ch-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781350137936.ch-001)
- [Production](https://doi.org/10.5040/9781350137936.ch-001)
- [JSON](https://api.crossref.org/works/10.5040/9781350137936.ch-001)
- [XML](https://api.crossref.org/works/10.5040/9781350137936.ch-001.xml)
<hr>

### 10.18574/nyu/9781479800117.003.0002
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9781479800117.003.0002)
- [Production](https://doi.org/10.18574/nyu/9781479800117.003.0002)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9781479800117.003.0002)
- [XML](https://api.crossref.org/works/10.18574/nyu/9781479800117.003.0002.xml)
<hr>

### 10.18502/crcp.v5i1.3606
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/crcp.v5i1.3606)
- [Production](https://doi.org/10.18502/crcp.v5i1.3606)
- [JSON](https://api.crossref.org/works/10.18502/crcp.v5i1.3606)
- [XML](https://api.crossref.org/works/10.18502/crcp.v5i1.3606.xml)
<hr>

### 10.2190/N2P9-Y189-49RC-4EB6
- [Chooser POC](https://chooser.fly.dev/?doi=10.2190/N2P9-Y189-49RC-4EB6)
- [Production](https://doi.org/10.2190/N2P9-Y189-49RC-4EB6)
- [JSON](https://api.crossref.org/works/10.2190/N2P9-Y189-49RC-4EB6)
- [XML](https://api.crossref.org/works/10.2190/N2P9-Y189-49RC-4EB6.xml)
<hr>

### 10.18502/jfsh.v6i1.6020
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/jfsh.v6i1.6020)
- [Production](https://doi.org/10.18502/jfsh.v6i1.6020)
- [JSON](https://api.crossref.org/works/10.18502/jfsh.v6i1.6020)
- [XML](https://api.crossref.org/works/10.18502/jfsh.v6i1.6020.xml)
<hr>

### 10.2307/1837458
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1837458)
- [Production](https://doi.org/10.2307/1837458)
- [JSON](https://api.crossref.org/works/10.2307/1837458)
- [XML](https://api.crossref.org/works/10.2307/1837458.xml)
<hr>

### 10.2110/cor.80.01.0161
- [Chooser POC](https://chooser.fly.dev/?doi=10.2110/cor.80.01.0161)
- [Production](https://doi.org/10.2110/cor.80.01.0161)
- [JSON](https://api.crossref.org/works/10.2110/cor.80.01.0161)
- [XML](https://api.crossref.org/works/10.2110/cor.80.01.0161.xml)
<hr>

### 10.1049/ic.2011.0044
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/ic.2011.0044)
- [Production](https://doi.org/10.1049/ic.2011.0044)
- [JSON](https://api.crossref.org/works/10.1049/ic.2011.0044)
- [XML](https://api.crossref.org/works/10.1049/ic.2011.0044.xml)
<hr>

### 10.1111/j.1475-4975.1982.tb00098.x
- [Chooser POC](https://chooser.fly.dev/?doi=10.1111/j.1475-4975.1982.tb00098.x)
- [Production](https://doi.org/10.1111/j.1475-4975.1982.tb00098.x)
- [JSON](https://api.crossref.org/works/10.1111/j.1475-4975.1982.tb00098.x)
- [XML](https://api.crossref.org/works/10.1111/j.1475-4975.1982.tb00098.x.xml)
<hr>

### 10.7868/S0033849413010087
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0033849413010087)
- [Production](https://doi.org/10.7868/S0033849413010087)
- [JSON](https://api.crossref.org/works/10.7868/S0033849413010087)
- [XML](https://api.crossref.org/works/10.7868/S0033849413010087.xml)
<hr>

### 10.2307/2167325
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2167325)
- [Production](https://doi.org/10.2307/2167325)
- [JSON](https://api.crossref.org/works/10.2307/2167325)
- [XML](https://api.crossref.org/works/10.2307/2167325.xml)
<hr>

### 10.2307/1857994
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1857994)
- [Production](https://doi.org/10.2307/1857994)
- [JSON](https://api.crossref.org/works/10.2307/1857994)
- [XML](https://api.crossref.org/works/10.2307/1857994.xml)
<hr>

### 10.2190/2MTV-KFMV-XC19-R6K9
- [Chooser POC](https://chooser.fly.dev/?doi=10.2190/2MTV-KFMV-XC19-R6K9)
- [Production](https://doi.org/10.2190/2MTV-KFMV-XC19-R6K9)
- [JSON](https://api.crossref.org/works/10.2190/2MTV-KFMV-XC19-R6K9)
- [XML](https://api.crossref.org/works/10.2190/2MTV-KFMV-XC19-R6K9.xml)
<hr>

### 10.30820/1616-8836-2019-1-85
- [Chooser POC](https://chooser.fly.dev/?doi=10.30820/1616-8836-2019-1-85)
- [Production](https://doi.org/10.30820/1616-8836-2019-1-85)
- [JSON](https://api.crossref.org/works/10.30820/1616-8836-2019-1-85)
- [XML](https://api.crossref.org/works/10.30820/1616-8836-2019-1-85.xml)
<hr>

### 10.5040/9781780937496.ch-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781780937496.ch-001)
- [Production](https://doi.org/10.5040/9781780937496.ch-001)
- [JSON](https://api.crossref.org/works/10.5040/9781780937496.ch-001)
- [XML](https://api.crossref.org/works/10.5040/9781780937496.ch-001.xml)
<hr>

### 10.3901/CJME.2010.06.788
- [Chooser POC](https://chooser.fly.dev/?doi=10.3901/CJME.2010.06.788)
- [Production](https://doi.org/10.3901/CJME.2010.06.788)
- [JSON](https://api.crossref.org/works/10.3901/CJME.2010.06.788)
- [XML](https://api.crossref.org/works/10.3901/CJME.2010.06.788.xml)
<hr>

### 10.2307/1839467
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1839467)
- [Production](https://doi.org/10.2307/1839467)
- [JSON](https://api.crossref.org/works/10.2307/1839467)
- [XML](https://api.crossref.org/works/10.2307/1839467.xml)
<hr>

### 10.1144/GSL.SP.1990.048.01.20
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1990.048.01.20)
- [Production](https://doi.org/10.1144/GSL.SP.1990.048.01.20)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1990.048.01.20)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1990.048.01.20.xml)
<hr>

### 10.1144/GSL.SP.2003.216.01.05
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.2003.216.01.05)
- [Production](https://doi.org/10.1144/GSL.SP.2003.216.01.05)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.2003.216.01.05)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.2003.216.01.05.xml)
<hr>

### 10.15530/urtec-2020-2432
- [Chooser POC](https://chooser.fly.dev/?doi=10.15530/urtec-2020-2432)
- [Production](https://doi.org/10.15530/urtec-2020-2432)
- [JSON](https://api.crossref.org/works/10.15530/urtec-2020-2432)
- [XML](https://api.crossref.org/works/10.15530/urtec-2020-2432.xml)
<hr>

### 10.18574/nyu/9780814789308.003.0009
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9780814789308.003.0009)
- [Production](https://doi.org/10.18574/nyu/9780814789308.003.0009)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9780814789308.003.0009)
- [XML](https://api.crossref.org/works/10.18574/nyu/9780814789308.003.0009.xml)
<hr>

### 10.2307/2171315
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2171315)
- [Production](https://doi.org/10.2307/2171315)
- [JSON](https://api.crossref.org/works/10.2307/2171315)
- [XML](https://api.crossref.org/works/10.2307/2171315.xml)
<hr>

### 10.2113/gsecongeo.17.5.370
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.17.5.370)
- [Production](https://doi.org/10.2113/gsecongeo.17.5.370)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.17.5.370)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.17.5.370.xml)
<hr>

### 10.5040/9781501332470.0009
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781501332470.0009)
- [Production](https://doi.org/10.5040/9781501332470.0009)
- [JSON](https://api.crossref.org/works/10.5040/9781501332470.0009)
- [XML](https://api.crossref.org/works/10.5040/9781501332470.0009.xml)
<hr>

### 10.2307/1862536
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1862536)
- [Production](https://doi.org/10.2307/1862536)
- [JSON](https://api.crossref.org/works/10.2307/1862536)
- [XML](https://api.crossref.org/works/10.2307/1862536.xml)
<hr>

### 10.1144/GSL.SP.1996.106.01.24
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1996.106.01.24)
- [Production](https://doi.org/10.1144/GSL.SP.1996.106.01.24)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1996.106.01.24)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1996.106.01.24.xml)
<hr>

### 10.1190/tle18020182.1
- [Chooser POC](https://chooser.fly.dev/?doi=10.1190/tle18020182.1)
- [Production](https://doi.org/10.1190/tle18020182.1)
- [JSON](https://api.crossref.org/works/10.1190/tle18020182.1)
- [XML](https://api.crossref.org/works/10.1190/tle18020182.1.xml)
<hr>

### 10.1190/1.1442546
- [Chooser POC](https://chooser.fly.dev/?doi=10.1190/1.1442546)
- [Production](https://doi.org/10.1190/1.1442546)
- [JSON](https://api.crossref.org/works/10.1190/1.1442546)
- [XML](https://api.crossref.org/works/10.1190/1.1442546.xml)
<hr>

### 10.5935/0103-507X.20160007
- [Chooser POC](https://chooser.fly.dev/?doi=10.5935/0103-507X.20160007)
- [Production](https://doi.org/10.5935/0103-507X.20160007)
- [JSON](https://api.crossref.org/works/10.5935/0103-507X.20160007)
- [XML](https://api.crossref.org/works/10.5935/0103-507X.20160007.xml)
<hr>

### 10.2307/1905213
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1905213)
- [Production](https://doi.org/10.2307/1905213)
- [JSON](https://api.crossref.org/works/10.2307/1905213)
- [XML](https://api.crossref.org/works/10.2307/1905213.xml)
<hr>

### 10.1070/PU1997v040n05ABEH001565
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/PU1997v040n05ABEH001565)
- [Production](https://doi.org/10.1070/PU1997v040n05ABEH001565)
- [JSON](https://api.crossref.org/works/10.1070/PU1997v040n05ABEH001565)
- [XML](https://api.crossref.org/works/10.1070/PU1997v040n05ABEH001565.xml)
<hr>

### 10.2307/1848008
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1848008)
- [Production](https://doi.org/10.2307/1848008)
- [JSON](https://api.crossref.org/works/10.2307/1848008)
- [XML](https://api.crossref.org/works/10.2307/1848008.xml)
<hr>

### 10.5040/9781474257954.ch-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781474257954.ch-002)
- [Production](https://doi.org/10.5040/9781474257954.ch-002)
- [JSON](https://api.crossref.org/works/10.5040/9781474257954.ch-002)
- [XML](https://api.crossref.org/works/10.5040/9781474257954.ch-002.xml)
<hr>

### 10.2307/2171310
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2171310)
- [Production](https://doi.org/10.2307/2171310)
- [JSON](https://api.crossref.org/works/10.2307/2171310)
- [XML](https://api.crossref.org/works/10.2307/2171310.xml)
<hr>

### 10.2307/3514748
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/3514748)
- [Production](https://doi.org/10.2307/3514748)
- [JSON](https://api.crossref.org/works/10.2307/3514748)
- [XML](https://api.crossref.org/works/10.2307/3514748.xml)
<hr>

### 10.1596/33345
- [Chooser POC](https://chooser.fly.dev/?doi=10.1596/33345)
- [Production](https://doi.org/10.1596/33345)
- [JSON](https://api.crossref.org/works/10.1596/33345)
- [XML](https://api.crossref.org/works/10.1596/33345.xml)
<hr>

### 10.1049/icp.2021.0344
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/icp.2021.0344)
- [Production](https://doi.org/10.1049/icp.2021.0344)
- [JSON](https://api.crossref.org/works/10.1049/icp.2021.0344)
- [XML](https://api.crossref.org/works/10.1049/icp.2021.0344.xml)
<hr>

### 10.2307/1848062
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1848062)
- [Production](https://doi.org/10.2307/1848062)
- [JSON](https://api.crossref.org/works/10.2307/1848062)
- [XML](https://api.crossref.org/works/10.2307/1848062.xml)
<hr>

### 10.2307/1873757
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1873757)
- [Production](https://doi.org/10.2307/1873757)
- [JSON](https://api.crossref.org/works/10.2307/1873757)
- [XML](https://api.crossref.org/works/10.2307/1873757.xml)
<hr>

### 10.2113/gsecongeo.74.7.1697
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.74.7.1697)
- [Production](https://doi.org/10.2113/gsecongeo.74.7.1697)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.74.7.1697)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.74.7.1697.xml)
<hr>

### 10.18502/jri.v21i4.4323
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/jri.v21i4.4323)
- [Production](https://doi.org/10.18502/jri.v21i4.4323)
- [JSON](https://api.crossref.org/works/10.18502/jri.v21i4.4323)
- [XML](https://api.crossref.org/works/10.18502/jri.v21i4.4323.xml)
<hr>

### 10.5040/9781501320316.0004
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781501320316.0004)
- [Production](https://doi.org/10.5040/9781501320316.0004)
- [JSON](https://api.crossref.org/works/10.5040/9781501320316.0004)
- [XML](https://api.crossref.org/works/10.5040/9781501320316.0004.xml)
<hr>

### 10.3109/10401239809148948
- [Chooser POC](https://chooser.fly.dev/?doi=10.3109/10401239809148948)
- [Production](https://doi.org/10.3109/10401239809148948)
- [JSON](https://api.crossref.org/works/10.3109/10401239809148948)
- [XML](https://api.crossref.org/works/10.3109/10401239809148948.xml)
<hr>

### 10.1144/GSL.SP.1995.087.01.12
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1995.087.01.12)
- [Production](https://doi.org/10.1144/GSL.SP.1995.087.01.12)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1995.087.01.12)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1995.087.01.12.xml)
<hr>

### 10.2307/1856811
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1856811)
- [Production](https://doi.org/10.2307/1856811)
- [JSON](https://api.crossref.org/works/10.2307/1856811)
- [XML](https://api.crossref.org/works/10.2307/1856811.xml)
<hr>

### 10.2307/1852726
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1852726)
- [Production](https://doi.org/10.2307/1852726)
- [JSON](https://api.crossref.org/works/10.2307/1852726)
- [XML](https://api.crossref.org/works/10.2307/1852726.xml)
<hr>

### 10.1049/cp:20060925
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp:20060925)
- [Production](https://doi.org/10.1049/cp:20060925)
- [JSON](https://api.crossref.org/works/10.1049/cp:20060925)
- [XML](https://api.crossref.org/works/10.1049/cp:20060925.xml)
<hr>

### 10.4135/9781412991452.d189
- [Chooser POC](https://chooser.fly.dev/?doi=10.4135/9781412991452.d189)
- [Production](https://doi.org/10.4135/9781412991452.d189)
- [JSON](https://api.crossref.org/works/10.4135/9781412991452.d189)
- [XML](https://api.crossref.org/works/10.4135/9781412991452.d189.xml)
<hr>

### 10.1144/GSL.SP.2006.260.01.30
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.2006.260.01.30)
- [Production](https://doi.org/10.1144/GSL.SP.2006.260.01.30)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.2006.260.01.30)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.2006.260.01.30.xml)
<hr>

### 10.4135/9781452226552
- [Chooser POC](https://chooser.fly.dev/?doi=10.4135/9781452226552)
- [Production](https://doi.org/10.4135/9781452226552)
- [JSON](https://api.crossref.org/works/10.4135/9781452226552)
- [XML](https://api.crossref.org/works/10.4135/9781452226552.xml)
<hr>

### 10.3109/10401239409148835
- [Chooser POC](https://chooser.fly.dev/?doi=10.3109/10401239409148835)
- [Production](https://doi.org/10.3109/10401239409148835)
- [JSON](https://api.crossref.org/works/10.3109/10401239409148835)
- [XML](https://api.crossref.org/works/10.3109/10401239409148835.xml)
<hr>

### 10.1049/icp.2021.1071
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/icp.2021.1071)
- [Production](https://doi.org/10.1049/icp.2021.1071)
- [JSON](https://api.crossref.org/works/10.1049/icp.2021.1071)
- [XML](https://api.crossref.org/works/10.1049/icp.2021.1071.xml)
<hr>

### 10.2307/1872989
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1872989)
- [Production](https://doi.org/10.2307/1872989)
- [JSON](https://api.crossref.org/works/10.2307/1872989)
- [XML](https://api.crossref.org/works/10.2307/1872989.xml)
<hr>

### 10.1093/law/9780198859871.003.0026
- [Chooser POC](https://chooser.fly.dev/?doi=10.1093/law/9780198859871.003.0026)
- [Production](https://doi.org/10.1093/law/9780198859871.003.0026)
- [JSON](https://api.crossref.org/works/10.1093/law/9780198859871.003.0026)
- [XML](https://api.crossref.org/works/10.1093/law/9780198859871.003.0026.xml)
<hr>

### 10.1144/GSL.SP.1982.010.01.19
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1982.010.01.19)
- [Production](https://doi.org/10.1144/GSL.SP.1982.010.01.19)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1982.010.01.19)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1982.010.01.19.xml)
<hr>

### 10.1070/RM1991v046n06ABEH002860
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1991v046n06ABEH002860)
- [Production](https://doi.org/10.1070/RM1991v046n06ABEH002860)
- [JSON](https://api.crossref.org/works/10.1070/RM1991v046n06ABEH002860)
- [XML](https://api.crossref.org/works/10.1070/RM1991v046n06ABEH002860.xml)
<hr>

### 10.7868/S0869565213080240
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0869565213080240)
- [Production](https://doi.org/10.7868/S0869565213080240)
- [JSON](https://api.crossref.org/works/10.7868/S0869565213080240)
- [XML](https://api.crossref.org/works/10.7868/S0869565213080240.xml)
<hr>

### 10.5040/9781838710712.ch-016
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781838710712.ch-016)
- [Production](https://doi.org/10.5040/9781838710712.ch-016)
- [JSON](https://api.crossref.org/works/10.5040/9781838710712.ch-016)
- [XML](https://api.crossref.org/works/10.5040/9781838710712.ch-016.xml)
<hr>

### 10.1144/GSL.SP.2006.265.01.04
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.2006.265.01.04)
- [Production](https://doi.org/10.1144/GSL.SP.2006.265.01.04)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.2006.265.01.04)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.2006.265.01.04.xml)
<hr>

### 10.1144/GSL.SP.2000.171.01.25
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.2000.171.01.25)
- [Production](https://doi.org/10.1144/GSL.SP.2000.171.01.25)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.2000.171.01.25)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.2000.171.01.25.xml)
<hr>

### 10.15446/caldasia.v37n1.50978
- [Chooser POC](https://chooser.fly.dev/?doi=10.15446/caldasia.v37n1.50978)
- [Production](https://doi.org/10.15446/caldasia.v37n1.50978)
- [JSON](https://api.crossref.org/works/10.15446/caldasia.v37n1.50978)
- [XML](https://api.crossref.org/works/10.15446/caldasia.v37n1.50978.xml)
<hr>

### 10.2307/1849183
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1849183)
- [Production](https://doi.org/10.2307/1849183)
- [JSON](https://api.crossref.org/works/10.2307/1849183)
- [XML](https://api.crossref.org/works/10.2307/1849183.xml)
<hr>

### 10.2307/2166872
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2166872)
- [Production](https://doi.org/10.2307/2166872)
- [JSON](https://api.crossref.org/works/10.2307/2166872)
- [XML](https://api.crossref.org/works/10.2307/2166872.xml)
<hr>

### 10.1144/GSL.SP.1987.028.01.06
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1987.028.01.06)
- [Production](https://doi.org/10.1144/GSL.SP.1987.028.01.06)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1987.028.01.06)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1987.028.01.06.xml)
<hr>

### 10.1070/PU1968v011n03ABEH003829
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/PU1968v011n03ABEH003829)
- [Production](https://doi.org/10.1070/PU1968v011n03ABEH003829)
- [JSON](https://api.crossref.org/works/10.1070/PU1968v011n03ABEH003829)
- [XML](https://api.crossref.org/works/10.1070/PU1968v011n03ABEH003829.xml)
<hr>

### 10.2307/1855941
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1855941)
- [Production](https://doi.org/10.2307/1855941)
- [JSON](https://api.crossref.org/works/10.2307/1855941)
- [XML](https://api.crossref.org/works/10.2307/1855941.xml)
<hr>

### 10.1111/j.1475-4975.1990.tb00206.x
- [Chooser POC](https://chooser.fly.dev/?doi=10.1111/j.1475-4975.1990.tb00206.x)
- [Production](https://doi.org/10.1111/j.1475-4975.1990.tb00206.x)
- [JSON](https://api.crossref.org/works/10.1111/j.1475-4975.1990.tb00206.x)
- [XML](https://api.crossref.org/works/10.1111/j.1475-4975.1990.tb00206.x.xml)
<hr>

### 10.1070/RM1970v025n03ABEH003808
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1970v025n03ABEH003808)
- [Production](https://doi.org/10.1070/RM1970v025n03ABEH003808)
- [JSON](https://api.crossref.org/works/10.1070/RM1970v025n03ABEH003808)
- [XML](https://api.crossref.org/works/10.1070/RM1970v025n03ABEH003808.xml)
<hr>

### 10.1596/33766
- [Chooser POC](https://chooser.fly.dev/?doi=10.1596/33766)
- [Production](https://doi.org/10.1596/33766)
- [JSON](https://api.crossref.org/works/10.1596/33766)
- [XML](https://api.crossref.org/works/10.1596/33766.xml)
<hr>

### 10.1070/RM1990v045n01ABEH002327
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1990v045n01ABEH002327)
- [Production](https://doi.org/10.1070/RM1990v045n01ABEH002327)
- [JSON](https://api.crossref.org/works/10.1070/RM1990v045n01ABEH002327)
- [XML](https://api.crossref.org/works/10.1070/RM1990v045n01ABEH002327.xml)
<hr>

### 10.2307/1849348
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1849348)
- [Production](https://doi.org/10.2307/1849348)
- [JSON](https://api.crossref.org/works/10.2307/1849348)
- [XML](https://api.crossref.org/works/10.2307/1849348.xml)
<hr>

### 10.2307/1848951
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1848951)
- [Production](https://doi.org/10.2307/1848951)
- [JSON](https://api.crossref.org/works/10.2307/1848951)
- [XML](https://api.crossref.org/works/10.2307/1848951.xml)
<hr>

### 10.1144/SP287.20
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/SP287.20)
- [Production](https://doi.org/10.1144/SP287.20)
- [JSON](https://api.crossref.org/works/10.1144/SP287.20)
- [XML](https://api.crossref.org/works/10.1144/SP287.20.xml)
<hr>

### 10.5040/9781350057098.ch-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781350057098.ch-002)
- [Production](https://doi.org/10.5040/9781350057098.ch-002)
- [JSON](https://api.crossref.org/works/10.5040/9781350057098.ch-002)
- [XML](https://api.crossref.org/works/10.5040/9781350057098.ch-002.xml)
<hr>

### 10.1144/SP520-2021-62
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/SP520-2021-62)
- [Production](https://doi.org/10.1144/SP520-2021-62)
- [JSON](https://api.crossref.org/works/10.1144/SP520-2021-62)
- [XML](https://api.crossref.org/works/10.1144/SP520-2021-62.xml)
<hr>

### 10.1049/et.2009.1702
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/et.2009.1702)
- [Production](https://doi.org/10.1049/et.2009.1702)
- [JSON](https://api.crossref.org/works/10.1049/et.2009.1702)
- [XML](https://api.crossref.org/works/10.1049/et.2009.1702.xml)
<hr>

### 10.18574/nyu/9781479846641.003.0024
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9781479846641.003.0024)
- [Production](https://doi.org/10.18574/nyu/9781479846641.003.0024)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9781479846641.003.0024)
- [XML](https://api.crossref.org/works/10.18574/nyu/9781479846641.003.0024.xml)
<hr>

### 10.7868/S0869565213060200
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0869565213060200)
- [Production](https://doi.org/10.7868/S0869565213060200)
- [JSON](https://api.crossref.org/works/10.7868/S0869565213060200)
- [XML](https://api.crossref.org/works/10.7868/S0869565213060200.xml)
<hr>

### 10.18574/nyu/9781479865437.003.0004
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9781479865437.003.0004)
- [Production](https://doi.org/10.18574/nyu/9781479865437.003.0004)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9781479865437.003.0004)
- [XML](https://api.crossref.org/works/10.18574/nyu/9781479865437.003.0004.xml)
<hr>

### 10.2307/1852311
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1852311)
- [Production](https://doi.org/10.2307/1852311)
- [JSON](https://api.crossref.org/works/10.2307/1852311)
- [XML](https://api.crossref.org/works/10.2307/1852311.xml)
<hr>

### 10.1144/GSL.SP.1995.090.01.15
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1995.090.01.15)
- [Production](https://doi.org/10.1144/GSL.SP.1995.090.01.15)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1995.090.01.15)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1995.090.01.15.xml)
<hr>

### 10.1070/RM1986v041n02ABEH003271
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1986v041n02ABEH003271)
- [Production](https://doi.org/10.1070/RM1986v041n02ABEH003271)
- [JSON](https://api.crossref.org/works/10.1070/RM1986v041n02ABEH003271)
- [XML](https://api.crossref.org/works/10.1070/RM1986v041n02ABEH003271.xml)
<hr>

### 10.1134/S0040363613050056
- [Chooser POC](https://chooser.fly.dev/?doi=10.1134/S0040363613050056)
- [Production](https://doi.org/10.1134/S0040363613050056)
- [JSON](https://api.crossref.org/works/10.1134/S0040363613050056)
- [XML](https://api.crossref.org/works/10.1134/S0040363613050056.xml)
<hr>

### 10.2307/1849293
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1849293)
- [Production](https://doi.org/10.2307/1849293)
- [JSON](https://api.crossref.org/works/10.2307/1849293)
- [XML](https://api.crossref.org/works/10.2307/1849293.xml)
<hr>

### 10.15446/dyna.v81n186.39760
- [Chooser POC](https://chooser.fly.dev/?doi=10.15446/dyna.v81n186.39760)
- [Production](https://doi.org/10.15446/dyna.v81n186.39760)
- [JSON](https://api.crossref.org/works/10.15446/dyna.v81n186.39760)
- [XML](https://api.crossref.org/works/10.15446/dyna.v81n186.39760.xml)
<hr>

### 10.1596/34583
- [Chooser POC](https://chooser.fly.dev/?doi=10.1596/34583)
- [Production](https://doi.org/10.1596/34583)
- [JSON](https://api.crossref.org/works/10.1596/34583)
- [XML](https://api.crossref.org/works/10.1596/34583.xml)
<hr>

### 10.1049/icp.2021.1104
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/icp.2021.1104)
- [Production](https://doi.org/10.1049/icp.2021.1104)
- [JSON](https://api.crossref.org/works/10.1049/icp.2021.1104)
- [XML](https://api.crossref.org/works/10.1049/icp.2021.1104.xml)
<hr>

### 10.2307/1859052
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1859052)
- [Production](https://doi.org/10.2307/1859052)
- [JSON](https://api.crossref.org/works/10.2307/1859052)
- [XML](https://api.crossref.org/works/10.2307/1859052.xml)
<hr>

### 10.2113/gsecongeo.30.7.715
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.30.7.715)
- [Production](https://doi.org/10.2113/gsecongeo.30.7.715)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.30.7.715)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.30.7.715.xml)
<hr>

### 10.2113/gsecongeo.29.7.697
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.29.7.697)
- [Production](https://doi.org/10.2113/gsecongeo.29.7.697)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.29.7.697)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.29.7.697.xml)
<hr>

### 10.5840/jsce201939114
- [Chooser POC](https://chooser.fly.dev/?doi=10.5840/jsce201939114)
- [Production](https://doi.org/10.5840/jsce201939114)
- [JSON](https://api.crossref.org/works/10.5840/jsce201939114)
- [XML](https://api.crossref.org/works/10.5840/jsce201939114.xml)
<hr>

### 10.2307/1847231
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1847231)
- [Production](https://doi.org/10.2307/1847231)
- [JSON](https://api.crossref.org/works/10.2307/1847231)
- [XML](https://api.crossref.org/works/10.2307/1847231.xml)
<hr>

### 10.1525/bio.2010.60.6.11
- [Chooser POC](https://chooser.fly.dev/?doi=10.1525/bio.2010.60.6.11)
- [Production](https://doi.org/10.1525/bio.2010.60.6.11)
- [JSON](https://api.crossref.org/works/10.1525/bio.2010.60.6.11)
- [XML](https://api.crossref.org/works/10.1525/bio.2010.60.6.11.xml)
<hr>

### 10.5040/9781350078260.ch-010
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781350078260.ch-010)
- [Production](https://doi.org/10.5040/9781350078260.ch-010)
- [JSON](https://api.crossref.org/works/10.5040/9781350078260.ch-010)
- [XML](https://api.crossref.org/works/10.5040/9781350078260.ch-010.xml)
<hr>

### 10.2307/1849051
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1849051)
- [Production](https://doi.org/10.2307/1849051)
- [JSON](https://api.crossref.org/works/10.2307/1849051)
- [XML](https://api.crossref.org/works/10.2307/1849051.xml)
<hr>

### 10.1070/PU1968v011n03ABEH004011
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/PU1968v011n03ABEH004011)
- [Production](https://doi.org/10.1070/PU1968v011n03ABEH004011)
- [JSON](https://api.crossref.org/works/10.1070/PU1968v011n03ABEH004011)
- [XML](https://api.crossref.org/works/10.1070/PU1968v011n03ABEH004011.xml)
<hr>

### 10.1070/PU1978v021n07ABEH005670
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/PU1978v021n07ABEH005670)
- [Production](https://doi.org/10.1070/PU1978v021n07ABEH005670)
- [JSON](https://api.crossref.org/works/10.1070/PU1978v021n07ABEH005670)
- [XML](https://api.crossref.org/works/10.1070/PU1978v021n07ABEH005670.xml)
<hr>

### 10.5040/9781350011847.ch-003
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781350011847.ch-003)
- [Production](https://doi.org/10.5040/9781350011847.ch-003)
- [JSON](https://api.crossref.org/works/10.5040/9781350011847.ch-003)
- [XML](https://api.crossref.org/works/10.5040/9781350011847.ch-003.xml)
<hr>

### 10.1049/cp:20061126
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp:20061126)
- [Production](https://doi.org/10.1049/cp:20061126)
- [JSON](https://api.crossref.org/works/10.1049/cp:20061126)
- [XML](https://api.crossref.org/works/10.1049/cp:20061126.xml)
<hr>

### 10.1070/PU1969v011n04ABEH003762
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/PU1969v011n04ABEH003762)
- [Production](https://doi.org/10.1070/PU1969v011n04ABEH003762)
- [JSON](https://api.crossref.org/works/10.1070/PU1969v011n04ABEH003762)
- [XML](https://api.crossref.org/works/10.1070/PU1969v011n04ABEH003762.xml)
<hr>

### 10.2307/2168438
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2168438)
- [Production](https://doi.org/10.2307/2168438)
- [JSON](https://api.crossref.org/works/10.2307/2168438)
- [XML](https://api.crossref.org/works/10.2307/2168438.xml)
<hr>

### 10.1070/PU1963v005n06ABEH003468
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/PU1963v005n06ABEH003468)
- [Production](https://doi.org/10.1070/PU1963v005n06ABEH003468)
- [JSON](https://api.crossref.org/works/10.1070/PU1963v005n06ABEH003468)
- [XML](https://api.crossref.org/works/10.1070/PU1963v005n06ABEH003468.xml)
<hr>

### 10.2307/2171509
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2171509)
- [Production](https://doi.org/10.2307/2171509)
- [JSON](https://api.crossref.org/works/10.2307/2171509)
- [XML](https://api.crossref.org/works/10.2307/2171509.xml)
<hr>

### 10.1180/claymin.2013.048.2.08
- [Chooser POC](https://chooser.fly.dev/?doi=10.1180/claymin.2013.048.2.08)
- [Production](https://doi.org/10.1180/claymin.2013.048.2.08)
- [JSON](https://api.crossref.org/works/10.1180/claymin.2013.048.2.08)
- [XML](https://api.crossref.org/works/10.1180/claymin.2013.048.2.08.xml)
<hr>

### 10.1144/GSL.SP.1979.008.01.58
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1979.008.01.58)
- [Production](https://doi.org/10.1144/GSL.SP.1979.008.01.58)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1979.008.01.58)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1979.008.01.58.xml)
<hr>

### 10.1070/RM1986v041n02ABEH003755
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1986v041n02ABEH003755)
- [Production](https://doi.org/10.1070/RM1986v041n02ABEH003755)
- [JSON](https://api.crossref.org/works/10.1070/RM1986v041n02ABEH003755)
- [XML](https://api.crossref.org/works/10.1070/RM1986v041n02ABEH003755.xml)
<hr>

### 10.2190/MXY8-5NX4-5MY5-CBAR
- [Chooser POC](https://chooser.fly.dev/?doi=10.2190/MXY8-5NX4-5MY5-CBAR)
- [Production](https://doi.org/10.2190/MXY8-5NX4-5MY5-CBAR)
- [JSON](https://api.crossref.org/works/10.2190/MXY8-5NX4-5MY5-CBAR)
- [XML](https://api.crossref.org/works/10.2190/MXY8-5NX4-5MY5-CBAR.xml)
<hr>

### 10.5949/UPO9781846312489.007
- [Chooser POC](https://chooser.fly.dev/?doi=10.5949/UPO9781846312489.007)
- [Production](https://doi.org/10.5949/UPO9781846312489.007)
- [JSON](https://api.crossref.org/works/10.5949/UPO9781846312489.007)
- [XML](https://api.crossref.org/works/10.5949/UPO9781846312489.007.xml)
<hr>

### 10.1049/icp.2020.0329
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/icp.2020.0329)
- [Production](https://doi.org/10.1049/icp.2020.0329)
- [JSON](https://api.crossref.org/works/10.1049/icp.2020.0329)
- [XML](https://api.crossref.org/works/10.1049/icp.2020.0329.xml)
<hr>

### 10.2307/1845614
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1845614)
- [Production](https://doi.org/10.2307/1845614)
- [JSON](https://api.crossref.org/works/10.2307/1845614)
- [XML](https://api.crossref.org/works/10.2307/1845614.xml)
<hr>

### 10.1070/RM2005v060n05ABEH003737
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM2005v060n05ABEH003737)
- [Production](https://doi.org/10.1070/RM2005v060n05ABEH003737)
- [JSON](https://api.crossref.org/works/10.1070/RM2005v060n05ABEH003737)
- [XML](https://api.crossref.org/works/10.1070/RM2005v060n05ABEH003737.xml)
<hr>

### 10.5406/j.ctt1ws7w2c.10
- [Chooser POC](https://chooser.fly.dev/?doi=10.5406/j.ctt1ws7w2c.10)
- [Production](https://doi.org/10.5406/j.ctt1ws7w2c.10)
- [JSON](https://api.crossref.org/works/10.5406/j.ctt1ws7w2c.10)
- [XML](https://api.crossref.org/works/10.5406/j.ctt1ws7w2c.10.xml)
<hr>

### 10.2307/1851747
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1851747)
- [Production](https://doi.org/10.2307/1851747)
- [JSON](https://api.crossref.org/works/10.2307/1851747)
- [XML](https://api.crossref.org/works/10.2307/1851747.xml)
<hr>

### 10.2307/1844997
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1844997)
- [Production](https://doi.org/10.2307/1844997)
- [JSON](https://api.crossref.org/works/10.2307/1844997)
- [XML](https://api.crossref.org/works/10.2307/1844997.xml)
<hr>

### 10.1144/GSL.SP.1982.010.01.06
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1982.010.01.06)
- [Production](https://doi.org/10.1144/GSL.SP.1982.010.01.06)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1982.010.01.06)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1982.010.01.06.xml)
<hr>

### 10.3343/lmo.2015.5.3.121
- [Chooser POC](https://chooser.fly.dev/?doi=10.3343/lmo.2015.5.3.121)
- [Production](https://doi.org/10.3343/lmo.2015.5.3.121)
- [JSON](https://api.crossref.org/works/10.3343/lmo.2015.5.3.121)
- [XML](https://api.crossref.org/works/10.3343/lmo.2015.5.3.121.xml)
<hr>

### 10.2307/1867342
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1867342)
- [Production](https://doi.org/10.2307/1867342)
- [JSON](https://api.crossref.org/works/10.2307/1867342)
- [XML](https://api.crossref.org/works/10.2307/1867342.xml)
<hr>

### 10.1144/GSL.SP.1999.164.01.08
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1999.164.01.08)
- [Production](https://doi.org/10.1144/GSL.SP.1999.164.01.08)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1999.164.01.08)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1999.164.01.08.xml)
<hr>

### 10.1049/cp.2019.0842
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp.2019.0842)
- [Production](https://doi.org/10.1049/cp.2019.0842)
- [JSON](https://api.crossref.org/works/10.1049/cp.2019.0842)
- [XML](https://api.crossref.org/works/10.1049/cp.2019.0842.xml)
<hr>

### 10.18502/ijps.v15i3.3823
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/ijps.v15i3.3823)
- [Production](https://doi.org/10.18502/ijps.v15i3.3823)
- [JSON](https://api.crossref.org/works/10.18502/ijps.v15i3.3823)
- [XML](https://api.crossref.org/works/10.18502/ijps.v15i3.3823.xml)
<hr>

### 10.5040/9781501349751.0009
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781501349751.0009)
- [Production](https://doi.org/10.5040/9781501349751.0009)
- [JSON](https://api.crossref.org/works/10.5040/9781501349751.0009)
- [XML](https://api.crossref.org/works/10.5040/9781501349751.0009.xml)
<hr>

### 10.15530/urtec-2014-1922974
- [Chooser POC](https://chooser.fly.dev/?doi=10.15530/urtec-2014-1922974)
- [Production](https://doi.org/10.15530/urtec-2014-1922974)
- [JSON](https://api.crossref.org/works/10.15530/urtec-2014-1922974)
- [XML](https://api.crossref.org/works/10.15530/urtec-2014-1922974.xml)
<hr>

### 10.3901/CJME.2005.03.457
- [Chooser POC](https://chooser.fly.dev/?doi=10.3901/CJME.2005.03.457)
- [Production](https://doi.org/10.3901/CJME.2005.03.457)
- [JSON](https://api.crossref.org/works/10.3901/CJME.2005.03.457)
- [XML](https://api.crossref.org/works/10.3901/CJME.2005.03.457.xml)
<hr>

### 10.2190/SH.8.1.j
- [Chooser POC](https://chooser.fly.dev/?doi=10.2190/SH.8.1.j)
- [Production](https://doi.org/10.2190/SH.8.1.j)
- [JSON](https://api.crossref.org/works/10.2190/SH.8.1.j)
- [XML](https://api.crossref.org/works/10.2190/SH.8.1.j.xml)
<hr>

### 10.5040/9781501337161.ch-002
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781501337161.ch-002)
- [Production](https://doi.org/10.5040/9781501337161.ch-002)
- [JSON](https://api.crossref.org/works/10.5040/9781501337161.ch-002)
- [XML](https://api.crossref.org/works/10.5040/9781501337161.ch-002.xml)
<hr>

### 10.5040/9781474224451.ch-001
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781474224451.ch-001)
- [Production](https://doi.org/10.5040/9781474224451.ch-001)
- [JSON](https://api.crossref.org/works/10.5040/9781474224451.ch-001)
- [XML](https://api.crossref.org/works/10.5040/9781474224451.ch-001.xml)
<hr>

### 10.1070/RM1983v038n01ABEH003404
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1983v038n01ABEH003404)
- [Production](https://doi.org/10.1070/RM1983v038n01ABEH003404)
- [JSON](https://api.crossref.org/works/10.1070/RM1983v038n01ABEH003404)
- [XML](https://api.crossref.org/works/10.1070/RM1983v038n01ABEH003404.xml)
<hr>

### 10.18502/ehj.v6i2.5022
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/ehj.v6i2.5022)
- [Production](https://doi.org/10.18502/ehj.v6i2.5022)
- [JSON](https://api.crossref.org/works/10.18502/ehj.v6i2.5022)
- [XML](https://api.crossref.org/works/10.18502/ehj.v6i2.5022.xml)
<hr>

### 10.1070/RM1992v047n06ABEH000978
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/RM1992v047n06ABEH000978)
- [Production](https://doi.org/10.1070/RM1992v047n06ABEH000978)
- [JSON](https://api.crossref.org/works/10.1070/RM1992v047n06ABEH000978)
- [XML](https://api.crossref.org/works/10.1070/RM1992v047n06ABEH000978.xml)
<hr>

### 10.7765/9781526117472.00006
- [Chooser POC](https://chooser.fly.dev/?doi=10.7765/9781526117472.00006)
- [Production](https://doi.org/10.7765/9781526117472.00006)
- [JSON](https://api.crossref.org/works/10.7765/9781526117472.00006)
- [XML](https://api.crossref.org/works/10.7765/9781526117472.00006.xml)
<hr>

### 10.1190/1.9781560801641.ch5
- [Chooser POC](https://chooser.fly.dev/?doi=10.1190/1.9781560801641.ch5)
- [Production](https://doi.org/10.1190/1.9781560801641.ch5)
- [JSON](https://api.crossref.org/works/10.1190/1.9781560801641.ch5)
- [XML](https://api.crossref.org/works/10.1190/1.9781560801641.ch5.xml)
<hr>

### 10.1144/GSL.SP.1996.117.01.10
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1996.117.01.10)
- [Production](https://doi.org/10.1144/GSL.SP.1996.117.01.10)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1996.117.01.10)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1996.117.01.10.xml)
<hr>

### 10.2113/gsecongeo.4.3.214
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/gsecongeo.4.3.214)
- [Production](https://doi.org/10.2113/gsecongeo.4.3.214)
- [JSON](https://api.crossref.org/works/10.2113/gsecongeo.4.3.214)
- [XML](https://api.crossref.org/works/10.2113/gsecongeo.4.3.214.xml)
<hr>

### 10.3109/10401230109148958
- [Chooser POC](https://chooser.fly.dev/?doi=10.3109/10401230109148958)
- [Production](https://doi.org/10.3109/10401230109148958)
- [JSON](https://api.crossref.org/works/10.3109/10401230109148958)
- [XML](https://api.crossref.org/works/10.3109/10401230109148958.xml)
<hr>

### 10.18502/pbr.v5i2.1580
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/pbr.v5i2.1580)
- [Production](https://doi.org/10.18502/pbr.v5i2.1580)
- [JSON](https://api.crossref.org/works/10.18502/pbr.v5i2.1580)
- [XML](https://api.crossref.org/works/10.18502/pbr.v5i2.1580.xml)
<hr>

### 10.2307/2165614
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2165614)
- [Production](https://doi.org/10.2307/2165614)
- [JSON](https://api.crossref.org/works/10.2307/2165614)
- [XML](https://api.crossref.org/works/10.2307/2165614.xml)
<hr>

### 10.1049/cp.2016.0628
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp.2016.0628)
- [Production](https://doi.org/10.1049/cp.2016.0628)
- [JSON](https://api.crossref.org/works/10.1049/cp.2016.0628)
- [XML](https://api.crossref.org/works/10.1049/cp.2016.0628.xml)
<hr>

### 10.5040/9781501356308.0007
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781501356308.0007)
- [Production](https://doi.org/10.5040/9781501356308.0007)
- [JSON](https://api.crossref.org/works/10.5040/9781501356308.0007)
- [XML](https://api.crossref.org/works/10.5040/9781501356308.0007.xml)
<hr>

### 10.2307/1858352
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1858352)
- [Production](https://doi.org/10.2307/1858352)
- [JSON](https://api.crossref.org/works/10.2307/1858352)
- [XML](https://api.crossref.org/works/10.2307/1858352.xml)
<hr>

### 10.2307/1851486
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1851486)
- [Production](https://doi.org/10.2307/1851486)
- [JSON](https://api.crossref.org/works/10.2307/1851486)
- [XML](https://api.crossref.org/works/10.2307/1851486.xml)
<hr>

### 10.1144/GSL.SP.1986.024.01.22
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1986.024.01.22)
- [Production](https://doi.org/10.1144/GSL.SP.1986.024.01.22)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1986.024.01.22)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1986.024.01.22.xml)
<hr>

### 10.18574/nyu/9781479880096.003.0006
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9781479880096.003.0006)
- [Production](https://doi.org/10.18574/nyu/9781479880096.003.0006)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9781479880096.003.0006)
- [XML](https://api.crossref.org/works/10.18574/nyu/9781479880096.003.0006.xml)
<hr>

### 10.2307/2171681
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/2171681)
- [Production](https://doi.org/10.2307/2171681)
- [JSON](https://api.crossref.org/works/10.2307/2171681)
- [XML](https://api.crossref.org/works/10.2307/2171681.xml)
<hr>

### 10.1353/sce.2017.0049
- [Chooser POC](https://chooser.fly.dev/?doi=10.1353/sce.2017.0049)
- [Production](https://doi.org/10.1353/sce.2017.0049)
- [JSON](https://api.crossref.org/works/10.1353/sce.2017.0049)
- [XML](https://api.crossref.org/works/10.1353/sce.2017.0049.xml)
<hr>

### 10.5040/9781838710712.ch-009
- [Chooser POC](https://chooser.fly.dev/?doi=10.5040/9781838710712.ch-009)
- [Production](https://doi.org/10.5040/9781838710712.ch-009)
- [JSON](https://api.crossref.org/works/10.5040/9781838710712.ch-009)
- [XML](https://api.crossref.org/works/10.5040/9781838710712.ch-009.xml)
<hr>

### 10.1144/GSL.SP.1994.079.01.20
- [Chooser POC](https://chooser.fly.dev/?doi=10.1144/GSL.SP.1994.079.01.20)
- [Production](https://doi.org/10.1144/GSL.SP.1994.079.01.20)
- [JSON](https://api.crossref.org/works/10.1144/GSL.SP.1994.079.01.20)
- [XML](https://api.crossref.org/works/10.1144/GSL.SP.1994.079.01.20.xml)
<hr>

### 10.1049/icp.2021.1427
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/icp.2021.1427)
- [Production](https://doi.org/10.1049/icp.2021.1427)
- [JSON](https://api.crossref.org/works/10.1049/icp.2021.1427)
- [XML](https://api.crossref.org/works/10.1049/icp.2021.1427.xml)
<hr>

### 10.1070/PU1986v029n06ABEH003427
- [Chooser POC](https://chooser.fly.dev/?doi=10.1070/PU1986v029n06ABEH003427)
- [Production](https://doi.org/10.1070/PU1986v029n06ABEH003427)
- [JSON](https://api.crossref.org/works/10.1070/PU1986v029n06ABEH003427)
- [XML](https://api.crossref.org/works/10.1070/PU1986v029n06ABEH003427.xml)
<hr>

### 10.2110/cor.94.01.0173
- [Chooser POC](https://chooser.fly.dev/?doi=10.2110/cor.94.01.0173)
- [Production](https://doi.org/10.2110/cor.94.01.0173)
- [JSON](https://api.crossref.org/works/10.2110/cor.94.01.0173)
- [XML](https://api.crossref.org/works/10.2110/cor.94.01.0173.xml)
<hr>

### 10.18574/nyu/9781479870479.003.0003
- [Chooser POC](https://chooser.fly.dev/?doi=10.18574/nyu/9781479870479.003.0003)
- [Production](https://doi.org/10.18574/nyu/9781479870479.003.0003)
- [JSON](https://api.crossref.org/works/10.18574/nyu/9781479870479.003.0003)
- [XML](https://api.crossref.org/works/10.18574/nyu/9781479870479.003.0003.xml)
<hr>

### 10.2307/1833553
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1833553)
- [Production](https://doi.org/10.2307/1833553)
- [JSON](https://api.crossref.org/works/10.2307/1833553)
- [XML](https://api.crossref.org/works/10.2307/1833553.xml)
<hr>

### 10.1049/cp.2014.1552
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp.2014.1552)
- [Production](https://doi.org/10.1049/cp.2014.1552)
- [JSON](https://api.crossref.org/works/10.1049/cp.2014.1552)
- [XML](https://api.crossref.org/works/10.1049/cp.2014.1552.xml)
<hr>

### 10.2190/0H1K-8GU7-NC5H-1LX0
- [Chooser POC](https://chooser.fly.dev/?doi=10.2190/0H1K-8GU7-NC5H-1LX0)
- [Production](https://doi.org/10.2190/0H1K-8GU7-NC5H-1LX0)
- [JSON](https://api.crossref.org/works/10.2190/0H1K-8GU7-NC5H-1LX0)
- [XML](https://api.crossref.org/works/10.2190/0H1K-8GU7-NC5H-1LX0.xml)
<hr>

### 10.2307/1855779
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1855779)
- [Production](https://doi.org/10.2307/1855779)
- [JSON](https://api.crossref.org/works/10.2307/1855779)
- [XML](https://api.crossref.org/works/10.2307/1855779.xml)
<hr>

### 10.7868/S0453881114010158
- [Chooser POC](https://chooser.fly.dev/?doi=10.7868/S0453881114010158)
- [Production](https://doi.org/10.7868/S0453881114010158)
- [JSON](https://api.crossref.org/works/10.7868/S0453881114010158)
- [XML](https://api.crossref.org/works/10.7868/S0453881114010158.xml)
<hr>

### 10.2307/1840970
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1840970)
- [Production](https://doi.org/10.2307/1840970)
- [JSON](https://api.crossref.org/works/10.2307/1840970)
- [XML](https://api.crossref.org/works/10.2307/1840970.xml)
<hr>

### 10.1049/cp:20060823
- [Chooser POC](https://chooser.fly.dev/?doi=10.1049/cp:20060823)
- [Production](https://doi.org/10.1049/cp:20060823)
- [JSON](https://api.crossref.org/works/10.1049/cp:20060823)
- [XML](https://api.crossref.org/works/10.1049/cp:20060823.xml)
<hr>

### 10.2307/1842503
- [Chooser POC](https://chooser.fly.dev/?doi=10.2307/1842503)
- [Production](https://doi.org/10.2307/1842503)
- [JSON](https://api.crossref.org/works/10.2307/1842503)
- [XML](https://api.crossref.org/works/10.2307/1842503.xml)
<hr>

### 10.2113/JEEG24.1.87
- [Chooser POC](https://chooser.fly.dev/?doi=10.2113/JEEG24.1.87)
- [Production](https://doi.org/10.2113/JEEG24.1.87)
- [JSON](https://api.crossref.org/works/10.2113/JEEG24.1.87)
- [XML](https://api.crossref.org/works/10.2113/JEEG24.1.87.xml)
<hr>

### 10.2110/jsr.43.580
- [Chooser POC](https://chooser.fly.dev/?doi=10.2110/jsr.43.580)
- [Production](https://doi.org/10.2110/jsr.43.580)
- [JSON](https://api.crossref.org/works/10.2110/jsr.43.580)
- [XML](https://api.crossref.org/works/10.2110/jsr.43.580.xml)
<hr>

### 10.18352/ts.162
- [Chooser POC](https://chooser.fly.dev/?doi=10.18352/ts.162)
- [Production](https://doi.org/10.18352/ts.162)
- [JSON](https://api.crossref.org/works/10.18352/ts.162)
- [XML](https://api.crossref.org/works/10.18352/ts.162.xml)
<hr>

### 10.18502/acta.v58i4.3920
- [Chooser POC](https://chooser.fly.dev/?doi=10.18502/acta.v58i4.3920)
- [Production](https://doi.org/10.18502/acta.v58i4.3920)
- [JSON](https://api.crossref.org/works/10.18502/acta.v58i4.3920)
- [XML](https://api.crossref.org/works/10.18502/acta.v58i4.3920.xml)
<hr>

