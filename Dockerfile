FROM public.ecr.aws/docker/library/python:3.11
#FROM public.ecr.aws/docker/library/python:3.11.1-alpine3.17

WORKDIR /app
ENV VIRTUAL_ENV=/opt/venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN python3 -m venv $VIRTUAL_ENV

RUN if [ ! -f "VERSION.txt" ]; then echo "Unspecified version" > VERSION.txt ; fi

COPY . /app


RUN pip install --no-cache-dir --upgrade pip==23.0 && pip install --use-pep517 --no-cache-dir -r requirements.txt

EXPOSE 9000
#CMD ["uvicorn", "--workers", "4", "--bind", "0.0.0.0:8000", "chooser:app"]
CMD ["uvicorn", "chooser:app", "--workers","4", "--host", "0.0.0.0", "--port", "9000", "--log-level", "critical"]