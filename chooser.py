"""
This is the main Chooser module that defines FastAPI entry points
"""

import json
import logging
import pathlib

import sentry_sdk
from aws_lambda_powertools.logging.formatter import JsonFormatter
from fastapi import FastAPI, Request, Response
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from crossrefutils.api import APIConnectionException
from crossrefutils.metadata import clear_metadata_cache, link_header, metadata

# Do we want to return a 300 when there are multiple choices?
# Theoretically- *yes*, but we have never done this before and
# it might break things.
ENABLE_SEMANTIC_LINK_HEADERS = False


class ExampleSentryException(Exception):
    """
    A placeholder exception for example Sentry errors
    """


# NB you need to set the SENTRY_DSN environment variable
sentry_sdk.init(
    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production,
    traces_sample_rate=1.0,
)


logging.basicConfig(level="INFO")

# pylint: disable=duplicate-code
logging.getLogger("uvicorn.access").disabled = True

logger = logging.getLogger(__name__)

handler = logging.StreamHandler()
handler.setFormatter(JsonFormatter())

logger.handlers = [handler]
logger.setLevel(logging.INFO)
logger.propagate = False


app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")


logging.getLogger("boto3").setLevel(logging.WARNING)
logging.getLogger("botocore").setLevel(logging.WARNING)
logging.getLogger("nose").setLevel(logging.WARNING)


async def not_found(request) -> templates.TemplateResponse:
    """
    Return a 404 response when the DOI is not found
    """
    return templates.TemplateResponse(
        "404.html", {"request": request}, status_code=404
    )


async def bad_request(request):
    """
    Return a 400 response when the DOI is not provided
    """
    return templates.TemplateResponse(
        "400.html", {"request": request}, status_code=400
    )


async def api_error(request):
    """
    Return a 504 response when the API is not available
    """
    return templates.TemplateResponse(
        "api-error.html", {"request": request}, status_code=504
    )


async def choice(request, summary):
    """
    Return the choice template with the summary data
    """
    _, headers = await link_header(summary)

    http_status = 200
    if ENABLE_SEMANTIC_LINK_HEADERS:
        http_status = 300 if headers else 200
    if _ := summary.get("member_id"):
        pass
    else:
        logger.warning("Missing member_id for %s", summary["doi"])

    if headers:
        try:
            for _, dict_value in headers.items():
                dict_value.encode("latin1")
        except UnicodeEncodeError:
            # this header cannot be handled by the latin1 encoding that is the
            # basis of Starlette, so don't send a header
            headers = {}

    return templates.TemplateResponse(
        "choose.html",
        {
            "request": request,
            "summary": summary,
            "none_value": "-",
        },
        status_code=http_status,
        headers=headers,
    )


@app.get("/", response_class=HTMLResponse)
async def root(request: Request, doi: str):
    """
    Handle the root path for the application
    """
    # exclude common non-DOI paths
    if doi:
        if doi.lower() in [
            "favicon.ico",
            "robots.txt",
            "style.css",
            "favicon.svg",
        ]:
            return Response(status_code=404)

    if not doi:
        return await bad_request(request)
    try:
        summary = await metadata(doi.lower())
    except APIConnectionException:
        logger.error(
            "Chooser APIConnectionException: Error getting metadata for %s",
            doi,
        )
        return await api_error(request)

    return (
        await choice(request, summary) if summary else await not_found(request)
    )


@app.get("/clear-cache/{doi_path:path}")
async def clear_cache(_: Request, doi_path: str = None, doi: str = None):
    """
    Clear the metadata cache for a given
    """
    doi = doi.lower() if doi else doi_path.lower()
    msg = await clear_metadata_cache(doi)
    return Response(msg)


@app.get("/sentry-debug")
async def trigger_error():
    """
    Trigger an error to test Sentry
    """
    raise ExampleSentryException(
        "This is an example of an exception being sent to Sentry."
    )


@app.get("/heartbeat")
async def heartbeat():
    """
    Return a simple heartbeat response
    """
    try:
        version = (
            pathlib.Path("VERSION.txt").read_text(encoding="utf-8").strip()
        )
    except FileNotFoundError:
        version = "Unspecified version"

    return Response(
        json.dumps({"version": version}),
        media_type="application/json",
    )


# always ensure that this path is last as it's the fallback that allows
# /doi endpoint to be handled by the root_path
@app.get("/{doi:path}", response_class=HTMLResponse)
async def root_path(request: Request, doi: str):
    """
    Handle the root path for the application
    """
    return await root(request=request, doi=doi)


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app)
