"""
This module contains functions that generate metadata for a given DOI. The
metadata is used to generate a choice page for the DOI.
"""

import calendar
import logging
import os
from datetime import timedelta

import tldextract
from aws_lambda_powertools.logging.formatter import JsonFormatter
from cashews import cache, invalidate_further
from tenacity import RetryError

from crossrefutils.api import clearbit_logo, fetch_work_metadata

# pylint: disable=duplicate-code
logger = logging.getLogger(__name__)

handler = logging.StreamHandler()
handler.setFormatter(JsonFormatter())

logger.handlers = [handler]
logger.setLevel(logging.INFO)
logger.propagate = False

ENV = os.environ.get("ENV", default="development").lower()

if ENV == "development":
    cache.setup("disk://?directory=chooser_cache", shards=4, disable=True)
else:
    cache.setup("disk://?directory=chooser_cache", shards=4)


def join_links(multires, coaccess):
    """
    Takes a multiple-resolution link and a coaccess link, and returns a
    classification and a dictionary of the links
    """
    if multires and coaccess:
        return "mixed", f"{multires}, {coaccess}"
    if multires:
        return "multiple_resolution", multires
    if coaccess:
        return "coaccess", coaccess

    return "direct", None


# pylint: disable=broad-except
async def link_header(summary):
    """
    Takes a summary of the metadata for a given DOI and returns a tuple
    containing the classification of the DOI and a dictionary of the links
    """
    try:
        multires = ",".join(
            [
                f"{resource['url']}; rel=\"multiple-resolution\""
                for resource in summary["multiple-resolution"]
            ]
        )
        coaccess = ",".join(
            [
                f"{resource['url']}; rel=\"co-access\""
                for resource in summary["coaccess"]
            ]
        )
        resolution_type, links = join_links(multires, coaccess)
        return (
            (resolution_type, {"Link": links})
            if links
            else (resolution_type, None)
        )

    except Exception as exception_object:
        logger.error("Error generating Link header: %s", exception_object)
        logger.error("summary: %s", summary)
        return None


def classify(summary):
    """
    If the DOI has both multiple-resolution and coaccess, it's both. If it
    has multiple-resolution, it's multiple-resolution. If it has coaccess,
    it's coaccess. Otherwise, it's standard
    """
    resolution_type = "standard"
    multiple_res = len(summary["multiple-resolution"]) > 0
    co_access = len(summary["coaccess"]) > 0
    if multiple_res and co_access:
        resolution_type = "both"
    elif multiple_res:
        resolution_type = "multiple-resolution"
    elif co_access:
        resolution_type = "coaccess"

    logger.debug("Classified: %s as %s", summary["doi"], resolution_type)

    return resolution_type


def tld(url):
    """
    It takes a URL and returns the registered domain

    :param url: The URL to extract the domain from
    :return: The registered domain of the url.
    """

    # this block protects against tldextract performing a regular expression
    # on a None value
    if url is None:
        return None

    return tldextract.extract(url).registered_domain


def multiple_resolution_metadata(resource):
    """
    It takes a resource, and returns a dictionary with the URL, the top level
    domain, and the Clearbit logo for the top level domain
    """
    return {
        "url": resource["URL"],
        "tld": tld(resource["URL"]),
        "clearbit-logo": clearbit_logo(tld(resource["URL"])),
    }


def add_multiple_resolution_data(record):
    """
    For each record, get the secondary resources, and for each secondary
    resource, get the URL and the top level domain, and the logos
    """
    return [
        multiple_resolution_metadata(resource)
        for resource in record["resource"].get("secondary", [])
    ]


async def coacesss_metadata(doi):
    """
    It takes a DOI, looks up the metadata in the Crossref API, and returns a
    dictionary with the DOI, the Crossref member ID, the publisher name,
    the primary resource URL, the top-level domain of the primary resource URL,
    and the Clearbit logo URL for the top-level domain

    :return: A dictionary with the following keys:
        - doi
        - member
        - publisher
        - primary-resource
        - tld
        - clearbit-logo
    """
    try:
        record = await fetch_work_metadata(doi)
        publisher = record["publisher"]

        # pylint: disable=pointless-string-statement
        """
        NB- the above "publisher" is wrong because it is wrong in the
        REST API. But this doesn't matter as we are (2023-08) not displaying
        the publisher name in the template. If we do end up showing the
        publisher name and the REST API is still broken, then you have to do
        an extra request to get he correct publisher name.  The following is
        correct: publisher = await fetch_member_name(record["member"])
        """

        return {
            "doi": doi,
            "member": record["member"],
            "publisher": publisher,
            "url": record["resource"]["primary"]["URL"],
            "tld": tld(record["resource"]["primary"]["URL"]),
            "clearbit-logo": clearbit_logo(
                tld(record["resource"]["primary"]["URL"])
            ),
        }
    except RetryError as exception_object:
        logger.error("Error getting coaccess metadata: %s", exception_object)
        return {
            "doi": None,
            "member": None,
            "publisher": None,
            "url": None,
            "tld": None,
            "clearbit-logo": None,
        }


async def add_coaccess_data(record):
    """
    It takes a record, and for each relation in the record, it checks if the
    relation is an is-identical-to relation, and if it is, it checks if the
    relation is a DOI, and if it is, it returns the coaccess metadata for
    that DOI
    """
    return [
        await coacesss_metadata(relation["id"])
        for relation in record["relation"].get("is-identical-to", [])
        if relation["id-type"] == "doi"
    ]


def add_finances_data(record):
    """
    It takes a record, and for each relation in the record, it checks if the
    relation is a
    finances record, and if it is, it checks if the relation is a DOI, and if
    it is, it returns the coaccess metadata for that DOI

    :param record: the record to be processed
    :return: A list of dictionaries.
    """
    doi_urls = []

    for relation in record["relation"].get("finances", []):
        if relation["id-type"] == "doi":
            doi = relation["id"]
            doi_url = f"https://doi.org/{doi}"
            doi_urls.append(doi_url)

    return doi_urls


def get_doi_url(doi):
    """
    It takes a DOI and returns a URL

    :param doi: the DOI of the article you want to get the metadata for
    :return: A string with the doi url
    """
    return f"https://doi.org/{doi}"


def add_supplementary_id(record):
    """
    It takes a record and returns a string of all the alternative ids separated
    by a pipe character

    :param record: the record to be processed
    :return: A string of all the alternative ids separated by a pipe.
    """
    if "alternative-id" in record:
        return " | ".join(record["alternative-id"])

    return None


def add_names(items_list):
    """
    If the list is empty, return; otherwise, if the item has a name, add it to
    the list; otherwise, if the item has a given and family name, add them
    together; otherwise, if the item has a given name, add it to the list

    :param items_list: a list of dictionaries, each of which has a 'name' key,
    or a 'given' and 'family'
    key
    :return: A string of the names of the contributors to the paper.
    """
    if not items_list:
        return None

    items = []
    for item in items_list:
        name = None
        if "name" in item:
            name = item["name"]
        elif "given" in item and "family" in item:
            name = item["given"] + " " + item["family"]
        elif "given" in item:
            name = item["given"]
        if name:
            items.append(name)
    if items:
        return " | ".join(items)

    return None


def add_generic(record, key):
    """
    Takes a record, and if it has the key field, it returns the result of
    calling add_names on that field. Otherwise, it returns None
    """
    return add_names(record[key] if key in record else None)


def add_lead_investigators(record):
    """
    Grab the lead investigator array from the record and loop through getting
    the investigator data

    :param record: the record we're working on
    :return: a list of lead investigators
    """

    if "lead-investigator" in record.get("project", [{}])[0]:
        return [
            multiple_investigator_metadata(resource)
            for resource in record.get("project", [{}])[0].get(
                "lead-investigator", [{}]
            )
        ]

    return []


def add_co_lead_investigators(record):
    """
    Grab the co-lead investigator array from the record and loop through
    getting the investigator data

    :param record: the record we're working on
    :return: a list of co-lead investigators
    """

    if "co-lead-investigator" in record.get("project", [{}])[0]:
        return [
            multiple_investigator_metadata(resource)
            for resource in record.get("project", [{}])[0].get(
                "co-lead-investigator", [{}]
            )
        ]

    return []


def add_investigators(record):
    """
    Grab the investigator array from the record and loop through getting the
    investigator data

    :param record: the record we're working on
    :return: a list of investigators
    """

    if "investigator" in record.get("project", [{}])[0]:
        return [
            multiple_investigator_metadata(resource)
            for resource in record.get("project", [{}])[0].get(
                "investigator", [{}]
            )
        ]

    return []


def add_investigator_affiliation_data(resource):
    """
    There is an affiliation record for this investigator so check it has
    content and add it to the return

    :param resource: The resource that is being processed
    :return: A dictionary with the investigator affiliation data
    """

    if resource.get("affiliation", [{}]):
        return {
            "name": resource.get("affiliation", [{}])[0].get("name", "none"),
            "rorid": resource.get("affiliation", [{}])[0]
            .get("id", [{}])[0]
            .get("id"),
            "country": resource.get("affiliation", [{}])[0].get("country"),
        }

    return None


def multiple_investigator_metadata(resource):
    """
    It takes a resource, and returns a dictionary with the given and family
    names, ORCID, affiliation name, id country. Using resource.get("key")
    usefully returns "None" if the key is not found.

    :param resource: The resource that is being processed
    :return: A dictionary with the investigator data
    """
    affiliations = []

    if "affiliation" in resource:
        affiliations = [add_investigator_affiliation_data(resource)]

    return {
        "given": resource["given"],
        "family": resource["family"],
        "ORCID": resource.get("ORCID"),
        "affiliations": affiliations,
    }


def add_project_description(record):
    """
    Grab the project description from the record if present

    :param record: The record we're working on
    :return: the project description
    """

    return (
        record.get("project", [{}])[0]
        .get("project-description", [{}])[0]
        .get("description", None)
    )


def add_award_amount(record):
    """
    Grab the award-amount and currency from the record if it's there

    :param record: The record we're working on
    :return: the award amount and currency
    """

    if "award-amount" in record.get("project", [{}])[0]:
        amount = (
            record.get("project", [{}])[0]
            .get("award-amount", [{}])
            .get("amount", 0)
        )

        return_value = (
            record.get("project", [{}])[0]
            .get("award-amount", [{}])
            .get("currency", "")
        )

        return (
            (f"{amount:,.0f}" + " " + return_value)
            if return_value
            else f"{amount:,.0f}"
        )

    return None


def add_funding_scheme(record):
    """
    If the project part of the record has a funding scheme, return it

    :param record: The record that is being processed
    :return: The funding scheme.
    """

    return (
        record.get("project", [{}])[0]
        .get("funding", [{}])[0]
        .get("scheme", None)
    )


def add_internal_award_number(record):
    """
    If the record has an award element, return it

    :param record: The record that is being processed
    :return: The award number.
    """

    return record.get("award", None)


def add_award_start(record):
    """
    If the record has an award start date element, return it, formatted in a
    sub-function

    :param record: The record that is being processed
    :return: The award start date in a nice format.
    """
    award_start = None
    if "award-start" in record.get("project", [{}])[0]:
        award_start = record.get("project", [{}])[0].get("award-start", [{}])

    elif "award-start" in record:
        award_start = record.get("award-start", [{}])

    if award_start and "date-parts" in award_start:
        return format_date(award_start)

    return None


def add_grant_info(record):
    """
    If the record has a 'funder' field, then for each funder in the record,
    if the funder has a 'name' and an 'award' field, then add the funder's
    name and award to the list of funders

    :param record: the record to be processed
    :return: A string of the funder name and award number.
    """

    if "funding" in record.get("project", [{}])[0]:
        if funders := [
            funder["funder"]["name"] + " (" + funder["type"] + ")"
            for funder in record.get("project", [{}])[0].get("funding", [{}])
            if "funder" in funder and "type" in funder
        ]:
            return " | ".join(funders)

    return None


def add_grant_info_funders(record):
    """
    If the record has a 'funder' field, then for each funder in the record,
    if the funder has a 'name' and an 'award' field, then add the funder's
    name and award to the list of funders

    :param record: the record to be processed
    :return: A string of the funder names.
    """

    if "funding" in record.get("project", [{}])[0]:
        if funders := [
            funder["funder"]["name"]
            for funder in record.get("project", [{}])[0].get("funding", [{}])
            if "funder" in funder
        ]:
            return " | ".join(funders)

    return None


def add_grant_info_funder_ids(record):
    """
    If the record has a 'funder' field, then for each funder in the record,
    if the funder has a 'name' and an 'award' field, then add the funder's
    name and award to the list of funders

    :param record: the record to be processed
    :return: A string of the funder names.
    """

    funders = []
    if "funding" in record.get("project", [{}])[0]:
        for funder in record.get("project", [{}])[0].get("funding", [{}]):
            if "funder" in funder:
                funders.append(
                    f"https://doi.org/{funder['funder']['id'][0]['id']}"
                )

    return " | ".join(funders)


def add_grant_info_type(record):
    """
    If the record has a 'funder' field, then return the funding type

    :param record: the record to be processed
    :return: A string of the funding type.
    """

    if "funding" in record.get("project", [{}])[0]:
        result = (
            record.get("project", [{}])[0]
            .get("funding", [{}])[0]
            .get("type", None)
        )

        return result.upper().strip() if result else None

    return None


def add_display_doi(record):
    """
    It takes a record, and if it has a DOI, it returns a link to the DOI

    :param record: the record to be modified
    :return: The DOI URL
    """
    if "DOI" in record:
        return get_doi_url(record["DOI"])

    return None


def add_generic_get(record, key):
    """
    If record has key, return it, otherwise return None
    """

    return record.get(key, None)


def add_title(record):
    """
    If the record has a title, return the first title in the list of titles,
    replacing any backslashes with nothing

    :param record: the record to be processed
    :return: The title of the record.
    """
    if "title" in record:
        return (
            record["title"][0].replace("\\", "")
            if len(record["title"]) > 0
            else ""
        )

    if "grant" == record.get("type", ""):
        return (
            record.get("project", [{}])[0]
            .get("project-title", [{}])[0]
            .get("title", None)
        )

    return None


def add_publication(record):
    """
    If the record has a container-title, return the first one

    :param record: the record to be processed
    :return: The first container-title in the record.
    """
    if "container-title" in record and len(record["container-title"]) > 0:
        return record["container-title"][0]

    return None


def add_published_date(record):
    """
    If the record has a published-print date, use that, otherwise use the
    published-online date, and if there is a date, format it

    :param record: the record we're working with
    :return: A string of the date in the format YYYY-MM-DD
    """
    published = None
    if "published-print" in record:
        published = record["published-print"]
    elif "published-online" in record:
        published = record["published-online"]

    if published and "date-parts" in published:
        return format_date(published)

    return None


def format_date(published):
    """
    If the date-parts array has a length of 3, then we're going to return the
    year, month, and day. If it has a length of 2, then we're going to return
    the year and month. If it has a length of 1, then we're going to return
    the year

    :param published: The date the article was published
    :return: A list of dictionaries.
    """
    date_parts = published["date-parts"][0]
    date_parts_len = len(date_parts)
    published_date = f"{str(date_parts[2])} " if date_parts_len > 2 else ""

    if date_parts_len > 1:
        published_date += f"{calendar.month_name[date_parts[1]]} "
    if date_parts_len > 0:
        published_date += str(date_parts[0])

    return published_date


def add_type(record):
    """
    If the record has a type, replace the hyphens with spaces and make it
    uppercase

    :param record: the record that we're currently processing
    :return: The item type is being returned.
    """
    if "type" in record:
        item_type = record["type"]
        return item_type.replace("-", " ").upper()

    return None


async def relationship_metadata(record):
    """
    Given a record, it returns a dictionary of metadata that is used to
    generate the choice page for the DOI
    """
    logger.debug("Adding resolution metadata")
    coaccess = await add_coaccess_data(record)
    multiple_resolution = add_multiple_resolution_data(record)

    return {
        "doi": record["DOI"],
        "member_id": record["member"],
        "member": record["publisher"],
        "container-title": (
            record["container-title"][0]
            if len(record.get("container-title", [])) > 0
            else ""
        ),
        "primary-resource": record["resource"]["primary"]["URL"],
        "tld": tld(record["resource"]["primary"]["URL"]),
        "clearbit-logo": clearbit_logo(
            tld(record["resource"]["primary"]["URL"])
        ),
        "coaccess": coaccess,
        "multiple-resolution": multiple_resolution,
    }


def citation_metadata(record):
    """
    Given a record, it returns a dictionary of metadata that is used to
    generate the choice page for the DOI
    """
    logger.debug("Adding citation metadata")

    return {
        "type": add_type(record),
        "published_date": add_published_date(record),
        "publication": add_publication(record),
        "title": add_title(record),
        "name": add_generic_get(record, "name"),
        "id": add_generic_get(record, "id"),
        "location": add_generic_get(record, "location"),
        "display_doi": add_display_doi(record),
        "grant_info": add_grant_info(record),
        "grant_info_funders": add_grant_info_funders(record),
        "grant_info_funder_ids": add_grant_info_funder_ids(record),
        "grant_info_type": add_grant_info_type(record),
        "multiple_lead_investigators": add_lead_investigators(record),
        "multiple_co_lead_investigators": add_co_lead_investigators(record),
        "multiple_investigators": add_investigators(record),
        "finances": add_finances_data(record),
        "project_description": add_project_description(record),
        "award_amount": add_award_amount(record),
        "award_start": add_award_start(record),
        "funding_scheme": add_funding_scheme(record),
        "internal_award_number": add_internal_award_number(record),
        "editors": add_generic(record, "editor"),
        "authors": add_generic(record, "author"),
        "chairs": add_generic(record, "chair"),
        "supplementary_ids": add_supplementary_id(record),
    }


@cache(ttl=timedelta(hours=24), key="{doi}")
async def metadata(doi: str = None):
    """
    Given DOI, returns a summary of all the metadata needed
    to make choice.
    """
    logger.info("Getting metadata for %s", doi)

    try:
        record = await fetch_work_metadata(doi)
    except RetryError:
        return None

    if not record:
        return None
    logger.info("Got metadata for %s", doi)
    logger.debug("Generating summary")
    resolution = await relationship_metadata(record)
    citation = citation_metadata(record)
    summary = resolution | citation
    logger.debug("Generated summary")
    return summary


async def clear_metadata_cache(doi: str = None):
    """
    It clears the cache for a given DOI, or all DOIs if no DOI is given
    """
    if doi:
        with invalidate_further():
            await cache.get(f"doi:{doi}")
        msg = f"Cleared cached metadata for {doi}"
    else:
        msg = "Clearing all cached metadata"
        await cache.clear()

    logger.info(msg)
    return msg
