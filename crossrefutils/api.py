"""
This module contains functions that interact with the Crossref API.
"""

import logging
from asyncio import CancelledError
from asyncio import TimeoutError as AsyncTimeoutError
from ssl import SSLWantReadError

import httpx
from aws_lambda_powertools.logging.formatter import JsonFormatter
from tenacity import retry, stop_after_attempt, wait_random_exponential

# pylint: disable=duplicate-code

logger = logging.getLogger(__name__)

handler = logging.StreamHandler()
handler.setFormatter(JsonFormatter())

logger.handlers = [handler]
logger.setLevel(logging.INFO)
logger.propagate = False

CROSSREF_API = "https://api.crossref.org"
DOI_RES = "https://doi.org"
CROSSREF_API_VERSION = "v1"
CITATION_STYLE = "text/x-bibliography; style=apa"
MAILTO = "mailto:chooser@crossref.org"
USER_AGENT = {"User-Agent": f"crossref_resource_chooser; {MAILTO}"}

CLEARBIT_URI = "https://logo.clearbit.com"


class APIConnectionException(Exception):
    """
    This exception is raised when there is a problem connecting to the API.
    """


@retry(
    stop=stop_after_attempt(3),
    wait=wait_random_exponential(multiplier=1, max=60),
)
async def crossref_api(uri: str):
    """
    This function is a wrapper for the Crossref API. It retries the request
    three times before giving up.
    """

    logger.info("Getting: %s", uri)
    try:
        async with httpx.AsyncClient() as client:
            res = await client.get(uri, headers=USER_AGENT)
    except AsyncTimeoutError as exception_object:
        # Handle the timeout error
        logger.error("*** Timeout getting: %s", uri)
        raise APIConnectionException(exception_object) from exception_object
    except CancelledError as exception_object:
        # Handle the cancellation
        logger.error("*** Cancelled getting: %s", uri)
        raise APIConnectionException(exception_object) from exception_object
    except SSLWantReadError as exception_object:
        # Handle the incomplete read operation
        logger.error("*** SSLWantReadError getting: %s", uri)
        raise APIConnectionException(exception_object) from exception_object
    except Exception as exception_object:
        logger.error("*** Exception getting: %s", uri)
        raise APIConnectionException(exception_object) from exception_object

    return res.json()["message"] if res.status_code == 200 else None


async def fetch_work_metadata(doi: str) -> dict:
    """
    Takes a DOI and returns a JSON object
    """
    uri = f"{CROSSREF_API}/{CROSSREF_API_VERSION}/works/{doi}"
    return await crossref_api(uri)


async def fetch_member_metadata(member_id):
    """
    Takes a member ID and returns a JSON object
    """
    uri = f"{CROSSREF_API}/{CROSSREF_API_VERSION}/members/{member_id}"
    return await crossref_api(uri)


async def member_name(member_id):
    """
    Takes a member ID and returns the primary name of the member
    """
    member = await fetch_member_metadata(member_id)
    return member["primary-name"] if member else None


# pylint: disable=broad-except
def clearbit_logo(tld):
    """
    If the logo exists, return the logo URI, otherwise return the URI for a
    default logo
    """
    logger.debug("Detecting logo")
    try:
        return (
            f"{CLEARBIT_URI}/{tld}"
            if httpx.head(f"{CLEARBIT_URI}/{tld}").status_code == 200
            else "/static/no_logo.svg"
        )
    except Exception:
        return "/static/no_logo.svg"
