#!/usr/bin/env bash
uvicorn chooser:app --reload --host 0.0.0.0 --port 9000 --log-level debug