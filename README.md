# README

![license](https://img.shields.io/gitlab/license/crossref/chooser) ![activity](https://img.shields.io/gitlab/last-commit/crossref/chooser) [![semantic-release: angular](https://img.shields.io/badge/semantic--release-angular-e10079?logo=semantic-release)](https://github.com/semantic-release/semantic-release)

## Chooser

Chooser is a frontend for allowing users to choose between multiple resources when they resolve multi-resource DOIs (i.e., **Multiple resolution**,**Co-Access**,**Opcit**). It also handles grant metadata and can serve as a landing page for funders who do not have their own web pages for every grant that they issue.

Chooser is written in Python using the FastAPI framework.

It is designed to:

- Be a simple replacement for the previous mechanisms we used for asking users to choose between resources.
- Present users with a sensible and well designed default UI for choosing between resources.
- Allow for the easy and independent customization of the UI for those members who choose to do so.
- Make it easy for Crossref support staff to test and deploy those customizations.

## Why Chooser?

Sometimes Crossref DOIs resolve to multiple locations. This happens with:

- **Multiple resolution**: where one DOI points to multiple resources - all of which are known, managed, and controlled by the member.
- **Co-Access**: where one DOI points to multiple resources, but where only some of the resources are known, managed, and controlled by the member. This scenario is more common with books and monographs where it is rare for a publisher to distribute electronic versions of the publications **exclusively** via their own platform.

At other times, we want to be able to display metadata on behalf of members. This happens, for instance, with **hosting funders' grant landing pages**. When members registering research grants do not have unique landing pages, Crossref can host a page displaying the metadata using Chooser.

Before 2022, the first two mechanisms were handled entirely separately. Both in the data modeling and the mechanism by which a human user is presented with the choice between resources.

Neither mechanism supports proper HTTP semantics for directing machines to multiple resources (i.e. a `300` HTTP status code + link headers), so DOIs that employ either are effectively “bot-hostile.”

In both cases, Crossref provided members who use these mechanisms with the option of customizing the page's presentation, allowing the user to choose which resource they want to access.

The customization option for these pages had several problems:

- The customizations were not well designed.
- The variation in customizations made for a confusing user experience.
- The customizations could only be implemented by Crossref staff.
- It was almost impossible for Crossref staff to test the customizations before they were deployed.
- New and updated customizations could only be deployed as part of the weekly (currently) content system deployment cycle.
- Many of the customizations were broken (as of November 2022).

Chooser solves these problems.

## Implementation

Chooser is a simple FastAPI application that uses the Crossref Polite REST API to retrieve needed metadata and render Jinja2 templates for delivering pages.

The app uses diskcache to cache needed data retrieved from the API. Currently the cache is set to its default values (1 GB limit on size and least-used ejection policy).

The templates can be semi-customised with member logos provided automatically via an API. At the moment, these are provided by [https://clearbit.com/](https://clearbit.com/), but we could easily replace this with another provider, such as [https://brandfetch.com/developers/logo-api](https://brandfetch.com/developers/logo-api).

The app records metrics to CloudWatch metrics from where they can be viewed via our hosted Grafana instance. The metrics collected are:

- bad_request_count
- not_found_count
- choice (member_id, resolution_type)

Where resolution_type can be:

- multiple-resolution
- co-access
- mixed
- direct

The app exposes a `/heartbeat` route for blackbox monitoring tools like Pingdom and to verify the current running version.

The app reports errors to `Sentry.io` **if** the `SENTRY_DSN` environment variable is set appropriately.

The application *can* return an HTTP 300 status for pages with multiple-resolution or co-access information. It returns the alternative resource URLs in the `Link` header. For example:

```
curl -I http://chooser-url:8092/\?doi\=10.1017%2F9789048552160.002
HTTP/1.1 300 MULTIPLE CHOICES
Server: Werkzeug/2.2.2 Python/3.11.0
Date: Tue, 29 Nov 2022 07:39:44 GMT
Link: http://www.jstor.org/stable/10.2307/j.ctv2r4kxf9.5; rel="co-access",https://scienceopen.com/book?vid=7ccf0555-23ff-484f-abae-20b9821a1c62; rel="co-access"
Content-Type: text/html; charset=utf-8
Content-Length: 5566
Access-Control-Allow-Origin: *
Connection: close
```

*NB* that multiple choice redirection (HTTP 300 response) is *not* turned on by default.

If the DOI parameter is a regular single-resource DOI, it will return a normal 200.

## Examples

Here are two pages with lists of examples of each type of resource:

- [Multiple resolution](https://gitlab.com/crossref/chooser-poc/-/blob/main/examples/multipleresolution_examples.md)
- [Co-access](https://gitlab.com/crossref/chooser-poc/-/blob/main/examples/co-access_examples.md)

## Edge Cases:

* Members sometimes [register the same resource multiple times](https://chooser.fly.dev/?doi=10.5040/9781350098299)
* Sometimes co-access is [provided via repositories via handle identifiers](https://chooser.fly.dev/?doi=10.17161/1808.32168)

### Debug

There is a `debug` expander at the bottom of every Chooser page. It includes links to the current production page that would be displayed for the DOI selected. It also has other useful metadata, such as links to the JSON and XML so that you can see how the resources are encoded in the retrieved metadata.

### Testing

Testing is handled by a set of almost 200 unit tests and a set of integration tests.

The unit tests comprehensively test the app's functionality. The integration tests test the app's ability to retrieve data from the Crossref API and to render the templates.

Integration tests use the [Integration Project](https://gitlab.com/crossref/integration) to spin up versions of the Cayenne system (the Crossref REST API) and a Localstack AWS instance.

### Load testing

The repo includes two files suitable for use by [siege](https://github.com/JoeDog/siege). These were generated from a sample taken of a one-minute period in September 2022 when we saw peak traffic (about 30% over normal).

### Heartbeat

https://chooser.crossref.org/heartbeat

## Contributions, Semantic Versioning, and Release Process

Contributions should follow the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) specification. This is a simple convention for adding human and machine-readable meaning to commit messages.

Semantic versioning is handled automatically by [semantic-release](https://www.npmjs.com/package/semantic-release) which uses the language of commits to determine the next version number.

Branch strategy is a feature branch and merge request against the main branch. The main branch is protected and requires a successful pipeline to merge.

## Installation, development and testing

### Local

#### Linux (standard) instructions

- `git clone git@gitlab.com:crossref/chooser.git`
- `cd .chooser`
- `python3 -m venv venv`
- `. venv/bin/activate`
- `pip install -r requirements.txt`
- `pip install -r requirements-dev.txt`
- `. start-debug.sh`

#### MacOS instructions

*Step 1: Clone the Chooser repo locally first* 
skip to the next step if you've already done this

- open a Terminal window
- `cd ~/Sites`
- `mkdir chooser`
- `cd chooser`
- `git clone git@gitlab.com:crossref/chooser.git .`

*Step 2: Get things running locally*
- open a Terminal window
- `cd ~/Sites/chooser`
- `git checkout template-update` (as of 4 June 2024 do this to make sure you are working with the beta template updates)
- `python3 -m venv venv`
- `. venv/bin/activate`
- `pip install -r requirements.txt`
- `pip install -r requirements-dev.txt`
- `sh start-debug.sh`

### Docker

- `docker-compose build`
- `docker-compose up`

### Testing
Here are some test URLs that can be used to explore different examples:
- [Standard multi record example](https://chooser.crossref.org/?doi=10.1215/9780822398073-006)
- [Grant Example 1](https://chooser.crossref.org/?doi=10.3030/680448): lots of content eg award etc but no investigators
- [Grant Example 2](https://chooser.crossref.org/?doi=10.46936/cpcy.proj.2019.50733/60006578): 1 lead investigator and several investigators

Copyright &copy; 2024 Crossref