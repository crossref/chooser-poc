"""
Test the chooser module
"""

import unittest
from unittest import IsolatedAsyncioTestCase
from unittest.mock import AsyncMock, patch

from starlette.templating import Jinja2Templates

from chooser import (
    ExampleSentryException,
    api_error,
    bad_request,
    choice,
    clear_cache,
    heartbeat,
    not_found,
    root,
    root_path,
    trigger_error,
)
from crossrefutils.api import APIConnectionException


class ChooserTestCase(IsolatedAsyncioTestCase):
    """
    Test the Chooser module
    """

    @staticmethod
    async def test_not_found():
        """
        Ensure that not found is logged to counter and returns the correct page
        """

        result: Jinja2Templates.TemplateResponse = await not_found("request")

        assert result.status_code == 404

    @staticmethod
    async def test_bad_request():
        """
        Ensure that bad request is logged to counter and returns the correct
        page
        """

        result: Jinja2Templates.TemplateResponse = await bad_request("request")

        assert result.status_code == 400

    @staticmethod
    async def test_api_error():
        """
        Ensure that API errors are logged to counter and returns the correct
        page
        """

        result: Jinja2Templates.TemplateResponse = await api_error("request")

        assert result.status_code == 504

    @patch("chooser.ENABLE_SEMANTIC_LINK_HEADERS", True)
    async def test_choice_with_semantic_link_header(self):
        """
        Test choices with semantic link headers enabled
        """
        summary = {
            "doi": "10.7868/s002347611306009x",
            "member_id": "4687",
            "member": "Akademizdatcenter Nauka",
            "container-title": "Кристаллография",
            "primary-resource": "http://www.maikonline.com/maik/"
            "showArticle.do?"
            "auid=VAHMLZV15K&lang=ru",
            "tld": "maikonline.com",
            "clearbit-logo": "/static/no_logo.svg",
            "coaccess": [],
            "multiple-resolution": [
                {
                    "url": "http://elibrary.ru/item.asp?doi=10.7868/"
                    "S002347611306009X",
                    "tld": "elibrary.ru",
                    "clearbit-logo": "https://logo.clearbit.com/elibrary.ru",
                }
            ],
            "type": "JOURNAL ARTICLE",
            "published_date": "2013",
            "publication": "Кристаллография",
            "supplementary_ids": None,
            "title": "Исследование структурной упорядоченности монослоев "
            "порфирин-фуллереновой диады ZnDHD6ee методами дифракции "
            "электронов и атомно-силовой микроскопии",
            "name": None,
            "id": None,
            "location": None,
            "display_doi": "https://doi.org/10.7868/s002347611306009x",
            "grant_info": None,
            "grant_info_funders": None,
            "grant_info_funder_ids": "",
            "grant_info_type": None,
            "multiple_lead_investigators": [],
            "multiple_co_lead_investigators": [],
            "multiple_investigators": [],
            "finances": [],
            "project_description": None,
            "award_amount": None,
            "award_start": None,
            "funding_scheme": None,
            "internal_award_number": None,
            "editors": None,
            "authors": "Ю. А. Дьякова | Е. И. Суворова | Андрей С. Орехов | "
            "Антон С. Орехов | А. С. Алексеев | Р. В. Гайнутдинов | "
            "В. В. Клечковская | Е. Ю. Терещенко | Н. В. Ткаченко | "
            "Х. Лемметюйнен | Л. А. Фейгин | М. В. Ковальчук",
            "chairs": None,
        }

        result: Jinja2Templates.TemplateResponse = await choice(
            "request", summary=summary
        )

        assert result.status_code == 300

    @patch("chooser.ENABLE_SEMANTIC_LINK_HEADERS", False)
    async def test_choice_without_semantic_link_header(self):
        """
        Test choices with semantic link headers enabled
        """

        summary = {
            "doi": "10.7868/s002347611306009x",
            "member_id": "4687",
            "member": "Akademizdatcenter Nauka",
            "container-title": "Кристаллография",
            "primary-resource": "http://www.maikonline.com/maik/"
            "showArticle.do?auid=VAHMLZV15K&lang=ru",
            "tld": "maikonline.com",
            "clearbit-logo": "/static/no_logo.svg",
            "coaccess": [],
            "multiple-resolution": [
                {
                    "url": "http://elibrary.ru/item.asp?doi=10.7868/"
                    "S002347611306009X",
                    "tld": "elibrary.ru",
                    "clearbit-logo": "https://logo.clearbit.com/"
                    "elibrary.ru",
                }
            ],
            "type": "JOURNAL ARTICLE",
            "published_date": "2013",
            "publication": "Кристаллография",
            "supplementary_ids": None,
            "title": "Исследование структурной упорядоченности монослоев "
            "порфирин-фуллереновой диады ZnDHD6ee методами "
            "дифракции электронов и атомно-силовой микроскопии",
            "name": None,
            "id": None,
            "location": None,
            "display_doi": "https://doi.org/10.7868/s002347611306009x",
            "grant_info": None,
            "grant_info_funders": None,
            "grant_info_funder_ids": "",
            "grant_info_type": None,
            "multiple_lead_investigators": [],
            "multiple_co_lead_investigators": [],
            "multiple_investigators": [],
            "finances": [],
            "project_description": None,
            "award_amount": None,
            "award_start": None,
            "funding_scheme": None,
            "internal_award_number": None,
            "editors": None,
            "authors": "Ю. А. Дьякова | Е. И. Суворова | Андрей С. Орехов "
            "| Антон С. Орехов | А. С. Алексеев | Р. В. "
            "Гайнутдинов | В. В. Клечковская | Е. Ю. "
            "Терещенко | Н. В. Ткаченко | Х. Лемметюйнен | "
            "Л. А. Фейгин | М. В. Ковальчук",
            "chairs": None,
        }

        result: Jinja2Templates.TemplateResponse = await choice(
            "request", summary=summary
        )

        assert result.status_code == 200

    @staticmethod
    async def test_root_no_doi():
        """
        Test a bad case with no DOI
        """
        await root("request", doi=None)
        await ChooserTestCase.test_bad_request()

    async def test_root_with_doi(self):
        """
        Test the root path with a DOI
        """
        summary = {
            "doi": "10.7868/s002347611306009x",
            "member_id": "4687",
            "member": "Akademizdatcenter Nauka",
            "container-title": "Кристаллография",
            "primary-resource": "http://www.maikonline.com/maik/"
            "showArticle.do?auid=VAHMLZV15K&lang=ru",
            "tld": "maikonline.com",
            "clearbit-logo": "/static/no_logo.svg",
            "coaccess": [],
            "multiple-resolution": [
                {
                    "url": "http://elibrary.ru/item.asp?doi=10.7868/"
                    "S002347611306009X",
                    "tld": "elibrary.ru",
                    "clearbit-logo": "https://logo.clearbit.com/"
                    "elibrary.ru",
                }
            ],
            "type": "JOURNAL ARTICLE",
            "published_date": "2013",
            "publication": "Кристаллография",
            "supplementary_ids": None,
            "title": "Исследование структурной упорядоченности монослоев "
            "порфирин-фуллереновой диады ZnDHD6ee методами "
            "дифракции электронов и атомно-силовой микроскопии",
            "name": None,
            "id": None,
            "location": None,
            "display_doi": "https://doi.org/10.7868/s002347611306009x",
            "grant_info": None,
            "grant_info_funders": None,
            "grant_info_funder_ids": "",
            "grant_info_type": None,
            "multiple_lead_investigators": [],
            "multiple_co_lead_investigators": [],
            "multiple_investigators": [],
            "finances": [],
            "project_description": None,
            "award_amount": None,
            "award_start": None,
            "funding_scheme": None,
            "internal_award_number": None,
            "editors": None,
            "authors": "Ю. А. Дьякова | Е. И. Суворова | Андрей С. Орехов "
            "| Антон С. Орехов | А. С. Алексеев | Р. В. "
            "Гайнутдинов | В. В. Клечковская | Е. Ю. "
            "Терещенко | Н. В. Ткаченко | Х. Лемметюйнен | "
            "Л. А. Фейгин | М. В. Ковальчук",
            "chairs": None,
        }

        with patch(
            "chooser.metadata",
            new_callable=AsyncMock,
        ) as mock_metadata:
            mock_metadata.return_value = summary
            await root("request", summary["doi"])
            mock_metadata.assert_called_once()
            await self.test_choice_without_semantic_link_header()

    async def test_root_with_favicon_request(self):
        """
        Test the root path with a favicon request
        """
        favicon_return = await root("request", "favicon.ico")

        assert favicon_return.status_code == 404

    @staticmethod
    async def test_root_with_doi_not_found():
        """
        Test the root path with a DOI that is not found
        """
        summary = {
            "doi": "10.7868/s002347611306009x",
            "member_id": "4687",
            "member": "Akademizdatcenter Nauka",
            "container-title": "Кристаллография",
            "primary-resource": "http://www.maikonline.com/maik/"
            "showArticle.do?auid=VAHMLZV15K&lang=ru",
            "tld": "maikonline.com",
            "clearbit-logo": "/static/no_logo.svg",
            "coaccess": [],
            "multiple-resolution": [
                {
                    "url": "http://elibrary.ru/item.asp?doi=10.7868/"
                    "S002347611306009X",
                    "tld": "elibrary.ru",
                    "clearbit-logo": "https://logo.clearbit.com/"
                    "elibrary.ru",
                }
            ],
            "type": "JOURNAL ARTICLE",
            "published_date": "2013",
            "publication": "Кристаллография",
            "supplementary_ids": None,
            "title": "Исследование структурной упорядоченности монослоев "
            "порфирин-фуллереновой диады ZnDHD6ee методами "
            "дифракции электронов и атомно-силовой микроскопии",
            "name": None,
            "id": None,
            "location": None,
            "display_doi": "https://doi.org/10.7868/s002347611306009x",
            "grant_info": None,
            "grant_info_funders": None,
            "grant_info_funder_ids": "",
            "grant_info_type": None,
            "multiple_lead_investigators": [],
            "multiple_co_lead_investigators": [],
            "multiple_investigators": [],
            "finances": [],
            "project_description": None,
            "award_amount": None,
            "award_start": None,
            "funding_scheme": None,
            "internal_award_number": None,
            "editors": None,
            "authors": "Ю. А. Дьякова | Е. И. Суворова | Андрей С. Орехов "
            "| Антон С. Орехов | А. С. Алексеев | Р. В. "
            "Гайнутдинов | В. В. Клечковская | Е. Ю. "
            "Терещенко | Н. В. Ткаченко | Х. Лемметюйнен | "
            "Л. А. Фейгин | М. В. Ковальчук",
            "chairs": None,
        }

        with patch(
            "chooser.metadata",
            new_callable=AsyncMock,
        ) as mock_metadata:
            mock_metadata.return_value = None
            await root("request", summary["doi"])
            mock_metadata.assert_called_once()
            await ChooserTestCase.test_not_found()

    @staticmethod
    async def test_root_with_error():
        """
        Test a bad case with an API error
        """
        summary = {
            "doi": "10.7868/s002347611306009x",
            "member_id": "4687",
            "member": "Akademizdatcenter Nauka",
            "container-title": "Кристаллография",
            "primary-resource": "http://www.maikonline.com/maik/"
            "showArticle.do?auid=VAHMLZV15K&lang=ru",
            "tld": "maikonline.com",
            "clearbit-logo": "/static/no_logo.svg",
            "coaccess": [],
            "multiple-resolution": [
                {
                    "url": "http://elibrary.ru/item.asp?doi=10.7868/"
                    "S002347611306009X",
                    "tld": "elibrary.ru",
                    "clearbit-logo": "https://logo.clearbit.com/"
                    "elibrary.ru",
                }
            ],
            "type": "JOURNAL ARTICLE",
            "published_date": "2013",
            "publication": "Кристаллография",
            "supplementary_ids": None,
            "title": "Исследование структурной упорядоченности монослоев "
            "порфирин-фуллереновой диады ZnDHD6ee методами "
            "дифракции электронов и атомно-силовой микроскопии",
            "name": None,
            "id": None,
            "location": None,
            "display_doi": "https://doi.org/10.7868/s002347611306009x",
            "grant_info": None,
            "grant_info_funders": None,
            "grant_info_funder_ids": "",
            "grant_info_type": None,
            "multiple_lead_investigators": [],
            "multiple_co_lead_investigators": [],
            "multiple_investigators": [],
            "finances": [],
            "project_description": None,
            "award_amount": None,
            "award_start": None,
            "funding_scheme": None,
            "internal_award_number": None,
            "editors": None,
            "authors": "Ю. А. Дьякова | Е. И. Суворова | Андрей С. Орехов "
            "| Антон С. Орехов | А. С. Алексеев | Р. В. "
            "Гайнутдинов | В. В. Клечковская | Е. Ю. "
            "Терещенко | Н. В. Ткаченко | Х. Лемметюйнен | "
            "Л. А. Фейгин | М. В. Ковальчук",
            "chairs": None,
        }

        with patch(
            "chooser.metadata",
            new_callable=AsyncMock,
        ) as mock_metadata:
            mock_metadata.side_effect = APIConnectionException
            await root("request", doi=summary["doi"])
            await ChooserTestCase.test_api_error()

    @staticmethod
    async def test_clear_cache():
        """
        Test clearing the cache
        """
        with patch(
            "chooser.clear_metadata_cache",
            new_callable=AsyncMock,
        ) as mock_clear_metadata_cache:
            doi = "10.7868/s002347611306009X"
            mock_clear_metadata_cache.return_value = "Message"
            await clear_cache("request", doi_path=doi, doi=None)
            mock_clear_metadata_cache.assert_called_once()

    async def test_trigger_error(self):
        """
        Test triggering an error in Sentry
        """
        with self.assertRaises(ExampleSentryException):
            await trigger_error()

    @staticmethod
    async def test_heartbeat():
        """
        Test the heartbeat endpoint
        """
        request = await heartbeat()

        assert request.status_code == 200

    @staticmethod
    async def test_root_path():
        """
        Test the root path
        """
        with patch(
            "chooser.root",
            new_callable=AsyncMock,
        ) as mock_root:
            doi = "10.7868/s002347611306009x"
            await root_path("request", doi)
            mock_root.assert_called_once()

    @staticmethod
    async def test_choice_with_null_url():
        """
        Test choices with semantic link headers enabled
        """

        summary = {
            "doi": "10.7868/s002347611306009x",
            "member_id": "4687",
            "member": "Akademizdatcenter Nauka",
            "container-title": "Кристаллография",
            "primary-resource": "http://www.maikonline.com/maik/"
            "showArticle.do?auid=VAHMLZV15K&lang=ru",
            "tld": "maikonline.com",
            "clearbit-logo": "/static/no_logo.svg",
            "coaccess": [],
            "multiple-resolution": [
                {
                    "url": "http://elibrary.ru/item.asp?doi=10.7868/"
                    "S002347611306009X",
                    "tld": "elibrary.ru",
                    "clearbit-logo": "https://logo.clearbit.com/"
                    "elibrary.ru",
                },
                {
                    "url": None,
                    "tld": "elibrary.ru",
                    "clearbit-logo": "https://logo.clearbit.com/"
                    "elibrary.ru",
                },
            ],
            "type": "JOURNAL ARTICLE",
            "published_date": "2013",
            "publication": "Кристаллография",
            "supplementary_ids": None,
            "title": "Исследование структурной упорядоченности монослоев "
            "порфирин-фуллереновой диады ZnDHD6ee методами "
            "дифракции электронов и атомно-силовой микроскопии",
            "name": None,
            "id": None,
            "location": None,
            "display_doi": "https://doi.org/10.7868/s002347611306009x",
            "grant_info": None,
            "grant_info_funders": None,
            "grant_info_funder_ids": "",
            "grant_info_type": None,
            "multiple_lead_investigators": [],
            "multiple_co_lead_investigators": [],
            "multiple_investigators": [],
            "finances": [],
            "project_description": None,
            "award_amount": None,
            "award_start": None,
            "funding_scheme": None,
            "internal_award_number": None,
            "editors": None,
            "authors": "Ю. А. Дьякова | Е. И. Суворова | Андрей С. Орехов "
            "| Антон С. Орехов | А. С. Алексеев | Р. В. "
            "Гайнутдинов | В. В. Клечковская | Е. Ю. "
            "Терещенко | Н. В. Ткаченко | Х. Лемметюйнен | "
            "Л. А. Фейгин | М. В. Ковальчук",
            "chairs": None,
        }

        result: Jinja2Templates.TemplateResponse = await choice(
            "request", summary=summary
        )

        assert result.status_code == 200


if __name__ == "__main__":
    unittest.main()
