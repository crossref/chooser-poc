"""
This module contains the unit tests for the metadata module.
"""

# pylint: disable=too-many-lines, too-many-public-methods
from unittest import IsolatedAsyncioTestCase
from unittest.mock import AsyncMock, MagicMock, patch

from crossrefutils.metadata import (
    add_award_amount,
    add_award_start,
    add_co_lead_investigators,
    add_coaccess_data,
    add_display_doi,
    add_finances_data,
    add_funding_scheme,
    add_generic,
    add_generic_get,
    add_grant_info,
    add_grant_info_funder_ids,
    add_grant_info_funders,
    add_grant_info_type,
    add_internal_award_number,
    add_investigator_affiliation_data,
    add_investigators,
    add_lead_investigators,
    add_multiple_resolution_data,
    add_names,
    add_project_description,
    add_publication,
    add_published_date,
    add_supplementary_id,
    add_title,
    add_type,
    citation_metadata,
    classify,
    clear_metadata_cache,
    coacesss_metadata,
    format_date,
    get_doi_url,
    join_links,
    link_header,
    metadata,
    multiple_investigator_metadata,
    multiple_resolution_metadata,
    relationship_metadata,
    tld,
)


class MetadataTestCase(IsolatedAsyncioTestCase):
    """
    Test case for the metadata module.
    """

    @staticmethod
    def test_join_links():
        """
        Test the join_links function.
        """

        request = join_links(None, None)
        assert request == ("direct", None)

        request = join_links("https://example.com", None)
        assert request == ("multiple_resolution", "https://example.com")

        request = join_links(None, "https://example.com")
        assert request == ("coaccess", "https://example.com")

        request = join_links("https://example1.com", "https://example2.com")
        assert request == (
            "mixed",
            "https://example1.com, https://example2.com",
        )

    @patch("crossrefutils.metadata.join_links")
    async def test_link_header_success(self, mock_join_links):
        """
        Test the link_header function when it succeeds.
        """
        # Mock the return value of join_links
        mock_join_links.return_value = ("multiple", "link_string")

        # Input summary
        summary = {
            "multiple-resolution": [
                {"url": "https://example.com/multi1"},
                {"url": "https://example.com/multi2"},
            ],
            "coaccess": [
                {"url": "https://example.com/coaccess1"},
                {"url": "https://example.com/coaccess2"},
            ],
        }

        # Call the function
        result = await link_header(summary)

        # Expected result
        expected_result = ("multiple", {"Link": "link_string"})

        # Assertions
        self.assertEqual(result, expected_result)
        mock_join_links.assert_called_once_with(
            'https://example.com/multi1; rel="multiple-resolution",'
            'https://example.com/multi2; rel="multiple-resolution"',
            'https://example.com/coaccess1; rel="co-access",'
            'https://example.com/coaccess2; rel="co-access"',
        )

    @patch("crossrefutils.metadata.join_links")
    async def test_link_header_no_links(self, mock_join_links):
        """
        Test the link_header function when there are no links.
        """
        # Mock the return value of join_links to simulate no links case
        mock_join_links.return_value = ("multiple", None)

        # Input summary
        summary = {"multiple-resolution": [], "coaccess": []}

        # Call the function
        result = await link_header(summary)

        # Expected result
        expected_result = ("multiple", None)

        # Assertions
        self.assertEqual(result, expected_result)
        mock_join_links.assert_called_once_with("", "")

    @patch("crossrefutils.metadata.join_links")
    async def test_link_header_exception(self, mock_join_links):
        """
        Test the link_header function when an exception occurs.
        """

        # Simulate an exception in join_links
        mock_join_links.side_effect = Exception("Test Exception")

        # Input summary
        summary = {
            "multiple-resolution": [{"url": "https://example.com/multi1"}],
            "coaccess": [{"url": "https://example.com/coaccess1"}],
        }

        # Call the function
        result = await link_header(summary)

        # Assertions
        self.assertIsNone(result)
        mock_join_links.assert_called_once()

    def test_classify_both(self):
        """
        Test the classify function when both multiple-resolution and coaccess
        are present.
        """
        # Test case where both multiple-resolution and coaccess are present
        summary = {
            "doi": "10.1000/testdoi",
            "multiple-resolution": [{"url": "https://example.com/multi1"}],
            "coaccess": [{"url": "https://example.com/coaccess1"}],
        }
        result = classify(summary)
        self.assertEqual(result, "both")

    def test_classify_multiple_resolution_only(self):
        """
        Test the classify function when only multiple-resolution is present.
        """
        # Test case where only multiple-resolution is present
        summary = {
            "doi": "10.1000/testdoi",
            "multiple-resolution": [{"url": "https://example.com/multi1"}],
            "coaccess": [],
        }
        result = classify(summary)
        self.assertEqual(result, "multiple-resolution")

    def test_classify_coaccess_only(self):
        """
        Test the classify function when only coaccess is present
        """
        # Test case where only coaccess is present
        summary = {
            "doi": "10.1000/testdoi",
            "multiple-resolution": [],
            "coaccess": [{"url": "https://example.com/coaccess1"}],
        }
        result = classify(summary)
        self.assertEqual(result, "coaccess")

    def test_classify_standard(self):
        """
        Test the classify function when neither multiple-resolution nor
        coaccess
        """
        # Test case where neither multiple-resolution nor coaccess is present
        summary = {
            "doi": "10.1000/testdoi",
            "multiple-resolution": [],
            "coaccess": [],
        }
        result = classify(summary)
        self.assertEqual(result, "standard")

    @patch("crossrefutils.metadata.tldextract.extract")
    def test_tld_valid_domain(self, mock_extract):
        """
        Test the tld function with a valid URL.
        """
        # Mock the return value of tldextract.extract
        mock_extract.return_value = MagicMock(registered_domain="example.com")

        # Test a valid URL
        url = "https://www.example.com/path/to/page"
        result = tld(url)
        self.assertEqual(result, "example.com")
        mock_extract.assert_called_once_with(url)

    @patch("crossrefutils.metadata.tldextract.extract")
    def test_tld_subdomain(self, mock_extract):
        """
        Test the tld function with a URL that has a subdomain.
        """
        # Mock the return value of tldextract.extract for a subdomain
        mock_extract.return_value = MagicMock(
            registered_domain="example.co.uk"
        )

        # Test a URL with a subdomain
        url = "https://sub.example.co.uk/path"
        result = tld(url)
        self.assertEqual(result, "example.co.uk")
        mock_extract.assert_called_once_with(url)

    @patch("crossrefutils.metadata.tldextract.extract")
    def test_tld_no_registered_domain(self, mock_extract):
        """
        Test the tld function when no registered domain is found.
        """
        # Mock the return value of tldextract.extract when no registered
        # domain is found
        mock_extract.return_value = MagicMock(registered_domain="")

        # Test a URL with no registered domain (e.g., localhost or an
        # invalid URL)
        url = "https://localhost"
        result = tld(url)
        self.assertEqual(result, "")
        mock_extract.assert_called_once_with(url)

    @patch("crossrefutils.metadata.tldextract.extract")
    def test_tld_invalid_url(self, mock_extract):
        """
        Test the tld function with an invalid URL.
        """
        # Mock the return value of tldextract.extract for an invalid URL
        mock_extract.return_value = MagicMock(registered_domain="")

        # Test an invalid URL
        url = "not a valid url"
        result = tld(url)
        self.assertEqual(result, "")
        mock_extract.assert_called_once_with(url)

    @patch("crossrefutils.metadata.clearbit_logo")
    @patch("crossrefutils.metadata.tld")
    def test_multiple_resolution_metadata(self, mock_tld, mock_clearbit_logo):
        """
        Test the multiple_resolution_metadata function.
        """
        # Mock the return values for tld and clearbit_logo
        mock_tld.return_value = "example.com"
        mock_clearbit_logo.return_value = (
            "https://logo.clearbit.com/example.com"
        )

        # Test input resource
        resource = {"URL": "https://www.example.com/path/to/resource"}

        # Expected output
        expected_result = {
            "url": "https://www.example.com/path/to/resource",
            "tld": "example.com",
            "clearbit-logo": "https://logo.clearbit.com/example.com",
        }

        # Call the function
        result = multiple_resolution_metadata(resource)

        # Assertions
        self.assertEqual(result, expected_result)
        self.assertEqual(mock_tld.call_count, 2)
        mock_clearbit_logo.assert_called_once_with("example.com")

    @patch("crossrefutils.metadata.clearbit_logo")
    @patch("crossrefutils.metadata.tld")
    def test_multiple_resolution_metadata_no_url(
        self, mock_tld, mock_clearbit_logo
    ):
        """
        Test the multiple_resolution_metadata function with no valid URL.
        """
        # Mock the return values for tld and clearbit_logo
        mock_tld.return_value = ""
        mock_clearbit_logo.return_value = ""

        # Test input resource with no valid URL
        resource = {"URL": "invalid-url"}

        # Expected output
        expected_result = {
            "url": "invalid-url",
            "tld": "",
            "clearbit-logo": "",
        }

        # Call the function
        result = multiple_resolution_metadata(resource)

        # Assertions
        self.assertEqual(result, expected_result)
        self.assertEqual(mock_tld.call_count, 2)
        mock_clearbit_logo.assert_called_once_with("")

    @patch("crossrefutils.metadata.multiple_resolution_metadata")
    def test_add_multiple_resolution_data_with_secondary_resources(
        self, mock_multiple_resolution_metadata
    ):
        """
        Test the add_multiple_resolution_data function with secondary
        resources.
        """
        # Mock the return value of multiple_resolution_metadata
        mock_multiple_resolution_metadata.side_effect = [
            {
                "url": "https://example.com/resource1",
                "tld": "example.com",
                "clearbit-logo": "https://logo.com/example.com",
            },
            {
                "url": "https://example.com/resource2",
                "tld": "example.com",
                "clearbit-logo": "https://logo.com/example.com",
            },
        ]

        # Input record with secondary resources
        record = {
            "resource": {
                "secondary": [
                    {"URL": "https://example.com/resource1"},
                    {"URL": "https://example.com/resource2"},
                ]
            }
        }

        # Expected result
        expected_result = [
            {
                "url": "https://example.com/resource1",
                "tld": "example.com",
                "clearbit-logo": "https://logo.com/example.com",
            },
            {
                "url": "https://example.com/resource2",
                "tld": "example.com",
                "clearbit-logo": "https://logo.com/example.com",
            },
        ]

        # Call the function
        result = add_multiple_resolution_data(record)

        # Assertions
        self.assertEqual(result, expected_result)
        self.assertEqual(mock_multiple_resolution_metadata.call_count, 2)
        mock_multiple_resolution_metadata.assert_any_call(
            {"URL": "https://example.com/resource1"}
        )
        mock_multiple_resolution_metadata.assert_any_call(
            {"URL": "https://example.com/resource2"}
        )

    @patch("crossrefutils.metadata.multiple_resolution_metadata")
    def test_add_multiple_resolution_data_with_no_secondary_resources(
        self, mock_multiple_resolution_metadata
    ):
        """
        Test the add_multiple_resolution_data function with no secondary
        """
        # Input record without secondary resources
        record = {"resource": {"secondary": []}}

        # Expected result
        expected_result = []

        # Call the function
        result = add_multiple_resolution_data(record)

        # Assertions
        self.assertEqual(result, expected_result)
        mock_multiple_resolution_metadata.assert_not_called()

    @patch("crossrefutils.metadata.multiple_resolution_metadata")
    def test_add_multiple_resolution_data_with_missing_secondary_key(
        self, mock_multiple_resolution_metadata
    ):
        """
        Test the add_multiple_resolution_data function with missing 'secondary'
        """
        # Input record without 'secondary' key in the resource
        record = {"resource": {}}

        # Expected result
        expected_result = []

        # Call the function
        result = add_multiple_resolution_data(record)

        # Assertions
        self.assertEqual(result, expected_result)
        mock_multiple_resolution_metadata.assert_not_called()

    @patch("crossrefutils.metadata.clearbit_logo")
    @patch("crossrefutils.metadata.tld")
    @patch(
        "crossrefutils.metadata.fetch_work_metadata", new_callable=AsyncMock
    )
    async def test_coacesss_metadata(
        self, mock_fetch_work_metadata, mock_tld, mock_clearbit_logo
    ):
        """
        Test the coacesss_metadata function
        """
        # Mock the return value of fetch_work_metadata
        mock_fetch_work_metadata.return_value = {
            "publisher": "Example Publisher",
            "member": "1234",
            "resource": {
                "primary": {"URL": "https://www.example.com/resource"}
            },
        }

        # Mock the return value of tld and clearbit_logo
        mock_tld.return_value = "example.com"
        mock_clearbit_logo.return_value = (
            "https://logo.clearbit.com/example.com"
        )

        # DOI to test
        doi = "10.1000/testdoi"

        # Expected result
        expected_result = {
            "doi": doi,
            "member": "1234",
            "publisher": "Example Publisher",
            "url": "https://www.example.com/resource",
            "tld": "example.com",
            "clearbit-logo": "https://logo.clearbit.com/example.com",
        }

        # Call the function
        result = await coacesss_metadata(doi)

        # Assertions
        self.assertEqual(result, expected_result)
        mock_fetch_work_metadata.assert_called_once_with(doi)
        mock_tld.assert_called_with("https://www.example.com/resource")
        mock_clearbit_logo.assert_called_with("example.com")

    @patch("crossrefutils.metadata.clearbit_logo")
    @patch("crossrefutils.metadata.tld")
    @patch(
        "crossrefutils.metadata.fetch_work_metadata", new_callable=AsyncMock
    )
    async def test_coacesss_metadata_no_primary_resource(
        self, mock_fetch_work_metadata, mock_tld, mock_clearbit_logo
    ):
        """
        Test the coacesss_metadata function with no primary resource.
        """
        # Mock the return value of fetch_work_metadata with no primary resource
        mock_fetch_work_metadata.return_value = {
            "publisher": "Example Publisher",
            "member": "1234",
            "resource": {"primary": {}},
        }

        # Mock the return value of tld and clearbit_logo
        mock_tld.return_value = ""
        mock_clearbit_logo.return_value = ""

        # DOI to test
        doi = "10.1000/testdoi"

        with self.assertRaises(KeyError):
            # Call the function
            await coacesss_metadata(doi)

    @patch("crossrefutils.metadata.coacesss_metadata", new_callable=AsyncMock)
    async def test_add_coaccess_data_with_identical_doi_relations(
        self, mock_coacesss_metadata
    ):
        """
        Test the add_coaccess_data function with is-identical-to
        """
        # Mock the return value of coacesss_metadata
        mock_coacesss_metadata.side_effect = [
            {"doi": "10.1000/testdoi1", "metadata": "data1"},
            {"doi": "10.1000/testdoi2", "metadata": "data2"},
        ]

        # Input record with is-identical-to relations containing DOIs
        record = {
            "relation": {
                "is-identical-to": [
                    {"id": "10.1000/testdoi1", "id-type": "doi"},
                    {"id": "10.1000/testdoi2", "id-type": "doi"},
                ]
            }
        }

        # Expected result
        expected_result = [
            {"doi": "10.1000/testdoi1", "metadata": "data1"},
            {"doi": "10.1000/testdoi2", "metadata": "data2"},
        ]

        # Call the function
        result = await add_coaccess_data(record)

        # Assertions
        self.assertEqual(result, expected_result)
        self.assertEqual(mock_coacesss_metadata.call_count, 2)
        mock_coacesss_metadata.assert_any_call("10.1000/testdoi1")
        mock_coacesss_metadata.assert_any_call("10.1000/testdoi2")

    @patch("crossrefutils.metadata.coacesss_metadata", new_callable=AsyncMock)
    async def test_add_coaccess_data_with_non_doi_relations(
        self, mock_coacesss_metadata
    ):
        """
        Test the add_coaccess_data function with is-identical-to relations
        """
        # Input record with is-identical-to relations but no DOIs
        record = {
            "relation": {
                "is-identical-to": [
                    {"id": "non-doi-id1", "id-type": "other"},
                    {"id": "non-doi-id2", "id-type": "other"},
                ]
            }
        }

        # Expected result is an empty list since there are no DOI relations
        expected_result = []

        # Call the function
        result = await add_coaccess_data(record)

        # Assertions
        self.assertEqual(result, expected_result)
        mock_coacesss_metadata.assert_not_called()

    @patch("crossrefutils.metadata.coacesss_metadata", new_callable=AsyncMock)
    async def test_add_coaccess_data_with_empty_relations(
        self, mock_coacesss_metadata
    ):
        """
        Test the add_coaccess_data function with an empty is-identical-to list
        """
        # Input record with an empty is-identical-to list
        record = {"relation": {"is-identical-to": []}}

        # Expected result is an empty list
        expected_result = []

        # Call the function
        result = await add_coaccess_data(record)

        # Assertions
        self.assertEqual(result, expected_result)
        mock_coacesss_metadata.assert_not_called()

    async def test_add_coaccess_data_with_missing_relations_key(
        self,
    ):
        """
        Test the add_coaccess_data function with missing "relation" key
        """
        # Input record with missing "relation" key
        record = {}

        # Call the function
        with self.assertRaises(KeyError):
            await add_coaccess_data(record)

    def test_add_finances_data_with_doi_relations(self):
        """
        Test the add_finances_data function with DOI relations
        """
        # Input record with finances relations containing DOIs
        record = {
            "relation": {
                "finances": [
                    {"id": "10.1000/testdoi1", "id-type": "doi"},
                    {"id": "10.1000/testdoi2", "id-type": "doi"},
                ]
            }
        }

        # Expected result
        expected_result = [
            "https://doi.org/10.1000/testdoi1",
            "https://doi.org/10.1000/testdoi2",
        ]

        # Call the function
        result = add_finances_data(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_add_finances_data_with_non_doi_relations(self):
        """
        Test the add_finances_data function with non-DOI relations
        """
        # Input record with finances relations that are not DOIs
        record = {
            "relation": {
                "finances": [
                    {"id": "non-doi-id1", "id-type": "other"},
                    {"id": "non-doi-id2", "id-type": "other"},
                ]
            }
        }

        # Expected result is an empty list since there are no DOI relations
        expected_result = []

        # Call the function
        result = add_finances_data(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_add_finances_data_with_empty_finances(self):
        """
        Test the add_finances_data function with an empty finances list
        """
        # Input record with an empty finances list
        record = {"relation": {"finances": []}}

        # Expected result is an empty list
        expected_result = []

        # Call the function
        result = add_finances_data(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_add_finances_data_with_missing_finances_key(self):
        """
        Test the add_finances_data function with missing "finances
        """
        # Input record with missing "finances" key
        record = {"relation": {}}

        # Expected result is an empty list since there are no finances
        # relations
        expected_result = []

        # Call the function
        result = add_finances_data(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_add_finances_data_with_missing_relation_key(self):
        """
        Test the add_finances_data function with missing "relation" key
        """
        # Input record with missing "relation" key
        record = {}

        # Call the function
        with self.assertRaises(KeyError):
            add_finances_data(record)

    def test_get_doi_url_with_valid_doi(self):
        """
        Test the get_doi_url function with a valid DOI.
        """
        # Test with a valid DOI
        doi = "10.1000/testdoi"
        expected_result = "https://doi.org/10.1000/testdoi"

        # Call the function
        result = get_doi_url(doi)

        # Assertion
        self.assertEqual(result, expected_result)

    def test_get_doi_url_with_empty_doi(self):
        """
        Test the get_doi_url function with an empty DOI. Should never happen.
        """
        # Test with an empty DOI
        doi = ""
        expected_result = "https://doi.org/"

        # Call the function
        result = get_doi_url(doi)

        # Assertion
        self.assertEqual(result, expected_result)

    def test_get_doi_url_with_special_characters(self):
        """
        Test the get_doi_url function with a DOI containing special characters.
        """
        # Test with a DOI containing special characters
        doi = "10.1000/!@#$%^&*()"
        expected_result = "https://doi.org/10.1000/!@#$%^&*()"

        # Call the function
        result = get_doi_url(doi)

        # Assertion
        self.assertEqual(result, expected_result)

    def test_get_doi_url_with_whitespace(self):
        """
        Test the get_doi_url function with a DOI containing whitespace. Should
        never happen.
        """
        # Test with a DOI containing whitespace
        doi = "10.1000/ test doi "
        expected_result = "https://doi.org/10.1000/ test doi "

        # Call the function
        result = get_doi_url(doi)

        # Assertion
        self.assertEqual(result, expected_result)

    def test_add_supplementary_id_with_alternative_ids(self):
        """
        Test the add_supplementary_id function with a record containing
        alternative ids.
        """
        # Test with a record containing alternative ids
        record = {"alternative-id": ["ID1", "ID2", "ID3"]}
        expected_result = "ID1 | ID2 | ID3"

        # Call the function
        result = add_supplementary_id(record)

        # Assertion
        self.assertEqual(result, expected_result)

    def test_add_supplementary_id_with_no_alternative_ids_key(self):
        """
        Test the add_supplementary_id function with a record that does not have
        the "alternative-id" key.
        """
        # Test with a record that does not have the "alternative-id" key
        record = {"other-key": ["value1", "value2"]}
        expected_result = None

        # Call the function
        result = add_supplementary_id(record)

        # Assertion
        self.assertEqual(result, expected_result)

    def test_add_supplementary_id_with_empty_alternative_ids(self):
        """
        Test the add_supplementary_id function with a record containing
        an empty "alternative-id" list.
        """
        # Test with a record containing an empty "alternative-id" list
        record = {"alternative-id": []}
        expected_result = ""

        # Call the function
        result = add_supplementary_id(record)

        # Assertion
        self.assertEqual(result, expected_result)

    def test_add_supplementary_id_with_single_alternative_id(self):
        """
        Test the add_supplementary_id function with a record containing a
        single alternative id.
        """
        # Test with a record containing a single alternative id
        record = {"alternative-id": ["ID1"]}
        expected_result = "ID1"

        # Call the function
        result = add_supplementary_id(record)

        # Assertion
        self.assertEqual(result, expected_result)

    def test_add_names_empty_list(self):
        """
        Test the add_names function with an empty list.
        """
        # Test with an empty list
        items_list = []
        expected_result = None

        # Call the function
        result = add_names(items_list)

        # Assertion
        self.assertEqual(result, expected_result)

    def test_add_names_with_mixed_entries(self):
        """
        Test the add_names function with a list containing different types of
        name entries.
        """
        # Test with a list containing different types of name entries
        items_list = [
            {"name": "John Doe"},
            {"given": "Jane", "family": "Doe"},
            {"given": "Alice"},
            {"other": "Ignored"},
        ]
        expected_result = "John Doe | Jane Doe | Alice"

        # Call the function
        result = add_names(items_list)

        # Assertion
        self.assertEqual(result, expected_result)

    def test_add_names_with_no_valid_names(self):
        """
        Test the add_names function with a list where no valid names are
        provided.
        """
        # Test with a list where no valid names are provided
        items_list = [{"other": "Ignored"}, {"some": "Value"}]
        expected_result = None

        # Call the function
        result = add_names(items_list)

        # Assertion
        self.assertEqual(result, expected_result)

    def test_add_names_all_given_and_family(self):
        """
        Test the add_names function with a list where all items have given and
        family names.
        """
        # Test with a list where all items have given and family names
        items_list = [
            {"given": "John", "family": "Smith"},
            {"given": "Jane", "family": "Doe"},
        ]
        expected_result = "John Smith | Jane Doe"

        # Call the function
        result = add_names(items_list)

        # Assertion
        self.assertEqual(result, expected_result)

    def test_add_names_all_single_given(self):
        """
        Test the add_names function with a list where all items have only given
        names.
        """
        # Test with a list where all items have only given names
        items_list = [{"given": "John"}, {"given": "Jane"}]
        expected_result = "John | Jane"

        # Call the function
        result = add_names(items_list)

        # Assertion
        self.assertEqual(result, expected_result)

    @patch("crossrefutils.metadata.add_names")
    def test_add_generic_with_author_field(self, mock_add_names):
        """
        Test the add_authors function with a record containing an 'author'
        field
        """
        # Setup mock for add_names
        mock_add_names.return_value = "John Doe | Jane Smith"

        # Input record with 'author' field
        record = {"author": [{"name": "John Doe"}, {"name": "Jane Smith"}]}

        # Expected result
        expected_result = "John Doe | Jane Smith"

        # Call the function
        result = add_generic(record, "author")

        # Assertions
        self.assertEqual(result, expected_result)
        mock_add_names.assert_called_once_with(record["author"])

    @patch("crossrefutils.metadata.add_names")
    def test_add_generic_without_author_field(self, mock_add_names):
        """
        Test the add_authors function with a record that does not have an
        'author' field
        """
        # Setup mock for add_names
        mock_add_names.return_value = None

        # Input record without 'author' field
        record = {}

        # Expected result
        expected_result = None

        # Call the function
        result = add_generic(record, "author")

        # Assertions
        self.assertEqual(result, expected_result)
        mock_add_names.assert_called_once_with(None)

    @patch("crossrefutils.metadata.add_names")
    def test_add_generic_with_empty_author_field(self, mock_add_names):
        """
        Test the add_authors function with a record containing an
        empty 'author'
        """
        # Setup mock for add_names
        mock_add_names.return_value = None

        # Input record with empty 'author' field
        record = {"author": []}

        # Expected result
        expected_result = None

        # Call the function
        result = add_generic(record, "author")

        # Assertions
        self.assertEqual(result, expected_result)
        mock_add_names.assert_called_once_with([])

    @patch("crossrefutils.metadata.add_names")
    def test_add_generic_with_invalid_author_field(self, mock_add_names):
        """
        Test the add_authors function with a record containing an invalid
        'author' field
        """
        # Setup mock for add_names
        mock_add_names.return_value = None

        # Input record with invalid 'author' field
        record = {"author": "Invalid Type"}

        # Expected result
        expected_result = None

        # Call the function
        result = add_generic(record, "author")

        # Assertions
        self.assertEqual(result, expected_result)
        mock_add_names.assert_called_once_with("Invalid Type")

    @patch("crossrefutils.metadata.multiple_investigator_metadata")
    def test_add_lead_investigators_with_valid_data(self, mock_metadata):
        """
        Test the add_lead_investigators function with valid 'lead-investigator'
        """
        # Setup mock for multiple_investigator_metadata
        mock_metadata.side_effect = lambda x: f"Metadata for {x['name']}"

        # Input record with valid 'lead-investigator' data
        record = {
            "project": [
                {
                    "lead-investigator": [
                        {"name": "John Doe"},
                        {"name": "Jane Smith"},
                    ]
                }
            ]
        }

        # Expected result
        expected_result = ["Metadata for John Doe", "Metadata for Jane Smith"]

        # Call the function
        result = add_lead_investigators(record)

        # Assertions
        self.assertEqual(result, expected_result)
        self.assertEqual(mock_metadata.call_count, 2)
        mock_metadata.assert_any_call({"name": "John Doe"})
        mock_metadata.assert_any_call({"name": "Jane Smith"})

    def test_add_lead_investigators_without_lead_investigators(self):
        """
        Test the add_lead_investigators function with no 'lead-investigator'
        """
        # Input record without 'lead-investigator'
        record = {"project": [{}]}

        # Expected result
        expected_result = []

        # Call the function
        result = add_lead_investigators(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_add_lead_investigators_with_no_project(self):
        """
        Test the add_lead_investigators function with no 'project'
        """
        # Input record with no 'project' field
        record = {}

        # Expected result
        expected_result = []

        # Call the function
        result = add_lead_investigators(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_add_lead_investigators_with_empty_project(self):
        """
        Test the add_lead_investigators function with an empty 'project'
        """
        # Input record with an empty 'project' field
        record = {"project": []}

        # Call the function
        with self.assertRaises(IndexError):
            add_lead_investigators(record)

    @patch("crossrefutils.metadata.multiple_investigator_metadata")
    def test_add_co_lead_investigators_with_valid_data(self, mock_metadata):
        """
        Test the add_co_lead_investigators function with valid
        'co-lead-investigator'
        """
        # Setup mock for multiple_investigator_metadata
        mock_metadata.side_effect = lambda x: f"Metadata for {x['name']}"

        # Input record with valid 'co-lead-investigator' data
        record = {
            "project": [
                {
                    "co-lead-investigator": [
                        {"name": "Alice Johnson"},
                        {"name": "Bob Smith"},
                    ]
                }
            ]
        }

        # Expected result
        expected_result = [
            "Metadata for Alice Johnson",
            "Metadata for Bob Smith",
        ]

        # Call the function
        result = add_co_lead_investigators(record)

        # Assertions
        self.assertEqual(result, expected_result)
        self.assertEqual(mock_metadata.call_count, 2)
        mock_metadata.assert_any_call({"name": "Alice Johnson"})
        mock_metadata.assert_any_call({"name": "Bob Smith"})

    def test_add_co_lead_investigators_without_co_lead_investigators(self):
        """
        Test the add_co_lead_investigators function with no
        'co-lead-investigator'
        """
        # Input record without 'co-lead-investigator'
        record = {"project": [{}]}

        # Expected result
        expected_result = []

        # Call the function
        result = add_co_lead_investigators(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_add_co_lead_investigators_with_no_project(self):
        """
        Test the add_co_lead_investigators function with no 'project'
        """
        # Input record with no 'project' field
        record = {}

        # Expected result
        expected_result = []

        # Call the function
        result = add_co_lead_investigators(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_add_co_lead_investigators_with_empty_project(self):
        """
        Test the add_co_lead_investigators function with an empty 'project'
        """
        # Input record with an empty 'project' field
        record = {"project": []}

        # Call the function
        with self.assertRaises(IndexError):
            add_co_lead_investigators(record)

    @patch("crossrefutils.metadata.multiple_investigator_metadata")
    def test_add_investigators_with_valid_data(self, mock_metadata):
        """
        Test the add_investigators function with valid 'investigator' data
        """
        # Setup mock for multiple_investigator_metadata
        mock_metadata.side_effect = lambda x: f"Metadata for {x['name']}"

        # Input record with valid 'investigator' data
        record = {
            "project": [
                {
                    "investigator": [
                        {"name": "Alice Johnson"},
                        {"name": "Bob Smith"},
                    ]
                }
            ]
        }

        # Expected result
        expected_result = [
            "Metadata for Alice Johnson",
            "Metadata for Bob Smith",
        ]

        # Call the function
        result = add_investigators(record)

        # Assertions
        self.assertEqual(result, expected_result)
        self.assertEqual(mock_metadata.call_count, 2)
        mock_metadata.assert_any_call({"name": "Alice Johnson"})
        mock_metadata.assert_any_call({"name": "Bob Smith"})

    def test_add_investigators_without_investigators(self):
        """
        Test the add_investigators function with no 'investigator'
        """
        # Input record without 'investigator'
        record = {"project": [{}]}

        # Expected result
        expected_result = []

        # Call the function
        result = add_investigators(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_add_investigators_with_no_project(self):
        """
        Test the add_investigators function with no 'project'
        """
        # Input record with no 'project' field
        record = {}

        # Expected result
        expected_result = []

        # Call the function
        result = add_investigators(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_add_investigators_with_empty_project(self):
        """
        Test the add_investigators function with an empty 'project'
        """
        # Input record with an empty 'project' field
        record = {"project": []}

        # Call the function
        with self.assertRaises(IndexError):
            add_investigators(record)

    def test_affiliation_with_complete_data(self):
        """
        Test the add_investigator_affiliation_data function with complete
        affiliation data.
        """
        # Input resource with complete affiliation data
        resource = {
            "affiliation": [
                {
                    "name": "University of Example",
                    "id": [{"id": "ror:1234"}],
                    "country": "Exampleland",
                }
            ]
        }

        # Expected result
        expected_result = {
            "name": "University of Example",
            "rorid": "ror:1234",
            "country": "Exampleland",
        }

        # Call the function
        result = add_investigator_affiliation_data(resource)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_affiliation_with_missing_fields(self):
        """
        Test the add_investigator_affiliation_data function with missing fields
        """
        # Input resource with missing fields in the affiliation
        resource = {
            "affiliation": [
                {
                    "name": "University of Example",
                    "id": [{"id": "ror:1234"}],  # Country is missing
                }
            ]
        }

        # Expected result (country should be None since it's missing)
        expected_result = {
            "name": "University of Example",
            "rorid": "ror:1234",
            "country": None,
        }

        # Call the function
        result = add_investigator_affiliation_data(resource)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_affiliation_with_empty_list(self):
        """
        Test the add_investigator_affiliation_data function with an empty
        affiliation list.
        """
        # Input resource where affiliation list is empty
        resource = {"affiliation": []}

        # Call the function
        result = add_investigator_affiliation_data(resource)

        # Assertions
        self.assertEqual(result, None)

    def test_affiliation_absent(self):
        """
        Test the add_investigator_affiliation_data function with no affiliation
        """
        # Input resource without any affiliation field
        resource = {}

        # Expected result should handle no affiliation information gracefully
        expected_result = {"name": "none", "rorid": None, "country": None}

        # Call the function
        result = add_investigator_affiliation_data(resource)

        # Assertions
        self.assertEqual(result, expected_result)

    @patch("crossrefutils.metadata.add_investigator_affiliation_data")
    def test_multiple_investigator_metadata_complete(self, mock_affiliation):
        """
        Test the multiple_investigator_metadata function with complete data
        """
        # Set up the mock for add_investigator_affiliation_data
        mock_affiliation.return_value = {
            "name": "University of Example",
            "rorid": "ror:1234",
            "country": "Exampleland",
        }

        # Input resource with complete data
        resource = {
            "given": "John",
            "family": "Doe",
            "ORCID": "0000-0001-2345-6789",
            "affiliation": {},
        }

        # Expected result
        expected_result = {
            "given": "John",
            "family": "Doe",
            "ORCID": "0000-0001-2345-6789",
            "affiliations": [
                {
                    "name": "University of Example",
                    "rorid": "ror:1234",
                    "country": "Exampleland",
                }
            ],
        }

        # Call the function
        result = multiple_investigator_metadata(resource)

        # Assertions
        self.assertEqual(result, expected_result)
        mock_affiliation.assert_called_once_with(resource)

    def test_multiple_investigator_metadata_incomplete(self):
        """
        Test the multiple_investigator_metadata function with incomplete data
        """
        # Input resource with incomplete data (missing ORCID and affiliation)
        resource = {"given": "Jane", "family": "Smith"}

        # Expected result, ORCID should be None, affiliations should be empty
        # list
        expected_result = {
            "given": "Jane",
            "family": "Smith",
            "ORCID": None,
            "affiliations": [],
        }

        # Call the function
        result = multiple_investigator_metadata(resource)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_project_description_present(self):
        """
        Test the add_project_description function with project description
        """
        # Input record with project description
        record = {
            "project": [
                {
                    "project-description": [
                        {
                            "description": "This is a sample project "
                            "description."
                        }
                    ]
                }
            ]
        }

        # Expected result
        expected_result = "This is a sample project description."

        # Call the function
        result = add_project_description(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_project_description_absent(self):
        """
        Test the add_project_description function with no project description
        """
        # Input record without project description
        record = {"project": [{"project-description": [{}]}]}

        # Call the function
        result = add_project_description(record)

        # Assertions
        self.assertIsNone(result)

    def test_no_project_description_key(self):
        """
        Test the add_project_description function with no
        'project-description' key
        """
        # Input record with no 'project-description' key
        record = {"project": [{}]}

        # Call the function
        result = add_project_description(record)

        # Assertions
        self.assertIsNone(result)

    def test_no_project_key(self):
        """
        Test the add_project_description function with no 'project' key
        """
        # Input record with no 'project' key
        record = {}

        # Call the function
        result = add_project_description(record)

        # Assertions
        self.assertIsNone(result)

    def test_empty_project_array(self):
        """
        Test the add_project_description function with an empty 'project' array
        """
        # Input record with an empty 'project' array
        record = {"project": []}

        # Call the function
        with self.assertRaises(IndexError):
            add_project_description(record)

    def test_award_amount_present_complete(self):
        """
        Test the add_award_amount function with complete award amount data
        """
        # Input record with complete award amount data
        record = {
            "project": [
                {"award-amount": {"amount": 500000, "currency": "USD"}}
            ]
        }

        # Expected result
        expected_result = "500,000 USD"

        # Call the function
        result = add_award_amount(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_award_amount_present_incomplete(self):
        """
        Test the add_award_amount function with incomplete award amount data
        """
        # Input record with award amount missing currency
        record = {
            "project": [
                {
                    "award-amount": {
                        "amount": 500000
                        # currency is missing
                    }
                }
            ]
        }

        # Expected result
        expected_result = "500,000"

        # Call the function
        result = add_award_amount(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_award_amount_absent(self):
        """
        Test the add_award_amount function with no award amount
        """
        # Input record without 'award-amount'
        record = {"project": [{}]}

        # Call the function
        result = add_award_amount(record)

        # Assertions
        self.assertIsNone(result)

    def test_award_amount_totally_absent(self):
        """
        Test the add_award_amount function with no award amount and no project
        """
        # Input record without 'award-amount'
        record = {}

        # Call the function
        result = add_award_amount(record)

        # Assertions
        self.assertIsNone(result)

    def test_award_empty_project_array(self):
        """
        Test the add_award_amount function with an empty 'project' array
        """
        # Input record with an empty 'project' array
        record = {"project": []}

        # Call the function
        with self.assertRaises(IndexError):
            add_award_amount(record)

    def test_funding_scheme_present(self):
        """
        Test the add_funding_scheme function with a funding scheme present
        """
        # Input record with a funding scheme
        record = {
            "project": [
                {"funding": [{"scheme": "National Science Foundation"}]}
            ]
        }

        # Expected result
        expected_result = "National Science Foundation"

        # Call the function
        result = add_funding_scheme(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_funding_scheme_absent(self):
        """
        Test the add_funding_scheme function with no funding scheme
        """
        # Input record without a funding scheme
        record = {"project": [{"funding": [{}]}]}

        # Call the function
        result = add_funding_scheme(record)

        # Assertions
        self.assertIsNone(result)

    def test_no_funding_key(self):
        """
        Test the add_funding_scheme function with no 'funding' key
        """
        # Input record with no 'funding' key
        record = {"project": [{}]}

        # Call the function
        result = add_funding_scheme(record)

        # Assertions
        self.assertIsNone(result)

    def test_funding_empty_project_array(self):
        """
        Test the add_funding_scheme function with an empty 'project' array
        """
        # Input record with an empty 'project' array
        record = {"project": []}

        # Call the function
        with self.assertRaises(IndexError):
            add_funding_scheme(record)

    def test_funding_no_project_key(self):
        """
        Test the add_funding_scheme function with no 'project' key
        """
        # Input record with no 'project' key
        record = {}

        # Call the function
        result = add_funding_scheme(record)

        # Assertions
        self.assertIsNone(result)

    def test_award_number_present(self):
        """
        Test the add_award_number function with an award number present
        """
        # Input record with an award number
        record = {"award": "12345"}

        # Expected result
        expected_result = "12345"

        # Call the function
        result = add_internal_award_number(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_award_number_absent(self):
        """
        Test the add_award_number function with no award number
        """
        # Input record without an award number
        record = {}

        # Call the function
        result = add_internal_award_number(record)

        # Assertions
        self.assertIsNone(result)

    def test_record_empty(self):
        """
        Test the add_internal_award_number function with an empty record
        """
        # Input with an empty record
        record = {}

        # Call the function
        result = add_internal_award_number(record)

        # Assertions
        self.assertIsNone(result)

    def test_record_with_null_award(self):
        """
        Test the add_internal_award_number function with a record containing a
        None award
        """
        # Input record with an explicitly set None award
        record = {"award": None}

        # Call the function
        result = add_internal_award_number(record)

        # Assertions
        self.assertIsNone(result)

    @patch("crossrefutils.metadata.format_date")
    def test_award_start_present_in_project(self, mock_format_date):
        """
        Test the add_award_start function with award start date inside the
        project
        """
        # Set up the mock to return a formatted date
        mock_format_date.return_value = "January 1, 2020"

        # Input record with award start date inside the project
        record = {"project": [{"award-start": {"date-parts": [[2020, 1, 1]]}}]}

        # Expected result
        expected_result = "January 1, 2020"

        # Call the function
        result = add_award_start(record)

        # Assertions
        self.assertEqual(result, expected_result)
        mock_format_date.assert_called_once_with(
            record["project"][0]["award-start"]
        )

    def test_award_start_present_directly_in_record(self):
        """
        Test the add_award_start function with award start date directly in the
        record
        """
        # Setup the test environment
        with patch(
            "crossrefutils.metadata.format_date",
            return_value="February 2, 2022",
        ) as mock_format_date:
            # Input record with award start date directly in the record
            record = {"award-start": {"date-parts": [[2022, 2, 2]]}}

            # Expected result
            expected_result = "February 2, 2022"

            # Call the function
            result = add_award_start(record)

            # Assertions
            self.assertEqual(result, expected_result)
            mock_format_date.assert_called_once_with(record["award-start"])

    def test_award_start_absent(self):
        """
        Test the add_award_start function with no award start date
        """
        # Input record without award start date
        record = {"project": [{}]}

        # Call the function
        result = add_award_start(record)

        # Assertions
        self.assertIsNone(result)

    def test_award_start_malformed_record(self):
        """
        Test the add_award_start function with a record containing a malformed
        award start date
        """
        # Input record with malformed date structure
        record = {"project": [{"award-start": {}}]}  # Missing 'date-parts'

        # Call the function
        result = add_award_start(record)

        # Assertions
        self.assertIsNone(result)

    def test_grant_info_with_complete_data(self):
        """
        Test the add_grant_info function with complete grant information
        """
        # Input record with complete grant information
        record = {
            "project": [
                {
                    "funding": [
                        {
                            "funder": {"name": "National Science Foundation"},
                            "type": "Grant",
                        },
                        {
                            "funder": {"name": "European Research Council"},
                            "type": "Contract",
                        },
                    ]
                }
            ]
        }

        # Expected result
        expected_result = (
            "National Science Foundation (Grant) | "
            "European Research Council (Contract)"
        )

        # Call the function
        result = add_grant_info(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_grant_info_with_missing_funder_name(self):
        """
        Test the add_grant_info function with a record containing a missing
        funder name
        """
        # Input record where one funder name is missing
        record = {
            "project": [
                {
                    "funding": [
                        {
                            "funder": {"name": "National Science Foundation"},
                            "type": "Grant",
                        },
                        {
                            "funder": {},
                            "type": "Contract",
                        },  # Missing funder name
                    ]
                }
            ]
        }

        # Call the function
        # KeyError as funder name is mandatory
        with self.assertRaises(KeyError):
            add_grant_info(record)

    def test_grant_info_with_missing_funder_type(self):
        """
        Test the add_grant_info function with a record containing a missing
        funder type
        """
        # Input record where one funder type is missing
        record = {
            "project": [
                {
                    "funding": [
                        {
                            "funder": {"name": "National Science Foundation"},
                            "type": "Grant",
                        },
                        {"funder": {"name": "ERC"}},  # Missing type
                    ]
                }
            ]
        }

        # Expected result: Only include funders with complete information
        expected_result = "National Science Foundation (Grant)"

        # Call the function
        result = add_grant_info(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_grant_info_absent(self):
        """
        Test the add_grant_info function with no grant information
        """
        # Input record without 'funding' field
        record = {"project": [{}]}

        # Call the function
        result = add_grant_info(record)

        # Assertions
        self.assertIsNone(result)

    def test_grant_info_empty_project_array(self):
        """
        Test the add_grant_info function with an empty 'project' array
        """
        # Input record with an empty 'project' array
        record = {"project": []}

        # Call the function
        with self.assertRaises(IndexError):
            add_grant_info(record)

    def test_funders_with_complete_data(self):
        """
        Test the add_grant_info_funders function with complete funder
        information
        """
        # Input record with complete funder information
        record = {
            "project": [
                {
                    "funding": [
                        {"funder": {"name": "National Science Foundation"}},
                        {"funder": {"name": "European Research Council"}},
                    ]
                }
            ]
        }

        # Expected result
        expected_result = (
            "National Science Foundation | European Research Council"
        )

        # Call the function
        result = add_grant_info_funders(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_funders_with_missing_funder_field(self):
        """
        Test the add_grant_info_funders function with a record containing a
        missing funder field
        """
        # Input record where one funder's name is missing
        record = {
            "project": [
                {
                    "funding": [
                        {"funder": {"name": "National Science Foundation"}},
                        {
                            "other": {"name": "Unknown"}
                        },  # This should not be included
                    ]
                }
            ]
        }

        # Expected result: Only valid funder names should be included
        expected_result = "National Science Foundation"

        # Call the function
        result = add_grant_info_funders(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_funders_with_missing_name(self):
        """
        Test the add_grant_info_funders function with a record containing a
        missing funder name
        """
        # Input record with missing funder names
        record = {
            "project": [
                {
                    "funding": [
                        {"funder": {}},  # Missing name
                        {"funder": {}},  # Missing name
                    ]
                }
            ]
        }

        # Call the function
        with self.assertRaises(KeyError):
            add_grant_info_funders(record)

    def test_funders_absent(self):
        """
        Test the add_grant_info_funders function with no funder information
        """
        # Input record without 'funding' field
        record = {"project": [{}]}

        # Call the function
        result = add_grant_info_funders(record)

        # Assertions
        self.assertIsNone(result)

    def test_add_grant_info_empty_project_array(self):
        """
        Test the add_grant_info_funders function with an empty 'project' array
        """

        # Input record with an empty 'project' array
        record = {"project": []}

        # Call the function
        with self.assertRaises(IndexError):
            add_grant_info_funders(record)

    def test_funder_ids_with_complete_data(self):
        """
        Test the add_grant_info_funder_ids function with complete funder
        information
        """
        # Input record with complete funder information
        record = {
            "project": [
                {
                    "funding": [
                        {"funder": {"id": [{"id": "000x0001"}]}},
                        {"funder": {"id": [{"id": "000x0002"}]}},
                    ]
                }
            ]
        }

        # Expected result
        expected_result = "https://doi.org/000x0001 | https://doi.org/000x0002"

        # Call the function
        result = add_grant_info_funder_ids(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_funder_ids_with_missing_ids(self):
        """
        Test the add_grant_info_funder_ids function with a record containing a
        missing funder ID
        """
        # Input record where one funder id is missing
        record = {
            "project": [
                {
                    "funding": [
                        {"funder": {"id": [{"id": "000x0001"}]}},
                        {"funder": {}},  # Missing ID
                    ]
                }
            ]
        }

        with self.assertRaises(KeyError):
            add_grant_info_funder_ids(record)

    def test_funder_ids_absent(self):
        """
        Test the add_grant_info_funder_ids function with no funder information
        """
        # Input record without 'funding' field
        record = {"project": [{}]}

        # Expected result
        expected_result = ""

        # Call the function
        result = add_grant_info_funder_ids(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_funder_ids_empty_project_array(self):
        """
        Test the add_grant_info_funder_ids function with an empty 'project'
        array
        """
        # Input record with an empty 'project' array
        record = {"project": []}

        # Call the function
        with self.assertRaises(IndexError):
            add_grant_info_funder_ids(record)

    def test_grant_info_type_present(self):
        """
        Test the add_grant_info_type function with a funding type present
        """
        # Input record with a funding type present
        record = {"project": [{"funding": [{"type": "grant"}]}]}

        # Expected result should be the funding type in uppercase
        expected_result = "GRANT"

        # Call the function
        result = add_grant_info_type(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_grant_info_type_absent(self):
        """
        Test the add_grant_info_type function with no funding type
        """
        # Input record where the funding type is absent
        record = {"project": [{"funding": [{}]}]}

        # Call the function
        result = add_grant_info_type(record)

        # Assertions
        self.assertIsNone(result)

    def test_no_funding_field(self):
        """
        Test the add_grant_info_type function with no 'funding' field
        """
        # Input record without a 'funding' field
        record = {"project": [{}]}

        # Call the function
        result = add_grant_info_type(record)

        # Assertions
        self.assertIsNone(result)

    def test_add_grant_info_type_empty_project_array(self):
        """
        Test the add_grant_info_type function with an empty 'project' array
        """
        # Input record with an empty 'project' array
        record = {"project": []}

        # Call the function
        with self.assertRaises(IndexError):
            add_grant_info_type(record)

    def test_funding_type_present_with_whitespace(self):
        """
        Test the add_grant_info_type function with a funding type containing
        whitespace
        """
        # Input record with a funding type that contains whitespace
        record = {"project": [{"funding": [{"type": "  grant  "}]}]}

        # Expected result should be the trimmed and uppercase funding type
        expected_result = "GRANT"

        # Call the function
        result = add_grant_info_type(record)

        # Assertions
        self.assertEqual(result, expected_result)

    @patch("crossrefutils.metadata.get_doi_url")
    def test_doi_present(self, mock_get_doi_url):
        """
        Test the add_display_doi function with a DOI present in the record
        """
        # Set up the mock to return a specific URL
        mock_get_doi_url.return_value = "https://doi.org/10.1234/example"

        # Input record with a DOI
        record = {"DOI": "10.1234/example"}

        # Expected result
        expected_result = "https://doi.org/10.1234/example"

        # Call the function
        result = add_display_doi(record)

        # Assertions
        self.assertEqual(result, expected_result)
        mock_get_doi_url.assert_called_once_with("10.1234/example")

    def test_doi_absent(self):
        """
        Test the add_display_doi function with no DOI present in the record
        Shouldn't happen.
        """
        # Input record without a DOI
        record = {}

        # Call the function
        result = add_display_doi(record)

        # Assertions
        self.assertIsNone(result)

    def test_add_generic_get_present(self):
        """
        Test the add_location function with a location present in the record
        """
        # Input record with a location
        record = {"location": "New York"}

        # Expected result
        expected_result = "New York"

        # Call the function
        result = add_generic_get(record, "location")

        # Assertions
        self.assertEqual(result, expected_result)

    def test_add_generic_get_absent(self):
        """
        Test the add_location function with no location present in the record
        """
        # Input record without a location
        record = {}

        # Call the function
        result = add_generic_get(record, "location")

        # Assertions
        self.assertIsNone(result)

    def test_title_present(self):
        """
        Test the add_title function with a title present in the record
        """
        # Input record with a title
        record = {"title": ["Sample Title"]}

        # Expected result
        expected_result = "Sample Title"

        # Call the function
        result = add_title(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_title_present_with_backslashes(self):
        """
        Test the add_title function with a title containing backslashes
        """
        # Input record with a title containing backslashes
        record = {"title": ["Sample\\ Title"]}

        # Expected result
        expected_result = "Sample Title"

        # Call the function
        result = add_title(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_title_absent(self):
        """
        Test the add_title function with no title present in the record
        """
        # Input record without a title
        record = {}

        # Call the function
        result = add_title(record)

        # Assertions
        self.assertIsNone(result)

    def test_empty_title_list(self):
        """
        Test the add_title function with an empty title
        """
        # Input record with an empty title list
        record = {"title": []}

        # Expected result
        expected_result = ""

        # Call the function
        result = add_title(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_grant_title_present(self):
        """
        Test the add_title function with a project title present in the record
        """
        # Input record with a grant type and a project title
        record = {
            "type": "grant",
            "project": [{"project-title": [{"title": "Grant Project Title"}]}],
        }

        # Expected result
        expected_result = "Grant Project Title"

        # Call the function
        result = add_title(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_grant_title_absent(self):
        """
        Test the add_title function with no project title present in the record
        """
        # Input record with a grant type but no project title
        record = {"type": "grant", "project": [{"project-title": [{}]}]}
        # Call the function
        result = add_title(record)

        # Assertions
        self.assertIsNone(result)

    def test_grant_project_field_absent(self):
        """
        Test the add_title function with no project field present in the record
        """
        # Input record with a grant type but no project field
        record = {"type": "grant"}

        # Call the function
        result = add_title(record)

        # Assertions
        self.assertIsNone(result)

    def test_container_title_present(self):
        """
        Test the add_publication function with a container-title present in the
        record
        """
        # Input record with a non-empty container-title list
        record = {"container-title": ["Journal of Testing", "Another Journal"]}

        # Expected result
        expected_result = "Journal of Testing"

        # Call the function
        result = add_publication(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_container_title_empty(self):
        """
        Test the add_publication function with an empty container-title list
        """
        # Input record with an empty container-title list
        record = {"container-title": []}

        # Call the function
        result = add_publication(record)

        # Assertions
        self.assertIsNone(result)

    def test_container_title_absent(self):
        """
        Test the add_publication function with no container-title present
        in the record
        """
        # Input record without a container-title
        record = {}

        # Call the function
        result = add_publication(record)

        # Assertions
        self.assertIsNone(result)

    def test_container_title_with_additional_fields(self):
        """
        Test the add_publication function with a container-title present in the
        record along with additional fields
        """
        # Input record with a container-title and additional fields
        record = {
            "container-title": ["Journal of Testing"],
            "id": "12345",
            "name": "Sample Name",
        }

        # Expected result
        expected_result = "Journal of Testing"

        # Call the function
        result = add_publication(record)

        # Assertions
        self.assertEqual(result, expected_result)

    @patch("crossrefutils.metadata.format_date")
    def test_published_print_date(self, mock_format_date):
        """
        Test the add_published_date function with published-print date
        """
        # Set up the mock to return a formatted date
        mock_format_date.return_value = "2023-01-01"

        # Input record with published-print date
        record = {"published-print": {"date-parts": [[2023, 1, 1]]}}

        # Expected result
        expected_result = "2023-01-01"

        # Call the function
        result = add_published_date(record)

        # Assertions
        self.assertEqual(result, expected_result)
        mock_format_date.assert_called_once_with(record["published-print"])

    @patch("crossrefutils.metadata.format_date")
    def test_published_online_date(self, mock_format_date):
        """
        Test the add_published_date function with published-online date
        """
        # Set up the mock to return a formatted date
        mock_format_date.return_value = "2023-01-02"

        # Input record with published-online date
        record = {"published-online": {"date-parts": [[2023, 1, 2]]}}

        # Expected result
        expected_result = "2023-01-02"

        # Call the function
        result = add_published_date(record)

        # Assertions
        self.assertEqual(result, expected_result)
        mock_format_date.assert_called_once_with(record["published-online"])

    def test_no_published_date(self):
        """
        Test the add_published_date function with no published date
        """
        # Input record without published-print and published-online dates
        record = {}

        # Call the function
        result = add_published_date(record)

        # Assertions
        self.assertIsNone(result)

    def test_missing_date_parts(self):
        """
        Test the add_published_date function with missing date-parts
        """
        # Input record with missing date-parts
        record = {"published-print": {}}

        # Call the function
        result = add_published_date(record)

        # Assertions
        self.assertIsNone(result)

    def test_date_with_year_month_day(self):
        """
        Test the format_date function with year, month, and day
        """
        # Input with year, month, and day
        published = {"date-parts": [[2023, 1, 15]]}

        # Expected result
        expected_result = "15 January 2023"

        # Call the function
        result = format_date(published)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_date_with_year_month(self):
        """
        Test the format_date function with year and month
        """
        # Input with year and month
        published = {"date-parts": [[2023, 1]]}

        # Expected result
        expected_result = "January 2023"

        # Call the function
        result = format_date(published)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_date_with_year_only(self):
        """
        Test the format_date function with only the year
        """
        # Input with only the year
        published = {"date-parts": [[2023]]}

        # Expected result
        expected_result = "2023"

        # Call the function
        result = format_date(published)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_date_with_missing_day(self):
        """
        Test the format_date function with year and month, but without day
        """
        # Input with year and month, but without day
        published = {"date-parts": [[2023, 2]]}

        # Expected result
        expected_result = "February 2023"

        # Call the function
        result = format_date(published)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_date_with_empty_date_parts(self):
        """
        Test the format_date function with empty date-parts
        """
        # Input with empty date-parts
        published = {"date-parts": [[]]}

        # Expected result
        expected_result = ""

        # Call the function
        result = format_date(published)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_type_present_with_hyphens(self):
        """
        Test the add_type function with a type containing hyphens
        """
        # Input record with a type containing hyphens
        record = {"type": "journal-article"}

        # Expected result
        expected_result = "JOURNAL ARTICLE"

        # Call the function
        result = add_type(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_type_present_without_hyphens(self):
        """
        Test the add_type function with a type without hyphens
        """
        # Input record with a type without hyphens
        record = {"type": "book"}

        # Expected result
        expected_result = "BOOK"

        # Call the function
        result = add_type(record)

        # Assertions
        self.assertEqual(result, expected_result)

    def test_type_absent(self):
        """
        Test the add_type function with no type present in the record
        """
        # Input record without a type
        record = {}

        # Call the function
        result = add_type(record)

        # Assertions
        self.assertIsNone(result)

    def test_type_with_additional_fields(self):
        """
        Test the add_type function with a type present in the record along with
        additional fields
        """
        # Input record with a type and additional fields
        record = {
            "type": "conference-paper",
            "id": "12345",
            "name": "Sample Name",
        }

        # Expected result
        expected_result = "CONFERENCE PAPER"

        # Call the function
        result = add_type(record)

        # Assertions
        self.assertEqual(result, expected_result)

    @patch("crossrefutils.metadata.add_coaccess_data", new_callable=AsyncMock)
    @patch("crossrefutils.metadata.add_multiple_resolution_data")
    @patch("crossrefutils.metadata.tld")
    @patch("crossrefutils.metadata.clearbit_logo")
    async def test_relationship_metadata(
        self,
        mock_clearbit_logo,
        mock_tld,
        mock_add_multiple_resolution_data,
        mock_add_coaccess_data,
    ):
        """
        Test the relationship_metadata function with a complete record
        """

        mock_add_coaccess_data.return_value = ["coaccess data"]
        mock_add_multiple_resolution_data.return_value = [
            "multiple resolution data"
        ]
        mock_tld.return_value = "example.com"
        mock_clearbit_logo.return_value = (
            "https://logo.clearbit.com/example.com"
        )

        # Input record
        record = {
            "DOI": "10.1234/example.doi",
            "member": "123",
            "publisher": "Example Publisher",
            "container-title": ["Journal of Testing"],
            "resource": {"primary": {"URL": "https://example.com/resource"}},
        }

        # Expected result
        expected_result = {
            "doi": "10.1234/example.doi",
            "member_id": "123",
            "member": "Example Publisher",
            "container-title": "Journal of Testing",
            "primary-resource": "https://example.com/resource",
            "tld": "example.com",
            "clearbit-logo": "https://logo.clearbit.com/example.com",
            "coaccess": ["coaccess data"],
            "multiple-resolution": ["multiple resolution data"],
        }

        # Call the function
        result = await relationship_metadata(record)

        # Assertions
        self.assertEqual(result, expected_result)
        mock_add_coaccess_data.assert_called_once_with(record)
        mock_add_multiple_resolution_data.assert_called_once_with(record)
        self.assertEqual(mock_tld.call_count, 2)
        mock_clearbit_logo.assert_called_once_with("example.com")

    # pylint: disable=too-many-arguments, too-many-locals, too-many-statements

    @patch("crossrefutils.metadata.add_type")
    @patch("crossrefutils.metadata.add_published_date")
    @patch("crossrefutils.metadata.add_publication")
    @patch("crossrefutils.metadata.add_title")
    @patch("crossrefutils.metadata.add_display_doi")
    @patch("crossrefutils.metadata.add_grant_info")
    @patch("crossrefutils.metadata.add_grant_info_funders")
    @patch("crossrefutils.metadata.add_grant_info_funder_ids")
    @patch("crossrefutils.metadata.add_grant_info_type")
    @patch("crossrefutils.metadata.add_lead_investigators")
    @patch("crossrefutils.metadata.add_co_lead_investigators")
    @patch("crossrefutils.metadata.add_investigators")
    @patch("crossrefutils.metadata.add_finances_data")
    @patch("crossrefutils.metadata.add_project_description")
    @patch("crossrefutils.metadata.add_award_amount")
    @patch("crossrefutils.metadata.add_award_start")
    @patch("crossrefutils.metadata.add_funding_scheme")
    @patch("crossrefutils.metadata.add_internal_award_number")
    @patch("crossrefutils.metadata.add_supplementary_id")
    def test_citation_metadata(
        self,
        mock_add_supplementary_id,
        mock_add_internal_award_number,
        mock_add_funding_scheme,
        mock_add_award_start,
        mock_add_award_amount,
        mock_add_project_description,
        mock_add_finances_data,
        mock_add_investigators,
        mock_add_co_lead_investigators,
        mock_add_lead_investigators,
        mock_add_grant_info_type,
        mock_add_grant_info_funder_ids,
        mock_add_grant_info_funders,
        mock_add_grant_info,
        mock_add_display_doi,
        mock_add_title,
        mock_add_publication,
        mock_add_published_date,
        mock_add_type,
    ):
        """
        Test the citation_metadata function with a complete record
        """
        # Set up the mocks
        mock_add_type.return_value = "TYPE"
        mock_add_published_date.return_value = "2023-01-01"
        mock_add_publication.return_value = "Journal of Testing"
        mock_add_title.return_value = "Sample Title"
        mock_add_display_doi.return_value = "https://doi.org/10.1234/example"
        mock_add_grant_info.return_value = "Grant Info"
        mock_add_grant_info_funders.return_value = "Funder 1 | Funder 2"
        mock_add_grant_info_funder_ids.return_value = (
            "https://doi.org/000x0001 | https://doi.org/000x0002"
        )
        mock_add_grant_info_type.return_value = "GRANT"
        mock_add_lead_investigators.return_value = ["Investigator 1"]
        mock_add_co_lead_investigators.return_value = ["Co-Investigator 1"]
        mock_add_investigators.return_value = ["Investigator 2"]
        mock_add_finances_data.return_value = ["Finance 1"]
        mock_add_project_description.return_value = "Project Description"
        mock_add_award_amount.return_value = "1000 USD"
        mock_add_award_start.return_value = "2023-01-01"
        mock_add_funding_scheme.return_value = "Funding Scheme"
        mock_add_internal_award_number.return_value = "Award Number"
        mock_add_supplementary_id.return_value = "Supp ID"

        # Input record
        record = {
            "DOI": "10.1234/example.doi",
            "member": "123",
            "publisher": "Example Publisher",
            "container-title": ["Journal of Testing"],
            "resource": {"primary": {"URL": "https://example.com/resource"}},
        }

        # Expected result
        expected_result = {
            "type": "TYPE",
            "published_date": "2023-01-01",
            "publication": "Journal of Testing",
            "supplementary_ids": "Supp ID",
            "title": "Sample Title",
            "name": None,
            "id": None,
            "location": None,
            "display_doi": "https://doi.org/10.1234/example",
            "grant_info": "Grant Info",
            "grant_info_funders": "Funder 1 | Funder 2",
            "grant_info_funder_ids": "https://doi.org/000x0001 | "
            "https://doi.org/000x0002",
            "grant_info_type": "GRANT",
            "multiple_lead_investigators": ["Investigator 1"],
            "multiple_co_lead_investigators": ["Co-Investigator 1"],
            "multiple_investigators": ["Investigator 2"],
            "finances": ["Finance 1"],
            "project_description": "Project Description",
            "award_amount": "1000 USD",
            "award_start": "2023-01-01",
            "funding_scheme": "Funding Scheme",
            "internal_award_number": "Award Number",
            "editors": None,
            "authors": None,
            "chairs": None,
        }

        # Call the function
        result = citation_metadata(record)

        # Assertions
        self.assertEqual(result, expected_result)
        mock_add_type.assert_called_once_with(record)
        mock_add_published_date.assert_called_once_with(record)
        mock_add_publication.assert_called_once_with(record)
        mock_add_title.assert_called_once_with(record)
        mock_add_display_doi.assert_called_once_with(record)
        mock_add_grant_info.assert_called_once_with(record)
        mock_add_grant_info_funders.assert_called_once_with(record)
        mock_add_grant_info_funder_ids.assert_called_once_with(record)
        mock_add_grant_info_type.assert_called_once_with(record)
        mock_add_lead_investigators.assert_called_once_with(record)
        mock_add_co_lead_investigators.assert_called_once_with(record)
        mock_add_investigators.assert_called_once_with(record)
        mock_add_finances_data.assert_called_once_with(record)
        mock_add_project_description.assert_called_once_with(record)
        mock_add_award_amount.assert_called_once_with(record)
        mock_add_award_start.assert_called_once_with(record)
        mock_add_funding_scheme.assert_called_once_with(record)
        mock_add_internal_award_number.assert_called_once_with(record)
        mock_add_supplementary_id.assert_called_once_with(record)

    @patch(
        "crossrefutils.metadata.fetch_work_metadata", new_callable=AsyncMock
    )
    @patch(
        "crossrefutils.metadata.relationship_metadata", new_callable=AsyncMock
    )
    @patch("crossrefutils.metadata.citation_metadata")
    async def test_metadata_successful(
        self,
        mock_citation_metadata,
        mock_relationship_metadata,
        mock_fetch_work_metadata,
    ):
        """
        Test the metadata function with a successful fetch
        """
        # Set up the mocks
        mock_fetch_work_metadata.return_value = {
            "DOI": "10.1234/example.doi",
            "member": "123",
            "publisher": "Example Publisher",
        }
        mock_relationship_metadata.return_value = {"resolution": "data"}
        mock_citation_metadata.return_value = {"citation": "data"}

        # Expected result
        expected_result = {"resolution": "data", "citation": "data"}

        # Call the function
        result = await metadata("10.1234/example.doi")

        # Assertions
        self.assertEqual(result, expected_result)
        mock_fetch_work_metadata.assert_called_once_with("10.1234/example.doi")

        mock_relationship_metadata.assert_called_once_with(
            mock_fetch_work_metadata.return_value
        )
        mock_citation_metadata.assert_called_once_with(
            mock_fetch_work_metadata.return_value
        )

    @patch(
        "crossrefutils.metadata.fetch_work_metadata", new_callable=AsyncMock
    )
    async def test_metadata_no_record(self, mock_fetch_work_metadata):
        """
        Test the metadata function with no record found
        """

        mock_fetch_work_metadata.return_value = None

        # Expected result
        expected_result = None

        # Call the function
        result = await metadata("10.1234/example.doi")

        # Assertions
        self.assertEqual(result, expected_result)
        mock_fetch_work_metadata.assert_called_once_with("10.1234/example.doi")

    @patch("crossrefutils.metadata.cache.get", new_callable=AsyncMock)
    @patch("crossrefutils.metadata.invalidate_further", new_callable=MagicMock)
    async def test_clear_cache_for_doi(
        self, mock_invalidate_further, mock_cache_get
    ):
        """
        Test the clear_metadata_cache function with a DOI
        """
        # Mock the context manager to do nothing
        mock_invalidate_further.return_value.__enter__.return_value = None
        mock_invalidate_further.return_value.__exit__.return_value = None

        # Expected message
        doi = "10.1234/example.doi"
        expected_msg = f"Cleared cached metadata for {doi}"

        # Call the function
        result = await clear_metadata_cache(doi)

        # Assertions
        self.assertEqual(result, expected_msg)
        mock_cache_get.assert_called_once_with(f"doi:{doi}")
        mock_invalidate_further.assert_called_once()

    @patch("crossrefutils.metadata.cache.clear", new_callable=AsyncMock)
    async def test_clear_all_cache(self, mock_cache_clear):
        """
        Test the clear_metadata_cache function with no DOI
        """
        # Expected message
        expected_msg = "Clearing all cached metadata"

        # Call the function
        result = await clear_metadata_cache()

        # Assertions
        self.assertEqual(result, expected_msg)
        mock_cache_clear.assert_called_once()
