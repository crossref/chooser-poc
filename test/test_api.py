"""
Test the Crossref API and the Clearbit fetcher
"""

from asyncio import CancelledError
from asyncio import TimeoutError as AsyncTimeoutError
from ssl import SSLWantReadError
from unittest import IsolatedAsyncioTestCase
from unittest.mock import AsyncMock, Mock, patch

from tenacity import RetryError

from crossrefutils import api


class APITestCase(IsolatedAsyncioTestCase):
    """
    Test the Crossref API and the Clearbit fetcher
    """

    @staticmethod
    @patch("crossrefutils.api.httpx.AsyncClient.get")
    async def test_crossref_api(patched_client):
        """
        Test the Crossref API for a good response
        """
        patched_client.return_value = HttpxReturnObject()

        request = await api.crossref_api("https://eve.gd")

        assert request == "test"

    @patch("crossrefutils.api.httpx.AsyncClient.get")
    async def test_crossref_api_with_timeout(self, patched_client):
        """
        Test the Crossref API for a Timeout error
        """
        patched_client.return_value = HttpxReturnObject()
        patched_client.side_effect = AsyncTimeoutError

        with self.assertRaises(RetryError):
            await api.crossref_api("https://eve.gd")

    @patch("crossrefutils.api.httpx.AsyncClient.get")
    async def test_crossref_api_with_cancelled(self, patched_client):
        """
        Test the Crossref API for a Cancelled error
        """
        patched_client.return_value = HttpxReturnObject()
        patched_client.side_effect = CancelledError

        with self.assertRaises(RetryError):
            await api.crossref_api("https://eve.gd")

    @patch("crossrefutils.api.httpx.AsyncClient.get")
    async def test_crossref_api_with_ssl_error(self, patched_client):
        """
        Test the Crossref API for an SSL error
        """
        patched_client.return_value = HttpxReturnObject()
        patched_client.side_effect = SSLWantReadError

        with self.assertRaises(RetryError):
            await api.crossref_api("https://eve.gd")

    @patch("crossrefutils.api.httpx.AsyncClient.get")
    async def test_crossref_api_with_generic_error(self, patched_client):
        """
        Test the Crossref API for a generic Exception
        """
        patched_client.return_value = HttpxReturnObject()
        patched_client.side_effect = Exception

        with self.assertRaises(RetryError):
            await api.crossref_api("https://eve.gd")

    @staticmethod
    async def test_fetch_work_metadata():
        """
        Test the fetch_work_metadata method
        """

        with patch(
            "crossrefutils.api.crossref_api", new_callable=AsyncMock
        ) as mock_crossref_api:
            await api.fetch_work_metadata("10.1002/2017GL074998")
            mock_crossref_api.assert_called_with(
                "https://api.crossref.org/v1/works/10.1002/2017GL074998"
            )

    @staticmethod
    async def test_fetch_member_metadata():
        """
        Test the fetch_member_metadata method
        """

        with patch(
            "crossrefutils.api.crossref_api", new_callable=AsyncMock
        ) as mock_crossref_api:
            await api.fetch_member_metadata("1234")
            mock_crossref_api.assert_called_with(
                "https://api.crossref.org/v1/members/1234"
            )

    @staticmethod
    async def test_clearbit_with_read_timeout_error():
        """
        Test the Clearbit fetcher for a read timeout error
        """
        with patch(
            "crossrefutils.api.httpx", new_callable=Mock
        ) as patched_client:
            patched_client.head.return_value = HttpxReturnObject()
            patched_client.head.side_effect = Exception

            request = api.clearbit_logo("https://eve.gd")

            assert request == "/static/no_logo.svg"

    @staticmethod
    async def test_clearbit_success():
        """
        Test the Clearbit fetcher for a successful fetch
        """
        with patch(
            "crossrefutils.api.httpx", new_callable=Mock
        ) as patched_client:
            patched_client.head.return_value = HttpxReturnObject()

            request = api.clearbit_logo("https://eve.gd")

            assert request == "https://logo.clearbit.com/https://eve.gd"

    @staticmethod
    async def test_member_name():
        """
        Test the member_name method
        """

        with patch(
            "crossrefutils.api.crossref_api", new_callable=AsyncMock
        ) as mock_crossref_api:
            mock_crossref_api.return_value = {"primary-name": "test"}
            request = await api.member_name("1234")
            mock_crossref_api.assert_called_with(
                "https://api.crossref.org/v1/members/1234"
            )

            assert request == "test"


class HttpxReturnObject:
    """
    A class to mock the return value of the httpx.AsyncClient.get method
    """

    def __init__(self):
        """
        Initialize the object
        """
        self._json = {"message": "test"}
        self.status_code = 200

    def json(self):
        """
        Return the JSON object
        """
        return self._json
