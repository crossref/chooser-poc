"""
Common functions for testing
"""

import csv
from collections import namedtuple
from pathlib import Path

ELK_SAMPLES_PATH = Path("resources")


def create_namedtuple_from_elk_sample(csv_data):
    """
    Create a namedtuple from the csv data.
    """
    Sample = namedtuple("Sample", ["doi", "status"])
    return [Sample(doi=row[0], status=row[1]) for row in csv_data]


def inject_test_dois(file):
    """
    Inject test dois into the csv file.
    """
    file = str(ELK_SAMPLES_PATH.joinpath(file))
    with open(file, "r", encoding="utf-8") as file_handle:
        csv_data = csv.reader(file_handle, delimiter="\t")
        return create_namedtuple_from_elk_sample(csv_data)
