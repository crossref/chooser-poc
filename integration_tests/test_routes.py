"""
This file contains the integration tests for the routes in the FastAPI app.
"""

import pytest
from starlette.testclient import TestClient

from chooser import app
from crossrefutils import api

client = TestClient(app)


class TestRoutes:
    """
    This class contains the integration tests for the routes in the FastAPI
    app
    """

    @pytest.mark.integration
    def test_root_no_doi(self):
        """
        Test the root route with no doi parameter
        """
        response = client.get("/")
        json_response = response.json()

        assert "detail" in json_response
        assert "loc" in json_response["detail"][0]
        assert "msg" in json_response["detail"][0]
        assert "type" in json_response["detail"][0]
        assert response.status_code == 422

    @pytest.mark.integration
    def test_root_doi_parameter(self):
        """
        Test the root route with a doi parameter
        """
        api.CROSSREF_API = "http://docker:3000"

        response = client.get("/?doi=10.1002/jnr.23992")

        assert "Journal of Neuroscience Research" in response.text

        # test that we get a good response code
        assert response.status_code == 200

    @pytest.mark.integration
    def test_root_doi_parameter_not_found(self):
        """
        Test the root route with a doi parameter that is not found
        """
        api.CROSSREF_API = "http://docker:3000"

        response = client.get("/?doi=10.1136/bmj.n254fsdhkjsfd32")

        assert "Not found" in response.text

        # test that we get a good response code
        assert response.status_code == 404

    @pytest.mark.integration
    def test_root_doi_path(self):
        """
        Test the root route with a doi path
        """
        api.CROSSREF_API = "http://docker:3000"

        response = client.get("/10.1002/jnr.23992")

        assert "Journal of Neuroscience Research" in response.text

        # test that we get a good response code
        assert response.status_code == 200

    @pytest.mark.integration
    def test_root_doi_path_not_found(self):
        """
        Test the root route with a doi path that is not found
        """
        api.CROSSREF_API = "http://docker:3000"

        response = client.get("/10.1136/bmj.n254asx")

        assert "Not found" in response.text

        # test that we get a good response code
        assert response.status_code == 404

    @pytest.mark.integration
    def test_heartbeat(self):
        """
        Test the heartbeat route
        """
        api.CROSSREF_API = "http://docker:3000"

        response = client.get("/heartbeat")

        assert "version" in response.json()

        # test that we get a good response code
        assert response.status_code == 200
