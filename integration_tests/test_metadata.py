"""
Integration tests for the metadata module.
"""

import pytest

from crossrefutils import api, metadata

pytest_plugins = ("pytest_asyncio",)


class TestAPI:
    """
    Test the metadata functions.
    """

    @pytest.mark.asyncio
    async def test_get_metadata(self):
        """
        Test the get_metadata function.
        """
        api.CROSSREF_API = "http://docker:3000"

        fetch_member_metadata = await metadata.metadata("10.1002/jnr.23992")

        keys = [
            "doi",
            "member_id",
            "member",
            "container-title",
            "primary-resource",
            "tld",
            "clearbit-logo",
            "coaccess",
            "multiple-resolution",
            "type",
            "published_date",
            "publication",
            "supplementary_ids",
            "title",
            "name",
            "id",
            "location",
            "display_doi",
            "grant_info",
            "grant_info_funders",
            "grant_info_funder_ids",
            "grant_info_type",
            "multiple_lead_investigators",
            "multiple_co_lead_investigators",
            "multiple_investigators",
            "finances",
            "project_description",
            "award_amount",
            "award_start",
            "funding_scheme",
            "internal_award_number",
            "editors",
            "authors",
            "chairs",
        ]

        for key in keys:
            assert key in fetch_member_metadata

    async def test_get_metadata_doi(self):
        """
        Test the get_metadata function returns the correct DOI.
        """
        api.CROSSREF_API = "http://docker:3000"

        fetch_member_metadata = await metadata.metadata("10.1002/jnr.23992")

        assert fetch_member_metadata["doi"] == "10.1002/jnr.23992"
