"""
Tests for the Crossref API.
"""

import pytest

from crossrefutils import api

pytest_plugins = ("pytest_asyncio",)


class TestAPI:
    """
    Test the Crossref API.
    """

    @pytest.mark.asyncio
    async def test_get_member_data(self):
        """
        Test that we can get member metadata from the Crossref API.
        Disabled for now until we work out how to get member data into
        the integration project's Cayenne instance.
        """

    @pytest.mark.asyncio
    async def test_get_member_name(self):
        """
        Test that we can get the name of a member from the Crossref API.
        Disabled for now until we work out how to get member data into
        the integration project's Cayenne instance.
        """

    @pytest.mark.asyncio
    async def test_get_work_metadata(self):
        """
        Test that we can get work metadata from the Crossref API.
        """
        api.CROSSREF_API = "http://docker:3000"
        fetch_work_metadata = await api.fetch_work_metadata(
            "10.1002/jnr.23992"
        )

        assert "DOI" in fetch_work_metadata
        assert fetch_work_metadata["DOI"] == "10.1002/jnr.23992"
