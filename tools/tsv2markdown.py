"""
This script takes a list of TSV files and generates markdown files
"""

import csv
import logging
from pathlib import Path
from typing import List

import typer
from rich.console import Console
from rich.logging import RichHandler

logging.basicConfig(
    level="INFO", handlers=[RichHandler(console=Console(stderr=True))]
)
logger = logging.getLogger(__name__)
SEEN = []


# pylint: disable=duplicate-code
def process_file(path, host, sample_type):
    """
    Process a TSV file and generate markdown
    """
    logger.info("processing: %s", path.name)

    with open(path, "r", encoding="utf-8") as file_handle:
        csv_data = csv.reader(file_handle, delimiter="\t")

        print(f"## {sample_type}\n\n")

        for row in csv_data:
            doi, _ = row

            if doi and doi not in SEEN:
                SEEN.append(doi)

                print(f"### {doi}")
                print(f"- [Chooser POC]({host}/?doi={doi})")
                print(f"- [Production](https://doi.org/{doi})")
                print(f"- [JSON](https://api.crossref.org/works/{doi})")
                print(f"- [XML](https://api.crossref.org/works/{doi}.xml)")
                print("<hr>\n")


def main(
    files: List[Path],
    _: int = 0,
    host: str = typer.Option(...),
    sample_type: str = typer.Option(...),
):
    """
    Process a list of TSV files and generate markdown
    """
    for path in files:
        process_file(path, host, sample_type)


if __name__ == "__main__":
    logger.info("starting")
    typer.run(main)
