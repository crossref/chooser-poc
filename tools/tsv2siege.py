"""
This file converts a TSV file to siege format
"""

import csv
import logging
from pathlib import Path
from typing import List

import typer
from rich.console import Console
from rich.logging import RichHandler

logging.basicConfig(
    level="INFO", handlers=[RichHandler(console=Console(stderr=True))]
)
logger = logging.getLogger(__name__)
SEEN = []


# pylint: disable=duplicate-code
def process_file(path, host):
    """
    Process a file for conversion
    """
    logger.info("processing: %s", path.name)

    with open(path, "r", encoding="utf-8") as file_handle:
        csv_data = csv.reader(file_handle, delimiter="\t")
        for row in csv_data:
            doi, _ = row

            if doi and doi not in SEEN:
                SEEN.append(doi)
                print(f"{host}/?doi={doi}")


def main(files: List[Path], _: int = 0, host: str = typer.Option(...)):
    """
    Convert a TSV file to siege format
    """
    for path in files:
        process_file(path, host)


if __name__ == "__main__":
    logger.info("starting")
    typer.run(main)
