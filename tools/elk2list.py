"""
This script reads a list of CSV files and extracts the DOIs from the requests.
"""

import csv
import logging
import sys
from pathlib import Path
from typing import List
from urllib.parse import parse_qs, urlparse

import typer
from rich.console import Console
from rich.logging import RichHandler

logging.basicConfig(
    level="INFO", handlers=[RichHandler(console=Console(stderr=True))]
)
logger = logging.getLogger(__name__)
SEEN = []


def process_file(path):
    """
    Process a CSV file and extract the DOIs from the requests.
    """
    logger.info("processing: %s", path.name)

    csv_writer = csv.writer(sys.stdout, delimiter="\t")
    with open(path, "r", encoding="utf-8") as file_handle:
        csv_data = csv.reader(file_handle)
        for row in csv_data:
            _, status, request = row
            doi = parse_qs(urlparse(request).query).get("doi", [None])[0]
            if doi and doi not in SEEN:
                SEEN.append(doi)

                csv_writer.writerow([doi, status])


def main(files: List[Path], _: int = 0):
    """
    Process a list of CSV files and extract the DOIs from the requests.
    """
    for path in files:
        process_file(path)


if __name__ == "__main__":
    logger.info("starting")
    typer.run(main)
