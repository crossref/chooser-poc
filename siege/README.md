# Siege files

These files were generated froma dump of ELK logs for an hour in September 2022 when both multiple resolution and coaccess experienced their most requests per minute.

Replay the files 100 times with 20 concurrent cores (assuming you have that many cores ;-))

- time siege -r 100 -c 20 -f siege/peak_multires.siege
- time siege -r 100 -c 20 -f siege/peak_coaccess.siege
