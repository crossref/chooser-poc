# Notes

## chooser:metadata()->fetch_work_metadata(doi)

gets the works metadata for the DOI

## chooser:metadata()->resolution_metadata(record)

gets all possible resolution metadata for the record, this means both multiresolution and coaccess because, theoretically, a DOI could support both.

bascially it is looking up critical information for related records, including the primary resource they point to

## resolution_metadata()->add_coaccess_data(record)

iterates over every is-identical-to to relationship that points to a DOI and returns coaccess metadata

## resolution_metadata()->add_multiple_resolution_data(record)

NB: The single most broken thing I discovered while doing this is that the REST API basically includes wrong publisher info for COACCESS items.

For example:

This DOI: `10.1515/9781575065403-020`

Let's look at the JSON metdata:

http://api.crossref.org/works/10.1515/9781575065403-020

Lists the publisher name as:  "Penn State University Press"
And the record lists the member id as `374`

But the number `374`` is *not* the member ID of "Penn State University Press", instead it is the member ID of "Walter de Gruyter GmbH"

The correct ID of "Penn State University Press" is `3287`

If we look at the relationships in the metadata, it lists the following DOI as identical-to:

`10.5325/j.ctv1bxh3p2.22`

If we look at the metadata for this DOI

http://api.crossref.org/works/10.5325/j.ctv1bxh3p2.22

It again lists the publisher name as:  "Penn State University Press", but this time it also lists the correct ID as being `3287`

## Other problem DOIs

10.1134/S2079562912050077
10.1134/s0040363613050056
10.1134/s2079562913070087

## Grant Template

Line 13 of crossrefutils/metadata.py disables the cache for development
Just change disable=True or remove it to use the cache again
cache.setup("disk://?directory=chooser_cache", shards=4, disable=True)

Grant Examples
http://localhost:8000/?doi=10.3030/680448 // lots of content eg award etc but no investigators
http://localhost:8000/?doi=10.46936/cpcy.proj.2019.50733/60006578 // 1 lead investigator and several investigators
http://localhost:8000/?doi=10.32013/vGVkUKI // affiliation data for the investigators
http://localhost:8000/?doi=10.1215/9780822398073-006

TODO: display ROR ID
